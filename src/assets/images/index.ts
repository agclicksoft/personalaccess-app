export { default as ImageBackground } from './background/background.png';
export { default as ImageLogo } from './logo/logo.png';
export { default as BarLogo } from './bar-logo/bar-logo.png';
