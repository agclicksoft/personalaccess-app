export { default as LoadingAnimation } from './loading/loading.json';
export { default as NotFoundAnimation } from './not-found/404.json';
export { default as AvatarAnimation } from './avatar-loading/avatar-loading.json';
export { default as FeedAnimation } from './feed-loading/loading.json';
export { default as SuccessAnimation } from './success/success.json';
export { default as WorkoutAnimation } from './workout/workout.json';
