/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

export const getStudentStatus = (status: string) => {
  switch (status) {
    case 'CONFIRMADO':
      return 'Confirmada';
    case 'PAGO':
      return 'Confirmada';
    case 'Aguardando pagamento':
      return 'Aguardando pagamento';
    case 'MAOS':
      return 'Pagamento em mãos';
    case 'CANCELADO':
      return 'Cancelada';
    case 'PENDENTE':
      return 'Aguardando confirmação';
    case 'ESTORNADA':
      return 'Estornada';
    case 'CONCLUIDO':
      return 'Concluída';
    case 'DISPONIVEL':
      return 'Aguardando professor';
    case 'REMARCADO':
      return 'Remarcada';
    case 'RECUSADO':
      return 'Recusada';
    case 'ANDAMENTO':
      return 'Em andamento';
    case 'EXPIRADO':
      return 'Expirada';

    default:
      return 'Responder';
  }
};

export const getStatus = (status: string) => {
  switch (status) {
    case 'CONFIRMADO':
      return 'Confirmada';
    case 'PAGO':
      return 'Confirmada';
    case 'Aguardando pagamento':
      return 'Aguardando pagamento';
    case 'MAOS':
      return 'Pagamento em mãos';
    case 'CANCELADO':
      return 'Cancelada';
    case 'PENDENTE':
      return 'Responder';
    case 'ESTORNADA':
      return 'Estornada';
    case 'CONCLUIDO':
      return 'Concluída';
    case 'DISPONIVEL':
      return 'Iniciar aula';
    case 'ANDAMENTO':
      return 'Em andamento';
    case 'REMARCADO':
      return 'Remarcada';
    case 'RECUSADO':
      return 'Recusada';
    case 'EXPIRADO':
      return 'Expirada';

    default:
      return 'Responder';
  }
};

export const getPaymentStatus = (status: string) => {
  switch (status) {
    case 'PENDENTE':
      return 'Pendente';

    case 'MAOS':
      return 'Em mãos';

    case 'BOLETO':
      return 'Boleto';

    case 'CARTAO':
      return 'Cartão';

    default:
      return 'Responder';
  }
};

export const getStudentStatusColor = (status: string) => {
  switch (status) {
    case 'CONFIRMADO':
      return '#049526';
    case 'PAGO':
      return '#049526';
    case 'MAOS':
      return '#049526';
    case 'Aguardando pagamento':
      return '#F76E1E';
    case 'CANCELADO':
      return '#D22020';
    case 'PENDENTE':
      return '#707070';
    case 'ESTORNADA':
      return '#F76E1E';
    case 'DISPONIVEL':
      return '#FFF';
    case 'CONCLUIDO':
      return '#37153D';
    case 'REMARCADO':
      return '#D22020';
    case 'RECUSADO':
      return '#D22020';
    case 'EXPIRADO':
      return '#FFEC00';

    default:
      return '#37153D';
  }
};

export const getStatusColor = (status: string) => {
  switch (status) {
    case 'CONFIRMADO':
      return '#049526';
    case 'PAGO':
      return '#049526';
    case 'MAOS':
      return '#049526';
    case 'Aguardando pagamento':
      return '#F76E1E';
    case 'CANCELADO':
      return '#D22020';
    case 'REMARCADO':
      return '#D22020';
    case 'RECUSADO':
      return '#D22020';
    case 'PENDENTE':
      return '#fff';
    case 'DISPONIVEL':
      return '#fff';
    case 'ANDAMENTO':
      return '#fff';
    case 'ESTORNADA':
      return '#F76E1E';
    case 'CONCLUIDO':
      return '#fff';
    case 'EXPIRADO':
      return '#FFEC00';

    default:
      return '#000';
  }
};

export const getStudentStatusBG = (status: string) => {
  switch (status) {
    case 'DISPONIVEL':
      return '#F76E1E';

    default:
      return '#E9E9E9';
  }
};

export const getStatusBG = (status: string) => {
  switch (status) {
    case 'DISPONIVEL':
      return '#F76E1E';
    case 'ANDAMENTO':
      return '#F76E1E';
    case 'CANCELADO':
      return '#E9E9E9';
    case 'CONFIRMADO':
      return '#E9E9E9';
    case 'CONCLUIDO':
      return '#37153D';
    case 'REMARCADO':
      return '#E9E9E9';
    case 'RECUSADO':
      return '#E9E9E9';
    case 'EXPIRADO':
      return '#E9E9E9';

    default:
      return '#37153D';
  }
};
