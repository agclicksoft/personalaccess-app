const specialtiesList = [
  { description: 'Hipertrofia', checked: false, id: 1 },
  { description: 'Emagrecimento', checked: false, id: 2 },
  { description: 'Condicionamento Físico', checked: false, id: 3 },
  { description: 'Preparação Física', checked: false, id: 4 },
  { description: 'Yoga', checked: false, id: 5 },
  { description: 'Pilates', checked: false, id: 6 },
  { description: 'Gestante', checked: false, id: 7 },
  { description: 'Kids', checked: false, id: 8 },
  { description: 'Atividades para Idosos', checked: false, id: 9 },
  { description: 'Dança', checked: false, id: 10 },
];

export default specialtiesList;
