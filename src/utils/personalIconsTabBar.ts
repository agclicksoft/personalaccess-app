import {
  ChartFocused,
  ChartUnfocused,
  ClassFocused,
  ClassUnfocused,
  ProfileFocused,
  ProfileUnfocused,
} from '~/assets/icons';

const personalIconsTabBar = {
  Statistics: {
    active: ChartFocused,
    inactive: ChartUnfocused,
  },
  Class: {
    active: ClassFocused,
    inactive: ClassUnfocused,
  },

  Profile: {
    active: ProfileFocused,
    inactive: ProfileUnfocused,
  },
};

export default personalIconsTabBar;
