const formatDate = (date: string, format?: string): string => {
  const [date_formated, hour_formated] = date.split(' ');
  const separate_date = date_formated.split('-');
  return `${separate_date[2]}/${separate_date[1]}/${separate_date[0]}`;
};

const generateDateToPicker = () => {
  const until_years_ago = 3;
  const monthsYear = [
    { text: 'Dezembro', number: 12 },
    { text: 'Novembro', number: 11 },
    { text: 'Outubro', number: 10 },
    { text: 'Setembro', number: 9 },
    { text: 'Agosto', number: 8 },
    { text: 'Julho', number: 7 },
    { text: 'Junho', number: 6 },
    { text: 'Maio', number: 5 },
    { text: 'Abril', number: 4 },
    { text: 'Março', number: 3 },
    { text: 'Fevereiro', number: 2 },
    { text: 'Janeiro', number: 1 },
  ];

  const allMonths = [];
  const year = new Date().getFullYear();

  for (let i = year; i > year - until_years_ago; i--) {
    monthsYear.forEach((month) => {
      allMonths.push({
        year: i,
        month,
        label: `${month.text}/${i}`,
        value: `${month.number}/${i}`,
        key: i + month.number + 1,
      });
    });
  }

  return allMonths;
};

export { formatDate, generateDateToPicker };
