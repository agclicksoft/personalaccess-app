import {
  HomeFocused,
  HomeUnfocused,
  ClassFocused,
  ClassUnfocused,
  ProfileFocused,
  ProfileUnfocused,
} from '~/assets/icons';

const listIconsTabBar = {
  Home: {
    active: HomeFocused,
    inactive: HomeUnfocused,
  },
  Class: {
    active: ClassFocused,
    inactive: ClassUnfocused,
  },

  Profile: {
    active: ProfileFocused,
    inactive: ProfileUnfocused,
  },
};

export default listIconsTabBar;
