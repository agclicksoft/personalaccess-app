export const PLACES_API = 'AIzaSyAj-X5gMkLO-x2UvebX5Dp_iR90MK0JjSI';

export const PlaceholderImage =
  'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png';

export const bankList = [
  {
    value: '001',
    label: 'Banco do Brasil S.A.',
  },
  {
    value: '104',
    label: 'Caixa Econômica Federal',
  },
  {
    value: '341',
    label: 'Itaú Unibanco S.A.',
  },
  {
    value: '237',
    label: 'Banco Bradesco S.A.',
  },
  {
    value: '033',
    label: 'Banco Santander S.A.',
  },
  {
    value: '070',
    label: 'BRB - Banco de Brasília S.A.',
  },
  {
    value: '003',
    label: 'Banco da Amazônia S.A.',
  },
  {
    value: '047',
    label: 'Banco do Estado de Sergipe S.A.',
  },
  {
    value: '037',
    label: 'Banco do Estado do Pará S.A.',
  },
  {
    value: '041',
    label: 'BANRISUL - Banco do Estado do Rio Grande do Sul S.A.',
  },
  {
    value: '004',
    label: 'Banco do Nordeste do Brasil S.A.',
  },
  {
    value: '021',
    label: 'BANESTES - Banco do Estado do Espírito Santo S.A.',
  },
  {
    value: '260',
    label: 'Nu Pagamentos S.A.',
  },
  {
    value: '290',
    label: 'Pag Seguro Internet S.A.',
  },
  {
    value: '380',
    label: 'Picpay Serviços S.A.',
  },
  {
    value: '323',
    label: 'Mercado Pago',
  },
  {
    value: '362',
    label: 'Cielo S.A.',
  },
  {
    value: '096',
    label: 'Banco B3 S.A.',
  },
  {
    value: '318',
    label: 'Banco BMG S.A.',
  },
  {
    value: '218',
    label: 'Banco BS2 S.A.',
  },
  {
    value: '756',
    label: 'Banco Cooperativo do Brasil S.A. - BANCOOB',
  },
  {
    value: '707',
    label: 'Banco Daycoval S.A.',
  },
  {
    value: '224',
    label: 'Banco Fibra S.A.',
  },
  {
    value: '077',
    label: 'Banco Inter S.A.',
  },
  {
    value: '212',
    label: 'Banco Original S.A.',
  },
  {
    value: '623',
    label: 'Banco PAN S.A.',
  },
  {
    value: '422',
    label: 'Banco Safra S.A.',
  },
];
