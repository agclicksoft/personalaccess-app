import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

import en from '~/lang/en';
import pt from '~/lang/pt';

i18n.translations = { pt, en };
i18n.locale = Localization.locale;
i18n.fallbacks = true;

export default i18n;
