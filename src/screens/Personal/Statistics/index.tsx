import React from 'react';
import { FlatList } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import {
  IconCost,
  IconLocation,
  IconCancel,
  IconTeam,
  IconKettleBell,
  IconAvaliation,
} from '~/assets/icons';

import {
  Container,
  HeaderContainer,
  Title,
  Divider,
  MenuButton,
  MenuIcon,
  TextWrapper,
  MenuTitle,
  MenuContainer,
  Holder,
} from './styles';

const options = [
  {
    title: 'Preços mais cobrados',
    icon: IconCost,
    screen: 'MostCharged',
  },
  {
    title: 'Preço médio cobrado por bairro',
    icon: IconLocation,
    screen: 'AveragePrice',
  },
  {
    title: 'Número médio de solicitações e rejeições por bairro',
    icon: IconAvaliation,
    screen: 'NumberRequests',
  },
  // {
  //   title: 'Número médio de rejeições por bairro',
  //   icon: IconCancel,
  //   screen: 'NumberRejects',
  // },
  {
    title: 'Média de aulas marcadas por professor',
    icon: IconTeam,
    screen: 'AverageSchedules',
  },
];

const Statistics: React.FC = () => {
  const { navigate } = useNavigation();

  return (
    <Container>
      <HeaderContainer>
        <Title>Estatísticas</Title>
      </HeaderContainer>
      <Divider />

      <FlatList
        showsVerticalScrollIndicator={false}
        data={options}
        keyExtractor={(option) => String(option.title)}
        ItemSeparatorComponent={Divider}
        renderItem={({ item }) => (
          <MenuButton
            key={item.title}
            onPress={() => {
              navigate(item.screen);
            }}
          >
            <MenuContainer>
              <Holder>
                <MenuIcon source={item.icon} />

                <TextWrapper>
                  <MenuTitle>{item.title}</MenuTitle>
                </TextWrapper>
              </Holder>

              <Ionicons
                name="ios-arrow-forward"
                size={21}
                color="rgba(112,112,112,0.5)"
              />
            </MenuContainer>
          </MenuButton>
        )}
      />
    </Container>
  );
};

export default Statistics;
