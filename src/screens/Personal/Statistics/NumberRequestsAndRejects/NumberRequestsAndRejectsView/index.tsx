/* eslint-disable @typescript-eslint/no-explicit-any */

import React from 'react';
import { Dimensions } from 'react-native';

import { useRoute } from '@react-navigation/native';
import { BarChart } from 'react-native-chart-kit';

import { ContainerScroll, HeaderBack } from '~/components';
import AverageChart from '~/models/AverageChart';

import {
  Container,
  Content,
  Title,
  Subtitle,
  Wrapper,
  Holder,
  Pin,
  PinLabel,
} from './styles';

interface Params {
  chartData: AverageChart;
  chartDataReject: AverageChart;
}

const NumberRequestsAndRejectsView: React.FC = () => {
  const { params } = useRoute();
  const { chartData, chartDataReject } = params as Params;
  const screenWidth = Dimensions.get('window').width;

  const chartConfig = {
    backgroundGradientFrom: 'rgba(248, 248, 248, 1)',
    backgroundGradientTo: 'rgba(248, 248, 248, 1)',
    decimalPlaces: 2,
    color: () => `#37153D`,
    labelColor: () => `#3a3939`,
    style: {
      borderRadius: 0,
    },
    propsForBackgroundLines: {
      strokeWidth: 0.4,
    },
    fillShadowGradientOpacity: 1,
    propsForLabels: {
      fontSize: 11,
      fill: '#37153D',
    },
  };

  const dataset = {
    labels: ['Mín', 'Mín', 'Média', 'Média', 'Máx', 'Máx'],
    datasets: [
      {
        data: [
          chartData?.min,
          chartDataReject?.min,
          Number(chartData?.avg),
          Number(chartDataReject?.avg),
          chartData?.max,
          chartDataReject?.max,
        ],
        colors: [
          () => `#37153D`,
          () => `#f55252`,
          () => `#37153D`,
          () => `#f55252`,
          () => `#37153D`,
          () => `#f55252`,
        ],
      },
    ],
  };

  return (
    <Container>
      <HeaderBack>Número médio de solicitações e rejeições</HeaderBack>

      <ContainerScroll>
        {chartData && (
          <Content>
            <Title>
              {chartData.neighborhood} - {chartData.city}
            </Title>

            <BarChart
              data={dataset}
              width={screenWidth}
              height={250}
              chartConfig={{
                ...chartConfig,
                data: dataset.datasets,
              }}
              yAxisLabel=""
              yAxisSuffix=""
              showValuesOnTopOfBars
              withCustomBarColorFromData
              fromZero
              withHorizontalLabels={false}
              style={{
                marginLeft: -70,
              }}
            />

            <Wrapper>
              <Holder>
                <Pin style={{ backgroundColor: '#37153D' }} />
                <PinLabel>Solicitações</PinLabel>
              </Holder>

              <Holder>
                <Pin style={{ backgroundColor: '#f55252' }} />
                <PinLabel>Rejeições</PinLabel>
              </Holder>
            </Wrapper>

            <Subtitle>Dados dos últimos 30 dias.</Subtitle>
          </Content>
        )}
      </ContainerScroll>
    </Container>
  );
};

export default NumberRequestsAndRejectsView;
