import React, { useCallback, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { showMessage } from 'react-native-flash-message';

import {
  Button,
  ContainerScroll,
  GoogleSearch,
  HeaderBack,
} from '~/components';
import api from '~/services/api';

import {
  Container,
  Content,
  Title,
  ButtonWrapper,
  Label,
  Bold,
  Result,
} from './styles';

interface Props {
  state?: string;
  city?: string;
  neighborhood?: string;
  street?: string;
  country?: string;
  name?: string;
  gym?: string;
  gymAddress?: string;
  uf?: string;
}

interface ResultsProps {
  avg: number;
  uf: string;
  city: string;
  neighborhood: string;
}

const NumberRequests: React.FC = () => {
  const { navigate } = useNavigation();
  const [loading, setLoading] = useState(false);
  const [address, setAddress] = useState<Props>();
  const [result, setResult] = useState<ResultsProps>();

  const handleSubmit = useCallback(async () => {
    try {
      setLoading(true);
      setResult(null);

      if (!address) {
        showMessage({
          type: 'danger',
          message: 'Favor preencher o endereço',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      const response = await api.get(
        `/reports/number-requests?uf=${address.uf}&neighborhood=${address.neighborhood}`,
      );

      if (!response.data.avg) {
        showMessage({
          type: 'danger',
          message: 'No momento não existem dados para o bairro solicitado.',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      const responseReject = await api.get(
        `/reports/number-reject-requests?uf=${address.uf}&neighborhood=${address.neighborhood}`,
      );

      if (!responseReject.data.avg) {
        showMessage({
          type: 'danger',
          message: 'No momento não existem dados para o bairro solicitado.',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      navigate('NumberRequestsAndRejectsView', {
        chartData: response.data,
        chartDataReject: responseReject.data,
      });

      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err?.response);

      showMessage({
        type: 'danger',
        message: 'No momento não existem dados para o bairro solicitado.',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [address, navigate]);

  return (
    <Container>
      <HeaderBack>Número médio de solicitações e rejeições</HeaderBack>

      <ContainerScroll>
        <Content>
          <Title>Endereço:</Title>

          <GoogleSearch setField={setAddress} />

          <ButtonWrapper>
            <Button onPress={() => handleSubmit()}>Selecionar</Button>
          </ButtonWrapper>

          {/* {result && (
            <>
              <Label>
                Número médio de solicitações e rejeições no bairro{' '}
                <Bold>{result.neighborhood}</Bold>:
              </Label>

              <Result>{Number(result.avg)} solicitações</Result>
            </>
          )} */}
        </Content>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default NumberRequests;
