import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Content = styled.View`
  width: 100%;
  padding: 10px;
  align-items: center;
`;

export const Title = styled.Text`
  color: ${({ theme }) => theme.colors.purple};
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  margin: 12px 0 12px 16px;
`;

export const Label = styled.Text`
  color: ${({ theme }) => theme.colors.purple};
  font-size: ${({ theme }) => theme.fontSizes.size16};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  margin-bottom: 35px;
`;

export const Wrapper = styled.View`
  flex-direction: row;
  justify-content: center;
  width: 100%;
`;

export const Holder = styled.View`
  align-items: center;
  width: 130px;
`;

export const Pin = styled.View`
  width: 20px;
  height: 20px;
  border-radius: 10px;
`;

export const PinLabel = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size16};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  margin-bottom: 35px;
`;

export const Subtitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.grayLight};
  margin-top: 40px;
`;
