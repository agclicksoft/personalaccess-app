/* eslint-disable @typescript-eslint/no-explicit-any */

import React from 'react';
import { Dimensions } from 'react-native';

import { useRoute } from '@react-navigation/native';
import { BarChart } from 'react-native-chart-kit';

import { ContainerScroll, HeaderBack } from '~/components';
import AverageChart from '~/models/AverageChart';

import { Container, Content, Title, Label, Subtitle } from './styles';

interface Params {
  chartData: AverageChart;
}

const AveragePriceView: React.FC = () => {
  const { params } = useRoute();
  const { chartData } = params as Params;
  const screenWidth = Dimensions.get('window').width;

  const chartConfig = {
    backgroundGradientFrom: 'rgba(248, 248, 248, 1)',
    backgroundGradientTo: 'rgba(248, 248, 248, 1)',
    decimalPlaces: 2,
    color: () => `#37153D`,
    labelColor: () => `#3a3939`,
    style: {
      borderRadius: 0,
    },
    propsForBackgroundLines: {
      strokeWidth: 0.5,
    },
    fillShadowGradientOpacity: 1,
    propsForLabels: {
      fontSize: 11,
      fill: '#37153D',
    },
    strokeWidth: 0.1,
    propsForDots: {
      r: '7',
      strokeWidth: '4',
      stroke: 'rgba(248, 248, 248, 1)',
    },
  };

  const dataset = {
    labels: ['Menor valor', 'Valor médio', 'Maior valor', 'Seu valor'],
    datasets: [
      {
        data: [
          chartData?.min,
          Number(chartData?.avg.toFixed(2)),
          chartData?.max,
          chartData?.price,
        ],
      },
    ],
  };

  return (
    <Container>
      <HeaderBack>Preço médio cobrado por bairro</HeaderBack>

      <ContainerScroll>
        {chartData && (
          <Content>
            <Title>
              {chartData.neighborhood} - {chartData.district}
            </Title>

            <Label>
              {chartData.plan_id > 1 ? 'Pacote de Aulas' : 'Aula Avulsa'}
            </Label>

            <BarChart
              data={dataset}
              width={screenWidth}
              height={200}
              chartConfig={{
                ...chartConfig,
                data: dataset.datasets,
              }}
              yAxisLabel="R$"
              yAxisSuffix=""
              showValuesOnTopOfBars
              fromZero
              yLabelsOffset={5}
            />

            <Subtitle>Dados dos últimos 30 dias.</Subtitle>
          </Content>
        )}
      </ContainerScroll>
    </Container>
  );
};

export default AveragePriceView;
