import React, { useCallback, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { showMessage } from 'react-native-flash-message';

import {
  Button,
  ContainerScroll,
  GoogleSearch,
  HeaderBack,
} from '~/components';
import api from '~/services/api';

import {
  Container,
  Content,
  Title,
  ButtonWrapper,
  Label,
  Bold,
  Result,
} from './styles';

interface Props {
  state?: string;
  city?: string;
  neighborhood?: string;
  street?: string;
  country?: string;
  name?: string;
  gym?: string;
  gymAddress?: string;
  uf?: string;
}

interface ResultsProps {
  avg: number;
  uf: string;
  city: string;
  neighborhood: string;
}

const NumberRejects: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const [address, setAddress] = useState<Props>();
  const [result, setResult] = useState<ResultsProps>();

  const handleSubmit = useCallback(async () => {
    try {
      setLoading(true);
      setResult(null);

      if (!address) {
        showMessage({
          type: 'danger',
          message: 'Favor preencher o endereço',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      const response = await api.get(
        `/reports/number-reject-requests?uf=${address.uf}&neighborhood=${address.neighborhood}`,
      );

      if (response.data[0]) {
        setResult(response.data[0]);
        // console.log(response.data[0]);
      } else {
        showMessage({
          type: 'danger',
          message: 'No momento não existem dados para o bairro solicitado.',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err?.response);

      showMessage({
        type: 'danger',
        message: 'No momento não existem dados para o bairro solicitado.',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [address]);

  return (
    <Container>
      <HeaderBack>Número médio de rejeições por bairro</HeaderBack>

      <ContainerScroll>
        <Content>
          <Title>Endereço:</Title>

          <GoogleSearch setField={setAddress} />

          <ButtonWrapper>
            <Button onPress={() => handleSubmit()}>Selecionar</Button>
          </ButtonWrapper>

          {result && (
            <>
              <Label>
                Número médio de rejeições no bairro{' '}
                <Bold>{result.neighborhood}</Bold>:
              </Label>

              <Result>{Number(result.avg).toFixed(2)} rejeições</Result>
            </>
          )}
        </Content>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default NumberRejects;
