/* eslint-disable no-bitwise */
/* eslint-disable @typescript-eslint/no-explicit-any */

import React from 'react';
import { Dimensions } from 'react-native';

import { useRoute } from '@react-navigation/native';
import { PieChart } from 'react-native-chart-kit';
import { MaskService } from 'react-native-masked-text';

import { ContainerScroll, HeaderBack } from '~/components';
import MostChargedChart from '~/models/MostChargedChart';

import { Container, Content, Title, Label, Subtitle } from './styles';

interface Props {
  state?: string;
  city?: string;
  neighborhood?: string;
  street?: string;
  country?: string;
  name?: string;
  gym?: string;
  gymAddress?: string;
  uf?: string;
}

interface Params {
  chartData: MostChargedChart[];
  address?: Props;
  plan?: number;
}

const MostChargedView: React.FC = () => {
  const { params } = useRoute();
  const { chartData, address, plan } = params as Params;
  const screenWidth = Dimensions.get('window').width;

  const chartConfig = {
    backgroundGradientFrom: '#1E2923',
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: '#08130D',
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2,
    barPercentage: 0.5,
    useShadowColorFromDataset: false,
    propsForLabels: {
      fontSize: 11,
      fontFamily: 'SofiaPro-Medium',
      fill: '#37153D',
    },
  };

  return (
    <Container>
      <HeaderBack>Preços mais cobrados</HeaderBack>

      <ContainerScroll>
        {chartData && (
          <Content>
            <Title>{address.city}</Title>

            <Label>{plan > 1 ? 'Pacote de Aulas' : 'Aula Avulsa'}</Label>

            <PieChart
              data={chartData.map((item) => {
                return {
                  name: MaskService.toMask('money', item.group),
                  population: Number(item.count),
                  color: `hsla(${Math.random() * 360}, 100%, 70%, 1)`,
                  legendFontColor: '#7F7F7F',
                  legendFontSize: 15,
                };
              })}
              width={screenWidth}
              height={200}
              chartConfig={chartConfig}
              accessor="population"
              backgroundColor="transparent"
              paddingLeft="0"
              avoidFalseZero
            />

            <Subtitle>Dados dos últimos 30 dias.</Subtitle>
          </Content>
        )}
      </ContainerScroll>
    </Container>
  );
};

export default MostChargedView;
