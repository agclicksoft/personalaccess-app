import React, { useCallback, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { showMessage } from 'react-native-flash-message';

import {
  Button,
  ContainerScroll,
  GoogleSearch,
  HeaderBack,
  Picker,
} from '~/components';
import api from '~/services/api';

import {
  Container,
  Content,
  Title,
  ButtonWrapper,
  PickerWrapper,
} from './styles';

interface Props {
  state?: string;
  city?: string;
  neighborhood?: string;
  street?: string;
  country?: string;
  name?: string;
  gym?: string;
  gymAddress?: string;
  uf?: string;
}

const selectPlanList = [
  {
    id: 1,
    value: 1,
    label: 'Aula avulsa',
  },
  {
    id: 3,
    value: 3,
    label: '8 aulas no mês',
  },
  {
    id: 17,
    value: 17,
    label: '10 aulas no mês',
  },
  {
    id: 4,
    value: 4,
    label: '12 aulas no mês',
  },
  {
    id: 19,
    value: 19,
    label: '15 aulas no mês',
  },
  {
    id: 5,
    value: 5,
    label: '16 aulas no mês',
  },
  {
    id: 6,
    value: 6,
    label: '20 aulas no mês',
  },
  {
    id: 7,
    value: 7,
    label: '24 aulas no mês',
  },
];

const MostCharged: React.FC = () => {
  const { navigate } = useNavigation();
  const [loading, setLoading] = useState(false);
  const [plan, setPlan] = useState();
  const [address, setAddress] = useState<Props>();

  const handleSubmit = useCallback(async () => {
    try {
      setLoading(true);

      if (!plan || !address) {
        showMessage({
          type: 'danger',
          message: 'Favor preencher todas as informações',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      console.log(plan, address.state);

      const response = await api.get(
        `/reports/most-charged-prices?plan_id=${plan}&state=${address.state}`,
      );

      console.log(response.data);

      if (!response.data.length) {
        showMessage({
          type: 'danger',
          message: 'No momento não existem dados para o bairro solicitado.',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      navigate('MostChargedView', {
        chartData: response.data,
        address,
        plan,
      });

      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err);

      showMessage({
        type: 'danger',
        message: 'No momento não existem dados para o bairro solicitado.',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [plan, address, navigate]);

  return (
    <Container>
      <HeaderBack>Preços mais cobrados</HeaderBack>

      <ContainerScroll>
        <Content>
          <Title>Plano:</Title>

          <PickerWrapper>
            <Picker
              items={selectPlanList}
              value={plan}
              onValueChange={(value) => setPlan(value)}
              label="Selecionar plano"
            />
          </PickerWrapper>

          <Title>Endereço:</Title>

          <GoogleSearch setField={setAddress} />

          <ButtonWrapper>
            <Button onPress={() => handleSubmit()}>Selecionar</Button>
          </ButtonWrapper>
        </Content>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default MostCharged;
