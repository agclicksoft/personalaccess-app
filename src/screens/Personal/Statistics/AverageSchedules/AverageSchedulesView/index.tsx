import React from 'react';
import { Dimensions } from 'react-native';

import { useRoute } from '@react-navigation/native';
import { BarChart } from 'react-native-chart-kit';

import { ContainerScroll, HeaderBack } from '~/components';
import AverageChart from '~/models/AverageChart';

import { Container, Content, Label } from './styles';

interface Params {
  chartData: AverageChart;
}

const AverageSchedulesView: React.FC = () => {
  const { params } = useRoute();
  const { chartData } = params as Params;
  const screenWidth = Dimensions.get('window').width;

  const chartConfig = {
    backgroundGradientFrom: 'rgba(248, 248, 248, 1)',
    backgroundGradientTo: 'rgba(248, 248, 248, 1)',
    decimalPlaces: 2,
    color: () => `#37153D`,
    labelColor: () => `#3a3939`,
    style: {
      borderRadius: 0,
    },
    propsForBackgroundLines: {
      strokeWidth: 0.4,
    },
    fillShadowGradientOpacity: 1,
    propsForLabels: {
      fontSize: 11,
      fill: '#37153D',
    },
  };

  const dataset = {
    labels: ['Mínimo', 'Média', 'Máximo', ' Você'],
    datasets: [
      {
        data: [
          chartData?.min,
          Number(chartData?.avg).toFixed(2),
          chartData?.max,
          chartData?.avg_professional ?? 0,
        ],
      },
    ],
  };

  return (
    <Container>
      <HeaderBack>Média de aulas marcadas por professor</HeaderBack>

      <ContainerScroll>
        <Content>
          {chartData && (
            <>
              <Label>Média de aulas marcadas por professor</Label>

              <BarChart
                data={dataset}
                width={screenWidth}
                height={250}
                chartConfig={{
                  ...chartConfig,
                  data: dataset.datasets,
                }}
                yAxisLabel=""
                yAxisSuffix=""
                showValuesOnTopOfBars
                fromZero
              />
            </>
          )}
        </Content>
      </ContainerScroll>
    </Container>
  );
};

export default AverageSchedulesView;
