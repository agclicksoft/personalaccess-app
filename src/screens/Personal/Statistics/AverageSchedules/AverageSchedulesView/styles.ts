import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Content = styled.View`
  width: 100%;
  padding: 10px;
`;

export const Title = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  margin: 12px 0 12px 16px;
`;

export const PickerWrapper = styled.View`
  width: 90%;
  align-self: center;
  margin-bottom: 10px;
`;

export const WeightHolder = styled.View`
  flex: 1;
  margin-right: 20px;
`;

export const HeightHolder = styled.View`
  flex: 1;
`;

export const ButtonWrapper = styled.View`
  margin: 30px 0;
  align-items: center;
`;

export const Label = styled.Text`
  color: ${({ theme }) => theme.colors.grayDark};
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  margin: 12px 0 30px;
  text-align: center;
`;

export const Bold = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.black};
`;

export const Result = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size21};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  margin-top: 5px;
  text-align: center;
`;
