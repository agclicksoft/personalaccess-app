import React, { useCallback, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { showMessage } from 'react-native-flash-message';

import {
  Button,
  ContainerScroll,
  GoogleSearch,
  HeaderBack,
} from '~/components';
import api from '~/services/api';

import { Container, Content, Title, ButtonWrapper } from './styles';

interface Props {
  state?: string;
  city?: string;
  neighborhood?: string;
  street?: string;
  country?: string;
  name?: string;
  gym?: string;
  gymAddress?: string;
  uf?: string;
}

const AverageSchedules: React.FC = () => {
  const { navigate } = useNavigation();
  const [loading, setLoading] = useState(false);
  const [address, setAddress] = useState<Props>();

  const handleSubmit = useCallback(async () => {
    try {
      setLoading(true);

      if (!address) {
        showMessage({
          type: 'danger',
          message: 'Favor preencher o endereço',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      const response = await api.get(
        `/reports/number-requests-by-professionals?uf=${address.uf}&neighborhood=${address.neighborhood}`,
      );

      if (!response.data.avg) {
        showMessage({
          type: 'danger',
          message: 'No momento não existem dados para o bairro solicitado.',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        return;
      }

      navigate('AverageSchedulesView', {
        chartData: response.data,
      });

      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err?.response);

      showMessage({
        type: 'danger',
        message: 'No momento não existem dados para o bairro solicitado.',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [address, navigate]);

  return (
    <Container>
      <HeaderBack>Média de aulas marcadas por professor</HeaderBack>

      <ContainerScroll>
        <Content>
          <Title>Endereço:</Title>

          <GoogleSearch setField={setAddress} />

          <ButtonWrapper>
            <Button onPress={() => handleSubmit()}>Selecionar</Button>
          </ButtonWrapper>
        </Content>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default AverageSchedules;
