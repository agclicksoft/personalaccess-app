import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import { Form } from '@unform/mobile';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { Button, ContainerScroll, HeaderBack, MaskedInput } from '~/components';
import { useAuth } from '~/hooks/auth';
import { ProfessionalPlan, User } from '~/models';
import api from '~/services/api';
import { getShowPersonalPlans } from '~/services/personal';
import { getValidationErrors } from '~/utils/getValidationErrors';

import { Container, Content, Title, Subtitle, Label } from './styles';

interface Params {
  servicePackage: ProfessionalPlan;
}

const ServicePackagesEditPrice: React.FC = () => {
  const { user, updatePersonal } = useAuth();
  const { navigate } = useNavigation();
  const { params } = useRoute();
  const { servicePackage } = params as Params;
  const formRef = useRef(null);

  const [loading, setLoading] = useState(false);

  const handleSubmitEditPrice = useCallback(
    async (data) => {
      const { price } = data;

      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          price: Yup.string().required('Preço é obrigatório'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        await api.post('/professionals/plans', {
          plan: {
            plan_id: servicePackage.plan.id,
            price,
          },
        });

        const response = await getShowPersonalPlans(user.professional.id);

        const formData: User = {
          ...user,
          id: user.id,
          professional: {
            ...user.professional,
            professionalPlans: response,
          },
        };

        await updatePersonal(formData);

        setLoading(false);

        showMessage({
          type: 'success',
          message: 'Pacote atualizado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        navigate('ServicePackages');
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);
        }

        setLoading(false);

        console.log(err);

        showMessage({
          type: 'danger',
          message: 'Digite um valor',
          description: 'Digite um valor acima de R$1,00',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [navigate, servicePackage.plan.id, updatePersonal, user],
  );

  return (
    <>
      <Container>
        <HeaderBack>Editar Pacotes de serviços</HeaderBack>

        <ContainerScroll>
          <Content>
            <Title>
              {servicePackage.plan.amount > 1
                ? 'Pacote de aula'
                : 'Aula avulsa'}
            </Title>

            <Subtitle>
              {servicePackage.plan.amount}{' '}
              {servicePackage.plan.amount > 1 ? 'aulas no mês' : 'única aula'}
            </Subtitle>

            <Form ref={formRef} onSubmit={handleSubmitEditPrice}>
              <Label>Preço</Label>
              <MaskedInput
                name="price"
                type="money"
                keyboardType="decimal-pad"
                placeholder="R$ 99,90"
                maxLength={10}
              />
            </Form>

            <Button onPress={() => formRef?.current.submitForm()}>
              Salvar
            </Button>
          </Content>
        </ContainerScroll>
      </Container>

      {/* <Modal isVisible={modalSuccessIsVisible}>
        <ModalSuccess>
          <ModalSuccessText>Pacote atualizado com sucesso!</ModalSuccessText>

          <LottieView
            style={{
              height: 64,
            }}
            source={SuccessAnimation}
            autoPlay
            loop={false}
          />
        </ModalSuccess>
      </Modal> */}

      {/* <Modal isVisible={loading}>
        <ModalSuccess>
          <ModalSuccessText>Atualizando pacote...</ModalSuccessText>

          <ActivityIndicator style={{ height: 64 }} size={32} color="#37153D" />
        </ModalSuccess>
      </Modal> */}

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </>
  );
};

export default ServicePackagesEditPrice;
