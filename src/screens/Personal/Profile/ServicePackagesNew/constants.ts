export const selectPackageList = [
  {
    id: 1,
    value: 'singleClass',
    label: 'Aula avulsa',
  },
  {
    id: 2,
    value: 'lessonsPackage',
    label: 'Pacote de aulas',
  },
];

export const packageList = [
  {
    id: 3,
    value: 3,
    label: '8 aulas no mês',
  },
  {
    id: 17,
    value: 17,
    label: '10 aulas no mês',
  },
  {
    id: 4,
    value: 4,
    label: '12 aulas no mês',
  },
  {
    id: 19,
    value: 19,
    label: '15 aulas no mês',
  },
  {
    id: 5,
    value: 5,
    label: '16 aulas no mês',
  },
  {
    id: 6,
    value: 6,
    label: '20 aulas no mês',
  },
  {
    id: 7,
    value: 7,
    label: '24 aulas no mês',
  },
];
