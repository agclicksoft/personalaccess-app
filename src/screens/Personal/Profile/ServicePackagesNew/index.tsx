import React, { useState } from 'react';

import { Button, HeaderBack } from '~/components';
import { Picker } from '~/components';

import { Container, Content, Label, ButtonContainer } from './styles';

import { selectPackageList } from './constants';
import ModalServicePackagesNew from './ModalServicePackagesNew';

const ServicePackagesNew: React.FC = () => {
  const [selectedPackage, setSelectedPackage] = useState('singleClass');
  const [
    modalServicePackagesNewIsVisible,
    setModalServicePackagesNewIsVisible,
  ] = useState(false);

  console.log('selectedPackage', selectedPackage);

  return (
    <>
      <Container>
        <HeaderBack>Pacotes de aulas</HeaderBack>

        <Content>
          <Label>Selecione seu pacote:</Label>
          <Picker
            items={selectPackageList}
            value={selectedPackage}
            onValueChange={(value) => setSelectedPackage(value)}
            placeholder={{}}
          />

          <ButtonContainer>
            <Button
              onPress={() =>
                setModalServicePackagesNewIsVisible((prevState) => !prevState)
              }
            >
              Selecionar
            </Button>
          </ButtonContainer>
        </Content>
      </Container>

      <ModalServicePackagesNew
        isVisible={modalServicePackagesNewIsVisible}
        typeOfPackage={selectedPackage}
        setModalClose={setModalServicePackagesNewIsVisible}
      />
    </>
  );
};

export default ServicePackagesNew;
