import styled from 'styled-components/native';

export const Container = styled.View``;

export const Content = styled.View`
  background-color: #f8f8f8;
  flex: 1;

  padding: 40px 24px;
  align-items: center;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size20};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};

  text-align: center;
  margin-bottom: 20px;
`;

export const Label = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: #2e2e2e;

  margin: 0 0 4px 16px;
`;

export const ButtonCancel = styled.TouchableOpacity``;

export const ButtonCancelText = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size11};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  color: #707070;

  text-align: center;
  margin-top: 16px;
`;

export const ModalSuccess = styled.View`
  align-items: center;
  padding: 40px 24px;
`;

export const ModalSuccessText = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.openSans};
  color: #2e2e2e;

  text-align: center;
`;
