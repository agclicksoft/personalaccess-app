import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import LottieView from 'lottie-react-native';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { SuccessAnimation } from '~/assets/animations';
import { Button, MaskedInput, Modal, Picker } from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import api from '~/services/api';
import { getShowPersonalPlans } from '~/services/personal';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Container,
  Content,
  Title,
  Label,
  ButtonCancel,
  ButtonCancelText,
  ModalSuccess,
  ModalSuccessText,
} from './styles';

import { packageList } from '../constants';

interface ModalServicePackagesNewProps {
  typeOfPackage: string;
  isVisible: boolean;
  setModalClose: (modalIsVisible: boolean) => void;
}

const ModalServicePackagesNew: React.FC<ModalServicePackagesNewProps> = ({
  isVisible,
  typeOfPackage,
  setModalClose,
}) => {
  const { user, updatePersonal } = useAuth();
  const { goBack } = useNavigation();
  const formRef = useRef<FormHandles>(null);

  const [loading, setLoading] = useState(false);
  const [selectedTypePackageId, setSelectedTypePackageId] = useState(3);

  const handleSubmit = useCallback(
    async (data) => {
      const { price } = data;

      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const plan_id =
          typeOfPackage === 'singleClass' ? 1 : selectedTypePackageId;

        const schema = Yup.object().shape({
          price: Yup.string().required('Preço é obrigatório'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        setModalClose(false);

        await api.post('/professionals/plans', {
          plan: {
            plan_id,
            price,
          },
        });

        const response = await getShowPersonalPlans(user.professional.id);

        const formData: User = {
          ...user,
          id: user.id,
          professional: {
            ...user.professional,
            professionalPlans: response,
          },
        };

        await updatePersonal(formData);

        setLoading(false);

        showMessage({
          type: 'success',
          message: 'Pacote criado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
        });

        goBack();
      } catch (err) {
        setLoading(false);

        console.log(err);

        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);

          showMessage({
            type: 'danger',
            message: 'Digite um valor',
            description: 'Digite um valor acima de R$1,00',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        showMessage({
          type: 'danger',
          message: 'Algo de errado ocorreu',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [
      goBack,
      selectedTypePackageId,
      setModalClose,
      typeOfPackage,
      updatePersonal,
      user,
    ],
  );

  const SingleClass: React.FC = () => {
    return (
      <Content>
        <Title>Aula avulsa</Title>

        <Form ref={formRef} onSubmit={handleSubmit}>
          <Label>Preço</Label>
          <MaskedInput
            name="price"
            type="money"
            keyboardType="decimal-pad"
            placeholder="R$ 99,90"
            maxLength={10}
          />
        </Form>

        <Button onPress={() => formRef?.current.submitForm()}>Salvar</Button>

        <ButtonCancel onPress={() => setModalClose(false)}>
          <ButtonCancelText>Cancelar</ButtonCancelText>
        </ButtonCancel>
      </Content>
    );
  };

  const ServicePackagesNew: React.FC = () => {
    return (
      <Content>
        <Title>Pacote de aulas</Title>

        <Form ref={formRef} onSubmit={handleSubmit}>
          <Label>Tipo</Label>
          <Picker
            items={packageList}
            value={selectedTypePackageId}
            onValueChange={(value) => setSelectedTypePackageId(value)}
            placeholder={{}}
          />

          <Label style={{ marginTop: 16 }}>Preço</Label>
          <MaskedInput
            name="price"
            type="money"
            keyboardType="decimal-pad"
            placeholder="R$ 99,90"
            maxLength={10}
          />
        </Form>

        <Button onPress={() => formRef?.current.submitForm()}>Salvar</Button>

        <ButtonCancel onPress={() => setModalClose(false)}>
          <ButtonCancelText>Cancelar</ButtonCancelText>
        </ButtonCancel>
      </Content>
    );
  };

  return (
    <>
      <Modal isVisible={isVisible} closeModal={() => setModalClose(false)}>
        {typeOfPackage === 'singleClass' ? (
          <SingleClass />
        ) : (
          <ServicePackagesNew />
        )}
      </Modal>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </>
  );
};

export default ModalServicePackagesNew;
