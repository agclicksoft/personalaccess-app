import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: #f8f8f8;
  flex: 1;
`;

export const Content = styled.View`
  padding: 24px;
`;

export const NotHavePlanText = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};

  text-align: center;
  margin-top: 64px;
`;

export const Label = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: #2e2e2e;

  margin-bottom: 4px;
`;

export const ButtonContainer = styled.View`
  align-items: center;
  margin-top: 24px;
`;
