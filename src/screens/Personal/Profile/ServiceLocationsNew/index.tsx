import React, { useState, useRef, useCallback, useEffect } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { IconPlace } from '~/assets/icons';
import {
  ContainerScroll,
  HeaderBack,
  GoogleSearch,
  Input,
  MaskedInput,
} from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import api from '~/services/api';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Container,
  Content,
  Title,
  SearchContainer,
  SearchContainerTitle,
  Holder,
  Image,
  TextWrapper,
  GymName,
  GymAddress,
  Label,
} from './styles';

interface Props {
  state?: string;
  city?: string;
  neighborhood?: string;
  street?: string;
  country?: string;
  name?: string;
  gym?: string;
  gymAddress?: string;
  uf?: string;
}

const NewGym: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { user, updatePersonal } = useAuth();
  const { goBack } = useNavigation();
  const [loading, setLoading] = useState(false);
  const [address, setAddress] = useState<Props>(null);
  const [cnpj, setCnpj] = useState(null);

  useEffect(() => {
    formRef.current.setData({
      name: address?.gym,
    });
  }, [address]);

  const handleSubmit = useCallback(
    async (data) => {
      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          name: Yup.string().required('Nome é obrigatório'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        const response = await api.post(
          `/professionals/${user.professional.id}/service-locations`,
          {
            professionalServiceLocations: {
              state: address?.state ?? null,
              district: address?.city ?? null,
              neighborhood: address?.neighborhood ?? null,
            },
          },
        );

        if (response.data.data.id) {
          const newLocation = await api.put(
            `/professionals/${user.professional.id}/service-locations/${response.data.data.id}`,
            {
              name: address?.gym,
              cnpj: cnpj ?? null,
              address_street: address?.gymAddress ?? null,
              address_neighborhood: address?.neighborhood ?? null,
              address_city: address?.city ?? null,
              address_state: address?.state ?? null,
              address_uf: address?.uf ?? null,
            },
          );

          console.log(newLocation.data);
        }

        const formData: User = {
          ...user,
          id: user.id,
          professional: user.professional,
        };

        console.log(formData);

        await updatePersonal(formData);

        showMessage({
          type: 'success',
          message: 'Academia cadastrada com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);

        goBack();
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);
        }

        setLoading(false);

        // console.log(err);

        showMessage({
          type: 'danger',
          message: 'Algo de errado ocorreu',
          description: 'Verifique e tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [user, address, updatePersonal, goBack, cnpj],
  );

  return (
    <>
      <Container>
        <HeaderBack
          rightButton
          rightButtonTitle="Salvar"
          onPress={() => formRef.current?.submitForm()}
        >
          Adicionar academia
        </HeaderBack>

        <ContainerScroll>
          <SearchContainer>
            <SearchContainerTitle>Endereço:</SearchContainerTitle>

            <GoogleSearch setField={setAddress} searchGyms />
          </SearchContainer>

          <Content>
            <Form ref={formRef} onSubmit={handleSubmit}>
              <Title>Nome:</Title>
              <Input
                name="name"
                placeholder="Nome"
                autoCapitalize="words"
                autoCorrect={false}
                width="90%"
              />

              <Title>CNPJ:</Title>
              <MaskedInput
                name="cnpj"
                type="cnpj"
                placeholder="CNPJ"
                keyboardType="number-pad"
                width="90%"
                value={cnpj ?? ''}
                onChangeText={(text) => {
                  setCnpj(text);
                }}
              />
            </Form>
          </Content>

          {address && (
            <Holder>
              <Image source={IconPlace} />

              <TextWrapper>
                <GymName>{address.gym}</GymName>

                <GymAddress>
                  <Label>CNPJ: </Label>
                  {cnpj || 'Não informado'}.
                </GymAddress>

                <GymAddress>
                  <Label>Endereço: </Label>
                  {address.gymAddress}.
                </GymAddress>
              </TextWrapper>
            </Holder>
          )}
        </ContainerScroll>

        {loading && (
          <View
            style={{
              flex: 1,
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              backgroundColor: '#00000029',
              position: 'absolute',
            }}
          >
            <ActivityIndicator size="large" color="#37153D" />
          </View>
        )}
      </Container>
    </>
  );
};

export default NewGym;
