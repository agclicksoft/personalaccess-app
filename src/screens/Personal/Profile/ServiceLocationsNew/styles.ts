import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Content = styled.View`
  align-items: center;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  font-size: ${({ theme }) => theme.fontSizes.size14};
  color: ${({ theme }) => theme.colors.purple};
  margin-top: 10px;
`;

export const SearchContainer = styled.View``;

export const SearchContainerTitle = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  font-size: ${({ theme }) => theme.fontSizes.size14};
  color: ${({ theme }) => theme.colors.purple};
  margin: 15px 20px;
`;

export const ModalAlert = styled.View`
  align-items: center;
`;

export const ModalAlertText = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.openSans};
  color: #2e2e2e;
  width: 200px;
  text-align: center;
`;

export const Holder = styled.View`
  flex-direction: row;
  margin: 30px 20px;
  background-color: white;
  border-radius: 10px;
  height: 110px;
  align-items: center;
  padding: 0 20px;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 45px;
  height: 45px;
  margin-right: 20px;
`;

export const TextWrapper = styled.View`
  flex: 1;
  flex-shrink: 1;
`;

export const GymName = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.purple};
`;

export const GymAddress = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.grayDark};
`;

export const Label = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
`;
