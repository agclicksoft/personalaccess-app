/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { FlatList } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import { useAuth } from '~/hooks/auth';

import {
  Container,
  HeaderContainer,
  Title,
  Divider,
  Wrapper,
  ProfileImage,
  NameWrapper,
  Name,
  TouchableOpacity,
  EditProfile,
  MenuButton,
  MenuContainer,
  Holder,
  MenuIcon,
  TextWrapper,
  MenuTitle,
  MenuLabel,
} from './styles';

import { options } from './constants';
import ModalPassword from './EditProfile/ModalPassword';

const Profile: React.FC = () => {
  const { user, signOut } = useAuth();
  const { navigate } = useNavigation();
  const [modalPassword, setModalPassword] = useState(false);

  useEffect(() => {
    if (user.is_redefinition) {
      setModalPassword(!modalPassword);
    }
  }, []);

  return (
    <>
      <Container>
        <HeaderContainer>
          <Title>Meu perfil</Title>
        </HeaderContainer>

        <Divider />

        <Wrapper>
          <ProfileImage
            source={{
              uri:
                user.img_profile ??
                'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png',
            }}
          />

          <NameWrapper>
            <Name>{user.name}</Name>

            <TouchableOpacity onPress={() => navigate('EditProfile')}>
              <EditProfile>Editar perfil</EditProfile>
            </TouchableOpacity>
          </NameWrapper>
        </Wrapper>

        <Divider />

        <FlatList
          showsVerticalScrollIndicator={false}
          data={options}
          keyExtractor={(option) => String(option.title)}
          ItemSeparatorComponent={Divider}
          renderItem={({ item }) => (
            <MenuButton
              key={item.title}
              onPress={() => {
                if (item.title !== 'Sair') {
                  navigate(item.screen);
                } else {
                  signOut();
                }
              }}
            >
              <MenuContainer>
                <Holder>
                  <MenuIcon source={item.icon} />

                  <TextWrapper>
                    <MenuTitle>{item.title}</MenuTitle>
                    <MenuLabel>{item.label}</MenuLabel>
                  </TextWrapper>
                </Holder>

                <Ionicons
                  name="ios-arrow-forward"
                  size={21}
                  color="rgba(112,112,112,0.5)"
                />
              </MenuContainer>
            </MenuButton>
          )}
        />
      </Container>

      <ModalPassword
        isRedefinition
        isVisible={modalPassword}
        closeModal={() => setModalPassword(!modalPassword)}
      />
    </>
  );
};

export default Profile;
