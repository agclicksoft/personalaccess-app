import React, { useCallback, useMemo, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { showMessage } from 'react-native-flash-message';

import { ContainerScroll, HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import { Specialty, User } from '~/models';
import api from '~/services/api';

import {
  Container,
  Content,
  CheckboxContainer,
  Checkbox,
  Label,
} from './styles';

import { specialtiesOptionsList } from './constants';

const Specialties: React.FC = () => {
  const { user, updatePersonal } = useAuth();
  const { goBack } = useNavigation();
  const [loading, setLoading] = useState(false);

  const [specialtiesSelected, setSpecialtiesSelected] = useState(
    user?.professional?.specialties.map((item) => item.description),
  );

  const handleSelectSpecialties = useCallback(
    async (item) => {
      let newArr = [];

      if (!specialtiesSelected.includes(item)) {
        newArr = [...specialtiesSelected, item];
      } else {
        newArr = specialtiesSelected.filter((a) => a !== item);
      }

      setSpecialtiesSelected(newArr);

      console.log(specialtiesSelected);
    },
    [specialtiesSelected],
  );

  const specialtiesOptions = useMemo(
    () =>
      specialtiesOptionsList.map((item: Specialty) => (
        <CheckboxContainer
          key={item.id}
          onPress={() => handleSelectSpecialties(item.description)}
        >
          <Checkbox
            isSelected={specialtiesSelected.includes(item.description)}
          />
          <Label>{item.description}</Label>
        </CheckboxContainer>
      )),
    [handleSelectSpecialties, specialtiesSelected],
  );

  const handleToggleSpecialtiesModal = useCallback(async () => {
    setLoading(true);

    try {
      const response = await api.post(
        `professionals/${user.professional.id}/specialties`,
        {
          specialties: specialtiesSelected.map((e) => {
            return { description: e };
          }),
        },
      );

      const formData: User = {
        ...user,
        id: user.id,
        professional: {
          ...user.professional,
          specialties: response.data.data,
        },
      };

      await updatePersonal(formData);

      setLoading(false);

      showMessage({
        type: 'success',
        message: 'Especialidades atualizadas com sucesso!',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });

      goBack();
    } catch (err) {
      setLoading(false);

      console.log(err?.response?.data);

      showMessage({
        type: 'danger',
        message: 'Erro inesperado',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [goBack, specialtiesSelected, updatePersonal, user]);

  return (
    <>
      <Container>
        <HeaderBack
          rightButton
          rightButtonTitle="Salvar"
          onPress={handleToggleSpecialtiesModal}
        >
          Especialidades
        </HeaderBack>

        <ContainerScroll>
          <Content>{specialtiesOptions}</Content>
        </ContainerScroll>
      </Container>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </>
  );
};

export default Specialties;
