export const specialtiesOptionsList = [
  {
    id: 1,
    description: 'Hipertrofia',
  },
  {
    id: 2,
    description: 'Emagrecimento',
  },
  {
    id: 3,
    description: 'Condicionamento físico',
  },
  {
    id: 4,
    description: 'Preparação Física',
  },
  {
    id: 5,
    description: 'Yoga',
  },
  {
    id: 6,
    description: 'Pilates',
  },
  {
    id: 7,
    description: 'Gestante',
  },
  {
    id: 8,
    description: 'Kids',
  },
  {
    id: 9,
    description: 'Atividades para Idosos',
  },
  {
    id: 10,
    description: 'Dança',
  },
];
