import styled, { css } from 'styled-components/native';

interface CheckboxProps {
  isSelected: boolean;
}
export const Container = styled.View`
  background-color: #f8f8f8;
  flex: 1;
`;

export const Content = styled.View``;

export const CheckboxContainer = styled.TouchableOpacity`
  flex-direction: row;
  padding: 16px 24px;

  border-bottom-width: 0.5px;
  border-bottom-color: #afc1c4;
`;

export const Checkbox = styled.View<CheckboxProps>`
  width: 24px;
  height: 24px;
  align-items: center;
  background: transparent;
  border: 1px solid #afc1c4;
  border-radius: 4px;
  margin-right: 12px;

  border-radius: 12px;

  ${({ isSelected }) =>
    isSelected &&
    css`
      background: ${({ theme }) => theme.colors.purple};
      border: 0;
    `}
`;

export const Label = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: #4d4d4d;
`;
