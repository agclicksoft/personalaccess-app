import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const FilterContainer = styled.View`
  align-items: center;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: stretch;
  background-color: #37153d;
  height: 120px;
  padding: 0 25px;
`;

export const LabelPicker = styled.Text`
  color: #fff;
`;

export const PaymentHistoryItem = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100px;
  padding: 20px 0;
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
`;

export const PaymentHistoryItemIconView = styled.View`
  width: 70px;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const PaymentHistoryItemIcon = styled.Image``;

export const PaymentHistoryItemView = styled.View`
  flex: 1;
  height: 70px;
`;

export const PaymentHistoryItemValue = styled.Text`
  color: #37153d;
  font-size: 22px;
  font-weight: 600;
`;

export const PaymentHistoryItemDescription = styled.Text`
  color: #2e2e2e;
  font-size: 12px;
  margin: 3px 0;
`;

export const PaymentHistoryItemDateView = styled.View`
  flex: 0.8;
  align-items: center;
  justify-content: space-between;
  height: 100%;
`;

export const PaymentHistoryItemStatus = styled.View`
  border-radius: 10px;
  padding: 5px 8px;
  width: 115px;
`;

export const PaymentHistoryItemText = styled.Text`
  color: #fff;
  font-size: 13px;
  text-align: center;
`;

export const PaymentHistoryItemDate = styled.Text`
  color: #707070;
  font-size: 13px;
`;

export const LottieContainer = styled.View`
  flex-direction: column;
  align-items: center;
  margin-top: 60px;
`;

export const LottieText = styled.Text`
  font-size: 15px;
  color: #707070;
`;

export const Divider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
`;
