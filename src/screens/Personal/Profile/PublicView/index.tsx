/* eslint-disable react/style-prop-object */
import React from 'react';

import { StatusBar } from 'expo-status-bar';

import { IconAvaliation } from '~/assets/icons';
import { ContainerScroll, HeaderPurple } from '~/components';
import { useAuth } from '~/hooks/auth';
import { PlaceholderImage } from '~/utils/consts';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  ConfefContainer,
  ConfefLabel,
  ConfefValue,
  AvaliationHolder,
  AvaliationIcon,
  Avaliation,
  Divider,
  Wrapper,
  Title,
  Text,
} from './styles';

const PersonalView: React.FC = () => {
  const { user } = useAuth();

  return (
    <>
      <StatusBar backgroundColor="#37153d" style="light" translucent={false} />
      <Container>
        <HeaderPurple>Personal</HeaderPurple>

        <ContainerScroll>
          <FormContainer>
            <ProfileImage
              source={{ uri: user.img_profile ?? PlaceholderImage }}
            />

            <Name>{user.name}</Name>

            {user.professional.doc_professional && (
              <ConfefContainer>
                <ConfefLabel>CREF/CONFEF: </ConfefLabel>
                <ConfefValue>{user.professional.doc_professional}</ConfefValue>
              </ConfefContainer>
            )}

            <AvaliationHolder>
              <AvaliationIcon source={IconAvaliation} />
              <Avaliation>
                {user.professional.rating[0]?.assessments
                  ? Number(user.professional?.rating[0]?.assessments).toFixed(1)
                  : 0}
              </Avaliation>
            </AvaliationHolder>

            <Divider />

            <Wrapper>
              <Title>
                {user.professional?.specialties?.length > 1
                  ? 'Especialidades'
                  : 'Especialidade'}
                :
              </Title>

              {user.professional?.specialties?.length > 1 ? (
                <Text>
                  {user.professional.specialties
                    .map((item) => item?.description)
                    .join(', ')}
                  .
                </Text>
              ) : (
                <Text>Não informado.</Text>
              )}

              {user.professional.gyms && (
                <>
                  <Title>
                    {user.professional.gyms.length > 1
                      ? 'Locais de atendimento'
                      : 'Local de atendimento'}
                    :
                  </Title>

                  {user.professional.gyms.length >= 1 ? (
                    <Text>
                      {user.professional.gyms.map((gym) => gym.name).join(', ')}
                      .
                    </Text>
                  ) : (
                    <Text>Nenhum local informado.</Text>
                  )}
                </>
              )}

              <Title>Biografia:</Title>
              <Text>{user.professional.bibliography ?? 'Não informado'}</Text>

              <Title>Instagram:</Title>
              <Text>{user.professional.instagram ?? 'Não informado'}</Text>
            </Wrapper>
          </FormContainer>
        </ContainerScroll>
      </Container>
    </>
  );
};

export default PersonalView;
