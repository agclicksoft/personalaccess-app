import React from 'react';

import { IconAvaliation } from '~/assets/icons';
import { Rating } from '~/models';
import { PlaceholderImage } from '~/utils/consts';

import {
  Card,
  CardContent,
  Image,
  Wrapper,
  Name,
  Description,
  Holder,
  AvaliationIcon,
  AvaliationNumber,
} from './styles';

interface Props {
  data: Rating;
}

const AvaliationCard: React.FC<Props> = ({ data }) => {
  const myRating: Rating = data;

  return (
    <Card onPress={() => null}>
      <CardContent>
        <Image
          source={{
            uri: myRating.anonimo
              ? PlaceholderImage
              : myRating.student.user.img_profile ?? PlaceholderImage,
          }}
        />

        <Wrapper>
          <Name>
            {myRating.anonimo ? 'Anônimo' : myRating.student.user.name}
          </Name>

          <Description>
            {myRating.comments ?? 'Nenhum comentário encontrado.'}
          </Description>

          <Holder>
            <AvaliationIcon source={IconAvaliation} />

            <AvaliationNumber>
              {myRating.assessments_value.toFixed(1)}
            </AvaliationNumber>
          </Holder>
        </Wrapper>
      </CardContent>
    </Card>
  );
};

export default AvaliationCard;
