import { Dimensions } from 'react-native';

import styled from 'styled-components/native';

const { width } = Dimensions.get('window');

export const Card = styled.TouchableOpacity.attrs({
  activeOpacity: 0.5,
})`
  background-color: ${({ theme }) => theme.colors.white};
  border-radius: 8px;
  width: 100%;
  padding: 25px 10px;
  margin: 10px 0;
  border-width: 0.8px;
  border-color: ${({ theme }) => theme.colors.shadow};
`;

export const CardContent = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  width: 100%;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'cover',
})`
  height: ${width * 0.3}px;
  width: ${width * 0.3}px;
  border-radius: 60px;
  margin: 0 10px;
`;

export const Wrapper = styled.View`
  flex: 1;
  flex-shrink: 1;
  width: 100%;
  justify-content: space-between;
`;

export const Name = styled.Text`
  color: ${({ theme }) => theme.colors.purple};
  font-size: ${({ theme }) => theme.fontSizes.size20};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  text-transform: capitalize;
`;

export const Description = styled.Text`
  color: ${({ theme }) => theme.colors.grayLight};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
`;

export const Holder = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 5px;
  background-color: #f2f2f2;
  padding: 5px;
  border-radius: 6px;
  width: 60px;
`;

export const AvaliationIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  height: 20px;
  width: 20px;
`;

export const AvaliationNumber = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size11};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  margin-left: 6px;
`;
