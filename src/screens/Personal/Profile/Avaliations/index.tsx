import React, { useMemo } from 'react';

import { ContainerScroll, HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import { Rating } from '~/models';

import { Container, Content } from './styles';

import AvaliationCard from './Card';

const Avaliations: React.FC = () => {
  const { user } = useAuth();

  const personalsListMemo = useMemo(
    () =>
      user.professional.assessments.map((item: Rating) => {
        return <AvaliationCard key={item.id} data={item} />;
      }),
    [user.professional.assessments],
  );

  return (
    <Container>
      <HeaderBack>Minhas avaliações</HeaderBack>

      <ContainerScroll>
        <Content>{personalsListMemo}</Content>
      </ContainerScroll>
    </Container>
  );
};

export default Avaliations;
