import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import {
  Button,
  ContainerScroll,
  HeaderBack,
  Input,
  Picker,
} from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import api from '~/services/api';
import { bankList } from '~/utils/consts';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Container,
  Content,
  Title,
  WeightHeightWrapper,
  WeightHolder,
  HeightHolder,
  Label,
  ButtonWrapper,
} from './styles';

const selectPackageList = [
  {
    key: 1,
    value: 'Corrente',
    label: 'Corrente',
  },
  {
    key: 2,
    value: 'Poupanca',
    label: 'Poupança',
  },
];

const BankAccount: React.FC = () => {
  const { user, updatePersonal } = useAuth();
  const { goBack } = useNavigation();
  const formRef = useRef<FormHandles>(null);
  const [loading, setLoading] = useState(false);
  const [accountType, setAccountType] = useState(
    user?.professional?.bank_type ?? null,
  );
  const [bank, setBank] = useState(user?.professional?.bank_name ?? null);

  const handleSubmit = useCallback(
    async (data) => {
      try {
        setLoading(true);

        const schema = Yup.object().shape({
          bank_agency: Yup.string().required('Agência é obrigatória'),
          bank_number: Yup.string().required('Conta é obrigatória'),
        });

        await schema.validate(data, { abortEarly: false });

        if (!bank || !accountType) {
          showMessage({
            type: 'danger',
            message: 'Ops!',
            description: 'Favor preencher todos os dados',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        const response = await api.post(
          `/professionals/${user.professional.id}/account`,
          {
            bank_number: data.bank_number,
            bank_agency: data.bank_agency,
            bank_type: accountType,
            bank_name: bank,
          },
        );

        console.log(response.data);

        const formData: User = {
          ...data,
          id: user.id,
          professional: {
            ...user.professional,
            bank_number: data.bank_number,
            bank_agency: data.bank_agency,
            bank_type: accountType,
            bank_name: bank,
          },
        };

        console.log(formData);

        await updatePersonal(formData);

        showMessage({
          type: 'success',
          message: 'Dados bancários atualizados com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);
        goBack();
      } catch (err) {
        setLoading(false);
        console.log(err?.response);

        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);

          console.log(errors);

          showMessage({
            type: 'danger',
            message: 'Erro ao atualizar seus dados.',
            description: 'Verifique e tente novamente',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        showMessage({
          type: 'danger',
          message: 'Erro inesperado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [accountType, bank, goBack, updatePersonal, user],
  );

  return (
    <Container>
      <HeaderBack>Conta Bancária</HeaderBack>

      <ContainerScroll>
        <Content>
          <Title>Tipo de conta:</Title>

          <Picker
            items={selectPackageList}
            value={accountType}
            onValueChange={(value) => setAccountType(value)}
            label="Selecionar conta"
          />

          <Title>Banco:</Title>
          <Picker
            items={bankList}
            value={bank}
            onValueChange={(value) => setBank(value)}
            label="Selecionar banco"
          />

          <Form
            ref={formRef}
            onSubmit={handleSubmit}
            initialData={user?.professional}
          >
            <WeightHeightWrapper>
              <WeightHolder>
                <Label>Agência:</Label>
                <Input
                  name="bank_agency"
                  width="100%"
                  keyboardType="decimal-pad"
                />
              </WeightHolder>

              <HeightHolder>
                <Label>Conta:</Label>
                <Input
                  name="bank_number"
                  width="100%"
                  keyboardType="decimal-pad"
                />
              </HeightHolder>
            </WeightHeightWrapper>
          </Form>

          <ButtonWrapper>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </ButtonWrapper>
        </Content>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default BankAccount;
