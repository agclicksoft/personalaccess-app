import React, { useRef, useState, useCallback, useEffect } from 'react';
import { ActivityIndicator, Platform, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { format, parseISO } from 'date-fns';
import * as ImagePicker from 'expo-image-picker';
import FormData from 'form-data';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { IconCamera } from '~/assets/icons';
import {
  Button,
  ContainerScroll,
  HeaderBack,
  Input,
  MaskedInput,
} from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Container,
  ImageWrapper,
  Image,
  CameraIcon,
  ImageButton,
  FormWrapper,
  Label,
  ButtonWrapper,
} from './styles';

import ModalPassword from './ModalPassword';

interface Picture {
  id?: number;
  filename?: string;
  mediaType?: string;
  uri?: string;
  type?: string;
}

const EditProfile: React.FC = () => {
  const { user, updatePersonal } = useAuth();
  const { goBack } = useNavigation();
  const formRef = useRef<FormHandles>(null);
  const [loading, setLoading] = useState(false);
  const [modalPassword, setModalPassword] = useState(false);

  useEffect(() => {
    formRef.current.setData({
      ...user,
      password: '*************',
      doc_professional: user.professional.doc_professional ?? null,
      instagram: user.professional.instagram ?? null,
      bibliography: user.professional.bibliography ?? null,
    });
  }, [user]);

  const handleSubmit = useCallback(
    async (data) => {
      try {
        setLoading(true);

        const schema = Yup.object().shape({
          name: Yup.string().required('Nome é obrigatório'),
          cpf: Yup.string().required('CPF é obrigatório'),
          birthday: Yup.string().required('Data de nascimento é Obrigatório '),
          email: Yup.string().required('Email é Obrigatório'),
        });

        await schema.validate(data, { abortEarly: false });

        const formData = {
          ...data,
          id: user.id,
          professional: {
            ...user.professional,
            doc_professional: data.doc_professional,
            instagram: data.instagram,
            bibliography: data.bibliography,
          },
        };

        console.log(formData);

        await updatePersonal(formData);

        showMessage({
          type: 'success',
          message: 'Perfil atualizado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });

        goBack();
      } catch (err) {
        setLoading(false);
        console.log(err?.response);

        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);

          console.log(errors);

          showMessage({
            type: 'danger',
            message: 'Erro ao atualizar seu perfil',
            description: 'Verifique e tente novamente',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        showMessage({
          type: 'danger',
          message: 'Erro inesperado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [user, updatePersonal, goBack],
  );

  const handlePickImage = useCallback(async () => {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 0.7,
      });

      if (!result.cancelled) {
        const personalData = new FormData();
        const photo = result as Picture;

        personalData.append('profile', {
          type: photo.mediaType ?? 'image/jpeg',
          name: photo.filename ?? `${user.name}-post`,
          uri:
            Platform.OS === 'android'
              ? photo.uri
              : photo.uri.replace('file://', ''),
        });

        const response = await api.post('/users/profile', personalData);

        const formData: User = {
          ...response.data.data,
          id: user.id,
          professional: user.professional,
        };

        console.log(formData);

        await updatePersonal(formData);

        showMessage({
          type: 'success',
          message: 'Foto atualizada com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    } catch (err) {
      console.log(err);

      showMessage({
        type: 'danger',
        message: 'Erro inesperado',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [user, updatePersonal]);

  return (
    <Container>
      <HeaderBack>Editar perfil</HeaderBack>

      <ContainerScroll>
        <FormWrapper>
          <ImageWrapper>
            <ImageButton onPress={handlePickImage}>
              <Image source={{ uri: user.img_profile ?? PlaceholderImage }} />

              <CameraIcon source={IconCamera} />
            </ImageButton>
          </ImageWrapper>

          <Form ref={formRef} onSubmit={handleSubmit} initialData={user}>
            <Label>CREF/CONFEF:</Label>
            <MaskedInput
              name="doc_professional"
              type="custom"
              options={{
                mask: '999.999-A/AA',
              }}
              placeholder="CREF/CONFEF"
              autoCapitalize="characters"
              initialValue={user.professional.doc_professional ?? null}
            />

            <Label>Nome:</Label>
            <Input
              name="name"
              placeholder="Nome"
              autoCapitalize="words"
              autoCorrect={false}
            />

            <Label>E-mail:</Label>
            <Input
              name="email"
              placeholder="E-mail"
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
            />

            <Label>Senha:</Label>
            <Input
              name="password"
              placeholder="Senha"
              secureTextEntry
              editable={false}
              value="*************"
              changePassword
              showModal={() => setModalPassword(!modalPassword)}
            />

            <Label>CPF:</Label>
            <MaskedInput
              name="cpf"
              type="cpf"
              placeholder="CPF"
              keyboardType="number-pad"
              initialValue={user.cpf}
            />

            <Label>Data de nascimento:</Label>
            <MaskedInput
              name="birthday"
              type="datetime"
              options={{
                format: 'DD/MM/YYYY',
              }}
              placeholder="Data de nascimento"
              maxLength={10}
              keyboardType="number-pad"
              initialValue={
                user.birthday
                  ? format(parseISO(user.birthday), "dd'/'MM'/'yyyy")
                  : ''
              }
            />

            <Label>Telefone/Celular:</Label>
            <MaskedInput
              name="contact"
              type="cel-phone"
              placeholder="Telefone/Celular"
              keyboardType="number-pad"
              initialValue={user.contact}
            />

            <Label>Telefone de emergências:</Label>
            <MaskedInput
              name="emergency_phone"
              type="cel-phone"
              placeholder="Telefone de emergências"
              keyboardType="number-pad"
              initialValue={user.emergency_phone}
            />

            <Label>Instagram:</Label>
            <Input
              name="instagram"
              placeholder="Instagram"
              autoCorrect={false}
            />

            <Label>Biografia:</Label>
            <Input
              name="bibliography"
              placeholder="Biografia"
              autoCapitalize="words"
              style={{
                textAlign: 'left',
                textAlignVertical: 'top',
                alignSelf: 'flex-start',
                marginTop: 10,
                borderRadius: 30,
                width: '100%',
                height: '100%',
              }}
              height={250}
              multiline
            />
          </Form>

          <ButtonWrapper>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </ButtonWrapper>
        </FormWrapper>
      </ContainerScroll>

      <ModalPassword
        isVisible={modalPassword}
        closeModal={() => setModalPassword(!modalPassword)}
      />

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default EditProfile;
