import styled from 'styled-components/native';

export const Content = styled.View`
  background-color: #f8f8f8;
  width: 100%;
  height: 100%;
  padding: 30px 0;
  align-items: center;
`;

export const ModalTitle = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaBold};
  font-size: 20px;
  line-height: 30px;
  color: #1e1e1e;
  text-align: center;
  margin-bottom: 24px;
`;

export const ButtonSave = styled.TouchableOpacity`
  width: 100%;
  height: 46px;
  /* height: 100%; */
  justify-content: center;
  align-items: center;
  border: 1px solid red;
`;

export const ButtonSaveLG = styled.View`
  border-radius: 5px;
  margin-top: 15px;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;

export const ButtonSaveText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaSemiBold};
`;
