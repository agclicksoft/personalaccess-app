import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Wrapper = styled.View`
  padding: 0 20px 150px;
`;

export const PageTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  color: ${({ theme }) => theme.colors.grayDark};
  margin: 25px 20px;
  text-align: center;
`;

export const PageSubtitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.orange};
  margin: 15px 0 10px;
  text-align: center;
`;

export const ButtonOptionsContainer = styled.View`
  flex: 1;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  margin: 10px 0;
`;

export const ButtonWrapper = styled.View`
  width: 100%;
  padding: 10px 0 30px;
  align-items: center;
  position: absolute;
  align-self: center;
  bottom: 0;
  background-color: #fff;
`;
