/* eslint-disable no-return-assign */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useState } from 'react';

import { useNavigation, useRoute } from '@react-navigation/native';
import { Calendar } from 'react-native-calendars';
import { showMessage } from 'react-native-flash-message';

import { ContainerScroll, HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import api from '~/services/api';

import {
  Container,
  Wrapper,
  PageTitle,
  PageSubtitle,
  ButtonOptionsContainer,
} from './styles';

import { HourItem } from './HourPicker';

interface Params {
  availableDates: any;
}

const BlockAgenda: React.FC = () => {
  const { user } = useAuth();
  const { params } = useRoute();
  const { availableDates } = params as Params;
  const { goBack } = useNavigation();
  const [selected, setSelected] = useState('');
  const [hours, setHours] = useState<any[]>([]);
  const [pickers, setPickers] = useState<number>(1);
  const [blocked, setBlock] = useState(false);

  const getScheduleHours = useCallback(
    async (date) => {
      const response = await api.get(
        `/professionals/${user.professional.id}/schedules-block/${date}`,
      );

      console.log(response.data);
      setHours(
        Object.keys(response.data).length > 0
          ? response.data
          : [
              {
                id: Math.floor(Math.random() * 100) + 1,
                date,
                period_of: '06:00:00',
                period_until: '23:00:00',
              },
            ],
      );
      setPickers(
        Object.keys(response.data).length > 0
          ? Object.keys(response.data).length
          : 1,
      );
      setBlock(Object.keys(response.data).length > 0);
    },
    [user.professional.id],
  );

  const handleSubmit = useCallback(async () => {
    try {
      if (!selected) {
        showMessage({
          type: 'warning',
          message: 'Favor selecionar o dia',
          titleStyle: {
            textAlign: 'center',
          },
        });

        return;
      }

      const response = await api.put(
        `/professionals/${user.professional.id}/schedules-block/${selected}`,
        {
          schedules: hours,
        },
      );

      console.log(response.data);

      if (hours.length > 0) {
        showMessage({
          type: 'success',
          message: 'Período bloqueado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
        });
      } else {
        showMessage({
          type: 'success',
          message: 'Período desbloqueado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
        });
      }

      goBack();
    } catch (err) {
      console.log(err);

      showMessage({
        type: 'danger',
        message: 'Algo de errado ocorreu',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [hours, selected, user.professional.id, goBack]);

  const changePeriodOf = (id, text) => {
    const updatedList = hours.map((item) => {
      if (item.id === id) {
        return { ...item, period_of: text };
      }
      return item;
    });

    setHours(updatedList);
  };

  const changePeriodUntil = (id, text) => {
    const updatedList = hours.map((item) => {
      if (item.id === id) {
        return { ...item, period_until: text };
      }
      return item;
    });

    setHours(updatedList);
  };

  return (
    <Container>
      <HeaderBack
        rightButton
        rightButtonTitle="Salvar"
        onPress={() => {
          handleSubmit();
        }}
      >
        Bloqueio de agenda
      </HeaderBack>

      <ContainerScroll>
        <Wrapper>
          <PageTitle>
            Selecione os dias/horários em que estará indisponível para
            agendamento.
          </PageTitle>

          <Calendar
            theme={{
              arrowColor: '#37153D',
              textDayFontFamily: 'SofiaPro-Medium',
              selectedDayTextColor: '#fff',
              selectedDayBackgroundColor: '#37153D',
              'stylesheet.day.period': {
                base: {
                  overflow: 'hidden',
                  height: 34,
                  alignItems: 'center',
                  width: 38,
                },
              },
              'stylesheet.day.basic': {
                base: {
                  width: 25,
                  height: 25,
                  alignItems: 'center',
                },
                text: {
                  marginTop: 0,
                  fontSize: 16,
                  fontFamily: 'SofiaPro-Medium',
                  fontWeight: '300',
                  color: '#43515c',
                  backgroundColor: 'rgba(255, 255, 255, 0)',
                },
              },
              'stylesheet.calendar.header': {
                monthText: {
                  margin: 0,
                  padding: 10,
                },
                arrow: {
                  padding: 2,
                },
              },
            }}
            disabledByDefault
            disableAllTouchEventsForDisabledDays
            hideExtraDays
            markedDates={{
              ...availableDates,
              [selected]: {
                selected: true,
              },
            }}
            onDayPress={async (date) => {
              setSelected(date.dateString);
              console.log(date.dateString);
              await getScheduleHours(date.dateString);
            }}
          />

          {blocked && selected !== '' && (
            <ButtonOptionsContainer>
              <PageSubtitle>
                {pickers !== 0 &&
                  (pickers > 1
                    ? 'Períodos já bloqueados neste dia: '
                    : 'Período já bloqueado neste dia: ')}
              </PageSubtitle>

              {pickers === 0 && (
                <PageSubtitle>
                  Salve para desbloquear os horários no dia selecionado.
                </PageSubtitle>
              )}

              {Array.from(Array(pickers)).map((x, index) => (
                <HourItem
                  key={Number(index)}
                  show
                  day={
                    hours[index] ?? {
                      id: Math.floor(Math.random() * 100) + 1,
                      period_of: '06:00:00',
                      period_until: '23:00:00',
                      date: selected,
                    }
                  }
                  onChangePeriodOf={async (text) =>
                    changePeriodOf(hours[index].id, text)
                  }
                  onChangePeriodUntil={async (text) =>
                    changePeriodUntil(hours[index].id, text)
                  }
                  onPressAdd={async () => {
                    setPickers(pickers + 1);
                    setHours([
                      ...hours,
                      {
                        id: Math.floor(Math.random() * 100) + 1,
                        period_of: '06:00:00',
                        period_until: '23:00:00',
                        date: selected,
                      },
                    ]);
                  }}
                  onPressRemove={() => {
                    setPickers(pickers - 1);
                    setHours(
                      hours.filter((item) => item.id !== hours[index].id),
                    );
                  }}
                />
              ))}
            </ButtonOptionsContainer>
          )}

          {!blocked && selected !== '' && (
            <ButtonOptionsContainer>
              {pickers === 0 && (
                <PageSubtitle>
                  Salve para desbloquear os horários no dia selecionado.
                </PageSubtitle>
              )}

              {pickers !== 0 && (
                <PageSubtitle>
                  Nenhum período bloqueado até o momento neste dia.
                </PageSubtitle>
              )}

              {Array.from(Array(pickers)).map((x, index) => (
                <HourItem
                  key={Number(index)}
                  show
                  day={
                    hours[index] ?? {
                      id: Math.floor(Math.random() * 100) + 1,
                      period_of: '06:00:00',
                      period_until: '23:00:00',
                      date: selected,
                    }
                  }
                  onChangePeriodOf={async (text) =>
                    changePeriodOf(hours[index].id, text)
                  }
                  onChangePeriodUntil={async (text) =>
                    changePeriodUntil(hours[index].id, text)
                  }
                  onPressAdd={async () => {
                    setPickers(pickers + 1);
                    setHours([
                      ...hours,
                      {
                        id: Math.floor(Math.random() * 100) + 1,
                        period_of: '06:00:00',
                        period_until: '23:00:00',
                        date: selected,
                      },
                    ]);
                  }}
                  onPressRemove={() => {
                    setPickers(pickers - 1);
                    setHours(
                      hours.filter((item) => item.id !== hours[index].id),
                    );
                  }}
                />
              ))}
            </ButtonOptionsContainer>
          )}
        </Wrapper>
      </ContainerScroll>
    </Container>
  );
};

export default BlockAgenda;
