import React, { useCallback, useEffect, useState } from 'react';
import { FlatList } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { addDays, addHours, addMonths, format, startOfToday } from 'date-fns';
import _ from 'lodash';
import moment from 'moment';

import { HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import api from '~/services/api';

import {
  Container,
  Divider,
  MenuButton,
  MenuContainer,
  Holder,
  TextWrapper,
  MenuTitle,
} from './styles';

export const options = [
  {
    id: 1,
    title: 'Horário padrão',
    screen: 'StandartSchedule',
  },
  {
    id: 2,
    title: 'Bloqueio de agenda',
    screen: 'BlockAgenda',
  },
  {
    id: 3,
    title: 'Minha agenda completa',
    screen: 'MyAgenda',
  },
];

const Schedule: React.FC = () => {
  const { navigate } = useNavigation();
  const { user } = useAuth();
  const [agenda, setAgenda] = useState();
  const [availableDates, setAvailableDates] = useState({});

  const getDaysAvailable = (weekday: string) => {
    switch (weekday) {
      case 'DOMINGO':
        return 0;
      case 'SEGUNDA':
        return 1;
      case 'TERCA':
        return 2;
      case 'QUARTA':
        return 3;
      case 'QUINTA':
        return 4;
      case 'SEXTA':
        return 5;
      case 'SABADO':
        return 6;
      default:
        return 'Dia inválido';
    }
  };

  const getAvailableDates = useCallback(() => {
    const enabledDates = {};
    const daysToEnable = user.professional.schedules.map((item) =>
      getDaysAvailable(item.week_day),
    );

    const start = format(addHours(new Date(), 1), "yyyy'-'MM'-'dd");
    const end = format(addMonths(startOfToday(), 6), "yyyy'-'MM'-'dd");
    for (let m = moment(start); m.diff(end, 'days') <= 0; m.add(1, 'days')) {
      if (_.includes(daysToEnable, m.weekday())) {
        enabledDates[m.format('YYYY-MM-DD')] = { disabled: false };
      }
    }
    setAvailableDates(enabledDates);
  }, [user]);

  const getAgendaSchedules = useCallback(async () => {
    const response = await api.get(
      `/professionals/${user.professional.id}/scheduled-services`,
    );

    console.log(response.data);
    setAgenda(response.data);
    getAvailableDates();
  }, [user, getAvailableDates]);

  useEffect(() => {
    getAgendaSchedules();
  }, [getAgendaSchedules]);

  return (
    <Container>
      <HeaderBack>Agenda</HeaderBack>

      <FlatList
        showsVerticalScrollIndicator={false}
        data={options}
        keyExtractor={(option) => String(option.id)}
        ItemSeparatorComponent={Divider}
        renderItem={({ item }) => (
          <MenuButton
            key={item.title}
            onPress={() => {
              if (agenda && availableDates) {
                navigate(item.screen, {
                  user,
                  agenda,
                  availableDates,
                });
              } else {
                null;
              }
            }}
          >
            <MenuContainer>
              <Holder>
                <TextWrapper>
                  <MenuTitle>{item.title}</MenuTitle>
                </TextWrapper>
              </Holder>

              <Ionicons
                name="ios-arrow-forward"
                size={21}
                color="rgba(112,112,112,0.5)"
              />
            </MenuContainer>
          </MenuButton>
        )}
      />
    </Container>
  );
};

export default Schedule;
