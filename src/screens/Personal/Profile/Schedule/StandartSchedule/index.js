import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback, ActivityIndicator } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { showMessage } from 'react-native-flash-message';

import { ContainerScroll, HeaderBack } from '~/components';
import api from '~/services/api';

import { HourItem } from './HourPicker';
import { Container, Content, Checkbox, Title } from './styles';

class StandartSchedule extends Component {
  constructor(props) {
    super(props);

    this.state = {
      weekDay: [
        {
          key: 'SEGUNDA',
          name: 'Segunda-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'TERCA',
          name: 'Terça-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'QUARTA',
          name: 'Quarta-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'QUINTA',
          name: 'Quinta-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'SEXTA',
          name: 'Sexta-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'SABADO',
          name: 'Sábado',
          isSelected: false,
          hours: [],
        },
        {
          key: 'DOMINGO',
          name: 'Domingo',
          isSelected: false,
          hours: [],
        },
      ],
      user: this.props.route.params.user,
      loading: false
    };
  }

  async componentDidMount() {
    await this.loadSchedules();
  }

  loadSchedules = async () => {
    const { weekDay } = this.state;
    const { user } = this.state;

    const aux = weekDay;
    await api
      .get(`/schedules/${user.professional.id}/format`)
      .then((response) => {
        response.data.map((e) => {
          const index = weekDay.findIndex((f) => f.key === e.key);
          aux[index].hours = e.hours;
          aux[index].isSelected = true;
        });

        this.setState({
          weekDay: aux,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  addHours = (key) => {
    const { weekDay } = this.state;
    weekDay[weekDay.findIndex((e) => e.key === key)].hours.push({
      period_of: '',
      period_until: '',
    });
    this.setState({
      weekDay,
    });
  };

  removeHour = (index, key) => {
    console.log(index, key);
    const { weekDay } = this.state;
    weekDay[weekDay.findIndex((e) => e.key === key)].hours.splice(index, 1);
    this.setState({
      weekDay,
    });
  };

  prepareItem = (item) => {
    const { week_day, period_of, period_until } = item;
    return {
      week_day,
      period_of,
      period_until,
    };
  };

  saveSchedule = async () => {
    const { weekDay } = this.state;
    const { user } = this.state;

    this.setState({
      loading: true
    })

    try {
      const response = await api.post(`/schedules/${user.professional.id}/format`, {
        schedulesFormat: weekDay,
      });

      console.log(response.data);

      this.setState({
        loading: false
      })

      showMessage({
        type: 'success',
        message: 'Agenda atualizada com sucesso!',
        titleStyle: {
          textAlign: 'center',
        },
      });

      this.props.navigation.goBack();
    } catch (err) {
      console.log(err.response.data);
      this.setState({
        loading: false
      })

      showMessage({
        type: 'danger',
        message: 'Algo de errado ocorreu',
        description: 'Verifique e tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  };

  render() {
    return (
      <Container>
        <HeaderBack rightButton rightButtonTitle="Salvar" onPress={() => this.saveSchedule()}>
          Horário padrão
        </HeaderBack>

        <ContainerScroll>
          <Content>
            <Title>
              Selecione os dias, horários e períodos de atendimento.
            </Title>

            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                paddingHorizontal: 45,
                paddingVertical: 20,
              }}
            >
              {this.state.weekDay.map((e) => {
                return (
                  <View
                    style={{
                      flexDirection: 'column',
                      width: 150,
                      marginVertical: 15,
                    }}
                  >
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        width: 200,
                      }}
                    >
                      {e.isSelected ? (
                        <>
                          <TouchableWithoutFeedback
                            onPress={async () =>
                              this.setState(() => {
                                e.hours = [];
                                e.isSelected = !e.isSelected;
                                e.isSelected ? this.addHours(e.key) : null;
                                return { e };
                              })
                            }
                          >
                            <Checkbox isSelected>
                              {true && (
                                <Ionicons
                                  name="md-checkmark"
                                  size={24}
                                  color="white"
                                />
                              )}
                            </Checkbox>
                          </TouchableWithoutFeedback>

                          <Text
                            style={{
                              marginLeft: 15,
                              fontFamily: 'SofiaPro-Regular',
                              color: '#4D4D4D',
                              fontSize: 14,
                              width: 100,
                            }}
                          >
                            {e.name}
                          </Text>
                          <View
                            style={{
                              marginLeft: 20,
                              paddingHorizontal: 6,
                            }}
                          >
                            <TouchableWithoutFeedback
                              onPress={() => this.addHours(e.key)}
                            >
                              <Ionicons
                                name="ios-add"
                                size={33}
                                color="#F76E1E"
                              />
                            </TouchableWithoutFeedback>
                          </View>
                        </>
                      ) : (
                        <>
                          <TouchableWithoutFeedback
                            onPress={async () =>
                              this.setState(() => {
                                e.hours = [];
                                e.isSelected = !e.isSelected;
                                e.isSelected ? this.addHours(e.key) : null;
                                return { e };
                              })
                            }
                          >
                            <Checkbox isSelected={false}>
                              {true && (
                                <Ionicons
                                  name="md-checkmark"
                                  size={24}
                                  color="white"
                                />
                              )}
                            </Checkbox>
                          </TouchableWithoutFeedback>

                          <Text
                            style={{
                              marginLeft: 15,
                              fontFamily: 'SofiaPro-Regular',
                              color: '#4D4D4D',
                              fontSize: 14,
                            }}
                          >
                            {e.name}
                          </Text>
                        </>
                      )}
                    </View>

                    {e.hours?.map((f, index) => {
                      return (
                        <HourItem
                          show={e.isSelected}
                          day={f}
                          onChangePeriodOf={async (text) =>
                            this.setState(() => {
                              f.period_of = text;
                              return { f };
                            })
                          }
                          onChangePeriodUntil={async (text) =>
                            this.setState(() => {
                              f.period_until = text;
                              return { f };
                            })
                          }
                          onPressHour={() => this.removeHour(index, e.key)}
                        />
                      );
                    })}
                  </View>
                );
              })}
            </View>
          </Content>
        </ContainerScroll>

        {this.state.loading && (
          <View
            style={{
              flex: 1,
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              backgroundColor: '#00000029',
              position: 'absolute',
            }}
          >
            <ActivityIndicator size="large" color="#37153D" />
          </View>
        )}
      </Container>
    );
  }
}

export default StandartSchedule;
