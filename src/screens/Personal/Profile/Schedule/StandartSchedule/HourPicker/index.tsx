/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable radix */
import React from 'react';

import { Ionicons } from '@expo/vector-icons';
import RNPickerSelect from 'react-native-picker-select';

import { Container, PickerWrapper, Spacer, style, Touchable } from './styles';

export const HourItem = ({
  day,
  onPressHour,
  show,
  onChangePeriodOf,
  onChangePeriodUntil,
}) => {
  const period_of = [];
  for (let i = 6; i < 24; i++) {
    period_of.push({
      label: `${i < 10 ? `0${i}` : i}:00`,
      value: `${i < 10 ? `0${i}` : i}:00:00`,
    });
  }

  const period_until = [];
  if (day.period_of)
    for (let i = parseInt(day.period_of.slice(0, 2)) + 1; i < 24; i++) {
      period_until.push({
        label: `${i < 10 ? `0${i}` : i}:00`,
        value: `${i < 10 ? `0${i}` : i}:00:00`,
      });
    }

  return (
    <>
      {show ? (
        <Container>
          <PickerWrapper>
            <RNPickerSelect
              style={style}
              placeholder={{
                label: 'De',
              }}
              doneText="Selecionar"
              useNativeAndroidPickerStyle={false}
              onValueChange={(itemValue) => {
                onChangePeriodOf(itemValue);
                console.log(itemValue);
              }}
              value={day.period_of}
              items={period_of}
              pickerProps={{ mode: 'dropdown' }}
            />
          </PickerWrapper>

          <Spacer>──</Spacer>

          <PickerWrapper>
            <RNPickerSelect
              style={style}
              placeholder={{
                label: 'Até',
              }}
              doneText="Selecionar"
              useNativeAndroidPickerStyle={false}
              onValueChange={(itemValue) => {
                onChangePeriodUntil(itemValue);
                console.log(itemValue);
              }}
              value={day.period_until}
              items={period_until}
              pickerProps={{ mode: 'dropdown' }}
            />
          </PickerWrapper>

          <Touchable onPress={onPressHour}>
            <Ionicons
              style={{
                marginLeft: 20,
              }}
              name="ios-remove"
              size={33}
              color="#F76E1E"
            />
          </Touchable>
        </Container>
      ) : null}
    </>
  );
};
