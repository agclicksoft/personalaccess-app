import { StyleSheet } from 'react-native';

import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const PickerWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: #fff;
  border-radius: 20px;
  margin: 10px 0;
  padding: 5px;
`;

export const Spacer = styled.Text`
  margin: 20px 10px;
`;

export const Touchable = styled.TouchableWithoutFeedback``;

export const style = StyleSheet.create({
  inputAndroid: {
    width: 70,
    height: 40,
    color: '#000',
    textAlign: 'center',
    fontFamily: 'SofiaPro-Regular',
    fontSize: 20,
    margin: 10,
  },
  inputIOS: {
    width: 70,
    height: 35,
    color: '#000',
    textAlign: 'center',
    fontFamily: 'SofiaPro-Regular',
    fontSize: 20,
    margin: 10,
  },
});
