import styled, { css } from 'styled-components/native';

interface CheckboxProps {
  isSelected: boolean;
}

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Content = styled.View`
  width: 100%;
  padding: 30px 0 60px;
`;

export const Title = styled.Text`
  color: ${({ theme }) => theme.colors.grayDark};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  text-align: center;
  padding: 0 80px;
`;

export const Wrapper = styled.View`
  margin: 45px 0 20px;
`;

export const DayAndHour = styled.View`
  width: 100%;
  margin-bottom: 32px;
`;

export const CheckboxContainer = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`;

export const Checkbox = styled.View<CheckboxProps>`
  width: 24px;
  height: 24px;
  align-items: center;
  background: transparent;
  border: 1px solid #afc1c4;
  border-radius: 4px;
  margin-right: 12px;

  ${({ isSelected }) =>
    isSelected &&
    css`
      background: #ff993e;
      border: 0;
    `}
`;

export const Label = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: #4d4d4d;
  width: 100px;
`;

export const PickerHourContainer = styled.View`
  flex-direction: row;

  margin-top: 16px;
`;

export const PickerText = styled.Text``;

export const TouchableOpacity = styled.TouchableOpacity``;
