/* eslint-disable prefer-destructuring */
import React, { useCallback, useEffect, useState } from 'react';
import { View, Text } from 'react-native';

import { useRoute } from '@react-navigation/native';
import { addMonths, format, startOfToday } from 'date-fns';
import { Agenda } from 'react-native-calendars';

import { HeaderBack } from '~/components';

import { Container } from './styles';

interface Params {
  agenda: string[];
}

const MyAgenda: React.FC = () => {
  const { params } = useRoute();
  const { agenda } = params as Params;
  const [daysAvailable, setDaysAvailable] = useState(null);

  const getAvailableDates = useCallback(() => {
    const newItems = {};
    Object.keys(agenda).forEach((key) => {
      newItems[key] = agenda[key][0];
    });

    console.log(newItems);

    setDaysAvailable(newItems);
  }, [agenda]);

  useEffect(() => {
    getAvailableDates();
  }, [getAvailableDates]);

  return (
    <Container>
      <HeaderBack>Minha agenda completa</HeaderBack>

      <Agenda
        items={agenda}
        renderItem={(item) => {
          return (
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                height: 80,
                backgroundColor: 'white',
                borderRadius: 5,
                padding: 20,
                marginRight: 20,
                marginTop: 33,
                alignItems: 'center',
              }}
            >
              <Text>{item.name}</Text>
            </View>
          );
        }}
        minDate={format(startOfToday(), "yyyy'-'MM'-'dd")}
        maxDate={format(addMonths(startOfToday(), 6), "yyyy'-'MM'-'dd")}
        pastScrollRange={6}
        futureScrollRange={7}
        renderEmptyData={() => {
          return (
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                height: 60,
                backgroundColor: 'white',
                borderRadius: 5,
                padding: 20,
                margin: 20,
                marginTop: 33,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text>Não existem aulas agendadas.</Text>
            </View>
          );
        }}
        rowHasChanged={(r1, r2) => {
          return r1.text !== r2.text;
        }}
        markedDates={daysAvailable}
        theme={{
          arrowColor: '#37153D',
          textDayFontFamily: 'SofiaPro-Medium',
          selectedDayTextColor: '#fff',
          selectedDayBackgroundColor: '#37153D',
          agendaTodayColor: '#F76E1E',
          agendaDayTextColor: 'black',
          agendaDayNumColor: '#37153D',
          todayTextColor: '#43515c',
          dotColor: '#F76E1E',
          'stylesheet.day.period': {
            base: {
              overflow: 'hidden',
              height: 34,
              alignItems: 'center',
              width: 38,
            },
          },
          'stylesheet.day.basic': {
            text: {
              marginTop: 0,
              fontSize: 16,
              fontFamily: 'SofiaPro-Medium',
              fontWeight: '300',
              color: '#43515c',
              backgroundColor: 'rgba(255, 255, 255, 0)',
            },
          },
        }}
      />
    </Container>
  );
};

export default MyAgenda;
