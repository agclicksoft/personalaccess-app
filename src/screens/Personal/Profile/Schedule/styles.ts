import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Divider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
  margin: 15px 0 0;
`;

export const MenuButton = styled.TouchableOpacity`
  width: 100%;
`;

export const MenuContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 10px 30px 10px 40px;
  width: 100%;
`;

export const Holder = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const TextWrapper = styled.View``;

export const MenuTitle = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  font-size: ${({ theme }) => theme.fontSizes.size14};
  color: ${({ theme }) => theme.colors.purple};
`;
