import {
  IconProfile,
  IconCalendar,
  IconPlace,
  IconVerified,
  IconCube,
  IconCoin,
  IconFile,
  IconSignOut,
  IconCreditCard,
  IconFavorite,
} from '~/assets/icons';

export const options = [
  {
    title: 'Perfil Público',
    label: 'Visualizar seu perfil público',
    icon: IconProfile,
    screen: 'PublicView',
  },
  {
    title: 'Agenda',
    label: 'Selecionar dias e horários de atendimento',
    icon: IconCalendar,
    screen: 'Schedule',
  },
  {
    title: 'Locais de atendimento',
    label: 'Selecionar locais',
    icon: IconPlace,
    screen: 'ServiceLocations',
  },
  {
    title: 'Especialidades',
    label: 'Selecionar suas especialidades',
    icon: IconVerified,
    screen: 'Specialties',
  },
  {
    title: 'Pacotes de serviços',
    label: 'Gerenciar opções de pacotes',
    icon: IconCube,
    screen: 'ServicePackages',
  },
  {
    title: 'Histórico de pagamentos',
    label: 'Visualizar aulas pagas',
    icon: IconCreditCard,
    screen: 'Payments',
  },
  {
    title: 'Minhas avaliações',
    label: 'Visualizar avaliações de aulas',
    icon: IconFavorite,
    screen: 'Avaliations',
  },
  {
    title: 'Conta bancária',
    label: 'Editar conta para recebimento',
    icon: IconCoin,
    screen: 'BankAccount',
  },
  {
    title: 'Termos de uso',
    label: 'Visualizar termos',
    icon: IconFile,
    screen: 'TermsOfUse',
  },
  {
    title: 'Sair',
    label: 'Sair da sua conta',
    icon: IconSignOut,
    screen: '',
  },
];
