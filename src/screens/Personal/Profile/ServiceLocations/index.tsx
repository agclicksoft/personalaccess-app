import React, { useCallback, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { IconPlace, IconEdit, IconCancel } from '~/assets/icons';
import { ContainerScroll, HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import { deletePersonalGym } from '~/services/personal';

import {
  Container,
  LocationsContainer,
  Holder,
  Image,
  TextWrapper,
  GymName,
  GymAddress,
  Label,
  Settings,
  ButtonSetting,
  ButtonSettingImage,
} from './styles';

const ServiceLocations: React.FC = () => {
  const { user, updatePersonal } = useAuth();
  const { navigate } = useNavigation();
  const [loading, setLoading] = useState(false);

  const handleEditGym = useCallback(
    (gym) => {
      navigate('EditGym', { gym });
    },
    [navigate],
  );

  const handleDeleteGym = useCallback(
    async (gymId) => {
      try {
        setLoading(true);

        await deletePersonalGym(user.professional.id, gymId);

        const formData: User = {
          ...user,
          id: user.id,
          professional: user.professional,
        };

        await updatePersonal(formData);
        setLoading(false);
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    },
    [updatePersonal, user],
  );

  return (
    <Container>
      <HeaderBack
        rightButton
        rightButtonTitle="Novo"
        onPress={() => navigate('NewGym')}
      >
        Locais de atendimento
      </HeaderBack>

      <ContainerScroll>
        <LocationsContainer>
          {user.professional.gyms.length >= 1 &&
            user.professional.gyms.map((gym) => (
              <Holder key={gym.id}>
                <Settings>
                  <ButtonSetting onPress={() => handleEditGym(gym)}>
                    <ButtonSettingImage source={IconEdit} />
                  </ButtonSetting>

                  <ButtonSetting onPress={() => handleDeleteGym(gym.pivot.id)}>
                    <ButtonSettingImage source={IconCancel} />
                  </ButtonSetting>
                </Settings>

                <Image source={IconPlace} />

                <TextWrapper>
                  <GymName>{gym.name}</GymName>

                  <GymAddress>
                    <Label>CNPJ: </Label>
                    {gym.cnpj ? gym.cnpj : 'Não informado'}.
                  </GymAddress>

                  <GymAddress>
                    <Label>Endereço: </Label>
                    {gym.address_street}.
                  </GymAddress>
                </TextWrapper>
              </Holder>
            ))}
        </LocationsContainer>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default ServiceLocations;
