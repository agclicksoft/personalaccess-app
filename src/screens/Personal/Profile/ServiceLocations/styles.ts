import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const LocationsContainer = styled.View`
  margin: 10px 0;
`;

export const Holder = styled.View`
  flex-direction: row;
  margin: 7px 20px;
  background-color: white;
  border-radius: 10px;
  height: 140px;
  align-items: center;
  padding: 0 20px;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 45px;
  height: 45px;
  margin-right: 20px;
`;

export const TextWrapper = styled.View`
  flex: 1;
  flex-shrink: 1;
  margin-top: 10px;
`;

export const GymName = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.purple};
  flex-shrink: 1;
`;

export const Label = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
`;

export const GymAddress = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.grayDark};
`;

export const Settings = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  position: absolute;
  right: 10px;
  top: 10px;
  z-index: 999;
`;

export const ButtonSetting = styled.TouchableOpacity`
  margin-left: 16px;
`;

export const ButtonSettingImage = styled.Image`
  width: 24px;
  height: 24px;
`;
