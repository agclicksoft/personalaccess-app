import React from 'react';

import { useNavigation } from '@react-navigation/native';

import { Button, ContainerScroll, HeaderBack } from '~/components';

import {
  Container,
  SaveButtonContainer,
  ButtonRefuse,
  ButtonRefuseLabel,
  Content,
  Text,
} from './styles';

const TermsOfUse: React.FC = () => {
  const { goBack } = useNavigation();

  return (
    <Container>
      <HeaderBack>Termos de uso</HeaderBack>

      <ContainerScroll>
        <Content>
          <Text>
            1. Definições Básicas: 1.1 PERSONAL ACCESS Aplicativo gerenciado
            pela HSTS CONSULTORIA LTDA, inscrita no Cadastro Nacional de Pessoas
            Jurídicas sob o nº 38.824.434/0001-89, sociedade de responsabilidade
            limitada estabelecida no Brasil, localizada na Rua Maestro Felício
            Toledo, nº 495, sala 1008, CEP 24.030-105. (“Personal Access”). 1.2
            ALUNO – Pessoa física, usuário dos serviços oferecidos pela
            plataforma, na qualidade de aluno/cliente do Professor. (“Aluno”).
            1.3 PROFESSOR (PERSONAL TRAINER) – Pessoa física, usuário dos
            serviços oferecidos pela plataforma, obrigatoriamente formado em
            Educação Física, regularmente inscrito no Conselho Profissional da
            categoria. (“Professor” ou “Personal Trainer”).
          </Text>

          <Text>
            2. Introdução 2.1 O presente termo de uso se aplica ao usuário do
            serviço na qualidade de Professor (Personal Trainer), através de
            aplicação tecnológica (Aplicativo), sítios de internet e conteúdos
            relacionados (Serviços). Ao iniciar o cadastro, o Professor estará
            vinculado às regras aqui descritas, concordando e comprometendo-se a
            respeitá-las fielmente. 2.2 Somente o Personal Access poderá
            modificar, apagar, acrescentar ou substituir estes Termos de Uso a
            qualquer momento e sem necessidade de aviso prévio. Quando alguma
            regra for alterada, o Professor poderá visualizá-las através do
            mesmo link "Termos de Uso" imediatamente após a sua publicação. 2.3
            Se o Professor não concordar com estes termos de uso, não deverá
            utilizar o Personal Access. 2.4 Ao aceitar estes Termos de Uso, o
            Professor declara e garante poder fazê-lo nos termos da legislação
            brasileira vigente; pessoa física, obrigatoriamente formado em
            Educação Física, regularmente inscrito no Conselho Profissional da
            categoria 2.5 O Personal Access é um aplicativo com a função de
            aproximar alunos e professores de Educação Física (Personal
            Trainer), regularmente inscritos no Conselho Profissional da
            categoria. 2.6 O Personal Access não controla, e não tem qualquer
            vinculação trabalhista, empregatícia, societária ou ingerência sobre
            os professores de Educação Física (Personal Trainer), seus conteúdos
            e/ou serviços. 2.7 O Professor concorda que o aplicativo Personal
            Access não tem nenhuma responsabilidade pela disponibilidade e/ou
            qualidade dos serviços que ele próprio oferece a seus Alunos,
            assumindo o Professor integralmente os riscos ao ser contratado. 2.8
            A definição dos preços cobrados pelos Professores de Educação Física
            é de sua exclusiva responsabilidade, não cabendo ao Personal Access
            ingerência neste sentido. 2.9 O Personal Access não oferece acesso à
            clubes, academias de ginástica, academias de musculação, academias
            de treino funcional, ou estabelecimentos privados de quaisquer
            modalidades esportivas. 2.10 O Professor e seus Alunos, deverão ter
            prévio acesso, por sua responsabilidade e risco, aos espaços de
            treinamento, a clubes, academias de ginástica, academias de
            musculação, academias de treino funcional, ou estabelecimentos
            públicos ou privados de quaisquer modalidades esportivas. 2.11 O
            serviço prestado pelo Personal Access se limita não inclui acesso às
            academias.
          </Text>

          <Text>
            3. Cadastro 3.1 Cadastro Inicial 3.1.1 Para o cadastro inicial no
            Personal Access (Aplicativo), o Professor deverá informar: nome,
            endereço eletrônico (e-mail), número de telefone e senha de acesso
            com 6 (seis) dígitos, sua inscrição no CREF/CONFEF e o CPF. Com esse
            cadastro, o Professor já poderá visualizar as funcionalidades do
            aplicativo, e estará apto a preencher o Cadastro Complementar. 3.2
            Cadastro Complementar 3.2.1 Com acesso ao Aplicativo após preencher
            o Cadastro Inicial, o Professor deverá acessar Meu Perfil onde
            deverá preencher sua Biografia, apresentando-se e/ou colando outras
            informações técnicas pertinentes que estarão visíveis para os
            Alunos. Em seguida, deverá preencher o Cadastro Complementar nos
            campos próprios, informando: 3.2.1.1 Agenda: O Professor deverá
            preencher, nessa ordem: a) Horário Padrão (indicando os dias e hora
            de início e fim em que estará disponível para agendamento); b)
            Bloqueio de Agenda (indicando os dias e hora de início e fim em que
            não estará disponível para agendamento, podendo utilizar a função
            para bloquear feriados, compromissos pessoais, etc.,). Feito isso, a
            partir do primeiro agendamento, já poderá consultar a Minha Agenda
            Completa. As informações fornecidas em Horário Padrão e em Bloqueio
            de Agenda estarão disponíveis para todos os Alunos. 3.2.1.2 Locais
            de atendimento: O Professor deverá preencher os locais de
            atendimento onde aceita ser contratado. Ao preencher o formulário
            serão solicitados o nome e o endereço completo do local. Se souber,
            o Professor poderá preencher também o CNPJ do estabelecimento, não
            sendo obrigatório. Sempre que um local de atendimento já tiver sido
            inserido por outro usuário, ele já aparecerá para escolha do próximo
            Professor. As informações fornecidas aparecerão para todos os
            Alunos. 3.2.1.2.1 “Local de Atendimento” é o espaço onde o Professor
            ministra suas aulas. Pode ser uma academia, clube, ou outro espaço
            regular. O Professor pode ter mais de um local de atendimento. O
            Aluno e o Professor devem se certificar de que já tenham, antes do
            início das aulas, prévio acesso a esse local de atendimento. O
            Personal Access não fornece acesso aos locais de atendimento.
            3.2.1.3 Especialidades: O Professor deverá preencher suas áreas de
            atuação, podendo escolher uma, mais de uma ou todas dentre as
            seguintes: Hipertrofia, Emagrecimento, Condicionamento físico,
            Preparação Física, Yoga, Pilates, Gestante, Kids, Atividades par
            Idosos e Dança. As informações fornecidas aparecerão para os Alunos
            que forem agendar serviços. 3.2.1.4 Pacotes de Serviços: O Professor
            deverá preencher os valores que efetivamente cobra pelos serviços,
            nas seguintes modalidades: 3.2.1.4.1 “Aula avulsa” é a aula
            singular, prestada em única oportunidade; 3.2.1.4.2 “Pacote de
            Aulas” é o grupo de aulas contratadas em conjunto. O Aluno poderá
            utilizar uma aula por dia, ou mais de uma aula em um mesmo dia,
            conforme sua necessidade. Os Pacotes de Aulas podem ser de 8 aulas
            (em especial para que usa 2 vezes na semana em meses de 4 semanas);
            10 aulas (em especial para que usa 2 vezes na semana em meses de 4
            semanas e meia); 12 aulas (em especial para que usa 3 vezes na
            semana em meses de 4 semanas); 15 aulas (em especial para que usa 3
            vezes na semana em meses de 4 semanas e meia); 16 aulas (em especial
            para que usa 4 vezes na semana em meses de 4 semanas); 20 aulas (em
            especial para que usa 4 vezes na semana em meses de 4 semanas e
            meia); 24 aulas (em especial para que usa 4 ou 5 vezes na semana em
            meses de 4 semanas e meia). 3.2.1.4.3 O “Pacote de Aulas” não tema
            finalidade de coincidir integralmente com o mês. A contagem é feita
            por número de aulas. A cada mês, dependendo do tamanho do mês (se
            28,29,30 ou 31 dias, bem como se há feriados ou não, e quantos dias
            efetivamente irá utilizar), o Aluno poderá comprar o pacote que mais
            se adapta a sua necessidade naquele período, podendo varia mês a
            mês. 3.2.1.4.4 É necessário cadastrar, pelo menos, o valor da aula
            avulsa e de 1 (um) pacote de serviços, para ficar visível para
            contratação dos Alunos. 3.2.1.4.5 As informações fornecidas
            aparecerão para todos os Alunos. As informações fornecidas não
            aparecerão para os outros Professores. 3.2.1.5 Conta Bancária: O
            Professor deverá preencher seus dados bancários para receber os
            pagamentos que porventura sejam feitos pelos Alunos usando cartão de
            crédito. Se o Professor tem conta em algum Banco que não está
            listado, deverá comunicar a ocorrência através do e-mail
            “personalaccess.app@gmail.com”. 3.2.1.6 As informações de e-mail e
            CPF são únicas na plataforma, não sendo permitido ter duas contas
            com o mesmo CPF e/ou e-mail. Se o usuário se cadastrar como
            Professor, não poderá se cadastrar como Aluno, e vice-versa. 3.3
            Perfil Público 3.3.1 Os Perfis dos Alunos (Perfil Público) são
            pessoais e restritos. Os Professores não visualizam os Alunos que
            estão próximos. Apenas o Professor que for contratado pelo Aluno
            poderá visualizar o perfil desse Aluno. Outros usuários, sejam
            outros Alunos, sejam Professores não contratados, não conseguirão
            visualizar o Perfil do Aluno que utilize o Personal Access. 3.3.2 O
            Perfil do Professor (Perfil Público do Professor) fica visível para
            todos os Alunos cadastrados. Nesse perfil, são exibidas as seguintes
            informações: Nome, CREF/CONFEF, nota de avaliação, link para
            agendamento de aulas, especialidades, locais de atendimento,
            biografia e link para o perfil de rede social (Instagram). 3.3.3 Ao
            clicar em Perfil Público, o Professor poderá visualizar exatamente o
            que os Alunos visualizam. 3.3.4 Nenhum outro Professor conseguirá
            visualizar o Perfil dos demais Professores. 3.3.5 O Perfil do
            Professor (Perfil Público do Professor) somente ficará visível para
            todos os Alunos quando forem preenchidos o local de atendimento, os
            horários da agenda e forem cadastrados os valores cobrados na aula
            avulsa e ao menos em 1(um) pacote de serviços.
          </Text>

          <Text>
            4. Cadastro Específico do Aluno – PAR-Q 4.1 Para efetuar a
            contratação de um Professor, será exigido que o Aluno conclua o
            cadastro, preenchendo o Questionário de Prontidão para Atividade
            Física - PAR-Q, aplicado ao Aluno antes do início da prática de
            atividades físicas regulares. 4.2 O PAR-Q tem como objetivo
            identificar possíveis limitações e restrições existentes. É um
            detalhamento do histórico de saúde do Aluno, verificando se este
            está apto para atividades físicas adequadas a sua condição. Através
            do questionário, o Professor poderá avaliar melhor o Aluno, apreciar
            as necessidades do cliente com suas aptidões profissionais e traçar
            de forma mais eficaz um plano individualizado. 4.3 Será exigido do
            Aluno que informe peso, altura, e responda às seguintes perguntas:
            “Qual seu objetivo? ”; “Na prática de atividades físicas, você sente
            algum desses sintomas? Tontura, Enjoo, Falta de ar, Nada”; “Algum
            outro desconforto?”; “Diabético?”; “Qual estágio?” (se marcar SIM
            para a pergunta anterior); “Hipertenso?”; “Qual a máxima?” (se
            marcar SIM para a pergunta anterior); “Possui algum problema
            cardíaco?”; “Qual?” (se marcar SIM para a pergunta anterior);
            “Problema respiratório?”; “Qual?” (se marcar SIM para a pergunta
            anterior); “Utiliza algum tipo de medicamento ou drogas?”; “Qual?”
            (se marcar SIM para a pergunta anterior); “Fez alguma cirurgia nos
            últimos 6 meses?”; “Qual?” (se marcar SIM para a pergunta anterior);
            “Sente algum incômodo ou dores em articulações e/ou coluna na
            prática de atividade física?”; “Qual?” (se marcar SIM para a
            pergunta anterior); “Tem alguma restrição ou recomendação médica
            para a prática de exercícios físicos?; “Qual?” (se marcar SIM para a
            pergunta anterior). 4.4 As informações do Perfil do Aluno que
            constam no PAR-Q são pessoais e restritas. Apenas o Professor que
            for contratado pelo Aluno poderá visualizar no seu perfil essas
            informações. Outros usuários, sejam outros Alunos, sejam Professores
            não contratados, não conseguirão visualizar o Perfil do Aluno que
            utilize o Personal Access. 4.5 As informações do PAR-Q permanecerão
            disponíveis para consulta do Professo contratado.
          </Text>

          <Text>
            5 Condições adversas 5.1 Caso o Aluno declare patologias como
            diabetes tipo 1 e tipo 2, hipertensão, problemas cardíacos de
            quaisquer tipos ou que tenha sofrido acidente vascular cerebral
            (AVC), será necessário enviar através de upload em link próprio
            atestado médico com autorização para a prática de atividade física.
            5.2 Essas informações serão disponibilizadas exclusivamente ao
            Professor (Personal Trainer) escolhido no momento da contratação.
            Diante das informações, caberá ao Professor contratado decidir com
            base em seu conhecimento técnico se está capacitado ou não para
            aceitar a demanda, apresentado justificativa ao Aluno em caso de
            recusa. 5.3 Apenas o Professor, profissional formado em Educação
            Física e inscrito no Conselho de Classe, tem capacidade técnica para
            decidir se deve ou não aceitar a contratação do Aluno, podendo
            inclusive exigir outros atestados ou laudos prévios para ministrar
            suas aulas. O Personal Access não tem nenhuma responsabilidade sobre
            a atuação técnica do profissional habilitado.
          </Text>

          <Text>
            6 Responsabilidades do Personal Access 6.1 O Personal Access não
            disponibiliza para terceiros, nem para os Professores cadastrados em
            sua plataforma, os dados pessoais referentes aos Alunos cadastrados.
            Apenas o Professor contratado terá acesso aos dados do Aluno
            contratante, exclusivamente, mediante aceitação do Aluno desses
            termos de uso, para que possa avaliar se está habilitado a aceitar
            ou não a demanda. 6.2 O Personal Access poderá coletar dados do uso
            da plataforma, de forma coletiva e sem individualizar os usuários,
            para pesquisas mercadológicas e informações estatísticas, para uso
            próprio e informações gerais aos Professores usuários.
          </Text>

          <Text>
            7 Responsabilidades acerca das informações cadastrais 7.1 O
            Professor estará assumindo integralmente a responsabilidade
            (inclusive cível e criminal) pela exatidão e veracidade das
            informações fornecidas no momento do cadastro, que poderá ser
            verificado, a qualquer momento, pelo Personal Access. 7.2 Em caso de
            informações incorretas, inverídicas ou não confirmadas, bem assim na
            hipótese da negativa em corrigi-las ou enviar documentação que
            comprove a correção, o Personal Access se reserva o direito de não
            concluir o cadastramento em curso ou, ainda, de bloquear o cadastro
            já existente, impedindo a utilização dos serviços on-line até que, a
            critério do Personal Access, a situação de anomalia esteja
            regularizada. 7.3 O Personal Access se reserva o direito de impedir,
            a seu critério, novos cadastros, ou cancelar os já efetuados, em
            caso de ser detectada anomalia que, em sua análise, seja revestida
            de gravidade ou demonstre tentativa deliberada de burlar as regras
            aqui descritas, obrigatórias para todos os usuários. 7.4 Adotar-se-á
            Política de Privacidade que protege todos estes dados que o
            Professor fornecerá. Serão utilizados os melhores esforços para
            garantir que as informações pessoais não serão
            interceptadas/acessadas por terceiros de maneira indevida e estranha
            a esta política de privacidade. 7.5 Ao criar uma conta no
            aplicativo, o Professor concorda que o Personal Access e seus
            parceiros poderão lhe enviar mensagens através de e-mail, de textos
            informativas (SMS), ou outro meio de comunicação, como parte das
            operações comerciais regulares, com conteúdo de natureza informativa
            e/ou promocional relacionada aos serviços. 7.6 O Personal Access
            verificará, por ocasião do cadastro inicial e periodicamente, a
            regularidade da inscrição do Professor no Conselho de Classe. A
            perda da habilitação conferida pelo Conselho de Classe também
            inabilita o Professor a utilizar a plataforma, ocasião em que o
            usuário será notificado acerca da irregularidade.
          </Text>

          <Text>
            8. Senha 8.1 O Professor deverá cadastrar senha com 6 (seis)
            dígitos, que deverá ser mantida em absoluto sigilo. Esta
            confidencialidade cabe ao Professor, não tendo o Personal Access
            qualquer responsabilidade pelas atividades que ocorram com o uso de
            sua senha, tampouco por eventuais perdas ou danos resultantes de
            eventual utilização não autorizada. 8.2 O Personal Access não possui
            acesso à senha do Professor. Se a senha for esquecida, o Professor
            deverá seguir o procedimento de recuperação, clicando em “Esqueci
            minha senha”. O Professor receberá um e-mail com a senha provisória,
            devendo cadastrar uma nova senha já no primeiro acesso. 8.3 O
            Professor deverá, ao final de cada acesso ao Sistema, encerrar a
            sessão clicando em 'Sair', evitando, desta maneira, o acesso
            indevido à sua conta. 8.4 O Professor concorda em notificar
            imediatamente ao Personal Access, através do e-mail
            “personalaccess.app@gmail.com”, sobre todo e qualquer acesso não
            permitido de sua senha ou conta. Não obstante esta notificação, o
            Professor poderá ser responsabilizado em caso de o Personal Access
            vir a sofrer qualquer dano ou prejuízo (i) pelo ingresso impróprio
            de sua conta ou (ii) pela utilização de sua senha por terceiros não
            autorizados. 8.5 O Professor poderá alterar sua senha sempre que
            desejar em Meu Perfil, opção Editar Perfil.
          </Text>

          <Text>
            9. Serviços 9.1 O uso da plataforma Personal Access é gratuita para
            o Aluno. Nenhum pagamento será exigido para pesquisar Professores. O
            serviço prestado pelo Professor (Personal Trainer) é remunerado,
            conforme o plano de aulas escolhido pelo Aluno. 9.2 O uso da
            plataforma Personal Access não é gratuita para o Professor. Conforme
            detalhado abaixo, além do Plano de Assinatura, o Professor pagará
            uma percentagem sobre o valor contratado pelo Aluno. 9.2.1
            Excepcionalmente, de forma temporária e promocional, o Personal
            Access poderá isentar o Professor do pagamento do Plano de
            Assinatura. 9.2.2 Sempre que o pagamento do Plano de Assinatura
            voltar a ser exigível, o Personal Access comunicará aos Professores
            e dará tempo hábil, não inferior a 1 (um) mês, para o usuário
            decidir se deseja contratar ou não. 9.3 Para utilizar os serviços
            oferecidos pelo Personal Access, basta ter uma conta ativa de acesso
            ao sistema e adquirir um dos Planos de Assinatura. 9.3.1 Sempre que
            o Aluno logar no sistema, verá os Professores que estão mais
            próximos do local em que se encontra, por georreferenciamento,
            podendo pesquisar também por outras localidades. 9.4 Os serviços
            prestados pelo Personal Access consistem na disponibilização de uma
            aplicação tecnológica que possibilita ao Aluno cadastrado localizar
            e contratar Professores de Educação Física inscritos regularmente no
            Conselho Profissional (Personal Trainer). 9.5 O serviço não deve ser
            utilizado para qualquer finalidade que não as expressamente
            autorizadas por estes Termos de Uso. A utilização da plataforma para
            fins diversos dos previstos poderá ensejar a exclusão do usuário do
            cadastro. O Personal Access monitorará constantemente a utilização
            da plataforma. 9.6 Após escolher Professor, o Aluno seleciona o
            plano de aulas, bem como os dias e horários na agenda, nos quais
            deseja ter o serviço prestado, escolhendo dentre os previamente
            disponibilizados pelo Professor, e o local de atendimento. Em
            seguida, deverá efetuar o pagamento, que poderá ser feito em cartão,
            pelo aplicativo, ou em dinheiro diretamente ao Personal Trainer. 9.7
            A hora-aula terá por base o tempo mínimo padrão de 50 (cinquenta)
            minutos, podendo ter pequenas variações para mais ou para menos,
            conforme a programação de atividades do dia conjugada com o objetivo
            do Aluno. Cabe ao Professor calcular o tempo médio ideia da
            hora-aula para que o Aluno atinja seu objetivo. 9.8 O valor do
            serviço é determinado exclusivamente pelo Professor, que é livre
            para estipular o preço cobrado. 9.9 O Professor concorda em cobrar o
            valor anunciado por ele mesmo no aplicativo, que deverá ser
            fidedigno à realidade. O Aluno concorda em pagar os valores dos
            serviços agendados conforme eles foram apresentados no momento do
            agendamento. 9.10 Após adquirir a aula-avulsa ou o pacote de aulas,
            o Aluno terá 32 (trinta e dois) dias para marcar, remarcar e
            efetivamente utilizar a aula comprada, sem possibilidade de
            prorrogação. Após o transcurso do prazo total sem utilização, sem
            marcação ou sem remarcação, a aula será considerada expirada. 9.11 O
            Professor se compromete a respeitar a validade das aulas compradas.
          </Text>

          <Text>
            10 Responsabilidade do Professor no fornecimento dos serviços
            prestados 10.1 O Professor concorda em comparecer a todo e qualquer
            agendamento ou reagendamento que tiver aceitado através do Personal
            Access. Caso não lhe seja possível, deverá seguir a Política de
            Cancelamento e Reagendamento abaixo descrita. 10.2 O Professor se
            compromete a não aceitar através do Personal Access nenhum
            agendamento que não pretenda comparecer ("agendamento seguido de não
            comparecimento proposital"), sob pena de esta prática, caso
            reincidente e a exclusivo critério do Personal Access, motivar a
            suspensão ou cancelamento de sua conta. Se a aula foi aceita e
            depois, por motivos diversos o Professor não poderá comparecer, este
            deverá propor o reagendamento. 10.3 O Professor deverá verificar em
            seus agendamentos, em “Minhas Solicitações”, as aulas marcadas, onde
            constará o dia e hora marcado, o Aluno contratante com PAR-Q e o
            local de atendimento. 10.4 O Personal Access não possui qualquer
            responsabilidade no caso de o Professor confirmar um agendamento e
            não comparecer ao local e horário escolhido, tentar cancelá-lo sem
            tempo hábil ou tardiamente. 10.4.1 Se for detectada abusividade em
            cancelamentos, reagendamentos, ou prática em desconformidade com os
            objetivos da plataforma, ou situações reiteradas que fujam da
            normalidade, detectada pelo Personal Access ou denunciada por
            Alunos, o Professor poderá ser impedido de utilizar o aplicativo.
            10.5 É dever do Professor seguir à risca os horários marcados. O
            mesmo dever é atribuído aos Alunos. Se houver atraso do Aluno, a
            aula será prestada apenas pelo tempo restante da hora marcada, sem
            prorrogação, para não prejudicar o Aluno seguinte. Se houver atraso
            do Professor, este deverá ver a melhor forma de ressarcir o tempo ao
            Aluno, para que ele não seja prejudicado.
          </Text>

          <Text>
            11 Política de Agendamento, Cancelamento e Reagendamento 11.1 Toda
            vez que a solicitação foi aceita pelo Professor, considera-se a aula
            como confirmada ou marcada. 11.2 Considera-se “no-show” do Aluno o
            agendamento realizado de forma regular, não remarcado, e cuja aula
            não foi ministrada por fatores atribuídos exclusivamente ao Aluno, e
            que sejam estranhos ao Professor (Personal Trainer). 11.3 Em caso de
            “no-show” injustificado do Aluno, isto é, falta do Aluno, haverá
            perda da hora-aula contratada, sendo o serviço considerado prestado,
            sem direito a ressarcimento ou nova remarcação, uma vez que o
            Professor (Personal Trainer) permaneceu o tempo marcado na hora e
            local combinados à disposição do Aluno, e este tempo desprendido
            pelo Professor, bem como seu custo de deslocamento, não poderão ser
            ressarcidos. 11.4 Em caso de “no-show” justificado pelo Aluno, como
            emergências médicas, ocorrências profissionais, ambas comprovadas,
            ou outras ocorrências relevantes e devidamente comprovadas, poderá
            ser restituído o direito à hora-aula, conforme a disponibilidade da
            agenda do Professor (Personal Trainer), a ser remarcada uma vez no
            período de 30 (trinta) dias. 11.5 Em caso de cancelamento de aula
            pré-agendada, o Aluno deverá fazê-lo com antecedência mínima de 24
            (vinte e quatro) horas. Qualquer cancelamento em prazo inferior será
            considerado “no-show”, salvo se houver disponibilidade na agenda do
            Professor (Personal Trainer) escolhido, e este anuir com a mudança
            de horário. 11.5.1 Em nenhuma hipótese serão aceitas
            remarcações/cancelamentos pelo Aluno no dia da prestação do serviço,
            uma vez que o Professor (Personal Trainer) deixou de atender outras
            demandas para permanecer o tempo marcado na hora e local combinados
            à disposição do Aluno, e este tempo desprendido pelo Professor, bem
            como seu custo de deslocamento, não poderão ser ressarcidos. Da
            mesma forma, em tempo tão exíguo, não será possível ao Professor
            (Personal Trainer) marcar outra aula para aproveitar a hora perdida.
            11.6 Considera-se “no-show” do Professor o agendamento realizado de
            forma regular, não remarcado, e cuja aula não foi ministrada por
            fatores atribuídos exclusivamente ao Professor, e que sejam
            estranhos ao Aluno. 11.7 Em caso de “no-show” do Professor,
            justificado ou injustificado, o Aluno terá devolvido o direito de
            remarcar a aula não dada, no período de 30 (trinta) dias, conforme a
            disponibilidade de agenda do Professor. 11.8 Da mesma forma, caso o
            Professor (Personal Trainer) venha a reagendar, alterar ou cancelar
            um agendamento, de forma justificada, o Aluno receberá uma
            notificação da ocorrência. Neste caso, o Aluno deverá reagendar o
            dia e horário da prestação do serviço, escolhendo dentre os
            disponíveis na agenda do professor. 11.9 Havendo disponibilidade na
            Agenda do Professor, a critério deste, outros prazos poderão ser
            adotados.
          </Text>

          <Text>
            12 Token de abertura de Aula, Avaliação dos Serviços e Registro dos
            Treinos 12.1 Antes de iniciar a aula, poderá ser solicitado ao Aluno
            que confirme a abertura da aula, colocando em tela específica no
            celular do professor, o código de confirmação (token), que serão
            sempre os 3 (três) últimos dígitos do CPF do Aluno. 12.2 Após a
            conclusão de um serviço contratado, poderá ser solicitado a o Aluno
            o preenchimento de uma avaliação acerca da qualidade do serviço
            prestado, para orientar a escolha de novos alunos que pretendam
            contratar o Professor. 12.3 Ao final da aula, será solicitado ao
            Professor que preencha os grupamentos musculares que foram
            trabalhados na aula finalizada, para facilitar a consulta do próximo
            Professor ou elaboração da próxima aula.
          </Text>

          <Text>
            13. Programas de Fidelidade 13.1 O Personal Access e alguns
            estabelecimentos parceiros poderão oferecer programas de fidelidade,
            conforme o Professor contrate os serviços ou adquira produtos. As
            regras dos programas de fidelidade são específicas por
            estabelecimento, que determinam quais serviços são elegíveis. Estes
            programas de fidelidade podem ser descontinuados a qualquer momento
            pelo Personal Access ou pelo estabelecimento parceiro. 13.2 O
            Personal Access se reserva o direito de estabelecer programas de
            fidelidade, isoladamente ou em conjunto com empresas parceiras.
          </Text>

          <Text>
            14. Pagamento Online 14.1 Quando o Professor for contratado, o Aluno
            poderá optar por realizar o pagamento online dos serviços. O
            pagamento online é realizado através de um gateway de pagamento e o
            valor é transferido para o Professor (Personal Trainer) conforme
            contrato com o Personal Access. A responsabilidade sobre a prestação
            do serviço é do Professor (Personal Trainer) independentemente se
            foi utilizado ou não o pagamento online. 14.2 O pagamento será
            realizado através de cartão, nas modalidades crédito ou débito,
            utilizando o Pag Seguro. O Personal Access. Poderá, a qualquer
            momento, alterar o gateway de pagamento. 14.3 O Personal Access não
            armazena dados de cartão utilizados em sua plataforma. Toda a
            transação realizada é feita através do Pag Seguro, sem interferência
            do Personal Access. 14.4 O Personal Access não se responsabiliza por
            recusas de cartão, devendo o Aluno verificar o motivo diretamente
            com sua operadora de cartão de crédito.
          </Text>

          <Text>
            15. Recebimento dos pacotes/aula avulsa vendidos e pagos com cartão
            15.1 Sempre que o Professor vender uma aula avulsa ou pacote de
            aulas, e o Aluno pagar em cartão de crédito/débito on line, o valor
            será transferido para a conta do Personal Access. 15.2 Em até 20
            dias, o Personal Access transferirá o valor pago para a conta
            cadastrada pelo professor, podendo utilizar depósito em conta, TED,
            DOC, PIX, ou outro meio de pagamento. 15.2.1 Pagamentos feitos com o
            cartão Diners poderão levar até 30 dias para repasse. 15.2.2 Todas
            as aulas avulsas ou pacote de aulas vendidos e pagos em cartão de
            crédito/débito, serão contabilizados da seguinte forma: Valor da
            Venda Bruta do Professor = 100%; Valor retido total (Custo de uso do
            Serviço) = Remuneração do meio de pagamento (taxa do cartão) +
            Remuneração do serviço da plataforma = 6% (seis por cento); Valor
            Líquido a receber pelo Professor = 94% da operação. 15.3 O Personal
            Access poderá tentar reduzir os prazos praticados e taxas
            praticados. 15.4 Se houver mudanças efetuadas pelas operadoras de
            cartão e/ou gateway de pagamento, que implique em alteração dos
            prazos e taxas, o Personal Access comunicará aos Professores. 15.5 O
            Personal Access não se responsabiliza por recusas de cartão. Se a
            compra não for aprovada, o Aluno deverá ser orientado pelo Professor
            a verificar o motivo diretamente com sua operadora de cartão de
            crédito/débito.
          </Text>

          <Text>
            16. Pagamento em Dinheiro 16.1 O Aluno poderá optar por realizar o
            pagamento dos serviços em dinheiro. O pagamento em mãos será
            realizado diretamente ao Professor (Personal Trainer), antes do
            início da prestação do serviço. A responsabilidade sobre a prestação
            do serviço é do Professor (Personal Trainer). A responsabilidade por
            cobrar o valor acordado em dinheiro, com pagamento em mãos, cabe ao
            Professor, exclusivamente.
          </Text>

          <Text>
            17. Planos de Assinatura 17.1 Para ter acesso à plataforma, o
            Professor deverá adquirir Plano de Assinatura. 17.2 O Professor
            poderá escolher entre o Plano Premium e o Plano Básico, conforme os
            valores vigentes. Plano Premium Periodicidade Valor Mensal R$ 69,99
            Trimestral R$ 179,99 Semestral R$ 329,99 Anual R$ 599,99 Plano
            Básico Periodicidade Valor Mensal R$ 49,99 Trimestral R$ 119,99
            Semestral R$ 209,99 Anual R$ 359,99 17.3 Esses valores poderão ser
            alterados a qualquer momento pelo Personal Access sem prévio aviso,
            para os novos contratos. 17.4. No Plano Premium o Professor terá
            acesso a todas as ferramentas, incluindo agenda, exibição do perfil
            para busca on line pelos Alunos e venda de aulas avulsas e pacotes
            de aulas. Terá também acesso total às informações estatísticas.
            17.5. O Plano Básico inclui todas as funções, com exceção das
            informações estatísticas, que não estarão incluídas neste Plano.
            17.6 O Personal Access poderá conceder período gratuito de testes
            para conhecimento da plataforma. 17.7 Excepcionalmente, de forma
            temporária e promocional, o Personal Access poderá isentar o
            Professor do pagamento do Plano de Assinatura, podendo retomar a
            exigibilidade a qualquer momento, mediante prévio aviso.
          </Text>

          <Text>
            18. Disponibilidade dos Serviços 18.1 O Personal Access não garante
            que o Sistema será ininterrupto ou que estará livre de defeitos,
            inexatidão, interferência, invasão de segurança, erros, vírus,
            imprecisões ou falhas. 18.2 O Professor concorda expressamente que
            os serviços são fornecidos conforme a disponibilidade técnica para
            seu uso, e não demandará nenhuma garantia ou reparação de qualquer
            tipo. 18.3 O Personal Access não se responsabiliza por quaisquer
            danos em decorrência de atos, fatos, omissões ou eventos praticados
            ou de responsabilidade de terceiros, incluindo o Professor (Personal
            Trainer) e o Aluno, assim como por motivos de força maior ou caso
            fortuito, tais como falhas no fornecimento de energia elétrica,
            conexão à internet ou ainda fechamento de academias ou de espaço de
            atendimento. 18.4 O Personal Access verificará previamente a
            existência e regularidade do registro do Professor (Personal
            Trainer) no Conselho Profissional. Não serão admitidos na
            plataforma, na condição de Professores, usuários que não tenham
            registro no Conselho Profissional. 18.5 Caso o Aluno se sinta
            desconfortável ou mesmo lesado por qualquer ação ou omissão do
            Professor (Personal Trainer), embora o Personal Access não tenha
            nenhuma ingerência ou responsabilidade direta, ele poderá enviar
            e-mail relatando o ocorrido, para verificação e eventuais
            providências. 18.6 Caso o Professor se sinta desconfortável ou mesmo
            lesado por qualquer ação ou omissão do Aluno, embora o Personal
            Access não tenha nenhuma ingerência ou responsabilidade direta, ele
            poderá enviar e-mail relatando o ocorrido, para verificação e
            eventuais providências.
          </Text>

          <Text>
            19 Outras obrigações do Professor 19.1 O Professor não praticará
            nenhuma ação que imponha uma sobrecarga desarrazoada ou
            desproporcional de dados, que venha a comprometer a usabilidade do
            Sistema; 19.2 O Professor não tentará, incentivará ou ajudará
            qualquer outra pessoa a modificar qualquer parte do Sistema; 19.3 O
            Professor não utilizará o Sistema para propagar vírus, spam
            (comunicações não solicitadas) ou outros ao Personal Access, a seus
            usuários - sejam clientes ou estabelecimentos - ou prestadores de
            serviços; 19.4 O Professor não utilizará o Sistema para exibir
            pornografia, atos obscenos, mensagens de ódio, racismo, ou outras
            condutas vedadas por lei, sob pena de exclusão da plataforma; 19.5 O
            Professor fará uso exclusivamente profissional da plataforma,
            voltado à execução dos serviços relacionados à Educação Física,
            devendo respeitar os ditames éticos, morais e sociais pertinentes,
            sob pena de exclusão da plataforma; 19.6 O Professor não utilizará
            nenhuma senha de acesso que não seja a sua para acessar a sua conta,
            sendo vedado o empréstimo ou compartilhamento de senhas; 19.7 O
            Professor não interferirá ou prejudicará, ainda que apenas no âmbito
            da tentativa, o acesso de qualquer usuário, estabelecimento ou
            prestadores de serviços; 19.8 O Professor não utilizará o Sistema
            para alguma outra forma não expressamente prevista neste Termos de
            Uso; 19.9 O Professor se compromete a informar dados verdadeiros e
            mantê-los atualizados, sendo motivo que justifica a exclusão da
            plataforma o registro de valores cobrados que não reproduzam a
            verdade. 19.10 O Professor fica ciente que o Personal Access
            coletará, de forma anônima e sem identificar o usuário
            individualmente aos demais, informações acerca de preços, quantidade
            de aulas, locais de contratação, e outras relacionadas com o uso da
            plataforma, para a formação das informações estatísticas. 19.11 O
            Professor não poderá divulgar as informações estatísticas para
            outros usuários sem autorização do Personal Access. A título
            acadêmico, de estudo ou outros, o Personal Access poderá autorizar a
            divulgação, desde que citada a fonte e previamente autorizado.
          </Text>

          <Text>
            20 Estatísticas 20.1 Os Professores assinantes do Plano Premium
            terão acesso às informações estatísticas, conjunto de dados que
            serão colhidos dentre os próprios usuários da plataforma, de forma
            anônima e sem possibilidade de identificar individualmente os demais
            usuários. Estas informações estarão disponíveis somente aos
            Professores assinantes do Plano Premium, de forma organizada. 20.2
            As informações estatísticas têm o objetivo de orientar o Professor
            sobre aspectos mercadológicos, tais como comparação de valores de
            serviço e de atividade em diversos locais. A formação do preço
            cobrado é prerrogativa exclusiva do Professor, não havendo nenhuma
            ingerência do Personal Access neste sentido. 20.3 “Preços mais
            cobrados” exibirá ao Professor quais são os preços mais praticados,
            isto é, os valores mais comuns que são cobrados aos Alunos pelos
            Professores cadastrados, na aula avulsa e nos pacotes de aulas. 20.4
            “Preço médio cobrado por bairro” exibirá ao Professor uma média
            calculada com todos os valores inseridos na plataforma, apresentando
            também qual foi o preço mínimo encontrado, e o preço máximo, para
            que o usuário possa ter uma noção de como o valor que cobra se
            posiciona no mercado. Os resultados também são separados por aula
            avulsa e por pacote de aulas. 20.5 “Número médio de solicitações e
            rejeições por bairro” exibirá ao Professor quais os locais que tem
            maior atividade, de contratação e de rejeição de demanda, com base
            nos registros dos usuários da plataforma, para que o Professor possa
            avaliar os melhores locais e os que possuem maior atividade para
            Personal Trainer. Os resultados são separados por aula avulsa e por
            pacote de aulas. 20.6 “Média de aulas marcadas por professor”
            exibirá ao Professor uma média calculada com a quantidade de aulas
            marcadas pela plataforma, apresentando também qual foi a quantidade
            mínima marcada para um Professor, e a maior quantidade, para que o
            usuário possa ter uma noção de como ele está se posicionando no
            mercado e como está seu desempenho naquele momento, quando comparado
            com os demais Professores. Os resultados também são separados por
            aula avulsa e por pacote de aulas. 20.7 As informações estatísticas
            serão atualizadas no dia 20 (vinte) de cada mês, refletindo sempre
            os 30 (trinta) dias anteriores. Se houver algum atraso pontual, por
            motivos técnicos, os usuários serão informados. 20.8 As informações
            estatísticas serão coletadas com base nas inserções feitas pelos
            próprios usuários. Se, em alguma localidade, não houver nenhum
            Professor cadastrado, já que nem todas as cidades do Brasil possuem
            academias ou professores habilitados, tal localidade não terá
            informações. Nesse caso, o Professor deverá refazer a busca,
            pautando-se por um bairro ou cidade semelhante, que possa servir de
            parâmetro. 20.9 Serão necessários ao menos 3 (três) usuários na
            localidade para que as informações sejam geradas. 20.10 As
            informações estatísticas se destinam ao uso exclusivo, pessoal e
            intransferível, do usuário que as tenha adquirido ao contratar o
            Plano Premium. 20.11 É vedado ao Professor compartilhar o acesso às
            informações estatísticas, por qualquer meio, ou divulgá-las sem
            autorização do Personal Access, sob pena de responsabilização cível
            e criminal, e exclusão da plataforma. 20.12 As informações
            estatísticas são de uso exclusivo do Personal Access. A título
            acadêmico, de estudo ou outros, o Personal Access poderá autorizar a
            divulgação, desde que citada a fonte e previamente autorizado. 20.13
            Nenhum uso das informações estatísticas, comercial ou não, será
            permitido sem autorização do Personal Access, sob pena de
            responsabilização cível e criminal.
          </Text>

          <Text>
            21 Divulgação e Propaganda 21.1 Ao aceitar os Termos de Uso, o
            Professor concorda em receber e-mails, mensagens de SMS ou por
            outros meios, com conteúdo promocional, de propaganda ou divulgação,
            tanto do Personal Access quanto de outras empresas parceiras. 21.2 O
            conteúdo promocional, de propaganda ou divulgação também poderá ser
            divulgado através do próprio aplicativo, pelo Personal Access ou por
            empresas parceiras.
          </Text>

          <Text>
            22. Responsabilidades e Propriedade Intelectual 22.1 O Professor
            respeitará todos os direitos de propriedade intelectual associados
            ao Personal Access, não podendo se utilizar de nenhuma imagem,
            logomarca, desenhos industriais, informações estatísticas e outros
            materiais protegidos por lei sem a devida autorização; 22.2 O
            Professor reconhece o direito de o Personal Access tomar as medidas
            que considere razoavelmente necessárias ou apropriadas para que o
            presente contrato seja cumprido, incluindo a edição de informações
            que o Professor tenha inserido no Sistema. Nesses casos, o Professor
            será informado sobre a inconsistência, sendo marcado prazo razoável
            para que ele mesmo corrija a informação incorreta. 22.3 O Personal
            Access divulgará às autoridades constituídas, sempre que solicitado,
            requisitado ou quando entender necessário, as informações
            necessárias.
          </Text>

          <Text>
            23. Cancelamento de acesso ao aplicativo 23.1 O Professor só poderá
            cancelar seu acesso ao aplicativo Personal Access ao término do
            período do plano contratado, devendo observar também se houve
            utilização de todas as aulas pendentes. 23.2 O Personal Access
            também poderá a seu exclusivo critério e sem aviso prévio: (i)
            rescindir este contrato e encerrar sua conta caso este viole
            qualquer uma das obrigações assumidas neste acordo; (ii) impedir o
            acesso ao Sistema caso o Professor represente algum risco atual ou
            potencial de responsabilização legal para o Personal Access ou (b)
            pratique de forma reincidente o agendamento seguido de não
            comparecimento proposital ou (iii) descontinuar os serviços
            prestados. 23.3 Quando a conta for encerrada pelo Aluno, pelo
            Professor ou pelo Personal Access, ou ainda nos casos de
            descontinuação dos serviços do aplicativo, será mantido no banco de
            dados as informações por até três meses, para que se ultimem as
            aulas pendentes e eventuais valores a pagar e a receber. 23.4 O
            Professor concorda que qualquer violação sua deste Termos de Uso
            constitui uma prática prejudicial, podendo o Personal Access buscar
            qualquer medida legal para a reparação de seus danos.
          </Text>

          <Text>
            24. Condições Finais 24.1 Estes Termos e Condições constituem o
            acordo integral entre o Professor e o Personal Access, prevalecendo
            sobre quaisquer outros acordos anteriores. 24.2 Se qualquer parte
            deste instrumento for considerada inválida ou inexequível, esta
            deverá ser interpretada de acordo com a lei aplicável de forma a
            refletir, o mais próximo possível, as intenções originais das
            partes, sendo que o restante do contrato permanecerá vigente e
            efetivo. 24.3 O Personal Access não será responsável pelo
            descumprimento de quaisquer obrigações em decorrência de caso
            fortuito, força maior, responsabilidade exclusiva de terceiros ou
            qualquer outra excludente de responsabilidade. 24.4 A comunicação
            entre o Personal Access e o Professor será realizada através do
            e-mail cadastrado no Sistema, sem prejuízo de outros meios. O
            Professor poderá enviar solicitações, críticas e sugestões para o
            e-mail “personalaccess.app@gmail.com”. 24.5 A HSTS CONSULTORIA LTDA,
            inscrita no Cadastro Nacional de Pessoas Jurídicas sob o nº
            38.824.434/0001-89, sociedade de responsabilidade limitada
            estabelecida no Brasil, é a única proprietária do aplicativo e dos
            serviços do Personal Access, incluindo o Sistema, suas interfaces,
            softwares, dados de referência alcançados, marcas registradas,
            informações estatísticas e todos os demais materiais protegidos por
            lei que permitem o funcionamento de nossos serviços. 24.6 O
            Professor não está autorizado a modificar, reproduzir, distribuir,
            criar adaptações ou de qualquer forma explorar o aplicativo Personal
            Access de forma não expressamente autorizada por este instrumento.
          </Text>

          <Text>
            25. Legislação Aplicável e Foro 25.1 Estes Termos e Condições de Uso
            devem ser interpretados de acordo com a legislação da República
            Federativa do Brasil e as partes concordam expressamente com o foro
            da Comarca da cidade de Niterói, Estado do Rio de Janeiro para
            processar toda e qualquer reclamação ou controvérsia referente ao
            presente acordo. Versão 1 - Vigente a partir de: 01/01/2021
          </Text>
        </Content>
      </ContainerScroll>

      <SaveButtonContainer>
        <Button width="85%" onPress={() => goBack()}>
          Li e aceito os termos de uso
        </Button>

        <ButtonRefuse onPress={() => goBack()}>
          <ButtonRefuseLabel>Recusar</ButtonRefuseLabel>
        </ButtonRefuse>
      </SaveButtonContainer>
    </Container>
  );
};

export default TermsOfUse;
