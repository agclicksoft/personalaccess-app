import styled from 'styled-components/native';

export const Container = styled.View`
  background: #ffffff;
  border-radius: 8px;
  margin-bottom: 16px;

  padding: 16px;
`;

export const Settings = styled.View`
  flex-direction: row;
  justify-content: flex-end;
`;

export const ButtonSetting = styled.TouchableOpacity`
  margin-left: 16px;
`;

export const ButtonSettingImage = styled.Image`
  width: 24px;
  height: 24px;
`;

export const Content = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 8px 24px;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'contain',
})`
  margin-right: 32px;
`;

export const Info = styled.View``;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size20};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
`;

export const DataContainer = styled.View`
  flex-direction: row;
`;

export const DataLabel = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size12};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
`;

export const DataText = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size12};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  color: #707070;
`;
