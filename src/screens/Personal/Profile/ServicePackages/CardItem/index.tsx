import React, { useCallback } from 'react';

import { useNavigation } from '@react-navigation/native';

import {
  IconEdit,
  IconCancel,
  IconDumbbellStraight,
  IconPackage,
} from '~/assets/icons';
import { useAuth } from '~/hooks/auth';
import { ProfessionalPlan, User } from '~/models';
import { deletePersonalPlan, getShowPersonalPlans } from '~/services/personal';

import {
  Container,
  Settings,
  ButtonSetting,
  ButtonSettingImage,
  Content,
  Image,
  Info,
  Title,
  DataContainer,
  DataLabel,
  DataText,
} from './styles';

interface CardItemProps {
  item: ProfessionalPlan;
  loadProfessionalServicesPackages: () => void;
}

const CardItem: React.FC<CardItemProps> = ({ item }) => {
  const { user, updatePersonal } = useAuth();
  const { navigate } = useNavigation();

  const handleEditServicePackage = useCallback(
    (servicePackage) => {
      navigate('ServicePackagesEditPrice', { servicePackage });
    },
    [navigate],
  );

  const handleDeleteServicePackage = useCallback(
    async (planId) => {
      try {
        await deletePersonalPlan(planId);

        const response = await getShowPersonalPlans(user.professional.id);

        const formData: User = {
          ...user,
          id: user.id,
          professional: {
            ...user.professional,
            professionalPlans: response,
          },
        };

        await updatePersonal(formData);
      } catch (err) {
        console.log(err);
      }
    },
    [updatePersonal, user],
  );

  return (
    <>
      <Container>
        <Settings>
          <ButtonSetting onPress={() => handleEditServicePackage(item)}>
            <ButtonSettingImage source={IconEdit} />
          </ButtonSetting>

          <ButtonSetting onPress={() => handleDeleteServicePackage(item.id)}>
            <ButtonSettingImage source={IconCancel} />
          </ButtonSetting>
        </Settings>

        <Content>
          <Image
            source={
              item.plan.description === 'Aula avulsa'
                ? IconDumbbellStraight
                : IconPackage
            }
          />

          <Info>
            <Title>{item.plan.description}</Title>

            <DataContainer>
              <DataLabel>Preço: </DataLabel>
              <DataText>{item.price}</DataText>
            </DataContainer>

            <DataContainer>
              <DataLabel>Tipo: </DataLabel>
              <DataText>
                {item.plan.amount}{' '}
                {item.plan.amount > 1 ? 'aulas no mês' : 'única aula'}
              </DataText>
            </DataContainer>
          </Info>
        </Content>
      </Container>
    </>
  );
};

export default CardItem;
