import { FlatList } from 'react-native';

import styled from 'styled-components/native';

import { ProfessionalPlan } from '~/models';

export const Container = styled.View`
  background-color: #f8f8f8;
  flex: 1;
`;

export const NotHavePlanText = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};

  text-align: center;
  margin-top: 64px;
`;

export const ListServicesPackages = styled(
  FlatList as new () => FlatList<ProfessionalPlan>,
).attrs({
  contentContainerStyle: {
    paddingTop: 8,
    paddingHorizontal: 16,
  },
})``;
