import React, { useCallback, useEffect, useState } from 'react';
import { View, ActivityIndicator } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import { ProfessionalPlan } from '~/models';
import formatValue from '~/utils/formatValue';

import { Container, NotHavePlanText, ListServicesPackages } from './styles';

import CardItem from './CardItem';

const ServicePackages: React.FC = () => {
  const { user } = useAuth();
  const { navigate } = useNavigation();

  const [loading, setLoading] = useState(false);
  const [servicePackagesData, setServicePackagesData] = useState<
    ProfessionalPlan[]
  >([]);

  const loadProfessionalServicesPackages = useCallback(async () => {
    setLoading(true);

    const professionalPlansFormatted = user.professional.professionalPlans.map(
      (item) => ({
        ...item,
        price: formatValue(item.price),
      }),
    );

    const responseFormattedByOrder = await professionalPlansFormatted.sort(
      (a, b) => (a.plan.amount > b.plan.amount ? 1 : -1),
    );

    setServicePackagesData(responseFormattedByOrder);

    setLoading(false);
  }, [user.professional.professionalPlans]);

  useEffect(() => {
    loadProfessionalServicesPackages();
  }, [loadProfessionalServicesPackages]);

  const Content: React.FC = () => {
    return servicePackagesData.length >= 1 ? (
      <ListServicesPackages
        data={servicePackagesData}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => (
          <CardItem
            item={item}
            loadProfessionalServicesPackages={loadProfessionalServicesPackages}
          />
        )}
        onRefresh={loadProfessionalServicesPackages}
        refreshing={false}
      />
    ) : (
      <NotHavePlanText>
        Você ainda não possui planos cadastrados.
      </NotHavePlanText>
    );
  };

  return (
    <>
      <Container>
        <HeaderBack
          rightButton
          rightButtonTitle="Novo pacote"
          onPress={() => navigate('ServicePackagesNew')}
        >
          Pacotes de serviços
        </HeaderBack>

        {loading ? (
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator size={64} color="#37153D" />
          </View>
        ) : (
          <Content />
        )}
      </Container>
    </>
  );
};

export default ServicePackages;
