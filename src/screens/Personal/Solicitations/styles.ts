import { FlatList } from 'react-native';

import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const HeaderContainer = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 0 10px 0px;
  padding-top: ${getStatusBarHeight()}px;
`;

export const Title = styled.Text`
  padding-left: 15px;
  font-size: ${({ theme }) => theme.fontSizes.size20};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
`;

export const Divider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
  margin: 15px 0 0;
`;

export const Feed = styled(FlatList)`
  flex: 1;
  width: 100%;
  padding: 0 20px;
`;

export const Card = styled.TouchableOpacity.attrs({
  activeOpacity: 0.5,
})`
  background-color: ${({ theme }) => theme.colors.white};
  border-radius: 8px;
  width: 60%;
  padding: 15px 10px;
  margin: 10px 0;
  border-width: 0.8px;
  border-color: ${({ theme }) => theme.colors.shadow};
  align-self: center;
`;

export const CardTitle = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  text-align: center;
`;
