/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect, useState } from 'react';
import { Linking } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { differenceInYears, parseISO, format, addHours } from 'date-fns';
import { pt } from 'date-fns/locale';
import { FlatList } from 'react-native-gesture-handler';

import {
  IconWhatsApp,
  IconEmail,
  IconPhone,
  IconEmergencyPhone,
  IconPackage,
} from '~/assets/icons';
import { ContainerScroll, HeaderPurple } from '~/components';
import { PersonalClass, Student } from '~/models';
import ParQQuestion from '~/models/ParQQuestion';
import ScheduledService from '~/models/ScheduledServices';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';
import {
  getPaymentStatus,
  getStatus,
  getStatusBG,
  getStatusColor,
} from '~/utils/geStatus';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Wrapper,
  Title,
  Holder,
  Question,
  Answer,
  BodyInfoContainer,
  BodyInfoHolder,
  Card,
  CardContent,
  Image,
  ClassWrapper,
  ClassHolder,
  ClassQuestion,
  ClassAnswer,
  ClassDivider,
  StatusButton,
  StatusButtonText,
  ButtonWrapper,
  TouchableOpacity,
  ButtonImage,
  ShowParQTitle,
  SchedulesTitle,
  SchedulesSubTitle,
  StatusPayment,
  StatusPaymentLabel,
  HeartDocumentWrapper,
  Label,
} from './styles';

import ConfirmationModal from '../ModalConfirmation';
import ModalReschedule from '../ModalReschedule';
import ModalToken from '../ModalToken';

interface Params {
  myClass: PersonalClass;
}

const MultipleClass: React.FC = () => {
  const { params } = useRoute();
  const { myClass } = params as Params;
  const { navigate } = useNavigation();
  const [student, setStudent] = useState<Student>(null);
  const [medicalData, setMedicalData] = useState(null);
  const [showParq, setShowParq] = useState(false);
  const [modal, setModal] = useState(false);
  const [modalReschedule, setModalReschedule] = useState(false);
  const [tokenModal, setTokenModal] = useState(false);
  const [selectedClass, setSelectedClass] = useState<ScheduledService>();
  const [classes, setClasses] = useState<PersonalClass>(myClass);
  const [canceled, setCanceled] = useState(false);

  useEffect(() => {
    async function getStudent() {
      const response = await api.get(`/students/${myClass.student.id}`);

      setStudent(response.data);
      setMedicalData(
        response.data.user?.faqAnswered.filter((item: ParQQuestion) => {
          return item.faq_question_id === 10;
        }),
      );
    }

    async function getClasses() {
      const response = await api.get(`/contracts/${myClass.id}`);

      setClasses(response.data);
      setCanceled(false);
    }

    getStudent();

    if (canceled) {
      getClasses();
    }
  }, [canceled, myClass]);

  const navigateWithStatus = useCallback(
    async (status: string) => {
      if (status === 'CONCLUIDO') {
        navigate('ClassDescriptionView', {
          myClass,
        });
      } else if (status === 'PENDENTE' || status === 'REMARCADO') {
        setModal(!modal);
      } else if (status === 'DISPONIVEL') {
        setTokenModal(!tokenModal);
      } else if (status === 'CONFIRMADO') {
        setModalReschedule(!modalReschedule);
      } else {
        null;
      }
    },
    [modal, modalReschedule, myClass, navigate, tokenModal],
  );

  return (
    <Container>
      <HeaderPurple>Aula</HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{
              uri: myClass.student.user.img_profile ?? PlaceholderImage,
            }}
          />

          <Name>{myClass.student.user.name}</Name>

          <Age>
            {myClass.student.user.birthday
              ? `${differenceInYears(
                  new Date(),
                  parseISO(myClass.student.user.birthday),
                )} anos`
              : 'Idade não informada'}
          </Age>

          <ButtonWrapper>
            <TouchableOpacity
              onPress={() => {
                Linking.canOpenURL('whatsapp://send?text=Oi').then(
                  (supported) => {
                    if (supported) {
                      return Linking.openURL(
                        `whatsapp://send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                      );
                    }
                    return Linking.openURL(
                      `https://api.whatsapp.com/send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                    );
                  },
                );
              }}
            >
              <ButtonImage source={IconWhatsApp} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `mailto:${student.user?.email}?subject=Olá, você me contratou pelo Personal Access!`,
                );
              }}
            >
              <ButtonImage source={IconEmail} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.contact}`);
              }}
            >
              <ButtonImage source={IconPhone} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.emergency_phone}`);
              }}
            >
              <ButtonImage source={IconEmergencyPhone} />
            </TouchableOpacity>
          </ButtonWrapper>

          <TouchableOpacity onPress={() => setShowParq(!showParq)}>
            <ShowParQTitle>
              {showParq ? 'Esconder PAR-Q' : 'Visualizar PAR-Q'}
            </ShowParQTitle>
          </TouchableOpacity>

          {!!showParq && (
            <Wrapper>
              <Title>PAR-Q</Title>

              {student?.user?.faqAnswered?.length > 0 ? (
                <>
                  <BodyInfoContainer>
                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 1 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} kg</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}

                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 2 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} m</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}
                  </BodyInfoContainer>

                  {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                    return item.faqQuestion.id > 2 && item.answered !== null ? (
                      <Holder key={item.id}>
                        <Question>{item.faqQuestion.question}</Question>
                        <Answer>
                          {item.answered === 'false'
                            ? item.answered === 'false'
                              ? 'Não'
                              : item.answered
                            : item.answered === 'true'
                            ? 'Sim'
                            : item.answered}
                        </Answer>
                      </Holder>
                    ) : null;
                  })}

                  {medicalData[0]?.attachments[0]?.path && (
                    <HeartDocumentWrapper
                      onPress={() =>
                        Linking.openURL(medicalData[0].attachments[0].path)
                      }
                    >
                      <Label>Visualizar atestado médico</Label>

                      <Ionicons
                        name="ios-cloud-download"
                        size={24}
                        color="#37153D"
                        style={{ marginTop: 10, marginLeft: 10 }}
                      />
                    </HeartDocumentWrapper>
                  )}
                </>
              ) : (
                <Title>Não preenchido até momento</Title>
              )}
            </Wrapper>
          )}

          <Divider />

          <SchedulesTitle>Agendamentos</SchedulesTitle>

          <SchedulesSubTitle>
            {myClass?.amount_used} de {myClass?.amount} aulas contratadas
          </SchedulesSubTitle>

          <StatusPayment>
            Pagamento:{' '}
            <StatusPaymentLabel>
              {getPaymentStatus(myClass.method_payment)}
            </StatusPaymentLabel>
          </StatusPayment>

          <FlatList
            showsVerticalScrollIndicator={false}
            data={classes?.scheduledServices?.map((e) => e)}
            keyExtractor={(option) => String(option.id)}
            renderItem={({ item }) => (
              <Card
                key={item.id}
                onPress={async () => {
                  if (item.status === 'ANDAMENTO') {
                    navigate('FinishClass', {
                      myClass: item,
                      contract: myClass,
                      student,
                    });
                  } else {
                    await setSelectedClass(item);
                    navigateWithStatus(item.status);
                  }
                }}
              >
                <CardContent>
                  <Image source={IconPackage} />

                  <ClassWrapper>
                    <ClassHolder>
                      <ClassQuestion>Data/hora: </ClassQuestion>
                      <ClassAnswer>
                        {format(
                          addHours(parseISO(item.date), 3),
                          "dd/MM/yy '-' HH:mm ",
                          {
                            locale: pt,
                          },
                        )}
                      </ClassAnswer>
                    </ClassHolder>

                    <ClassHolder>
                      <ClassQuestion>Local: </ClassQuestion>
                      <ClassAnswer>{item.establishment.name}</ClassAnswer>
                    </ClassHolder>
                  </ClassWrapper>
                </CardContent>

                <ClassDivider />

                <StatusButton
                  style={{
                    backgroundColor: getStatusBG(item.status),
                  }}
                >
                  <StatusButtonText
                    style={{
                      color: getStatusColor(item.status),
                    }}
                  >
                    {getStatus(item.status)}
                  </StatusButtonText>
                </StatusButton>
              </Card>
            )}
          />
        </FormContainer>
      </ContainerScroll>

      <ConfirmationModal
        isVisible={modal}
        schedule={selectedClass}
        student={myClass.student}
        closeModal={() => setModal(!modal)}
        setCanceled={setCanceled}
      />

      <ModalReschedule
        isVisible={modalReschedule}
        student={myClass.student}
        schedule={selectedClass}
        closeModal={() => setModalReschedule(!modalReschedule)}
        setCanceled={setCanceled}
      />

      <ModalToken
        isVisible={tokenModal}
        id={selectedClass?.id}
        student={myClass.student}
        closeModal={() => setTokenModal(!tokenModal)}
        setRight={() => {
          setCanceled(true);

          navigate('FinishClass', {
            myClass: selectedClass,
            contract: myClass,
            student,
          });
        }}
      />
    </Container>
  );
};

export default MultipleClass;
