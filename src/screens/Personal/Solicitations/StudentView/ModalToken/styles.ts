import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: ${({ theme }) => theme.colors.background};
  height: 330px;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  margin: 0 20px;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size22};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  padding: 15px 45px 5px;
  text-align: center;
`;

export const TouchableOpacity = styled.TouchableOpacity`
  margin-top: 20px;
`;

export const Subtitle = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  font-size: 18px;
  color: ${({ theme }) => theme.colors.gray};
  text-align: center;
`;
