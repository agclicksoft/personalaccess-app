/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, Alert, View } from 'react-native';

import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { showMessage } from 'react-native-flash-message';
import Modal from 'react-native-modal';
import * as Yup from 'yup';

import { Button, MaskedInput } from '~/components';
import { Student } from '~/models';
import api from '~/services/api';
import { getValidationErrors } from '~/utils/getValidationErrors';

import { Container, Title, Subtitle, TouchableOpacity } from './styles';

interface Props {
  isVisible: boolean;
  id: number;
  student: Student;
  closeModal: any;
  setRight?: any;
}

const ModalToken: React.FC<Props> = ({
  isVisible,
  closeModal,
  id,
  student,
  setRight,
}) => {
  const formRef = useRef<FormHandles>(null);
  const [loading, setLoading] = useState(false);
  const [studentCpf] = useState<string>(
    student?.user?.cpf?.substr(student?.user?.cpf?.length - 3),
  );

  const handleSubmit = useCallback(
    async (data) => {
      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          cpf: Yup.string().required('CPF é obrigatório'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        console.log(data.cpf);
        console.log(studentCpf);

        if (studentCpf !== data.cpf) {
          setLoading(false);

          Alert.alert(
            'Números incorretos, favor verificar e tentar novamente.',
          );

          return;
        }

        const response = await api.post(
          `/scheduled-services/${id}/give-present/${student.id}`,
          {
            tk_auth_service: data.cpf,
          },
        );

        if (response.status === 200) {
          await api.put(`/scheduled-services/${id}/status`, {
            status: 'ANDAMENTO',
          });

          setRight();
          closeModal();
          setLoading(false);
        }
      } catch (err) {
        setLoading(false);
        console.log(err);

        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);
        }

        showMessage({
          type: 'danger',
          message: 'Erro ao iniciar aula.',
          description: 'Verifique os campos digitados.',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [closeModal, id, setRight, student.id, studentCpf],
  );

  return (
    <Modal
      backdropOpacity={0.5}
      isVisible={isVisible}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      useNativeDriver
      hideModalContentWhileAnimating
      propagateSwipe
      style={{
        width: '100%',
        height: '50%',
        margin: 0,
        padding: 0,
      }}
    >
      <>
        <Container>
          <Title>
            Para iniciar a aula, digite os 3 últimos números do CPF de seu
            aluno:
          </Title>

          <Form ref={formRef} onSubmit={handleSubmit}>
            <MaskedInput
              name="cpf"
              type="custom"
              options={{
                mask: '999',
              }}
              maxLength={3}
              placeholder="3 últimos números do CPF do aluno"
              initialValue=""
              keyboardType="number-pad"
              style={{
                textAlign: 'center',
              }}
            />
          </Form>

          <Button
            style={{ backgroundColor: '#F76E1E' }}
            onPress={() => formRef.current?.submitForm()}
          >
            Confirmar
          </Button>

          <TouchableOpacity onPress={closeModal}>
            <Subtitle>Voltar</Subtitle>
          </TouchableOpacity>
        </Container>
      </>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Modal>
  );
};

export default ModalToken;
