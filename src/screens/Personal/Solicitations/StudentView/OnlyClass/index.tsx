/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Linking } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { differenceInYears, parseISO, format, addHours } from 'date-fns';
import { pt } from 'date-fns/locale';

import {
  IconWhatsApp,
  IconEmail,
  IconPhone,
  IconEmergencyPhone,
} from '~/assets/icons';
import { ContainerScroll, HeaderPurple } from '~/components';
import { PersonalClass } from '~/models';
import ParQQuestion from '~/models/ParQQuestion';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';
import {
  getPaymentStatus,
  getStatus,
  getStatusBG,
  getStatusColor,
} from '~/utils/geStatus';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Wrapper,
  Title,
  Holder,
  Question,
  Answer,
  BodyInfoContainer,
  BodyInfoHolder,
  Card,
  CardContent,
  ClassWrapper,
  ClassHolder,
  ClassQuestion,
  ClassAnswer,
  ClassDivider,
  StatusButton,
  StatusButtonText,
  ButtonWrapper,
  TouchableOpacity,
  ButtonImage,
  ShowParQTitle,
  StatusPayment,
  StatusPaymentLabel,
  HeartDocumentWrapper,
  Label,
} from './styles';

import ConfirmationModal from '../ModalConfirmation';
import ModalReschedule from '../ModalReschedule';
import ModalToken from '../ModalToken';

interface Params {
  myClass: PersonalClass;
}

const OnlyClass: React.FC = () => {
  const { params } = useRoute();
  const { myClass } = params as Params;
  const { navigate } = useNavigation();
  const [student, setStudent] = useState(null);
  const [medicalData, setMedicalData] = useState(null);
  const [showParq, setShowParq] = useState(false);
  const [modal, setModal] = useState(false);
  const [modalReschedule, setModalReschedule] = useState(false);
  const [selectedClass, setSelectedClass] = useState<PersonalClass>(myClass);
  const [canceled, setCanceled] = useState(false);
  const [tokenModal, setTokenModal] = useState(false);

  useEffect(() => {
    async function getStudent() {
      const response = await api.get(`/students/${myClass.student.id}`);

      setStudent(response.data);
      setMedicalData(
        response.data.user?.faqAnswered.filter((item: ParQQuestion) => {
          return item.faq_question_id === 10;
        }),
      );
    }

    async function getMyClass() {
      const response = await api.get(`/contracts/${myClass.id}`);

      setSelectedClass(response.data);
    }

    getStudent();

    if (canceled) {
      getMyClass();
    }
  }, [canceled, myClass]);

  const formattedDate = format(
    addHours(parseISO(myClass.scheduledServices[0].date), 3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Data/hora: ', answer: formattedDate },
    {
      id: 2,
      question: 'Local: ',
      answer: myClass.scheduledServices[0].establishment.name,
    },
    {
      id: 3,
      question: 'Pacote: ',
      answer: myClass.professionalPlan.plan.description,
    },
    // { id: 4, question: 'Pagamento: ', answer: myClass.method_payment },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <ClassHolder key={item.id}>
          <ClassQuestion>{item.question}</ClassQuestion>
          <ClassAnswer>{item.answer}</ClassAnswer>
        </ClassHolder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const navigateWithStatus = useCallback(
    async (status: string) => {
      if (status === 'CONCLUIDO') {
        navigate('ClassDescriptionView', {
          myClass,
        });
      } else if (status === 'PENDENTE' || status === 'REMARCADO') {
        setModal(!modal);
      } else if (status === 'DISPONIVEL') {
        setTokenModal(!tokenModal);
      } else if (status === 'CONFIRMADO') {
        setModalReschedule(!modalReschedule);
      } else {
        null;
      }
    },
    [modal, modalReschedule, myClass, navigate, tokenModal],
  );

  return (
    <Container>
      <HeaderPurple>Aula</HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{
              uri: myClass.student.user.img_profile ?? PlaceholderImage,
            }}
          />

          <Name>{myClass.student.user.name}</Name>

          <Age>
            {myClass.student.user.birthday
              ? `${differenceInYears(
                  new Date(),
                  parseISO(myClass.student.user.birthday),
                )} anos`
              : 'Idade não informada'}
          </Age>

          <Divider />

          {selectedClass.scheduledServices[0].status !== 'CANCELADO' &&
            selectedClass.scheduledServices[0].status !== 'RECUSADO' && (
              <StatusPayment>
                Pagamento:{' '}
                <StatusPaymentLabel>
                  {getPaymentStatus(selectedClass?.method_payment)}
                </StatusPaymentLabel>
              </StatusPayment>
            )}

          <Card
            onPress={() => {
              if (selectedClass.scheduledServices[0].status === 'ANDAMENTO') {
                navigate('FinishClass', {
                  myClass: selectedClass.scheduledServices[0],
                  contract: selectedClass,
                  student,
                });
              } else {
                navigateWithStatus(selectedClass.scheduledServices[0].status);
              }
            }}
          >
            <CardContent>
              <ClassWrapper>{optionsMemo}</ClassWrapper>
            </CardContent>

            <ClassDivider />

            <StatusButton
              style={{
                backgroundColor: getStatusBG(
                  selectedClass.scheduledServices[0].status,
                ),
              }}
            >
              <StatusButtonText
                style={{
                  color: getStatusColor(
                    selectedClass.scheduledServices[0].status,
                  ),
                }}
              >
                {getStatus(selectedClass.scheduledServices[0].status)}
              </StatusButtonText>
            </StatusButton>
          </Card>

          <ButtonWrapper>
            <TouchableOpacity
              onPress={() => {
                Linking.canOpenURL('whatsapp://send?text=Oi').then(
                  (supported) => {
                    if (supported) {
                      return Linking.openURL(
                        `whatsapp://send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                      );
                    }
                    return Linking.openURL(
                      `https://api.whatsapp.com/send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                    );
                  },
                );
              }}
            >
              <ButtonImage source={IconWhatsApp} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `mailto:${student.user?.email}?subject=Olá, você me contratou pelo Personal Access!`,
                );
              }}
            >
              <ButtonImage source={IconEmail} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.contact}`);
              }}
            >
              <ButtonImage source={IconPhone} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.emergency_phone}`);
              }}
            >
              <ButtonImage source={IconEmergencyPhone} />
            </TouchableOpacity>
          </ButtonWrapper>

          <TouchableOpacity onPress={() => setShowParq(!showParq)}>
            <ShowParQTitle>
              {showParq ? 'Esconder PAR-Q' : 'Visualizar PAR-Q'}
            </ShowParQTitle>
          </TouchableOpacity>

          {!!showParq && (
            <Wrapper>
              <Title>PAR-Q</Title>

              {student?.user?.faqAnswered?.length > 0 ? (
                <>
                  <BodyInfoContainer>
                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 1 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} kg</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}

                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 2 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} m</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}
                  </BodyInfoContainer>

                  {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                    return item.faqQuestion.id > 2 && item.answered !== null ? (
                      <Holder key={item.id}>
                        <Question>{item.faqQuestion.question}</Question>
                        <Answer>
                          {item.answered === 'false'
                            ? item.answered === 'false'
                              ? 'Não'
                              : item.answered
                            : item.answered === 'true'
                            ? 'Sim'
                            : item.answered}
                        </Answer>
                      </Holder>
                    ) : null;
                  })}

                  {medicalData[0]?.attachments[0]?.path && (
                    <HeartDocumentWrapper
                      onPress={() =>
                        Linking.openURL(medicalData[0].attachments[0].path)
                      }
                    >
                      <Label>Visualizar atestado médico</Label>

                      <Ionicons
                        name="ios-cloud-download"
                        size={24}
                        color="#37153D"
                        style={{ marginTop: 10, marginLeft: 10 }}
                      />
                    </HeartDocumentWrapper>
                  )}
                </>
              ) : (
                <Title>Não preenchido até momento</Title>
              )}
            </Wrapper>
          )}
        </FormContainer>
      </ContainerScroll>

      <ConfirmationModal
        isVisible={modal}
        schedule={myClass.scheduledServices[0]}
        student={myClass.student}
        closeModal={() => setModal(!modal)}
        setCanceled={setCanceled}
      />

      <ModalReschedule
        isVisible={modalReschedule}
        student={myClass.student}
        schedule={myClass.scheduledServices[0]}
        closeModal={() => setModalReschedule(!modalReschedule)}
        setCanceled={setCanceled}
      />

      <ModalToken
        isVisible={tokenModal}
        id={myClass?.scheduledServices[0].id}
        student={myClass?.student}
        closeModal={() => setTokenModal(!tokenModal)}
        setRight={() => {
          setCanceled(true);

          navigate('FinishClass', {
            myClass: myClass?.scheduledServices[0],
            contract: myClass,
            student,
          });
        }}
      />
    </Container>
  );
};

export default OnlyClass;
