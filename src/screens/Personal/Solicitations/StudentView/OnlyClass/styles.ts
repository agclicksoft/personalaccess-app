import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.purple};
`;

export const FormContainer = styled.View`
  flex: 1;
  width: 100%;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.background};
  border-top-left-radius: 31px;
  border-top-right-radius: 31px;
  padding: 30px 0;
`;

export const ProfileImage = styled.Image`
  width: 170px;
  height: 170px;
  border-radius: 85px;
  margin-bottom: 8px;
`;

export const Name = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size22};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  text-transform: capitalize;
`;

export const Age = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.black};
`;

export const Divider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
  margin: 35px 0 15px;
`;

export const Wrapper = styled.View`
  background-color: ${({ theme }) => theme.colors.white};
  align-items: center;
  justify-content: center;
  border-radius: 15px;
  padding: 20px;
  width: 85%;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
  margin: 15px 0 5px;
`;

export const Holder = styled.View`
  margin: 15px 0 5px;
  align-items: center;
`;

export const Question = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.grayDark};
  text-align: center;
`;

export const Answer = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  color: ${({ theme }) => theme.colors.grayDark};
  margin-top: 3px;
  text-align: center;
`;

export const BodyInfoContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const BodyInfoHolder = styled.View`
  flex-direction: row;
  margin: 15px 0 5px;
  width: 95px;
  align-items: center;
  justify-content: center;
`;

export const Card = styled.TouchableOpacity.attrs({
  activeOpacity: 0.5,
})`
  background-color: ${({ theme }) => theme.colors.white};
  align-items: center;
  justify-content: center;
  border-radius: 15px;
  padding: 20px;
  width: 85%;
`;

export const CardContent = styled.View`
  flex-direction: row;
  justify-content: center;
  width: 100%;
`;

export const ClassWrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  flex-shrink: 1;
`;

export const ClassHolder = styled.View`
  flex: 1;
  width: 100%;
  flex-direction: row;
  justify-content: center;
`;

export const ClassQuestion = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
`;

export const ClassAnswer = styled.Text`
  color: ${({ theme }) => theme.colors.blackLight};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  text-transform: capitalize;
`;

export const ClassDivider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
  margin: 15px 0 0;
`;

export const StatusButton = styled.View`
  width: 70%;
  height: 35px;
  border-radius: 30px;
  margin: 25px 0 0;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

export const StatusButtonText = styled.Text`
  color: ${({ theme }) => theme.colors.blackLight};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
`;

export const ButtonWrapper = styled.View`
  flex-direction: row;
`;

export const TouchableOpacity = styled.TouchableOpacity``;

export const ButtonImage = styled.Image`
  width: 40px;
  height: 40px;
  margin: 20px 5px;
`;

export const ShowParQTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
  margin: 5px 0 25px;
  text-decoration: underline;
  text-decoration-color: ${({ theme }) => theme.colors.orange};
`;

export const StatusPayment = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  text-transform: capitalize;
  margin-bottom: 20px;
`;

export const StatusPaymentLabel = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.orange};
  text-transform: capitalize;
  margin-top: 20px;
`;

export const HeartDocumentWrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 10px 0;
`;

export const Label = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  margin: 12px 0 0 16px;
`;
