/* eslint-disable no-nested-ternary */
import React, { useEffect, useMemo, useState } from 'react';
import { Linking } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { differenceInYears, parseISO, format, addHours } from 'date-fns';
import { pt } from 'date-fns/locale';

import {
  IconWhatsApp,
  IconEmail,
  IconPhone,
  IconEmergencyPhone,
} from '~/assets/icons';
import { ContainerScroll } from '~/components';
import { PersonalClass } from '~/models';
import ParQQuestion from '~/models/ParQQuestion';
import TrainingDescription from '~/models/TrainingDescription';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';

import {
  Container,
  HeaderPurple,
  HeaderWrapper,
  HeaderTitle,
  GenericButton,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Wrapper,
  Title,
  Holder,
  Question,
  Answer,
  BodyInfoContainer,
  BodyInfoHolder,
  Card,
  CardContent,
  ClassWrapper,
  ClassHolder,
  ClassQuestion,
  ClassAnswer,
  ButtonWrapper,
  TouchableOpacity,
  ButtonImage,
  ShowParQTitle,
  SchedulesTitle,
  TrainingTitle,
  TrainingCardContent,
  TrainingDescriptionText,
} from './styles';

interface Params {
  myClass: PersonalClass;
}

const ClassDescriptionView: React.FC = () => {
  const { params } = useRoute();
  const { myClass } = params as Params;
  const { navigate } = useNavigation();
  const [student, setStudent] = useState(null);
  const [showParq, setShowParq] = useState(false);
  const [trainingDetails, setTrainingDetails] = useState<TrainingDescription>(
    null,
  );

  useEffect(() => {
    async function getStudent() {
      const response = await api.get(`/students/${myClass.student.id}`);

      setStudent(response.data);
    }

    async function getTrainingDetails() {
      const response = await api.get(
        `/scheduled-services/${myClass.scheduledServices[0].id}`,
      );

      setTrainingDetails(response.data.trainingDescription);
    }

    getStudent();
    getTrainingDetails();
  }, [myClass]);

  const formattedDate = format(
    addHours(parseISO(myClass.scheduledServices[0].date), 3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Data/hora: ', answer: formattedDate },
    {
      id: 2,
      question: 'Local: ',
      answer: myClass.scheduledServices[0].establishment.name,
    },
    {
      id: 3,
      question: 'Pacote: ',
      answer: myClass.professionalPlan.plan.description,
    },
    // { id: 4, question: 'Pagamento: ', answer: myClass.method_payment },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <ClassHolder key={item.id}>
          <ClassQuestion>{item.question}</ClassQuestion>
          <ClassAnswer>{item.answer}</ClassAnswer>
        </ClassHolder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <Container>
      <HeaderPurple>
        <GenericButton onPress={() => navigate('Class')}>
          <HeaderWrapper>
            <Ionicons name="ios-arrow-round-back" size={24} color="#fff" />
            <HeaderTitle>Aula concluída</HeaderTitle>
          </HeaderWrapper>
        </GenericButton>
      </HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{
              uri: myClass.student.user.img_profile ?? PlaceholderImage,
            }}
          />

          <Name>{myClass.student.user.name}</Name>

          <Age>
            {myClass.student.user.birthday
              ? `${differenceInYears(
                  new Date(),
                  parseISO(myClass.student.user.birthday),
                )} anos`
              : 'Idade não informada'}
          </Age>

          <Divider />

          <Card onPress={() => null}>
            <CardContent>
              <ClassWrapper>{optionsMemo}</ClassWrapper>
            </CardContent>
          </Card>

          <ButtonWrapper>
            <TouchableOpacity
              onPress={() => {
                Linking.canOpenURL('whatsapp://send?text=Oi').then(
                  (supported) => {
                    if (supported) {
                      return Linking.openURL(
                        `whatsapp://send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                      );
                    }
                    return Linking.openURL(
                      `https://api.whatsapp.com/send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                    );
                  },
                );
              }}
            >
              <ButtonImage source={IconWhatsApp} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `mailto:${student.user?.email}?subject=Olá, você me contratou pelo Personal Access!`,
                );
              }}
            >
              <ButtonImage source={IconEmail} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.contact}`);
              }}
            >
              <ButtonImage source={IconPhone} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.emergency_phone}`);
              }}
            >
              <ButtonImage source={IconEmergencyPhone} />
            </TouchableOpacity>
          </ButtonWrapper>

          <TouchableOpacity onPress={() => setShowParq(!showParq)}>
            <ShowParQTitle>
              {showParq ? 'Esconder PAR-Q' : 'Visualizar PAR-Q'}
            </ShowParQTitle>
          </TouchableOpacity>

          {!!showParq && (
            <Wrapper>
              <Title>PAR-Q</Title>

              {student?.user?.faqAnswered?.length > 0 ? (
                <>
                  <BodyInfoContainer>
                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 1 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} kg</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}

                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 2 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} m</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}
                  </BodyInfoContainer>

                  {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                    return item.faqQuestion.id > 2 && item.answered !== null ? (
                      <Holder key={item.id}>
                        <Question>{item.faqQuestion.question}</Question>
                        <Answer>
                          {item.answered === 'false'
                            ? item.answered === 'false'
                              ? 'Não'
                              : item.answered
                            : item.answered === 'true'
                            ? 'Sim'
                            : item.answered}
                        </Answer>
                      </Holder>
                    ) : null;
                  })}
                </>
              ) : (
                <Title>Não preenchido até momento</Title>
              )}
            </Wrapper>
          )}

          <Divider />

          <SchedulesTitle>Descrição de treino</SchedulesTitle>

          {trainingDetails && (
            <Card onPress={() => null}>
              <TrainingCardContent>
                <TrainingTitle>Grupamentos:</TrainingTitle>
                <TrainingDescriptionText>
                  {trainingDetails.groupings.split(',').join(', ') ??
                    'Não informado'}
                  .
                </TrainingDescriptionText>

                <TrainingTitle>Modalidade:</TrainingTitle>
                <TrainingDescriptionText>
                  {trainingDetails.modality.split(',').join(', ') ??
                    'Não informado'}
                  .
                </TrainingDescriptionText>

                <TrainingTitle>Observações:</TrainingTitle>
                <TrainingDescriptionText>
                  {trainingDetails.note ?? 'Não informado'}
                </TrainingDescriptionText>
              </TrainingCardContent>
            </Card>
          )}

          {!trainingDetails && (
            <Card onPress={() => null}>
              <TrainingCardContent>
                <TrainingTitle>Treinamento não localizado.</TrainingTitle>
              </TrainingCardContent>
            </Card>
          )}
        </FormContainer>
      </ContainerScroll>
    </Container>
  );
};

export default ClassDescriptionView;
