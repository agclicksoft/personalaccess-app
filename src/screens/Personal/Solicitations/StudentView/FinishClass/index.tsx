/* eslint-disable no-nested-ternary */
import React, { useCallback, useMemo, useState } from 'react';
import { Alert, Linking } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import { differenceInYears, parseISO, format, addHours } from 'date-fns';
import { pt } from 'date-fns/locale';

import {
  IconWhatsApp,
  IconEmail,
  IconPhone,
  IconEmergencyPhone,
} from '~/assets/icons';
import { ContainerScroll, HeaderPurple } from '~/components';
import { PersonalClass, Student } from '~/models';
import ParQQuestion from '~/models/ParQQuestion';
import ScheduledService from '~/models/ScheduledServices';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';
import { getStatusBG, getStatusColor } from '~/utils/geStatus';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Wrapper,
  Title,
  Holder,
  Question,
  Answer,
  BodyInfoContainer,
  BodyInfoHolder,
  Card,
  CardContent,
  ClassWrapper,
  ClassHolder,
  ClassQuestion,
  ClassAnswer,
  ClassDivider,
  StatusButton,
  StatusButtonText,
  ButtonWrapper,
  TouchableOpacity,
  ButtonImage,
  ShowParQTitle,
} from './styles';

interface Params {
  myClass: ScheduledService;
  student: Student;
  contract: PersonalClass;
}

const FinishClass: React.FC = () => {
  const { params } = useRoute();
  const { myClass, student, contract } = params as Params;
  const { navigate } = useNavigation();
  const [showParq, setShowParq] = useState(false);
  const [modal, setModal] = useState(false);

  const formattedDate = format(
    addHours(parseISO(myClass.date), 3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Data/hora: ', answer: formattedDate },
    {
      id: 2,
      question: 'Local: ',
      answer: myClass.establishment.name,
    },
    {
      id: 3,
      question: 'Pacote: ',
      answer: contract.professionalPlan.plan.description,
    },
    // { id: 4, question: 'Pagamento: ', answer: myClass.method_payment },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <ClassHolder key={item.id}>
          <ClassQuestion>{item.question}</ClassQuestion>
          <ClassAnswer>{item.answer}</ClassAnswer>
        </ClassHolder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <Container>
      <HeaderPurple>Iniciar aula</HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{
              uri: student.user.img_profile ?? PlaceholderImage,
            }}
          />

          <Name>{student.user.name}</Name>

          <Age>
            {student.user.birthday
              ? `${differenceInYears(
                  new Date(),
                  parseISO(student.user.birthday),
                )} anos`
              : 'Idade não informada'}
          </Age>

          <Divider />

          <Card
            onPress={() =>
              navigate('ClassDescription', {
                myClass,
                contract,
                student,
              })
            }
          >
            <CardContent>
              <ClassWrapper>{optionsMemo}</ClassWrapper>
            </CardContent>

            <ClassDivider />

            <StatusButton
              style={{
                backgroundColor: getStatusBG(myClass.status),
              }}
            >
              <StatusButtonText
                style={{
                  color: getStatusColor(myClass.status),
                }}
              >
                Concluir aula
              </StatusButtonText>
            </StatusButton>
          </Card>

          <ButtonWrapper>
            <TouchableOpacity
              onPress={() => {
                Linking.canOpenURL('whatsapp://send?text=Oi').then(
                  (supported) => {
                    if (supported) {
                      return Linking.openURL(
                        `whatsapp://send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                      );
                    }
                    return Linking.openURL(
                      `https://api.whatsapp.com/send?phone=55${student?.user.contact}&text=Olá, você me contratou no Personal Access, tudo bem?`,
                    );
                  },
                );
              }}
            >
              <ButtonImage source={IconWhatsApp} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `mailto:${student.user?.email}?subject=Olá, você me contratou pelo Personal Access!`,
                );
              }}
            >
              <ButtonImage source={IconEmail} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.contact}`);
              }}
            >
              <ButtonImage source={IconPhone} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${student.user?.emergency_phone}`);
              }}
            >
              <ButtonImage source={IconEmergencyPhone} />
            </TouchableOpacity>
          </ButtonWrapper>

          <TouchableOpacity onPress={() => setShowParq(!showParq)}>
            <ShowParQTitle>
              {showParq ? 'Esconder PAR-Q' : 'Visualizar PAR-Q'}
            </ShowParQTitle>
          </TouchableOpacity>

          {!!showParq && (
            <Wrapper>
              <Title>PAR-Q</Title>

              {student?.user?.faqAnswered?.length > 0 ? (
                <>
                  <BodyInfoContainer>
                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 1 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} kg</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}

                    {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                      return item.faqQuestion.id === 2 ? (
                        <BodyInfoHolder key={item.id}>
                          <Question>{item.faqQuestion.question}: </Question>
                          <Answer>{item.answered} m</Answer>
                        </BodyInfoHolder>
                      ) : null;
                    })}
                  </BodyInfoContainer>

                  {student?.user?.faqAnswered?.map((item: ParQQuestion) => {
                    return item.faqQuestion.id > 2 && item.answered !== null ? (
                      <Holder key={item.id}>
                        <Question>{item.faqQuestion.question}</Question>
                        <Answer>
                          {item.answered === 'false'
                            ? item.answered === 'false'
                              ? 'Não'
                              : item.answered
                            : item.answered === 'true'
                            ? 'Sim'
                            : item.answered}
                        </Answer>
                      </Holder>
                    ) : null;
                  })}
                </>
              ) : (
                <Title>Não preenchido até momento</Title>
              )}
            </Wrapper>
          )}
        </FormContainer>
      </ContainerScroll>
    </Container>
  );
};

export default FinishClass;
