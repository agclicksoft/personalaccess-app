/* eslint-disable no-nested-ternary */
import React, { useCallback, useMemo, useRef, useState } from 'react';
import { View, ActivityIndicator } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { showMessage } from 'react-native-flash-message';

import { Button, ContainerScroll, HeaderBack, Input } from '~/components';
import { PersonalClass, Student } from '~/models';
import ScheduledService from '~/models/ScheduledServices';
import api from '~/services/api';

import {
  Container,
  Content,
  CheckboxContainer,
  Checkbox,
  Label,
  Title,
  FormWrapper,
  ButtonWrapper,
} from './styles';

import { groupOptionsList, modalityOptionsList } from './constants';

interface Params {
  myClass: ScheduledService;
  student: Student;
  contract: PersonalClass;
}

const ClassDescription: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { params } = useRoute();
  const { myClass, student, contract } = params as Params;
  const { navigate } = useNavigation();
  const [loading, setLoading] = useState(false);
  const [groupOptions, setGroupOptions] = useState([]);
  const [modalitiesOptions, setModalitiesOptions] = useState([]);

  const handleSubmit = useCallback(
    async (data) => {
      setLoading(true);
      try {
        const response = await api.post(
          `/scheduled/${myClass.id}/description`,
          {
            groupings: groupOptions
              .map((e) => {
                return e;
              })
              .toString(),
            modality: modalitiesOptions
              .map((e) => {
                return e;
              })
              .toString(),
            note: data.observation,
          },
        );

        console.log(response.data);

        await api.put(`scheduled-services/${myClass.id}/status`, {
          status: 'CONCLUIDO',
        });

        setLoading(false);

        showMessage({
          type: 'success',
          message: 'Treinamento cadastrado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
        });

        navigate('ClassDescriptionView', {
          myClass: contract,
        });
      } catch (err) {
        setLoading(false);

        console.log(err?.response?.data);

        showMessage({
          type: 'danger',
          message: 'Erro inesperado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [contract, groupOptions, modalitiesOptions, myClass.id, navigate],
  );

  const handleSelectGroups = useCallback(
    async (item) => {
      let newArr = [];

      if (!groupOptions.includes(item)) {
        newArr = [...groupOptions, item];
      } else {
        newArr = groupOptions.filter((a) => a !== item);
      }

      setGroupOptions(newArr);

      console.log(groupOptions);
    },
    [groupOptions],
  );

  const groupsOptionsMemo = useMemo(
    () =>
      groupOptionsList.map((item) => (
        <CheckboxContainer
          key={item.id}
          onPress={() => handleSelectGroups(item.description)}
        >
          <Checkbox isSelected={groupOptions.includes(item.description)} />
          <Label>{item.description}</Label>
        </CheckboxContainer>
      )),
    [handleSelectGroups, groupOptions],
  );

  const handleSelectModalities = useCallback(
    async (item) => {
      let newArr = [];

      if (!modalitiesOptions.includes(item)) {
        newArr = [...modalitiesOptions, item];
      } else {
        newArr = modalitiesOptions.filter((a) => a !== item);
      }

      setModalitiesOptions(newArr);

      console.log(modalitiesOptions);
    },
    [modalitiesOptions],
  );

  const modalitiesOptionsMemo = useMemo(
    () =>
      modalityOptionsList.map((item) => (
        <CheckboxContainer
          key={item.id}
          onPress={() => handleSelectModalities(item.description)}
        >
          <Checkbox isSelected={modalitiesOptions.includes(item.description)} />
          <Label>{item.description}</Label>
        </CheckboxContainer>
      )),
    [handleSelectModalities, modalitiesOptions],
  );

  return (
    <Container>
      <HeaderBack>Descrição de treino</HeaderBack>

      <ContainerScroll>
        <Content>
          <Title>Grupamentos:</Title>
          {groupsOptionsMemo}

          <Title>Modalidade:</Title>
          {modalitiesOptionsMemo}

          <Form ref={formRef} onSubmit={handleSubmit}>
            <Title>Observações:</Title>

            <FormWrapper>
              <Input
                name="observation"
                placeholder="Observações"
                autoCapitalize="words"
                style={{
                  textAlign: 'left',
                  textAlignVertical: 'top',
                  alignSelf: 'flex-start',
                  marginTop: 10,
                  borderRadius: 30,
                  width: '100%',
                  height: '100%',
                }}
                height={250}
                multiline
              />
            </FormWrapper>

            <ButtonWrapper>
              <Button onPress={() => formRef.current?.submitForm()}>
                Salvar
              </Button>
            </ButtonWrapper>
          </Form>
        </Content>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default ClassDescription;
