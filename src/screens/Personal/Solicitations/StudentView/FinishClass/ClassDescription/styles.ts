import styled, { css } from 'styled-components/native';

interface CheckboxProps {
  isSelected: boolean;
}

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Content = styled.View`
  padding-bottom: 80px;
  width: 100%;
`;

export const FormWrapper = styled.View`
  padding: 0 25px;
  width: 100%;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size16};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.grayDark};
  padding: 20px 25px 5px;
`;

export const CheckboxContainer = styled.TouchableOpacity`
  flex-direction: row;
  padding: 10px 24px;
`;

export const Checkbox = styled.View<CheckboxProps>`
  width: 24px;
  height: 24px;
  align-items: center;
  background: transparent;
  border: 1px solid #afc1c4;
  border-radius: 4px;
  margin-right: 12px;

  /* border-radius: 12px; */

  ${({ isSelected }) =>
    isSelected &&
    css`
      background: ${({ theme }) => theme.colors.purple};
      border: 0;
    `}
`;

export const Label = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: #4d4d4d;
`;

export const ButtonWrapper = styled.View`
  margin: 20px 0;
  align-self: center;
`;
