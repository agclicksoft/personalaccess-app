export const groupOptionsList = [
  {
    id: 1,
    description: 'Peito',
  },
  {
    id: 2,
    description: 'Costas',
  },
  {
    id: 3,
    description: 'Bíceps',
  },
  {
    id: 4,
    description: 'Antebraço',
  },
  {
    id: 5,
    description: 'Tríceps',
  },
  {
    id: 6,
    description: 'Ombros',
  },
  {
    id: 7,
    description: 'Abdômen',
  },
  {
    id: 8,
    description: 'Lombar',
  },
  {
    id: 9,
    description: 'Quadríceps',
  },
  {
    id: 10,
    description: 'Posterior',
  },
  {
    id: 11,
    description: 'Glúteos',
  },
  {
    id: 12,
    description: 'Panturrilha',
  },
];

export const modalityOptionsList = [
  {
    id: 1,
    description: 'Musculação',
  },
  {
    id: 2,
    description: 'Aeróbico',
  },
  {
    id: 3,
    description: 'HIIT',
  },
  {
    id: 4,
    description: 'Circuito',
  },
  {
    id: 5,
    description: 'Alongamento',
  },
];
