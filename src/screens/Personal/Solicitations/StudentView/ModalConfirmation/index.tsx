/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { useCallback, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { addHours, format, parseISO } from 'date-fns';
import { pt } from 'date-fns/locale';
import Modal from 'react-native-modal';

import { Button } from '~/components';
import { useAuth } from '~/hooks/auth';
import { useNotification } from '~/hooks/notifications';
import { Student } from '~/models';
import ScheduledService from '~/models/ScheduledServices';
import api from '~/services/api';

import { Container, Title, Subtitle, TouchableOpacity } from './styles';

interface Props {
  isVisible: boolean;
  schedule: ScheduledService;
  student: Student;
  closeModal: any;
  setCanceled?: any;
}

const ConfirmationModal: React.FC<Props> = ({
  isVisible,
  closeModal,
  schedule,
  setCanceled,
  student,
}) => {
  const { user } = useAuth();
  const { sendNewNotification } = useNotification();
  const [loading, setLoading] = useState(false);

  const confirmSchedule = useCallback(async () => {
    try {
      setLoading(true);

      const response = await api.put(
        `/scheduled-services/${schedule?.id}/status`,
        {
          status: 'CONFIRMADO',
        },
      );

      console.log(response.data);

      await sendNewNotification({
        token: student?.user?.expo_data,
        title: 'Aula confirmada!',
        body: `Sua aula com ${user.name}, do dia ${format(
          addHours(parseISO(schedule?.date), 3),
          "dd/MM/yy 'às' HH:mm",
          {
            locale: pt,
          },
        )} foi confirmada.`,
      });

      setLoading(false);
      setCanceled(true);
      closeModal();
    } catch (err) {
      setLoading(false);

      console.log(err);
      console.log(err.response);
    }
  }, [
    schedule?.id,
    schedule?.date,
    sendNewNotification,
    student?.user?.expo_data,
    user.name,
    setCanceled,
    closeModal,
  ]);

  const cancelSchedule = useCallback(async () => {
    try {
      setLoading(true);

      const response = await api.put(
        `/scheduled-services/${schedule?.id}/status`,
        {
          status: 'RECUSADO',
        },
      );

      console.log(response.data);

      await sendNewNotification({
        token: student?.user?.expo_data,
        title: 'Infelizmente sua aula foi recusada.',
        body: `Sua aula com ${user.name}, do dia ${format(
          addHours(parseISO(schedule?.date), 3),
          "dd/MM/yy 'às' HH:mm",
          {
            locale: pt,
          },
        )} foi recusada, prossiga com o reagendamento da mesma.`,
      });

      setLoading(false);
      setCanceled(true);
      closeModal();
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  }, [
    schedule?.id,
    schedule?.date,
    sendNewNotification,
    student?.user?.expo_data,
    user.name,
    setCanceled,
    closeModal,
  ]);

  return (
    <Modal
      backdropOpacity={0.5}
      isVisible={isVisible}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      useNativeDriver
      hideModalContentWhileAnimating
      propagateSwipe
      onBackdropPress={closeModal}
      onBackButtonPress={closeModal}
      style={{
        width: '100%',
        height: '50%',
        margin: 0,
        padding: 0,
      }}
    >
      <>
        <Container>
          <Title>Solicitação</Title>

          <Button onPress={confirmSchedule}>Confirmar</Button>

          <Button
            onPress={cancelSchedule}
            style={{ backgroundColor: '#D22020' }}
          >
            Recusar demanda
          </Button>

          <TouchableOpacity onPress={closeModal}>
            <Subtitle>Voltar</Subtitle>
          </TouchableOpacity>
        </Container>
      </>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Modal>
  );
};

export default ConfirmationModal;
