import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: ${({ theme }) => theme.colors.background};
  height: 400px;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  margin: 0 15px;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size22};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  margin: 15px 0 5px;
  text-align: center;
`;

export const TouchableOpacity = styled.TouchableOpacity`
  margin-top: 20px;
`;

export const Subtitle = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  font-size: 18px;
  color: ${({ theme }) => theme.colors.gray};
  text-align: center;
`;
