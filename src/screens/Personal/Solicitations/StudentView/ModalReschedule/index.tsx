/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View, Keyboard } from 'react-native';

import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { addHours, format, parseISO } from 'date-fns';
import { pt } from 'date-fns/locale';
import Modal from 'react-native-modal';

import { Button, Input } from '~/components';
import { useAuth } from '~/hooks/auth';
import { useNotification } from '~/hooks/notifications';
import { Student } from '~/models';
import ScheduledService from '~/models/ScheduledServices';
import api from '~/services/api';

import { Container, Title, Subtitle, TouchableOpacity } from './styles';

interface Props {
  isVisible: boolean;
  schedule: ScheduledService;
  student: Student;
  closeModal: any;
  setCanceled?: any;
}

const ModalReschedule: React.FC<Props> = ({
  isVisible,
  closeModal,
  schedule,
  setCanceled,
  student,
}) => {
  const { user } = useAuth();
  const { sendNewNotification } = useNotification();
  const [loading, setLoading] = useState(false);
  const formRef = useRef<FormHandles>(null);

  const handleSubmit = useCallback(
    async (data) => {
      try {
        setLoading(true);

        const response = await api.put(
          `/scheduled-services/${schedule?.id}/status`,
          {
            status: 'CANCELADO',
            comments: data.justification,
          },
        );

        console.log(response.data);

        await sendNewNotification({
          token: student?.user?.expo_data,
          title: 'Infelizmente sua aula foi cancelada.',
          body: `Sua aula com ${user.name}, do dia ${format(
            addHours(parseISO(schedule?.date), 3),
            "dd/MM/yy 'às' HH:mm",
            {
              locale: pt,
            },
          )} foi cancelada, prossiga com o agendamento de uma nova.`,
        });
        setLoading(false);
        setCanceled(true);
        closeModal();
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    },
    [
      closeModal,
      schedule?.date,
      schedule?.id,
      sendNewNotification,
      setCanceled,
      student?.user?.expo_data,
      user.name,
    ],
  );

  return (
    <Modal
      backdropOpacity={0.5}
      isVisible={isVisible}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      useNativeDriver
      hideModalContentWhileAnimating
      propagateSwipe
      onBackdropPress={() => Keyboard.dismiss()}
      onBackButtonPress={closeModal}
      style={{
        width: '100%',
        height: '90%',
        margin: 0,
        padding: 0,
      }}
    >
      <Container>
        <Title>{'Aula agendada.\n Deseja cancelar?'}</Title>

        <Form ref={formRef} onSubmit={handleSubmit}>
          <Input
            name="justification"
            placeholder="Justificativa (opcional)"
            style={{
              textAlign: 'left',
              textAlignVertical: 'top',
              alignSelf: 'flex-start',
              marginTop: 10,
              borderRadius: 30,
            }}
            height={160}
            width="90%"
            onSubmitEditing={() => Keyboard.dismiss()}
            returnKeyType="done"
            multiline
          />
        </Form>

        <Button
          onPress={() => formRef.current?.submitForm()}
          style={{ backgroundColor: '#D22020' }}
        >
          Cancelar aula
        </Button>

        <TouchableOpacity onPress={closeModal}>
          <Subtitle>Voltar</Subtitle>
        </TouchableOpacity>
      </Container>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Modal>
  );
};

export default ModalReschedule;
