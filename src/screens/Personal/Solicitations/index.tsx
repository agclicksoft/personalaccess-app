import React, { useEffect, useState, useCallback } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { PersonalClassCard } from '~/components';
import { useAuth } from '~/hooks/auth';
import { PersonalClass } from '~/models';
import { getPersonalClasses } from '~/services/personal';

import {
  Container,
  HeaderContainer,
  Title,
  Divider,
  Feed,
  Card,
  CardTitle,
} from './styles';

const Solicitations: React.FC = () => {
  const { user } = useAuth();
  const { addListener } = useNavigation();
  const [classes, setClasses] = useState([]);
  const [page, setPage] = useState(1);
  const [refreshing] = useState(false);
  const [loading, setLoading] = useState(true);

  const loadPersonalClasses = useCallback(async () => {
    await setPage(1);
    const classesQuery = await getPersonalClasses(user.professional.id, 1);
    await setClasses(classesQuery);
    setLoading(false);
  }, [user]);

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      try {
        loadPersonalClasses();
      } catch (err) {
        console.log(err);
      }
    });

    return unsubscribe;
  }, [addListener, loadPersonalClasses]);

  const refreshList = useCallback(async () => {
    setLoading(true);
    await setPage(1);
    await setClasses([]);
    const classesQuery = await getPersonalClasses(user.professional.id, 1);
    await setClasses(classesQuery);
    setLoading(false);
  }, [user]);

  const loadOnScroll = useCallback(async () => {
    await setPage(page + 1);
    const classesQuery = await getPersonalClasses(
      user.professional.id,
      page + 1,
      classes,
    );
    await setClasses(classesQuery);
  }, [page, classes, user]);

  return (
    <>
      <Container>
        <HeaderContainer>
          <Title>Solicitações</Title>
        </HeaderContainer>
        <Divider />

        {classes.length !== 0 ? (
          <Feed
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}
            onEndReachedThreshold={0.5}
            onEndReached={loadOnScroll}
            refreshing={refreshing}
            onRefresh={refreshList}
            data={classes.map((e) => e)}
            keyExtractor={(item: PersonalClass) => String(item.id)}
            renderItem={({ item }) => {
              const myClass: PersonalClass = item;
              return <PersonalClassCard key={myClass.id} data={myClass} />;
            }}
          />
        ) : null}

        {classes.length === 0 && !loading ? (
          <Card>
            <CardTitle>Nenhuma aula encontrada.</CardTitle>
          </Card>
        ) : null}
      </Container>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </>
  );
};

export default Solicitations;
