/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useRef, useCallback } from 'react';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import LottieView from 'lottie-react-native';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { LoadingAnimation } from '~/assets/animations';
import { ImageBackground, ImageLogo } from '~/assets/images';
import { Button, ContainerScroll, Input, MaskedInput } from '~/components';
import { useAuth } from '~/hooks/auth';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Background,
  Logo,
  FormContainer,
  FormTitle,
  FormSubTitle,
  SignUpContainer,
  SignUpPhrase,
  SignUpCTA,
  GenericButton,
  Holder,
} from './styles';

interface SignUpFormData {
  name: string;
  username: string;
  email: string;
  password: string;
  contact: string;
  role: string;
  doc_professional?: string;
  cpf?: string;
}

const SignUp: any = ({ route }) => {
  const { personal } = route.params;
  const { signUp } = useAuth();
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  const formRef = useRef<FormHandles>(null);

  const handleSubmit = useCallback(
    async (data: SignUpFormData, { reset }) => {
      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          name: Yup.string().required('Nome é obrigatório'),
          email: Yup.string()
            .required('E-mail é obrigatório')
            .email('Digite um e-mail válido'),
          contact: Yup.string().required('Celular é obrigatório'),
          password: Yup.string().min(6, 'No mínimo 6 dígitos'),
          doc_professional: personal
            ? Yup.string().required('CREF é obrigatório')
            : null,
          cpf: personal ? Yup.string().required('CPF é obrigatório') : null,
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        if (personal) {
          await signUp({
            email: data.email,
            password: data.password,
            name: data.name,
            contact: data.contact,
            role: 'professional',
            cref: data.doc_professional,
            cpf: data.cpf,
            isPersonal: true,
          });
        } else {
          await signUp({
            email: data.email,
            password: data.password,
            name: data.name,
            contact: data.contact,
            role: 'student',
            isPersonal: false,
          });
        }

        reset();
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);
        }

        setLoading(false);

        console.log(err.response);

        if (err?.response?.status === 409) {
          setLoading(false);

          showMessage({
            type: 'danger',
            message: 'E-mail já registrado.',
            description: 'Tente novamente.',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        showMessage({
          type: 'danger',
          message: 'Erro ao efetuar o cadastro.',
          description: 'Verifique os campos digitados.',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [signUp, personal],
  );

  return (
    <ContainerScroll>
      <Background source={ImageBackground}>
        <Logo source={ImageLogo} />

        <FormContainer>
          <FormTitle>Vamos começar?</FormTitle>

          <FormSubTitle>Crie sua conta.</FormSubTitle>

          <Form ref={formRef} onSubmit={handleSubmit}>
            <Input
              name="name"
              placeholder="Nome"
              autoCapitalize="words"
              autoCorrect={false}
              style={centerAlignInput}
            />

            <Input
              name="email"
              placeholder="E-mail"
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              style={centerAlignInput}
            />

            <MaskedInput
              name="contact"
              type="cel-phone"
              initialValue=""
              placeholder="Telefone"
              style={centerAlignInput}
            />

            <Input
              name="password"
              placeholder="Senha"
              returnKeyType="next"
              secureTextEntry
              style={centerAlignInput}
            />

            {personal ? (
              <>
                <MaskedInput
                  name="doc_professional"
                  type="custom"
                  options={{
                    mask: '999.999-A/AA',
                  }}
                  placeholder="CREF/CONFEF"
                  initialValue=""
                  autoCapitalize="characters"
                  keyboardType="name-phone-pad"
                  style={centerAlignInput}
                />

                <MaskedInput
                  name="cpf"
                  type="cpf"
                  placeholder="CPF"
                  keyboardType="number-pad"
                  style={centerAlignInput}
                />
              </>
            ) : null}
          </Form>

          <Holder>
            {loading === true ? (
              <LottieView
                style={{
                  height: 55,
                }}
                source={LoadingAnimation}
                autoPlay
                loop
              />
            ) : (
              <Button onPress={() => formRef.current?.submitForm()}>
                Criar
              </Button>
            )}
          </Holder>

          <GenericButton onPress={() => navigation.navigate('SignIn')}>
            <SignUpContainer>
              <SignUpPhrase>Já tem uma conta?</SignUpPhrase>
              <SignUpCTA>Entre agora.</SignUpCTA>
            </SignUpContainer>
          </GenericButton>
        </FormContainer>
      </Background>
    </ContainerScroll>
  );
};

const centerAlignInput = {
  textAlign: 'center',
};

export default SignUp;
