import styled from 'styled-components/native';

export const Background = styled.ImageBackground.attrs({
  resizeMode: 'cover',
})`
  flex: 1;
  align-items: center;
  justify-content: space-between;
`;

export const Logo = styled.Image.attrs({
  resizeMode: 'contain',
})`
  flex: 1;
  width: 170px;
  height: 170px;
  margin: 30px 0;
`;

export const FormContainer = styled.View`
  flex: 1;
  width: 100%;
  align-items: center;
  justify-content: center;
  background-color: #f8f8f8;
  border-top-left-radius: 31px;
  border-top-right-radius: 31px;
  padding: 30px 0;
`;

export const FormTitle = styled.Text`
  font-size: 23px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  text-align: center;
  padding: 10px;
  color: #f76e1e;
`;

export const FormSubTitle = styled.Text`
  font-size: 12px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  text-align: center;
  padding-bottom: 30px;
  color: #707070;
`;

export const FormForgotPassword = styled.Text`
  font-size: 12px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  text-align: center;
  margin: 20px 0;
  color: #37153d;
`;

export const SignUpContainer = styled.View`
  margin: 20px 0;
  flex-direction: row;
  align-items: center;
`;

export const SignUpContainerTeacher = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const SignUpPhrase = styled.Text`
  font-size: 12px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  text-align: center;
  color: #37153d;
`;

export const SignUpCTA = styled.Text`
  font-size: 12px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  text-align: center;
  color: #37153d;
  margin-left: 3px;
`;

export const GenericButton = styled.TouchableOpacity``;

export const Holder = styled.View`
  margin: 20px 0 10px;
`;
