import React, { useRef, useCallback, useState } from 'react';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import LottieView from 'lottie-react-native';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { LoadingAnimation } from '~/assets/animations';
import { ImageBackground, ImageLogo } from '~/assets/images';
import { Button, ContainerScroll, Input } from '~/components';
import { useAuth } from '~/hooks/auth';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Background,
  Logo,
  FormContainer,
  FormTitle,
  FormSubTitle,
  FormForgotPassword,
  SignInContainer,
  SignInContainerTeacher,
  SignUpPhrase,
  SignUpCTA,
  GenericButton,
} from './styles';

interface SignInFormData {
  email: string;
  password: string;
}

const SignIn: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const formRef = useRef<FormHandles>(null);
  const navigation = useNavigation();

  const { signIn } = useAuth();

  const handleSubmit = useCallback(
    async (data: SignInFormData, { reset }) => {
      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          email: Yup.string()
            .required('E-mail é obrigatório')
            .email('Digite um e-mail válido'),
          password: Yup.string().min(6, 'No mínimo 6 dígitos'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        await signIn({ email: data.email, password: data.password });

        reset();
      } catch (err) {
        setLoading(false);
        console.log(err);
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);
        }

        if (err?.response?.status === 401) {
          showMessage({
            type: 'danger',
            message: 'Erro ao efetuar login.',
            description: 'Usuário e/ou senha inválidos.',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        if (err?.response?.status === 404) {
          showMessage({
            type: 'danger',
            message: 'Erro ao efetuar login.',
            description: 'Usuário não encontrado.',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        showMessage({
          type: 'danger',
          message: 'Erro ao efetuar login.',
          description: 'Verifique os campos digitados.',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [signIn],
  );

  return (
    <ContainerScroll>
      <Background source={ImageBackground}>
        <Logo source={ImageLogo} />

        <FormContainer>
          <FormTitle>Olá!</FormTitle>

          <FormSubTitle>Faça login na sua conta.</FormSubTitle>

          <Form ref={formRef} onSubmit={handleSubmit}>
            <Input
              name="email"
              placeholder="E-mail"
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              style={centerAlignInput}
            />

            <Input
              name="password"
              placeholder="Senha"
              returnKeyType="next"
              secureTextEntry
              style={centerAlignInput}
            />
          </Form>

          <GenericButton onPress={() => navigation.navigate('ResetPassword')}>
            <FormForgotPassword>Esqueci minha senha</FormForgotPassword>
          </GenericButton>

          {loading === true ? (
            <LottieView
              style={{
                height: 55,
              }}
              source={LoadingAnimation}
              autoPlay
              loop
            />
          ) : (
            <Button onPress={() => formRef.current?.submitForm()}>
              Entrar
            </Button>
          )}

          <GenericButton
            onPress={() =>
              navigation.navigate('SignUp', {
                personal: false,
              })
            }
          >
            <SignInContainer>
              <SignUpPhrase>Cadastre-se:</SignUpPhrase>
              <SignUpCTA>Aluno.</SignUpCTA>
            </SignInContainer>
          </GenericButton>

          <GenericButton
            onPress={() =>
              navigation.navigate('SignUp', {
                personal: true,
              })
            }
          >
            <SignInContainerTeacher>
              <SignUpPhrase>Cadastre-se:</SignUpPhrase>
              <SignUpCTA>Professor.</SignUpCTA>
            </SignInContainerTeacher>
          </GenericButton>
        </FormContainer>
      </Background>
    </ContainerScroll>
  );
};

const centerAlignInput = {
  textAlign: 'center',
};

export default SignIn;
