import React, { useRef, useCallback, useState } from 'react';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import LottieView from 'lottie-react-native';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { LoadingAnimation } from '~/assets/animations';
import { ImageBackground, ImageLogo } from '~/assets/images';
import { Button, ContainerScroll, Input } from '~/components';
import api from '~/services/api';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Background,
  Logo,
  FormContainer,
  FormTitle,
  FormSubTitle,
  SignInContainer,
  SignUpPhrase,
  SignUpCTA,
  GenericButton,
  Holder,
} from './styles';

interface SignInFormData {
  email: string;
  password: string;
}

const ResetPassword: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const formRef = useRef<FormHandles>(null);
  const { navigate } = useNavigation();

  const handleSubmit = useCallback(
    async (data: SignInFormData, { reset }) => {
      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          email: Yup.string()
            .required('E-mail é obrigatório')
            .email('Digite um e-mail válido'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        const response = await api.post(`/users/forgot-password`, {
          email: data.email,
        });

        console.log(response.data);

        if (response.status === 204) {
          setLoading(false);

          showMessage({
            type: 'success',
            message: 'Tudo certo!',
            description: 'Verifique seu e-mail.',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          navigate('SignIn');
        }

        reset();
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);
        }

        setLoading(false);

        showMessage({
          type: 'danger',
          message: 'Erro ao recuperar senha.',
          description: 'Verifique os campos digitados.',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [navigate],
  );

  return (
    <ContainerScroll>
      <Background source={ImageBackground}>
        <Logo source={ImageLogo} />

        <FormContainer>
          <FormTitle>Esqueceu sua senha?</FormTitle>

          <FormSubTitle>Nos informe seu e-mail de cadastro.</FormSubTitle>

          <Form ref={formRef} onSubmit={handleSubmit}>
            <Input
              name="email"
              placeholder="E-mail"
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              style={centerAlignInput}
            />
          </Form>

          {loading === true ? (
            <LottieView
              style={{
                height: 55,
              }}
              source={LoadingAnimation}
              autoPlay
              loop
            />
          ) : (
            <Holder>
              <Button onPress={() => formRef.current?.submitForm()}>
                Recuperar senha
              </Button>
            </Holder>
          )}

          <GenericButton
            onPress={() =>
              navigate('SignIn', {
                personal: false,
              })
            }
          >
            <SignInContainer>
              <SignUpPhrase>Lembrou seu acesso?</SignUpPhrase>
              <SignUpCTA>Entre agora.</SignUpCTA>
            </SignInContainer>
          </GenericButton>
        </FormContainer>
      </Background>
    </ContainerScroll>
  );
};

const centerAlignInput = {
  textAlign: 'center',
};

export default ResetPassword;
