import React from 'react';

import { useNavigation } from '@react-navigation/native';

import { Button, ContainerScroll, HeaderBack } from '~/components';

import {
  Container,
  SaveButtonContainer,
  ButtonRefuse,
  ButtonRefuseLabel,
  Content,
  Text,
} from './styles';

const TermsOfUse: React.FC = () => {
  const { goBack } = useNavigation();

  return (
    <Container>
      <HeaderBack>Termos de uso</HeaderBack>

      <ContainerScroll>
        <Content>
          <Text>
            1. Definições Básicas: 1.1 PERSONAL ACCESS Aplicativo gerenciado
            pela HSTS CONSULTORIA LTDA, inscrita no Cadastro Nacional de Pessoas
            Jurídicas sob o nº 38.824.434/0001-89, sociedade de responsabilidade
            limitada estabelecida no Brasil, localizada na Rua Maestro Felício
            Toledo, nº 495, sala 1008, CEP 24.030-105. (“Personal Access”). 1.2
            ALUNO – Pessoa física, usuário dos serviços oferecidos pela
            plataforma, na qualidade de aluno/cliente do Professor. (“Aluno”).
            1.3 PROFESSOR (PERSONAL TRAINER) – Pessoa física, usuário dos
            serviços oferecidos pela plataforma, obrigatoriamente formado em
            Educação Física, regularmente inscrito no Conselho Profissional da
            categoria. (“Professor” ou “Personal Trainer”).
          </Text>

          <Text>
            2. Introdução 2.1 O presente termo de uso se aplica ao usuário do
            serviço na qualidade de Aluno, através de aplicação tecnológica
            (Aplicativo), sítios de internet e conteúdos relacionados
            (Serviços). Ao iniciar o cadastro, o Aluno estará vinculado às
            regras aqui descritas, concordando e comprometendo-se a respeitá-las
            fielmente. 2.2 Somente o Personal Access poderá modificar, apagar,
            acrescentar ou substituir ("alteração") estes Termos de Uso a
            qualquer momento e sem necessidade de aviso prévio. Quando alguma
            regra for alterada, o Aluno poderá visualizá-las através do mesmo
            link "Termos de Uso" imediatamente após a sua publicação. 2.3 Se
            porventura o Aluno não concordar com estes termos de uso, não deverá
            utilizar o Personal Access. 2.4 Ao aceitar estes Termos de Uso, o
            Aluno declara e garante poder fazê-lo nos termos da legislação
            brasileira vigente; devendo ser maior de 18 anos. 2.5 Será também
            permitido o uso do aplicativo por menores de 18 anos, com anuência
            dos seus pais/responsáveis legais, que serão os responsáveis pelo
            pagamento ao Professor, em caso de contratação. 2.6 O Personal
            Access é um aplicativo com a função de aproximar alunos e
            professores de Educação Física (Personal Trainer), regularmente
            inscritos no Conselho Profissional da categoria. 2.7 O Personal
            Access não controla e não tem qualquer vinculação trabalhista,
            empregatícia, societária ou ingerência sobre os professores de
            Educação Física (Personal Trainer), seus conteúdos e/ou serviços. O
            Aluno concorda que o aplicativo Personal Access não temos nenhuma
            responsabilidade pela disponibilidade e/ou qualidade dos serviços
            oferecidos pelo Professor, que assume integralmente os riscos ao ser
            contratado. 2.8 A definição dos preços cobrados pelos e professores
            de Educação Física é de sua exclusiva responsabilidade, não cabendo
            ao Personal Access ingerência neste sentido. 2.9 O Personal Access
            não oferece acesso a clubes, academias de ginástica, academias de
            musculação, academias de treino funcional, ou estabelecimentos
            privados de quaisquer modalidades esportivas. 2.10 O Aluno e seu(s)
            Professor(es) deverão ter prévio acesso, por sua responsabilidade e
            risco, aos espaços de treinamento, a clubes, academias de ginástica,
            academias de musculação, academias de treino funcional, ou outros
            estabelecimentos públicos ou privados de quaisquer modalidades
            esportivas. 2.11 O serviço prestado pelo Personal Access não inclui
            acesso às academias.
          </Text>

          <Text>
            3. Cadastro 3.1 Cadastro Inicial 3.1.1 Para o cadastro inicial no
            Personal Access (Aplicativo), o Aluno deverá informar: nome,
            endereço eletrônico (e-mail), número de telefone e senha de acesso
            com 6 (seis) dígitos. Com esse cadastro, o aluno já poderá
            visualizar as funcionalidades do aplicativo, e localizar Professores
            que estejam na Academia ou próximos à Academia em que já tenha
            acesso ou que pretenda contratar. 3.1.2 O Perfil do Aluno (Perfil
            Publico) é pessoal e restrito. Apenas o Professor que for contratado
            pelo Aluno poderá visualizar o seu perfil. Outros usuários, sejam
            outros Alunos, sejam Professores não contratados, não conseguirão
            visualizar o Perfil do Aluno que utilize o Personal Access. 3.1.3 Ao
            clicar em Perfil Público, o Aluno poderá visualizar exatamente o que
            o Professor contratado visualiza. Nenhum outro usuário, Aluno ou
            Professor, terá acesso a essas informações pessoais. 3.2 Cadastro
            Complementar 3.2.1 Com acesso ao Aplicativo após preencher o
            Cadastro Inicial, o Aluno deverá acessar Meu Perfil - Editar Perfil,
            onde deverá preencher o Cadastro Complementar, informando número de
            CPF, data de nascimento, número de telefone de emergência e endereço
            com CEP. 3.2.2 As informações do Perfil do Aluno que constam no
            Cadastro Complementar são pessoais e restritas. Apenas o Professor
            que for contratado pelo Aluno poderá visualizar no seu perfil o
            nome, idade, e-mail, telefone, e telefone de emergência. Outros
            usuários, sejam outros Alunos, sejam Professores não contratados,
            não conseguirão visualizar o Perfil do Aluno que utilize o Personal
            Access. 3.2.3 Ao clicar em Perfil Público, o Aluno poderá visualizar
            exatamente o que o Professor contratado visualiza. Nenhum outro
            usuário, Aluno ou Professor, terá acesso a essas informações
            pessoais. 3.2.4 As informações de e-mail e CPF são únicas na
            plataforma, não sendo permitido ter duas contas com o mesmo CPF e/ou
            e-mail.
          </Text>

          <Text>
            4. Cadastro Específico – PAR-Q 4.1 Para efetuar a contratação de um
            Professor, será necessário que o Aluno conclua o cadastro,
            preenchendo o Questionário de Prontidão para Atividade Física -
            PAR-Q, aplicado ao Aluno antes do início da prática de atividades
            físicas regulares. 4.2 O PAR-Q tem como objetivo identificar
            possíveis limitações e restrições existentes. É um detalhamento do
            histórico de saúde da pessoa, verificando se o Aluno está apto para
            atividades físicas adequadas a sua condição. Através do
            questionário, o Professor poderá avaliar melhor o Aluno, apreciar as
            necessidades do cliente com suas aptidões profissionais e traçar de
            forma mais eficaz um plano individualizado. 4.3 O Aluno deverá
            preencher peso, altura, e responder às seguintes perguntas: “Qual
            seu objetivo? ”; “Na prática de atividades físicas, você sente algum
            desses sintomas? Tontura, Enjoo, Falta de ar, Nada”; “Algum outro
            desconforto?”; “Diabético?”; “Qual estágio?” (se marcar SIM para a
            pergunta anterior); “Hipertenso?”; “Qual a máxima?” (se marcar SIM
            para a pergunta anterior); “Possui algum problema cardíaco?”;
            “Qual?” (se marcar SIM para a pergunta anterior); “Problema
            respiratório?”; “Qual?” (se marcar SIM para a pergunta anterior);
            “Utiliza algum tipo de medicamento ou drogas?”; “Qual?” (se marcar
            SIM para a pergunta anterior); “Fez alguma cirurgia nos últimos 6
            meses?”; “Qual?” (se marcar SIM para a pergunta anterior); “Sente
            algum incômodo ou dores em articulações e/ou coluna na prática de
            atividade física?”; “Qual?” (se marcar SIM para a pergunta
            anterior); “Tem alguma restrição ou recomendação médica para a
            prática de exercícios físicos?; “Qual?” (se marcar SIM para a
            pergunta anterior). 4.4 As informações do Perfil do Aluno que
            constam no PAR-Q são pessoais e restritas. Apenas o Professor que
            for contratado pelo Aluno poderá visualizar no seu perfil essas
            informações. Outros usuários, sejam outros Alunos, sejam Professores
            não contratados, não conseguirão visualizar o Perfil do Aluno que
            utilize o Personal Access. 4.5 Ao clicar em Perfil Público, o Aluno
            poderá visualizar exatamente o que o Professor contratado visualiza.
            Nenhum outro usuário, Aluno ou Professor, terá acesso a essas
            informações pessoais.
          </Text>

          <Text>
            5 Condições adversas 5.1 Caso o Aluno declare patologias como
            diabetes tipo 1 e tipo 2, hipertensão, problemas cardíacos de
            quaisquer tipos ou que tenha sofrido acidente vascular cerebral
            (AVC), será necessário enviar através de upload em link próprio
            atestado médico com autorização para a prática de atividade física.
            5.2 Essas informações serão disponibilizadas exclusivamente ao
            Professor (Personal Trainer) escolhido no momento da contratação.
            Diante das informações, caberá ao Professor contratado decidir com
            base em seu conhecimento técnico se está capacitado ou não para
            aceitar a demanda, apresentado justificativa ao Aluno em caso de
            recusa.
          </Text>

          <Text>
            6 Responsabilidades do Personal Access 6.1 O Personal Access não
            disponibiliza para terceiros, nem para os Professores cadastrados em
            sua plataforma, os dados pessoais referentes aos Alunos cadastrados.
            Apenas o Professor contratado terá acesso aos dados do Aluno
            contratante, exclusivamente, mediante aceitação do Aluno desses
            termos de uso, para que possa avaliar se está habilitado a aceitar
            ou não a demanda. 6.2 O Personal Access poderá coletar dados do uso
            da plataforma, de forma coletiva e sem individualizar os usuários,
            para pesquisas mercadológicas e informações estatísticas, para uso
            próprio e informações gerais aos Professores usuários.
          </Text>

          <Text>
            7 Responsabilidades acerca das informações cadastrais 7.1 O Aluno
            estará assumindo integralmente a responsabilidade (inclusive cível e
            criminal) pela exatidão e veracidade das informações fornecidas no
            momento do cadastro, que poderá ser verificado, a qualquer momento,
            pelo Personal Access. 7.2 Em caso de informações incorretas,
            inverídicas ou não confirmadas, bem assim na hipótese da negativa em
            corrigi-las ou enviar documentação que comprove a correção, o
            Personal Access se reserva o direito de não concluir o cadastramento
            em curso ou, ainda, de bloquear o cadastro já existente, impedindo a
            utilização dos serviços on-line até que, a critério do Personal
            Access, a situação de anomalia esteja regularizada. 7.3 O Personal
            Access se reserva o direito de impedir, a seu critério, novos
            cadastros, ou cancelar os já efetuados, em caso de ser detectada
            anomalia que, em sua análise, seja revestida de gravidade ou
            demonstre tentativa deliberada de burlar as regras aqui descritas,
            obrigatórias para todos os usuários. 7.4 Adotar-se-á Política de
            Privacidade que protege todos estes dados que o Aluno fornecerá.
            Serão utilizados os melhores esforços para garantir que as
            informações pessoais não serão interceptadas/acessadas por terceiros
            de maneira indevida e estranha a esta política de privacidade. 7.5
            Ao criar uma conta no aplicativo, o Aluno concorda que o Personal
            Access e seus parceiros poderão lhe enviar mensagens através de
            e-mail, de textos informativas (SMS), ou outro meio de comunicação,
            como parte das operações comerciais regulares, com conteúdo de
            natureza informativa e/ou promocional relacionada aos serviços.
          </Text>

          <Text>
            8. Senha 8.1 O Aluno deverá cadastrar senha com seis dígitos, que
            deverá ser mantida em absoluto sigilo. Esta confidencialidade cabe
            ao Aluno, não tendo o Personal Access qualquer responsabilidade
            pelas atividades que ocorram com o uso de sua senha, tampouco por
            eventuais perdas ou danos resultantes de eventual utilização não
            autorizada. 8.2 O Personal Access não possui acesso à senha do
            Aluno. Se a senha for esquecida, o Aluno deverá seguir o
            procedimento de recuperação, clicando em “Esqueci minha senha”. O
            Aluno receberá um e-mail com a senha provisória, devendo cadastrar
            uma nova senha já no primeiro acesso. 8.3 O Aluno deverá, ao final
            de cada acesso ao Sistema, encerrar a sessão clicando em 'Sair',
            evitando, desta maneira, o acesso indevido à sua conta. 8.4 O Aluno
            concorda em notificar imediatamente ao Personal Access, através do
            e-mail “personalaccess.app@gmail.com”, sobre todo e qualquer acesso
            não permitido de sua senha ou conta. Não obstante esta notificação,
            o Aluno poderá ser responsabilizado em caso de o Personal Access vir
            a sofrer qualquer dano ou prejuízo (i) pelo ingresso impróprio de
            sua conta ou (ii) pela utilização de sua senha por terceiros não
            autorizados. 8.5 O Aluno poderá alterar sua senha sempre que desejar
            em Meu Perfil, opção Editar Perfil.
          </Text>

          <Text>
            9. Serviços 9.1 O uso da plataforma Personal Access é gratuita para
            o Aluno. Nenhum pagamento será exigido para pesquisar Professores. O
            serviço prestado pelo Professor (“Personal Trainer) é remunerado,
            conforme o plano de aulas escolhido pelo Aluno. O Personal Access
            não fornece acesso a academias. 9.2 Para utilizar os serviços
            oferecidos pelo Personal Access, basta ter uma conta ativa de acesso
            ao sistema. Sempre que logar em sua conta, o Aluno verá os
            Professores que estão mais próximos do local em que se encontra, por
            georreferenciamento, podendo pesquisar também por outras
            localidades. 9.3 Os serviços prestados pelo Personal Access
            consistem na disponibilização de uma aplicação tecnológica que
            possibilita ao Aluno cadastrado localizar e contratar Professores de
            Educação Física inscritos regularmente no Conselho Profissional
            (Personal Trainer). 9.4 O serviço não deve ser utilizado para
            qualquer finalidade que não as expressamente autorizadas por estes
            Termos de Uso. A utilização da plataforma para fins diversos dos
            previstos poderá ensejar a exclusão do usuário do cadastro. O
            Personal Access monitorará constantemente a utilização da
            plataforma. 9.5 O Personal Access se reserva o direito de passar a
            cobrar pelos serviços prestados aos Alunos, ou parte dos serviços, a
            qualquer tempo. O usuário será previamente informado caso isso
            ocorra e terá a oportunidade de consentir com tal cobrança, ou
            cessar o uso do serviço. 9.6 Para utilizar o serviço
            disponibilizado, o Aluno deve buscar o Professor (Personal Trainer)
            através das ferramentas disponibilizadas, escolhendo o que mais se
            encaixa em sua necessidade (localização, especialidade, etc.). Após
            escolher o Personal Trainer, o Aluno deverá escolher o local de
            atendimento e, em seguida, escolher uma das modalidades de
            contratação disponibilizadas, quais sejam: a) aula avulsa; b) pacote
            de aulas (8, 10, 12, 15, 16, 20 e 24 aulas). 9.6.1 “Aula avulsa” é a
            aula singular, prestada em única oportunidade; 9.6.2 “Pacote de
            Aulas” é o grupo de aulas contratadas em conjunto. O Aluno poderá
            utilizar uma aula por dia, ou mais de uma aula em um mesmo dia,
            conforme sua necessidade. Os Pacotes de Aulas podem ser de 8 aulas
            (em especial para que usa 2 vezes na semana em meses de 4 semanas);
            10 aulas (em especial para que usa 2 vezes na semana em meses de 4
            semanas e meia); 12 aulas (em especial para que usa 3 vezes na
            semana em meses de 4 semanas); 15 aulas (em especial para que usa 3
            vezes na semana em meses de 4 semanas e meia); 16 aulas (em especial
            para que usa 4 vezes na semana em meses de 4 semanas); 20 aulas (em
            especial para que usa 4 vezes na semana em meses de 4 semanas e
            meia); 24 aulas (em especial para que usa 4 ou 5 vezes na semana em
            meses de 4 semanas e meia). 9.6.2.1 O “Pacote de Aulas” não tema
            finalidade de coincidir integralmente com o mês. A contagem é feita
            por número de aulas. Assim, a cada mês, dependendo do tamanho do mês
            (se 28,29,30 ou 31 dias, bem como se há feriados ou não, e quantos
            dias efetivamente irá utilizar), o Aluno poderá comprar o pacote que
            mais se adapta a sua necessidade naquele período, podendo varia mês
            a mês. 9.6.3 “Local de Atendimento” é o espaço onde o Professor
            ministra suas aulas. Pode ser uma academia, clube, ou outro espaço
            regular. O Professor pode ter mais de um local de atendimento. O
            Aluno deve se certificar de que já tenha, antes do início das aulas,
            prévio acesso a esse local de atendimento. O Personal Access não
            fornece acesso aos locais de atendimento. 9.7 Após escolher
            Professor, o Aluno deverá selecionar o plano de aulas, bem como os
            dias e horários na agenda, nos quais deseja ter o serviço prestado,
            escolhendo dentre os previamente disponibilizados pelo Professor, e
            o local de atendimento. Em seguida, deverá efetuar o pagamento, que
            poderá ser feito em cartão, pelo aplicativo, ou em dinheiro
            diretamente ao Personal Trainer. 9.8 A hora-aula terá por base o
            tempo mínimo padrão de 50 (cinquenta) minutos, podendo ter pequenas
            variações para mais ou para menos, conforme a programação de
            atividades do dia conjugada com o objetivo do Aluno. Cabe ao
            Professor calcular o tempo médio ideia da hora-aula para que o Aluno
            atinja seu objetivo. 9.9 O valor do serviço é determinado
            exclusivamente pelo Professor, que é livre para estipular o preço
            cobrado. 9.10 O Aluno concorda em pagar os valores dos serviços
            agendados conforme eles foram apresentados no momento do
            agendamento. 9.11 Após adquirir a aula-avulsa ou o pacote de aulas,
            o Aluno terá 32 (trinta e dois) dias para marcar, remarcar e
            efetivamente utilizar a aula comprada, sem possibilidade de
            prorrogação. Após o transcurso do prazo total sem utilização, sem
            marcação ou sem remarcação, a aula será considerada expirada.
          </Text>

          <Text>
            10 Responsabilidade do Aluno na utilização dos serviços prestados
            10.1 O Aluno concorda em comparecer a todo e qualquer agendamento ou
            reagendamento que tiver realizado através do Personal Access. Caso
            não lhe seja possível, deverá seguir a Política de Cancelamento e
            Reagendamento abaixo descrita. 10.2 O Aluno se compromete a não
            realizar através do Personal Access nenhum agendamento que não
            pretenda comparecer ("agendamento seguido de não comparecimento
            proposital"), sob pena de esta prática, caso reincidente e a
            exclusivo critério do Personal Access, motivar a suspensão ou
            cancelamento de sua conta. 10.3 O Aluno deverá verificar em seus
            agendamentos, em “Minhas Aulas”, as aulas marcadas, onde constará o
            dia e hora marcado, o Professor escolhido e o local de atendimento.
            10.4 O Personal Access não possui qualquer responsabilidade no caso
            de o Aluno confirmar um agendamento e não comparecer ao local e
            horário escolhido, tentar cancelá-lo sem tempo hábil ou tardiamente.
            10.5 É dever do Aluno seguir à risca os horários marcados. O mesmo
            dever é atribuído aos Professores. Se houver atraso do Aluno, a aula
            será prestada apenas pelo tempo restante da hora marcada, sem
            prorrogação, para não prejudicar o Aluno seguinte. Se houver atraso
            do Professor, este deverá ver a melhor forma de ressarcir o tempo ao
            Aluno, para que ele não seja prejudicado.
          </Text>

          <Text>
            11 Política de Agendamento, Cancelamento e Reagendamento 11.1 Toda
            vez que a solicitação foi aceita pelo Professor, considera-se a aula
            como confirmada ou marcada. 11.2 Considera-se “no-show” do Aluno o
            agendamento realizado de forma regular, não remarcado, e cuja aula
            não foi ministrada por fatores atribuídos exclusivamente ao Aluno, e
            que sejam estranhos ao Professor (Personal Trainer). 11.3 Em caso de
            “no-show” injustificado do Aluno, isto é, falta do Aluno, haverá
            perda da hora-aula contratada, sendo o serviço considerado prestado,
            sem direito a ressarcimento ou nova remarcação, uma vez que o
            Professor (Personal Trainer) permaneceu o tempo marcado na hora e
            local combinados à disposição do Aluno, e este tempo desprendido
            pelo Professor, bem como seu custo de deslocamento, não poderão ser
            ressarcidos. 11.4 Em caso de “no-show” justificado pelo Aluno, como
            emergências médicas, ocorrências profissionais, ambas comprovadas,
            ou outras ocorrências relevantes e devidamente comprovadas, poderá
            ser restituído o direito à hora-aula, conforme a disponibilidade da
            agenda do Professor (Personal Trainer), a ser remarcada uma vez no
            período de 30 (trinta) dias. 11.5 Em caso de cancelamento de aula
            pré-agendada, o Aluno deverá fazê-lo com antecedência mínima de 24
            (vinte e quatro) horas. Qualquer cancelamento em prazo inferior será
            considerado “no-show”, salvo se houver disponibilidade na agenda do
            Professor (Personal Trainer) escolhido, e este anuir com a mudança
            de horário. 11.5.1 Em nenhuma hipótese serão aceitas
            remarcações/cancelamentos pelo Aluno no dia da prestação do serviço,
            uma vez que o Professor (Personal Trainer) deixou de atender outras
            demandas para permanecer o tempo marcado na hora e local combinados
            à disposição do Aluno, e este tempo desprendido pelo Professor, bem
            como seu custo de deslocamento, não poderão ser ressarcidos. Da
            mesma forma, em tempo tão exíguo, não será possível ao Professor
            (Personal Trainer) marcar outra aula para aproveitar a hora perdida.
            11.6 Considera-se “no-show” do Professor o agendamento realizado de
            forma regular, não remarcado, e cuja aula não foi ministrada por
            fatores atribuídos exclusivamente ao Professor, e que sejam
            estranhos ao Aluno. 11.7 Em caso de “no-show” do Professor,
            justificado ou injustificado, o Aluno terá devolvido o direito de
            remarcar a aula não dada, no período de 30 (trinta) dias, conforme a
            disponibilidade de agenda do Professor. 11.8 Da mesma forma, caso o
            Professor (Personal Trainer) venha a reagendar, alterar ou cancelar
            um agendamento, de forma justificada, o Aluno receberá uma
            notificação da ocorrência. Neste caso, o Aluno deverá reagendar o
            dia e horário da prestação do serviço, escolhendo dentre os
            disponíveis na agenda do professor. 11.9 Havendo disponibilidade na
            Agenda do Professor, a critério deste, outros prazos poderão ser
            adotados.
          </Text>

          <Text>
            12 Token de abertura de Aula, Avaliação dos Serviços e Registro dos
            Treinos 12.1 Antes de iniciar a aula, poderá ser solicitado ao Aluno
            que confirme a abertura da aula, colocando em tela específica no
            celular do professor, o código de confirmação (token), que serão
            sempre os 3 (três) últimos dígitos do CPF do Aluno. 12.2 Após a
            conclusão de um serviço contratado, poderá ser solicitado a o Aluno
            o preenchimento de uma avaliação acerca da qualidade do serviço
            prestado, para orientar a escolha de novos alunos que pretendam
            contratar o Professor. O Aluno concorda em não fornecer conteúdo que
            seja difamatório, calunioso, injurioso, violento, obsceno, ilegal ou
            de qualquer modo ofensivo. A nota média da avaliação do Professor
            ficará disponível, de forma anônima, para outros Alunos. 12.3 Ao
            final da aula, será solicitado ao Professor que preencha os
            grupamentos musculares que foram trabalhados na aula finalizada,
            para facilitar a consulta do próximo Professor ou elaboração da
            próxima aula.
          </Text>

          <Text>
            13. Programas de Fidelidade 13.1 O Personal Access e alguns
            estabelecimentos parceiros poderão oferecer programas de fidelidade,
            conforme o Aluno contrate os serviços ou adquira produtos. As regras
            dos programas de fidelidade são específicas por estabelecimento, que
            determinam quais serviços são elegíveis. Estes programas de
            fidelidade podem ser descontinuados a qualquer momento pelo Personal
            Access ou pelo estabelecimento parceiro. 13.2 O Personal Access se
            reserva o direito de estabelecer programas de fidelidade,
            isoladamente ou em conjunto com empresas parceiras.
          </Text>

          <Text>
            14. Pagamento Online 14.1 O Aluno poderá optar por realizar o
            pagamento online dos serviços. O pagamento online é realizado
            através de um gateway de pagamento e o valor é transferido para o
            Professor (Personal Trainer) conforme contrato com o Personal
            Access. A responsabilidade sobre a prestação do serviço é do
            Professor (Personal Trainer) independentemente se foi utilizado ou
            não o pagamento online. 14.2 O pagamento será realizado através de
            cartão, nas modalidades crédito ou débito, utilizando o Pag Seguro.
            O Personal Access. Poderá, a qualquer momento, alterar o gateway de
            pagamento. 14.3 O Personal Access não armazena dados de cartão
            utilizados em sua plataforma. Toda a transação realizada é feita
            através do Pag Seguro, sem interferência do Personal Access. 14.4 O
            Personal Access não se responsabiliza por recusas de cartão, devendo
            o Aluno verificar o motivo diretamente com sua operadora de cartão
            de crédito.
          </Text>

          <Text>
            15. Pagamento em Dinheiro 15.1 O Aluno poderá optar por realizar o
            pagamento dos serviços em dinheiro. O pagamento em mãos será
            realizado diretamente ao Professor (Personal Trainer) conforme
            contrato com o Personal Access, antes do início da prestação do
            serviço. A responsabilidade sobre a prestação do serviço é do
            Professor (Personal Trainer).
          </Text>

          <Text>
            16. Disponibilidade dos Serviços 16.1 O Personal Access não garante
            que o Sistema será ininterrupto ou que estará livre de defeitos,
            inexatidão, interferência, invasão de segurança, erros, vírus,
            imprecisões ou falhas. 16.2 O Aluno concorda expressamente que os
            serviços são fornecidos conforme a disponibilidade técnica para seu
            uso, e não demandará nenhuma garantia ou reparação de qualquer tipo.
            16.3 O Personal Access não se responsabiliza por quaisquer danos em
            decorrência de atos, fatos, omissões ou eventos praticados ou de
            responsabilidade de terceiros, incluindo o Professor (Personal
            Trainer), assim como por motivos de força maior ou caso fortuito,
            tais como falhas no fornecimento de energia elétrica, conexão à
            internet ou ainda fechamento de academias ou de espaço de
            atendimento. 16.4 O Personal Access verificará previamente a
            existência e regularidade do registro do Professor (Personal
            Trainer) no Conselho Profissional. Não serão admitidos na
            plataforma, na condição de Professores, usuários que não tenham
            registro no Conselho Profissional. 16.5 Caso o Aluno se sinta
            desconfortável ou mesmo lesado por qualquer ação ou omissão do
            Professor (Personal Trainer), embora o Personal Access não tenha
            nenhuma ingerência ou responsabilidade direta, deve ser enviado
            e-mail relatando o ocorrido, para verificação e eventuais
            providências.
          </Text>

          <Text>
            17 Outras obrigações do Aluno 17.1 O Aluno não praticará nenhuma
            ação que imponha uma sobrecarga desarrazoada ou desproporcional de
            dados, que venha a comprometer a usabilidade do Sistema; 17.2 O
            Aluno não tentará, incentivará ou ajudará qualquer outra pessoa a
            modificar qualquer parte do Sistema; 17.3 O Aluno não utilizará o
            Sistema para propagar vírus, spam (comunicações não solicitadas) ou
            outros ao Personal Access, a seus usuários - sejam clientes ou
            estabelecimentos - ou prestadores de serviços; 17.4 O Aluno não
            utilizará o Sistema para exibir pornografia, atos obscenos,
            mensagens de ódio, racismo, ou outras condutas vedadas por lei, sob
            pena de exclusão da plataforma; 17.5 O Aluno não utilizará nenhuma
            senha de acesso que não seja a sua para acessar a sua conta; 17.6 O
            Aluno não interferirá ou prejudicará, ainda que apenas no âmbito da
            tentativa, o acesso de qualquer usuário, estabelecimento ou
            prestadores de serviços; 17.7 O Aluno não utilizará o Sistema para
            alguma outra forma não expressamente prevista neste Termos de Uso;
            17.8 O Aluno se compromete a informar dados verdadeiros e mantê-los
            atualizados.
          </Text>

          <Text>
            18 Divulgação e Propaganda 18.1 Ao aceitar os Termos de Uso, o Aluno
            concorda em receber e-mails, mensagens de SMS ou por outros meios,
            com conteúdo promocional, de propaganda ou divulgação, tanto do
            Personal Access quanto de outras empresas parceiras. 18.2 O conteúdo
            promocional, de propaganda ou divulgação também poderá ser divulgado
            através do próprio aplicativo, pelo Personal Access ou por empresas
            parceiras.
          </Text>

          <Text>
            19. Responsabilidades e Propriedade Intelectual 19.1 O Aluno
            respeitará todos os direitos de propriedade intelectual associados
            ao Personal Access, não podendo se utilizar de nenhuma imagem,
            logomarca, desenhos industriais e outros materiais protegidos por
            lei sem a devida autorização; e 19.2 O Aluno reconhece o direito de
            o Personal Access tomar as medidas que considere razoavelmente
            necessárias ou apropriadas para que o presente contrato seja
            cumprido, incluindo a edição de informações que o Aluno tenha
            inserido no Sistema. Nesses casos, o Aluno será informado sobre a
            inconsistência, sendo marcado prazo razoável para que ele mesmo
            corrija a informação incorreta. 19.3 O Personal Access divulgará às
            autoridades constituídas, sempre que solicitado, requisitado ou
            quando entender necessário, as informações necessárias.
          </Text>

          <Text>
            20. Cancelamento de acesso ao aplicativo 20.1 O Aluno poderá
            cancelar seu acesso ao aplicativo Personal Access a qualquer
            momento, por qualquer motivo, enviado mensagem de e-mail com a
            solicitação para desativar sua conta, para o endereço
            “personalaccess.app@gmail.com”. Não é necessário informar a
            motivação, mas a informação poderá ser solicitada para pesquisa de
            melhora do serviço prestado. A exclusão do cadastro somente se dará
            após a utilização de todas as aulas pendentes. 20.2 O Personal
            Access também poderá a seu exclusivo critério e sem aviso prévio:
            (i) rescindir este contrato e encerrar sua conta caso este viole
            qualquer uma das obrigações assumidas neste acordo; (ii) impedir o
            acesso ao Sistema caso o Aluno represente algum risco atual ou
            potencial de responsabilização legal para o Personal Access ou (b)
            pratique de forma reincidente o agendamento seguido de não
            comparecimento proposital ou (iii) descontinuar os serviços
            prestados. 20.3 Quando a conta for encerrada pelo Aluno, pelo
            Professor ou pelo Personal Access, ou ainda nos casos de
            descontinuação dos serviços do aplicativo, será mantido no banco de
            dados as informações por até três meses, para que se ultimem as
            aulas pendentes e eventuais valores a pagar e a receber. 20.4 O
            Aluno concorda que qualquer violação sua deste Termos de Uso
            constitui uma prática prejudicial, podendo o Personal Access buscar
            qualquer medida legal para a reparação de seus danos.
          </Text>

          <Text>
            21. Condições Finais 21.1 Estes Termos e Condições constituem o
            acordo integral entre o Aluno e o Personal Access, prevalecendo
            sobre quaisquer outros acordos anteriores. 21.2 Se qualquer parte
            deste instrumento for considerada inválida ou inexequível, esta
            deverá ser interpretada de acordo com a lei aplicável de forma a
            refletir, o mais próximo possível, as intenções originais das
            partes, sendo que o restante do contrato permanecerá vigente e
            efetivo. 21.3 O Personal Access não será responsável pelo
            descumprimento de quaisquer obrigações em decorrência de caso
            fortuito, força maior, responsabilidade exclusiva de terceiros ou
            qualquer outra excludente de responsabilidade. 21.4 A comunicação
            entre o Personal Access e o Aluno será realizada através do e-mail
            cadastrado no Sistema, sem prejuízo de outros meios. O Aluno poderá
            enviar solicitações, críticas e sugestões para o e-mail
            “personalaccess.app@gmail.com”. 21.5 A HSTS CONSULTORIA LTDA,
            inscrita no Cadastro Nacional de Pessoas Jurídicas sob o nº
            38.824.434/0001-89, sociedade de responsabilidade limitada
            estabelecida no Brasil, é a única proprietária do aplicativo e dos
            serviços do Personal Access, incluindo o Sistema, suas interfaces,
            softwares, dados de referência alcançados, marcas registradas, dados
            estatísticos e todos os demais materiais protegidos por lei que
            permitem o funcionamento de nossos serviços. 21.6 O Aluno não está
            autorizado a modificar, reproduzir, distribuir, criar adaptações ou
            de qualquer forma explorar o aplicativo Personal Access de forma não
            expressamente autorizada por este instrumento.
          </Text>

          <Text>
            22. Legislação Aplicável e Foro 22.1 Estes Termos e Condições de Uso
            devem ser interpretados de acordo com a legislação da República
            Federativa do Brasil e as partes concordam expressamente com o foro
            da Comarca da cidade de Niterói, Estado do Rio de Janeiro para
            processar toda e qualquer reclamação ou controvérsia referente ao
            presente acordo. Versão 1 - Vigente a partir de: 01/01/2021
          </Text>
        </Content>
      </ContainerScroll>

      <SaveButtonContainer>
        <Button width="85%" onPress={() => goBack()}>
          Li e aceito os termos de uso
        </Button>

        <ButtonRefuse onPress={() => goBack()}>
          <ButtonRefuseLabel>Recusar</ButtonRefuseLabel>
        </ButtonRefuse>
      </SaveButtonContainer>
    </Container>
  );
};

export default TermsOfUse;
