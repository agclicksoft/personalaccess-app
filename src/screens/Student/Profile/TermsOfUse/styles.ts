import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Content = styled.View`
  padding: 10px 20px 160px;
`;

export const SaveButtonContainer = styled.View.attrs({
  shadowColor: '#000',
  shadowOffset: { width: 1, height: 1 },
  shadowOpacity: 0.2,
  borderRadius: 5,
  shadowRadius: 3,
  elevation: 5,
})`
  position: absolute;
  bottom: 0;
  width: 100%;
  padding: 5px 10% 25px;
  background-color: #fff;
  align-items: center;
`;

export const ButtonRefuse = styled.TouchableOpacity`
  margin-top: 15px;
`;

export const ButtonRefuseLabel = styled.Text`
  color: ${({ theme }) => theme.colors.grayDark};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
`;

export const Text = styled.Text`
  color: ${({ theme }) => theme.colors.blackLight};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  text-align: justify;
  margin: 5px 0;
`;
