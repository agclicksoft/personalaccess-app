import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const HeaderContainer = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 0 10px 0px;
  padding-top: ${getStatusBarHeight()}px;
`;

export const Title = styled.Text`
  padding-left: 15px;
  font-size: ${({ theme }) => theme.fontSizes.size20};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
`;

export const Divider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
  margin: 15px 0 0;
`;

export const Wrapper = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  padding: 15px 20px 0;
`;

export const ProfileImage = styled.Image`
  width: 97px;
  height: 97px;
  border-radius: 48.5px;
  margin-right: 21px;
`;

export const NameWrapper = styled.View``;

export const Name = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size20};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
`;

export const TouchableOpacity = styled.TouchableOpacity``;

export const EditProfile = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size12};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  color: ${({ theme }) => theme.colors.purple};
  padding: 5px 0;
`;

export const MenuButton = styled.TouchableOpacity`
  width: 100%;
`;

export const MenuContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  padding-right: 30px;
  width: 100%;
`;

export const Holder = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const MenuIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  margin-right: 15px;
  height: 30px;
  width: 30px;
`;

export const TextWrapper = styled.View``;

export const MenuTitle = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  font-size: ${({ theme }) => theme.fontSizes.size16};
  color: ${({ theme }) => theme.colors.purple};
`;

export const MenuLabel = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  font-size: ${({ theme }) => theme.fontSizes.size11};
  color: ${({ theme }) => theme.colors.grayDark};
  margin-top: 5px;
`;
