/* eslint-disable no-nested-ternary */
import React from 'react';

import { differenceInYears, parseISO } from 'date-fns';

import { ContainerScroll, HeaderPurple } from '~/components';
import { useAuth } from '~/hooks/auth';
import ParQQuestion from '~/models/ParQQuestion';
import { PlaceholderImage } from '~/utils/consts';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Wrapper,
  Title,
  Holder,
  Question,
  Answer,
  BodyInfoContainer,
  BodyInfoHolder,
} from './styles';

const PublicView: React.FC = () => {
  const { user } = useAuth();

  return (
    <Container>
      <HeaderPurple>Perfil público</HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{ uri: user.img_profile ?? PlaceholderImage }}
          />

          <Name>{user.name}</Name>

          <Age>
            {user.birthday
              ? `${differenceInYears(new Date(), parseISO(user.birthday))} anos`
              : 'Idade não informada'}
          </Age>

          <Divider />

          <Wrapper>
            <Title>PAR-Q</Title>

            {user.student.user.faqAnswered?.length > 0 ? (
              <>
                <BodyInfoContainer>
                  {user.student.user.faqAnswered?.map((item: ParQQuestion) => {
                    return item.faqQuestion.id === 1 ? (
                      <BodyInfoHolder key={item.id}>
                        <Question>{item.faqQuestion.question}: </Question>
                        <Answer>{item.answered} kg</Answer>
                      </BodyInfoHolder>
                    ) : null;
                  })}

                  {user.student.user.faqAnswered?.map((item: ParQQuestion) => {
                    return item.faqQuestion.id === 2 ? (
                      <BodyInfoHolder key={item.id}>
                        <Question>{item.faqQuestion.question}: </Question>
                        <Answer>{item.answered} m</Answer>
                      </BodyInfoHolder>
                    ) : null;
                  })}
                </BodyInfoContainer>

                {user.student.user.faqAnswered?.map((item: ParQQuestion) => {
                  return item.faqQuestion.id > 2 && item.answered !== null ? (
                    <Holder key={item.id}>
                      <Question>{item.faqQuestion.question}</Question>
                      <Answer>
                        {item.answered === 'false'
                          ? item.answered === 'false'
                            ? 'Não'
                            : item.answered
                          : item.answered === 'true'
                          ? 'Sim'
                          : item.answered}
                      </Answer>
                    </Holder>
                  ) : null;
                })}
              </>
            ) : (
              <Title>Não preenchido até momento</Title>
            )}
          </Wrapper>
        </FormContainer>
      </ContainerScroll>
    </Container>
  );
};

export default PublicView;
