/* eslint-disable array-callback-return */
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { ActivityIndicator, Linking, Platform, View } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import axios from 'axios';
import * as DocumentPicker from 'expo-document-picker';
import FormData from 'form-data';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import {
  ContainerScroll,
  HeaderBack,
  Button,
  Input,
  MaskedInput,
} from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import ParQQuestion from '~/models/ParQQuestion';
import api from '~/services/api';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Container,
  Title,
  Wrapper,
  Label,
  WeightHeightWrapper,
  HeightHolder,
  WeightHolder,
  SymptomsWrapper,
  ButtonWrapper,
  YesOrNoQuestionWrapper,
  HeartButtonWrapper,
  HeartDocumentWrapper,
} from './styles';

import Checkbox from './Checkbox';

const symptomsList = [
  { text: 'Tontura' },
  { text: 'Enjoo' },
  { text: 'Falta de ar' },
  { text: 'Nada' },
];

const optionsList = [
  { text: 'Sim', value: true },
  { text: 'Não', value: false },
];

interface FileProps {
  type: string;
  name: string;
  size: number;
  uri: string;
  lastModified?: number;
  file?: File;
  output?: FileList;
}

const ParQ: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { user, updateStudent } = useAuth();
  const { goBack } = useNavigation();
  const [loading, setLoading] = useState(false);
  const [parqData] = useState<ParQQuestion[]>(
    user.student?.user?.faqAnswered ?? [],
  );
  const [symptoms, setSymptoms] = useState(
    parqData[3]?.answered?.split(', ') ?? ['Nada'],
  );
  const [diabete, setDiabete] = useState(false);
  const [hipertensao, setHipertensao] = useState(false);
  const [cardiaco, setCardiaco] = useState(false);
  const [respiratorio, setRespiratorio] = useState(false);
  const [medicamento, setMedicamento] = useState(false);
  const [cirurgia, setCirurgia] = useState(false);
  const [dores, setDores] = useState(false);
  const [restricao, setRestricao] = useState(false);
  const [file, setFile] = useState(null);
  const [filePath, setFilePath] = useState<string>(null);

  const saveAttachment = useCallback(
    async (id) => {
      if (cardiaco && file) {
        try {
          const data = new FormData();
          data.append('attachments', {
            name: file.name,
            type: file.type,
            uri:
              Platform.OS === 'android'
                ? file.uri
                : file.uri.replace('file://', ''),
          });

          await axios({
            url: `https://api.personalaccess.app/api/faq-questions/${id}`,
            method: 'POST',
            data,
            headers: {
              Accept: 'application/json',
              'Content-Type': 'multipart/form-data',
              Authorization: api.defaults.headers.Authorization,
            },
          });
        } catch (err) {
          console.log(err.response);
        }
      }
    },
    [cardiaco, file],
  );

  const handleSubmit = useCallback(
    async (data) => {
      try {
        setLoading(true);

        const schema = Yup.object().shape({
          peso: Yup.string().required('Peso é obrigatório'),
          altura: Yup.string().required('Altura é obrigatório'),
          objetivo: Yup.string().required('Objetivo é obrigatório'),
          desconforto: Yup.string().required('Objetivo é obrigatório'),
          diabete: diabete
            ? Yup.string().required('Estágio é obrigatório')
            : null,
          hipertensao: hipertensao
            ? Yup.string().required('Máxima é obrigatório')
            : null,
          cardiaco: cardiaco
            ? Yup.string().required('Qual é obrigatório')
            : null,
          respiratorio: respiratorio
            ? Yup.string().required('Qual é obrigatório')
            : null,
          medicamento: medicamento
            ? Yup.string().required('Qual é obrigatório')
            : null,
          cirurgia: cirurgia
            ? Yup.string().required('Qual é obrigatório')
            : null,
          dores: dores ? Yup.string().required('Qual é obrigatório') : null,
          restricao: restricao
            ? Yup.string().required('Qual é obrigatório')
            : null,
        });

        await schema.validate(data, { abortEarly: false });

        const formData = [
          {
            faq_question_id: 1,
            answered: data.peso,
          },
          {
            faq_question_id: 2,
            answered: data.altura,
          },
          {
            faq_question_id: 3,
            answered: data.objetivo,
          },
          {
            faq_question_id: 4,
            answered: symptoms.join(', '),
          },
          {
            faq_question_id: 5,
            answered: data.desconforto,
          },
          {
            faq_question_id: 6,
            answered: diabete,
          },
          {
            faq_question_id: 7,
            answered: data.diabete ?? null,
          },
          {
            faq_question_id: 8,
            answered: hipertensao,
          },
          {
            faq_question_id: 9,
            answered: data.hipertensao ?? null,
          },
          {
            faq_question_id: 10,
            answered: cardiaco,
          },
          {
            faq_question_id: 11,
            answered: data.cardiaco ?? null,
          },
          {
            faq_question_id: 12,
            answered: respiratorio,
          },
          {
            faq_question_id: 13,
            answered: data.respiratorio ?? null,
          },
          {
            faq_question_id: 14,
            answered: medicamento,
          },
          {
            faq_question_id: 15,
            answered: data.medicamento ?? null,
          },
          {
            faq_question_id: 16,
            answered: cirurgia,
          },
          {
            faq_question_id: 17,
            answered: data.cirurgia ?? null,
          },
          {
            faq_question_id: 18,
            answered: dores,
          },
          {
            faq_question_id: 19,
            answered: data.dores ?? null,
          },
          {
            faq_question_id: 20,
            answered: restricao,
          },
          {
            faq_question_id: 21,
            answered: data.restricao ?? null,
          },
        ];

        const response = await api.put('/student/faq-question', {
          faqAnswered: formData,
        });

        let heartId = null;

        await response.data.data.filter((e) => {
          e.faq_question_id === 10 ? (heartId = e.id) : null;
        });

        if (file) {
          await saveAttachment(heartId);
        }

        setLoading(false);

        const updatedStudent: User = {
          ...user,
          id: user.id,
          student: {
            ...user.student,
            user: {
              faqAnswered: response.data.data,
            },
          },
        };

        console.log(formData);

        await updateStudent(updatedStudent);

        showMessage({
          type: 'success',
          message: 'Par-Q atualizado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });

        setLoading(false);
        goBack();
      } catch (err) {
        setLoading(false);
        console.log(err?.response);

        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);

          console.log(errors);

          showMessage({
            type: 'danger',
            message: 'Erro ao atualizar seu Par-Q.',
            description: 'Verifique e tente novamente',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        showMessage({
          type: 'danger',
          message: 'Erro inesperado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [
      cardiaco,
      cirurgia,
      diabete,
      dores,
      file,
      goBack,
      hipertensao,
      medicamento,
      respiratorio,
      restricao,
      saveAttachment,
      symptoms,
      updateStudent,
      user,
    ],
  );

  const toggleSymptom = useCallback(
    (item: string) => {
      if (item !== 'Nada') {
        const nothingExist = symptoms.some((a) => a === 'Nada');

        if (nothingExist) {
          setSymptoms([item]);
        } else if (symptoms.includes(item)) {
          setSymptoms(symptoms.filter((a) => a !== item));
        } else {
          setSymptoms([...symptoms, item]);
        }
      } else {
        setSymptoms([item]);
      }
    },
    [symptoms],
  );

  const symptomsMemo = useMemo(
    () =>
      symptomsList.map((item) => (
        <Checkbox
          key={item.text}
          title={item.text}
          onPress={() => {
            toggleSymptom(item.text);
          }}
          value={symptoms.includes(item.text)}
        />
      )),
    [symptoms, toggleSymptom],
  );

  const renderItem = useCallback((value, setValue) => {
    return optionsList.map((item) => (
      <Checkbox
        key={item.text}
        title={item.text}
        onPress={() => {
          setValue(item.value);
        }}
        value={value === item.value}
      />
    ));
  }, []);

  const pickDocument = useCallback(async () => {
    const result = await DocumentPicker.getDocumentAsync({
      type: 'application/pdf',
    });

    if (result.type === 'success') {
      setFile(result);
    } else {
      return;
    }

    console.log(result);
  }, []);

  useEffect(() => {
    if (parqData.length >= 1) {
      setTimeout(() => {
        formRef.current.setData({
          peso: parqData[0].answered,
          altura: parqData[1].answered,
          objetivo: parqData[2].answered,
          desconforto: parqData[4].answered,
          diabete: parqData[6].answered,
          hipertensao: parqData[8].answered,
          cardiaco: parqData[10].answered,
          respiratorio: parqData[12].answered,
          medicamento: parqData[14].answered,
          cirurgia: parqData[16].answered,
          dores: parqData[18].answered,
          restricao: parqData[20].answered,
        });
      }, 200);

      setDiabete(parqData[5].answered !== 'false');
      setHipertensao(parqData[7].answered !== 'false');
      setCardiaco(parqData[9].answered !== 'false');
      setRespiratorio(parqData[11].answered !== 'false');
      setMedicamento(parqData[13].answered !== 'false');
      setCirurgia(parqData[15].answered !== 'false');
      setDores(parqData[17].answered !== 'false');
      setRestricao(parqData[19].answered !== 'false');
      setFilePath(parqData[9]?.attachments[0]?.path);
    }
  }, [parqData]);

  return (
    <Container>
      <HeaderBack>PAR-Q</HeaderBack>

      <ContainerScroll>
        <Wrapper>
          <Title>{`Questionário de Prontidão\n para Atividade Física (PAR-Q)`}</Title>

          <Form ref={formRef} onSubmit={handleSubmit} initialData={parqData}>
            <WeightHeightWrapper>
              <WeightHolder>
                <Label>Peso:</Label>
                <MaskedInput
                  name="peso"
                  type="custom"
                  options={{
                    mask: '999',
                  }}
                  placeholder="Peso"
                  keyboardType="decimal-pad"
                  width="100%"
                  maxLength={3}
                  initialValue={parqData[0]?.answered ?? null}
                />
              </WeightHolder>

              <HeightHolder>
                <Label>Altura:</Label>

                <MaskedInput
                  name="altura"
                  type="custom"
                  options={{
                    mask: '9,99',
                  }}
                  placeholder="Altura"
                  keyboardType="decimal-pad"
                  width="100%"
                  maxLength={4}
                  initialValue={parqData[1]?.answered ?? null}
                />
              </HeightHolder>
            </WeightHeightWrapper>

            <Label>Qual o seu objetivo?</Label>
            <Input name="objetivo" width="100%" />

            <Label>
              Na prática de atividades físicas, você sente algum desses
              sintomas?
            </Label>

            <SymptomsWrapper>{symptomsMemo}</SymptomsWrapper>

            <Label>Algum outro desconforto?</Label>
            <Input name="desconforto" width="100%" />

            <Label>Diabético?</Label>
            <YesOrNoQuestionWrapper>
              {renderItem(diabete, setDiabete)}
            </YesOrNoQuestionWrapper>

            {diabete && (
              <>
                <Label>Qual estágio?</Label>
                <Input name="diabete" width="100%" />
              </>
            )}

            <Label>Hipertenso?</Label>
            <YesOrNoQuestionWrapper>
              {renderItem(hipertensao, setHipertensao)}
            </YesOrNoQuestionWrapper>

            {hipertensao && (
              <>
                <Label>Qual a máxima?</Label>
                <Input name="hipertensao" width="100%" />
              </>
            )}

            <Label>Possui algum problema cardíaco?</Label>
            <YesOrNoQuestionWrapper>
              {renderItem(cardiaco, setCardiaco)}
            </YesOrNoQuestionWrapper>

            {cardiaco && (
              <>
                <Label>Qual?</Label>
                <Input name="cardiaco" width="100%" />

                {filePath && (
                  <HeartDocumentWrapper
                    onPress={() => Linking.openURL(filePath)}
                  >
                    <Label>Visualizar atestado</Label>

                    <Ionicons
                      name="ios-cloud-download"
                      size={24}
                      color="#37153D"
                      style={{ marginTop: 10, marginLeft: 10 }}
                    />
                  </HeartDocumentWrapper>
                )}

                <HeartButtonWrapper>
                  {file ? (
                    <Button
                      onPress={() => pickDocument()}
                      style={{ backgroundColor: '#F76E1E' }}
                    >
                      Atestado anexado
                    </Button>
                  ) : (
                    <Button onPress={() => pickDocument()}>
                      {filePath ? 'Anexar novo atestado' : 'Anexar atestado'}
                    </Button>
                  )}
                </HeartButtonWrapper>
              </>
            )}

            <Label>Problema respiratório?</Label>
            <YesOrNoQuestionWrapper>
              {renderItem(respiratorio, setRespiratorio)}
            </YesOrNoQuestionWrapper>

            {respiratorio && (
              <>
                <Label>Qual?</Label>
                <Input name="respiratorio" width="100%" />
              </>
            )}

            <Label>Utiliza algum tipo de medicamento ou drogas?</Label>
            <YesOrNoQuestionWrapper>
              {renderItem(medicamento, setMedicamento)}
            </YesOrNoQuestionWrapper>

            {medicamento && (
              <>
                <Label>Qual?</Label>
                <Input name="medicamento" width="100%" />
              </>
            )}

            <Label>Fez alguma cirurgia nos últimos 6 meses?</Label>
            <YesOrNoQuestionWrapper>
              {renderItem(cirurgia, setCirurgia)}
            </YesOrNoQuestionWrapper>

            {cirurgia && (
              <>
                <Label>Qual?</Label>
                <Input name="cirurgia" width="100%" />
              </>
            )}

            <Label>
              Sente incômodo ou dores em articulações e/ou coluna na prática de
              atividade física?
            </Label>

            <YesOrNoQuestionWrapper>
              {renderItem(dores, setDores)}
            </YesOrNoQuestionWrapper>

            {dores && (
              <>
                <Label>Qual?</Label>
                <Input name="dores" width="100%" />
              </>
            )}

            <Label>
              Tem alguma restrição ou recomendação médica para a prática de
              exercícios físicos?
            </Label>

            <YesOrNoQuestionWrapper>
              {renderItem(restricao, setRestricao)}
            </YesOrNoQuestionWrapper>

            {restricao && (
              <>
                <Label>Qual?</Label>
                <Input name="restricao" width="100%" />
              </>
            )}
          </Form>

          <ButtonWrapper>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </ButtonWrapper>
        </Wrapper>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default ParQ;
