import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Wrapper = styled.View`
  padding: 10px 20px 60px;
  width: 100%;
  align-items: center;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size16};
  font-family: ${({ theme }) => theme.fontFamily.sofiaSemiBold};
  color: ${({ theme }) => theme.colors.purple};
  margin: 15px 0 5px;
  text-align: center;
`;

export const Label = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  margin: 12px 0 0 16px;
`;

export const WeightHeightWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-top: 20px;
`;

export const WeightHolder = styled.View`
  flex: 1;
  margin-right: 20px;
`;

export const HeightHolder = styled.View`
  flex: 1;
`;

export const SymptomsWrapper = styled.View`
  margin-bottom: 10px;
`;

export const YesOrNoQuestionWrapper = styled.View`
  flex-direction: row;
`;

export const ButtonWrapper = styled.View`
  margin: 30px 0;
`;

export const HeartButtonWrapper = styled.View`
  align-items: center;
  justify-content: center;
  margin-top: -5px;
`;

export const HeartDocumentWrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 10px 0;
`;
