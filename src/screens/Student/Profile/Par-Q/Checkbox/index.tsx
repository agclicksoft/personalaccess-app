import React from 'react';
import { TouchableOpacityProps } from 'react-native';

import { Container, Box, Title } from './styles';

interface CheckboxProps extends TouchableOpacityProps {
  value: boolean;
  title: string;
}

const Checkbox: React.FC<CheckboxProps> = ({ value, onPress, title }) => {
  return (
    <Container onPress={onPress}>
      <Box value={value} />
      <Title>{title}</Title>
    </Container>
  );
};

export default Checkbox;
