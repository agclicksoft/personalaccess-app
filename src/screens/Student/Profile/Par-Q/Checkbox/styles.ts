import styled from 'styled-components/native';

interface CheckBoxProps {
  value: boolean;
}

export const Container = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  padding: 20px 20px 0;
`;

export const Box = styled.View<CheckBoxProps>`
  align-items: center;
  justify-content: center;
  width: 22px;
  height: 22px;
  border-radius: 11px;
  background: ${({ value }) => (value ? '#37153D' : '#BCBCBC')};
  margin-right: 10px;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  color: ${({ theme }) => theme.colors.grayDark};
`;
