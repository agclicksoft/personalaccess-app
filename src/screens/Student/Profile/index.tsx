import React from 'react';
import { FlatList } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import {
  IconProfile,
  IconCardiology,
  IconCreditCard,
  IconSignOut,
  IconFile,
} from '~/assets/icons';
import { useAuth } from '~/hooks/auth';
import { PlaceholderImage } from '~/utils/consts';

import {
  Container,
  HeaderContainer,
  Title,
  Divider,
  Wrapper,
  ProfileImage,
  NameWrapper,
  Name,
  TouchableOpacity,
  EditProfile,
  MenuContainer,
  Holder,
  MenuButton,
  MenuIcon,
  TextWrapper,
  MenuTitle,
  MenuLabel,
} from './styles';

const options = [
  {
    title: 'Perfil Público',
    label: 'Visualizar seu perfil público',
    icon: IconProfile,
    screen: 'PublicView',
  },
  {
    title: 'PAR-Q',
    label: 'Questionário de Prontidão para Atividade Física',
    icon: IconCardiology,
    screen: 'ParQ',
  },
  {
    title: 'Histórico de pagamentos',
    label: 'Visualizar aulas pagas',
    icon: IconCreditCard,
    screen: 'Payments',
  },
  {
    title: 'Termos de uso',
    label: 'Visualizar termos',
    icon: IconFile,
    screen: 'TermsOfUse',
  },
  {
    title: 'Sair',
    label: 'Sair da sua conta',
    icon: IconSignOut,
    screen: '',
  },
];

const Profile: React.FC = () => {
  const { navigate } = useNavigation();
  const { user, signOut } = useAuth();

  return (
    <Container>
      <HeaderContainer>
        <Title>Meu perfil</Title>
      </HeaderContainer>

      <Divider />

      <Wrapper>
        <ProfileImage
          source={{
            uri: user.img_profile ?? PlaceholderImage,
          }}
        />

        <NameWrapper>
          <Name>{user.name}</Name>

          <TouchableOpacity onPress={() => navigate('EditProfile')}>
            <EditProfile>Editar perfil</EditProfile>
          </TouchableOpacity>
        </NameWrapper>
      </Wrapper>

      <Divider />

      <FlatList
        showsVerticalScrollIndicator={false}
        data={options}
        keyExtractor={(option) => String(option.title)}
        ItemSeparatorComponent={Divider}
        renderItem={({ item }) => (
          <MenuButton
            key={item.title}
            onPress={() => {
              if (item.title !== 'Sair') {
                navigate(item.screen);
              } else {
                signOut();
              }
            }}
          >
            <MenuContainer>
              <Holder>
                <MenuIcon source={item.icon} />

                <TextWrapper>
                  <MenuTitle>{item.title}</MenuTitle>
                  <MenuLabel>{item.label}</MenuLabel>
                </TextWrapper>
              </Holder>

              <Ionicons
                name="ios-arrow-forward"
                size={21}
                color="rgba(112,112,112,0.5)"
              />
            </MenuContainer>
          </MenuButton>
        )}
      />
    </Container>
  );
};

export default Profile;
