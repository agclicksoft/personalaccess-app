import { StyleSheet } from 'react-native';

export const style = StyleSheet.create({
  inputIOS: {
    height: 40,
    color: '#2E2E2E',
    backgroundColor: '#fff',
    borderRadius: 23,
    borderWidth: 1,
    borderColor: '#fff',
    paddingHorizontal: 24,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'normal',
    fontFamily: 'SofiaPro-SemiBold',
  },
  inputAndroid: {
    height: 40,
    color: '#2E2E2E',
    backgroundColor: '#fff',
    borderRadius: 23,
    borderWidth: 1,
    borderColor: '#fff',
    paddingHorizontal: 24,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'normal',
    fontFamily: 'SofiaPro-SemiBold',
  },
});
