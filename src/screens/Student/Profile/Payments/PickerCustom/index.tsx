/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import RNPickerSelect from 'react-native-picker-select';

import { style } from './styles';

interface Values extends PickerSelectProps {
  items: any;
  value: any;
  setValue: any;
}

const PickerCustom: React.FC<Values> = ({ items, value, setValue }) => {
  return (
    <RNPickerSelect
      items={items}
      value={value}
      style={style}
      placeholder={{
        label: 'Selecionar',
      }}
      doneText="Selecionar"
      useNativeAndroidPickerStyle={false}
      pickerProps={{ mode: 'dropdown' }}
      textInputProps={{ allowFontScaling: false }}
      onValueChange={(itemValue) => setValue(itemValue)}
    />
  );
};

export default PickerCustom;
