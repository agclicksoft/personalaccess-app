import React, { useCallback, useState, useEffect } from 'react';
import { FlatList } from 'react-native';

import LottieView from 'lottie-react-native';

import { WorkoutAnimation, LoadingAnimation } from '~/assets/animations';
import { IconCreditCard, IconMoney } from '~/assets/icons';
import { HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import api from '~/services/api';
import { formatDate, generateDateToPicker } from '~/utils/formatDate';
import formatValue from '~/utils/formatValue';

import {
  Container,
  FilterContainer,
  LabelPicker,
  LottieContainer,
  LottieText,
  PaymentHistoryItem,
  PaymentHistoryItemIconView,
  PaymentHistoryItemIcon,
  PaymentHistoryItemView,
  PaymentHistoryItemValue,
  PaymentHistoryItemDescription,
  PaymentHistoryItemDateView,
  PaymentHistoryItemDate,
  PaymentHistoryItemStatus,
  PaymentHistoryItemText,
} from './styles';

import PickerCustom from './PickerCustom';

const Payments: React.FC = () => {
  const { user } = useAuth();
  const [loading, setLoading] = useState(false);
  const [dateToPicker] = useState(generateDateToPicker());
  const [chosenDate, setChosenDate] = useState(dateToPicker[0].value);
  const [page, setPage] = useState(1);
  const [dataPaymentHistory, setDataPaymentHistory] = useState([]);

  const handlePaymentHistory = useCallback(
    async (pageNumber) => {
      try {
        setLoading(true);

        const [month, year] = chosenDate.split('/');
        const response = await api.get(
          `/students/${user.id}/contracts?month=${month}&year=${year}&method_payment=PENDENTE&page=${pageNumber}`,
        );

        if (response.data) {
          setDataPaymentHistory(
            pageNumber === 1
              ? response.data.data
              : [...dataPaymentHistory, ...response.data.data],
          );
        }

        setLoading(false);
      } catch (err) {
        setLoading(false);
        console.log(err);
      }
    },
    [chosenDate, dataPaymentHistory, user],
  );

  useEffect(() => {
    (async () => {
      await handlePaymentHistory(1);
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chosenDate]);

  const loadOnScroll = useCallback(async () => {
    setPage(page + 1);

    await handlePaymentHistory(page + 1);
  }, [handlePaymentHistory, page]);

  return (
    <Container>
      <HeaderBack>Histórico de pagamento</HeaderBack>

      <FilterContainer>
        <LabelPicker>Filtrar:</LabelPicker>
        <PickerCustom
          items={dateToPicker}
          value={chosenDate}
          setValue={setChosenDate}
        />

        <PaymentHistoryItemDate
          style={{
            color: '#fff',
            fontSize: 11,
          }}
        >
          Lista meramente ilustrativa.
        </PaymentHistoryItemDate>
      </FilterContainer>

      {dataPaymentHistory.length !== 0 ? (
        <FlatList
          data={dataPaymentHistory}
          onEndReachedThreshold={0.5}
          onEndReached={loadOnScroll}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => String(item.id)}
          renderItem={({ item }) => (
            <>
              {item.method_payment !== 'PENDENTE' && (
                <PaymentHistoryItem>
                  <PaymentHistoryItemIconView>
                    <PaymentHistoryItemIcon
                      source={
                        item.method_payment !== 'CARTAO'
                          ? IconMoney
                          : IconCreditCard
                      }
                    />
                  </PaymentHistoryItemIconView>

                  <PaymentHistoryItemView>
                    <PaymentHistoryItemValue>
                      {formatValue(item.professionalPlan.price)}
                    </PaymentHistoryItemValue>

                    <PaymentHistoryItemDescription>
                      {`Personal: ${item.professional.user.name}`}
                    </PaymentHistoryItemDescription>
                  </PaymentHistoryItemView>

                  <PaymentHistoryItemDateView>
                    <PaymentHistoryItemDate>
                      {formatDate(item.created_at)}
                    </PaymentHistoryItemDate>

                    <PaymentHistoryItemStatus
                      style={{
                        backgroundColor:
                          item.status_payment === 'PAGO'
                            ? '#009819'
                            : '#f55252',
                      }}
                    >
                      <PaymentHistoryItemText>
                        {item.status_payment === 'PAGO'
                          ? 'Confirmado'
                          : 'Não confirmado'}
                      </PaymentHistoryItemText>
                    </PaymentHistoryItemStatus>
                  </PaymentHistoryItemDateView>
                </PaymentHistoryItem>
              )}
            </>
          )}
        />
      ) : null}

      {dataPaymentHistory.length === 0 && !loading ? (
        <LottieContainer>
          <LottieText>Nenhum pagamento encontrado.</LottieText>

          <LottieView
            style={{
              height: 170,
            }}
            source={WorkoutAnimation}
            autoPlay
            loop
          />
        </LottieContainer>
      ) : null}

      {loading && dataPaymentHistory.length === 0 && (
        <LottieContainer>
          <LottieView
            style={{
              height: 170,
            }}
            source={WorkoutAnimation}
            autoPlay
            loop
          />
        </LottieContainer>
      )}
    </Container>
  );
};

export default Payments;
