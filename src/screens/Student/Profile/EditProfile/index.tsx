import React, { useRef, useState, useCallback } from 'react';
import { ActivityIndicator, Alert, Platform, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { format, parseISO } from 'date-fns';
import * as ImagePicker from 'expo-image-picker';
import FormData from 'form-data';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { IconCamera } from '~/assets/icons';
import {
  Button,
  ContainerScroll,
  HeaderBack,
  Input,
  MaskedInput,
} from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';
import { getValidationErrors } from '~/utils/getValidationErrors';

import {
  Container,
  ImageWrapper,
  Image,
  CameraIcon,
  ImageButton,
  FormWrapper,
  Label,
  ButtonWrapper,
} from './styles';

import ModalPassword from './ModalPassword';

interface Picture {
  id?: number;
  filename?: string;
  mediaType?: string;
  uri?: string;
  type?: string;
}

const EditProfile: React.FC = () => {
  const { user, updateStudent } = useAuth();
  const navigation = useNavigation();
  const formRef = useRef<FormHandles>(null);
  const [loading, setLoading] = useState(false);
  const [cepText, setCepText] = useState(user?.address_zipcode);
  const [modalPassword, setModalPassword] = useState(false);

  const handleSubmit = useCallback(
    async (data: User) => {
      try {
        setLoading(true);

        const schema = Yup.object().shape({
          name: Yup.string().required('Nome é obrigatório'),
          cpf: Yup.string().required('CPF é obrigatório'),
          birthday: Yup.string().required('Data de nascimento é obrigatório '),
          email: Yup.string().required('Email é obrigatório'),
          contact: Yup.string().required('Telefone é obrigatório'),
        });

        await schema.validate(data, { abortEarly: false });

        const formData = {
          ...data,
          id: user.id,
          address_zipcode: cepText,
          student: user.student,
        };

        console.log(formData);

        await updateStudent(formData);

        showMessage({
          type: 'success',
          message: 'Perfil atualizado com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });

        navigation.goBack();
      } catch (err) {
        setLoading(false);
        console.log(err?.response);

        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);

          console.log(errors);

          showMessage({
            type: 'danger',
            message: 'Erro ao atualizar seu perfil',
            description: 'Verifique e tente novamente',
            titleStyle: {
              textAlign: 'center',
            },
            textStyle: {
              textAlign: 'center',
            },
          });

          return;
        }

        showMessage({
          type: 'danger',
          message: 'Erro inesperado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [user, cepText, updateStudent, navigation],
  );

  const handlePickImage = useCallback(async () => {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 0.7,
      });

      if (!result.cancelled) {
        const studentData = new FormData();
        const photo = result as Picture;

        studentData.append('profile', {
          type: photo.mediaType ?? 'image/jpeg',
          name: photo.filename ?? `${user.name}-post`,
          uri:
            Platform.OS === 'android'
              ? photo.uri
              : photo.uri.replace('file://', ''),
        });

        const response = await api.post('/users/profile', studentData);

        const formData: User = {
          ...response.data.data,
          id: user.id,
          student: user.student,
        };

        console.log(formData);

        await updateStudent(formData);

        showMessage({
          type: 'success',
          message: 'Foto atualizada com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    } catch (err) {
      console.log(err);

      showMessage({
        type: 'danger',
        message: 'Erro inesperado',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [user, updateStudent]);

  const getAddressByZipcode = useCallback(async (text: string) => {
    setCepText(text);

    if (text.length === 9) {
      const response = await api.get(`https://viacep.com.br/ws/${text}/json/`);

      if (response.data.logradouro) {
        const currentData = formRef.current?.getData();

        console.log(currentData);

        formRef.current?.setData({
          ...currentData,
          password: '*************',
          address_street: response.data.logradouro,
          address_neighborhood: response.data.bairro,
          address_city: response.data.localidade,
          address_uf: response.data.uf,
        });
      } else {
        Alert.alert('CEP não localizado, favor inserir endereço manualmente.');
      }
    }
  }, []);

  return (
    <Container>
      <HeaderBack>Editar perfil</HeaderBack>

      <ContainerScroll>
        <FormWrapper>
          <ImageWrapper>
            <ImageButton onPress={handlePickImage}>
              <Image source={{ uri: user.img_profile ?? PlaceholderImage }} />

              <CameraIcon source={IconCamera} />
            </ImageButton>
          </ImageWrapper>

          <Form ref={formRef} onSubmit={handleSubmit} initialData={user}>
            <Label>Nome:</Label>
            <Input
              name="name"
              placeholder="Nome"
              autoCapitalize="words"
              autoCorrect={false}
            />

            <Label>CPF:</Label>
            <MaskedInput
              name="cpf"
              type="cpf"
              placeholder="CPF"
              keyboardType="number-pad"
              initialValue={user.cpf}
            />

            <Label>Data de nascimento:</Label>
            <MaskedInput
              name="birthday"
              type="datetime"
              options={{
                format: 'DD/MM/YYYY',
              }}
              placeholder="Data de nascimento"
              maxLength={10}
              keyboardType="number-pad"
              initialValue={
                user.birthday
                  ? format(parseISO(user.birthday), "dd'/'MM'/'yyyy")
                  : ''
              }
            />

            <Label>E-mail:</Label>
            <Input
              name="email"
              placeholder="E-mail"
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
            />

            <Label>Senha:</Label>
            <Input
              name="password"
              placeholder="Senha"
              secureTextEntry
              editable={false}
              value="*************"
              changePassword
              showModal={() => setModalPassword(!modalPassword)}
            />

            <Label>Telefone/Celular:</Label>
            <MaskedInput
              name="contact"
              type="cel-phone"
              placeholder="Telefone/Celular"
              keyboardType="number-pad"
              initialValue={user.contact}
            />

            <Label>Telefone de emergências:</Label>
            <MaskedInput
              name="emergency_phone"
              type="cel-phone"
              placeholder="Telefone de emergências"
              keyboardType="number-pad"
              initialValue={user.emergency_phone}
            />

            <Label>CEP:</Label>
            <MaskedInput
              name="address_zipcode"
              type="zip-code"
              placeholder="CEP"
              keyboardType="decimal-pad"
              value={cepText}
              maxLength={9}
              onChangeText={(text) => {
                getAddressByZipcode(text);
              }}
            />

            <Label>Estado:</Label>
            <Input name="address_uf" placeholder="Estado" />

            <Label>Cidade:</Label>
            <Input name="address_city" placeholder="Cidade" />

            <Label>Bairro:</Label>
            <Input name="address_neighborhood" placeholder="Bairro" />

            <Label>Endereço:</Label>
            <Input name="address_street" placeholder="Endereço" />

            <Label>Número:</Label>
            <Input
              name="address_number"
              placeholder="Número"
              keyboardType="decimal-pad"
            />

            <Label>Complemento:</Label>
            <Input name="address_complement" placeholder="Complemento" />
          </Form>

          <ButtonWrapper>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </ButtonWrapper>
        </FormWrapper>
      </ContainerScroll>

      <ModalPassword
        isVisible={modalPassword}
        closeModal={() => setModalPassword(!modalPassword)}
      />

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default EditProfile;
