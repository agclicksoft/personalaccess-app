import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { Form } from '@unform/mobile';
import { showMessage } from 'react-native-flash-message';
import * as Yup from 'yup';

import { Button, Input, Modal } from '~/components';
import { useAuth } from '~/hooks/auth';
import { User } from '~/models';
import api from '~/services/api';

import { Content, ModalTitle, ButtonSaveLG } from './styles';

interface ModalComponentProps {
  isRedefinition?: boolean;
  isVisible: boolean;
  closeModal: () => void;
}

interface UpdatePasswordFormData {
  password: string;
}

const ModalPassword: React.FC<ModalComponentProps> = ({
  isRedefinition,
  isVisible,
  closeModal,
}) => {
  const formRef = useRef(null);
  const { user, updateStudent } = useAuth();

  const [loading, setLoading] = useState(false);

  const handleUpdatePassword = useCallback(
    async (data: UpdatePasswordFormData) => {
      try {
        setLoading(true);
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          password: Yup.string().required(),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        await api.post(`/users/change-password`, {
          password: data.password,
        });

        const formData: User = {
          id: user.id,
          is_redefinition: false,
          student: {
            ...user.student,
          },
        };

        console.log(formData);

        await updateStudent(formData);

        closeModal();

        showMessage({
          type: 'success',
          message: 'Senha atualizada com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      } catch (err) {
        console.log(err);

        showMessage({
          type: 'danger',
          message: 'Houve algum erro, verifique seus dados!',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }

      setLoading((prevState) => !prevState);
    },
    [closeModal, updateStudent, user],
  );

  return (
    <>
      <Modal isVisible={isVisible} closeModal={closeModal}>
        <Content>
          <ModalTitle>
            {isRedefinition ? 'Insira aqui sua nova senha' : 'Alterar senha'}
          </ModalTitle>
          <Form ref={formRef} onSubmit={handleUpdatePassword}>
            <Input
              name="password"
              placeholder="Nova senha"
              secureTextEntry
              width="80%"
            />
          </Form>

          <ButtonSaveLG>
            <Button onPress={() => formRef?.current.submitForm()}>
              {loading ? 'Salvando...' : 'Salvar'}
            </Button>
          </ButtonSaveLG>
        </Content>
      </Modal>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </>
  );
};

export default ModalPassword;
