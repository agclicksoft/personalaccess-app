import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const ImageWrapper = styled.View`
  width: 100%;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: 170px;
  height: 170px;
  border-radius: 85px;
`;

export const CameraIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 40px;
  height: 40px;
  position: absolute;
  top: 35%;
`;

export const ImageButton = styled.TouchableOpacity`
  align-items: center;
`;

export const FormWrapper = styled.View`
  padding: 30px 0;
  width: 100%;
  align-items: center;
`;

export const Label = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  margin: 12px 0 0 16px;
`;

export const ButtonWrapper = styled.View`
  margin: 20px 0;
`;
