import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Wrapper = styled.View`
  padding: 0 20px 150px;
`;

export const PageTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.orange};
  margin: 15px 0 25px;
  text-align: center;
`;

export const PageSubtitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size16};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.orange};
  margin: 15px 0;
  text-align: left;
`;

export const PackageTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  margin-top: 15px;
  text-align: center;
`;

export const ExistingPlanContainer = styled.View`
  align-items: center;
  justify-content: center;
`;

export const ExistingPlanTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.orange};
  text-align: center;
  margin-bottom: 15px;
`;

export const ExistingPlanAmount = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  text-align: center;
`;

export const ButtonOptionsContainer = styled.View`
  flex: 1;
  flex-wrap: wrap;
  flex-direction: row;
`;

export const ButtonWrapper = styled.View`
  width: 100%;
  padding: 10px 0 30px;
  align-items: center;
  position: absolute;
  align-self: center;
  bottom: 0;
  background-color: #fff;
`;
