import React, { useState, useMemo, useCallback } from 'react';

import { useNavigation, useRoute } from '@react-navigation/native';

import { IconGps } from '~/assets/icons';
import { Button, ContainerScroll, HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import { Personal } from '~/models';
import api from '~/services/api';

import {
  Container,
  PageTitle,
  Wrapper,
  Holder,
  TouchableOpacity,
  Image,
  TextWrapper,
  GymName,
  GymAddress,
  ButtonWrapper,
} from './styles';

import ConfirmationModal from './Confirmation';

interface RouteParam {
  personalData: Personal;
  date: string;
  hour: string;
  planId: number;
}

interface ExistingPlanProps {
  amount?: number;
  amount_used?: number;
  id?: number;
  ipte: null;
  method_payment?: string;
  payment_due_date?: string;
  price?: number;
  professional_id?: number;
  professionals_plan_id?: number;
  status?: string;
  status_payment?: string;
  student_id?: 2;
  updated_at?: string;
}

const Gyms: React.FC = () => {
  const { navigate } = useNavigation();
  const { user } = useAuth();
  const { params } = useRoute();
  const { personalData, date, hour, planId } = params as RouteParam;
  const [gym, setGym] = useState(null);
  const [scheduled, setScheduled] = useState(false);

  const handleNavigation = useCallback(
    async (data: ExistingPlanProps) => {
      setTimeout(() => {
        setScheduled(false);

        if (data) {
          navigate('Schedule', {
            personalData,
            planId: data.professionals_plan_id,
            plan: data,
          });
        } else {
          navigate('Schedule', { personalData });
        }
      }, 5000);
    },
    [navigate, personalData],
  );

  const getExistingPlan = useCallback(async () => {
    try {
      const response = await api.get(
        `/contracts/${user.student.id}/isvalid/${personalData.id}`,
      );

      console.log(response.data);

      if (response.data) {
        handleNavigation(response.data);
      }
    } catch (err) {
      console.log(err);
    }
  }, [handleNavigation, personalData.id, user.student.id]);

  const scheduleClass = useCallback(async () => {
    const body = {
      professional_id: personalData.id,
      establishment_id: gym,
      date: `${date} ${hour}`,
      student_id: user.student.id,
      plan_id: planId,
    };

    try {
      const response = await api.post('/scheduled-services', body);
      console.log(response.data);

      if (response.data) {
        setScheduled(true);
        getExistingPlan();
      }
    } catch (err) {
      console.log(err);
    }
  }, [
    personalData.id,
    gym,
    date,
    hour,
    user.student.id,
    planId,
    getExistingPlan,
  ]);

  const checkNull = (text: string) => {
    if (text) {
      return text;
    }
    return null;
  };

  const gymsOptions = useMemo(
    () =>
      personalData.gyms.map((item) => (
        <Holder key={item.id} style={{ opacity: gym === item.id ? 1 : 0.5 }}>
          <TouchableOpacity onPress={() => setGym(item.id)}>
            <Image source={IconGps} />

            <TextWrapper>
              <GymName>{item.name}</GymName>

              {item.address_street !== '' && (
                <GymAddress>
                  {item.address_street}, {checkNull(item.address_number)} -{' '}
                  {checkNull(item.address_neighborhood)}.
                </GymAddress>
              )}
            </TextWrapper>
          </TouchableOpacity>
        </Holder>
      )),
    [personalData, gym],
  );

  return (
    <>
      <Container>
        <HeaderBack>Selecionar academia</HeaderBack>

        <ContainerScroll>
          <Wrapper>
            <PageTitle>Selecione a academia</PageTitle>

            {gymsOptions}

            {gym && (
              <ButtonWrapper>
                <Button onPress={() => scheduleClass()}>Confirmar</Button>
              </ButtonWrapper>
            )}
          </Wrapper>
        </ContainerScroll>
      </Container>

      <ConfirmationModal isVisible={scheduled} />
    </>
  );
};

export default Gyms;
