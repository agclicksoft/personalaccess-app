import React, { useState, useMemo } from 'react';

import { useNavigation, useRoute } from '@react-navigation/native';

import { IconOneClass, IconMultipleClass } from '~/assets/icons';
import { Button, ContainerScroll, HeaderBack } from '~/components';
import { Personal } from '~/models';
import formatValue from '~/utils/formatValue';

import {
  Container,
  PageTitle,
  Wrapper,
  Holder,
  TouchableOpacity,
  Image,
  TextWrapper,
  PlanTitle,
  PlanAmount,
  PlanPrice,
  ButtonWrapper,
} from './styles';

interface RouteParam {
  personalData: Personal;
}

const Plans: React.FC = () => {
  const { navigate } = useNavigation();
  const { params } = useRoute();
  const { personalData } = params as RouteParam;
  const [plan, setPlan] = useState(null);
  const [planAmount, setPlanAmount] = useState(null);

  const plansOptions = useMemo(
    () =>
      personalData.professionalPlans.map((item) => (
        <Holder key={item.id} style={{ opacity: plan === item.id ? 1 : 0.5 }}>
          <TouchableOpacity
            onPress={() => {
              setPlan(item.id);
              setPlanAmount(item.plan.amount);
            }}
          >
            <Image
              source={item.plan.amount === 1 ? IconOneClass : IconMultipleClass}
            />

            <TextWrapper>
              <PlanTitle>{item.plan.description}</PlanTitle>
              <PlanPrice>{formatValue(item.price)}</PlanPrice>
              <PlanAmount>
                {item.plan.amount === 1
                  ? `${item.plan.amount} aula.`
                  : `${item.plan.amount} aulas.`}
              </PlanAmount>
            </TextWrapper>
          </TouchableOpacity>
        </Holder>
      )),
    [personalData, plan],
  );

  return (
    <Container>
      <HeaderBack>Selecionar plano</HeaderBack>

      <ContainerScroll>
        <PageTitle>Selecione seu plano de aulas</PageTitle>

        <Wrapper>
          {plansOptions}

          {plan && (
            <ButtonWrapper>
              <Button
                onPress={() =>
                  navigate('Schedule', {
                    personalData,
                    planId: plan,
                    planAmount,
                  })
                }
              >
                Continuar
              </Button>
            </ButtonWrapper>
          )}
        </Wrapper>
      </ContainerScroll>
    </Container>
  );
};

export default Plans;
