import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const PageTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.orange};
  margin: 15px 0 25px;
  text-align: center;
`;

export const Wrapper = styled.View`
  margin: 20px;
  align-items: center;
`;

export const Holder = styled.View`
  margin: 10px 0;
  background-color: white;
  border-radius: 10px;
  width: 100%;
  height: 100px;
  align-items: center;
  justify-content: center;
`;

export const TouchableOpacity = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 67px;
  height: 67px;
  margin-right: 30px;
`;

export const TextWrapper = styled.View`
  width: 120px;
`;

export const PlanTitle = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
`;

export const PlanPrice = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size20};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
`;

export const PlanAmount = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
`;

export const ButtonWrapper = styled.View`
  margin: 20px 0;
  align-items: center;
  width: 100%;
`;
