import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.purple};
`;

export const FormContainer = styled.View`
  flex: 1;
  width: 100%;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.background};
  border-top-left-radius: 31px;
  border-top-right-radius: 31px;
  padding: 30px 0;
`;

export const ProfileImage = styled.Image`
  width: 170px;
  height: 170px;
  border-radius: 85px;
  margin-bottom: 8px;
`;

export const Name = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size22};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  text-transform: capitalize;
`;

export const ConfefContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 5px 0;
`;

export const ConfefLabel = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size12};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
  text-transform: uppercase;
`;

export const ConfefValue = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size12};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  color: ${({ theme }) => theme.colors.black};
  text-transform: uppercase;
`;

export const AvaliationHolder = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 5px;
  background-color: #f2f2f2;
  padding: 5px;
  border-radius: 6px;
`;

export const Avaliation = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size11};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  margin-left: 6px;
`;

export const AvaliationIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  height: 20px;
  width: 20px;
`;

export const Divider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
  margin: 35px 0 15px;
`;

export const Wrapper = styled.View`
  background-color: ${({ theme }) => theme.colors.white};
  align-items: center;
  justify-content: center;
  border-radius: 15px;
  padding: 15px;
  width: 85%;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size14};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  color: ${({ theme }) => theme.colors.orange};
  margin: 15px 0 5px;
`;

export const Text = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size12};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  color: ${({ theme }) => theme.colors.grayDark};
  text-align: center;
`;
