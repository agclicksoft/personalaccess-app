import React, { useCallback, useEffect, useState } from 'react';

import { useNavigation, useRoute } from '@react-navigation/native';

import { IconAvaliation } from '~/assets/icons';
import { Button, ContainerScroll, HeaderPurple } from '~/components';
import { useAuth } from '~/hooks/auth';
import { Gym, Personal } from '~/models';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  ConfefContainer,
  ConfefLabel,
  ConfefValue,
  AvaliationHolder,
  AvaliationIcon,
  Avaliation,
  Divider,
  Wrapper,
  Title,
  Text,
} from './styles';

interface RouteParam {
  personalData: Personal;
}

interface ExistingPlanProps {
  amount?: number;
  amount_used?: number;
  id?: number;
  ipte: null;
  method_payment?: string;
  payment_due_date?: string;
  price?: number;
  professional_id?: number;
  professionals_plan_id?: number;
  status?: string;
  status_payment?: string;
  student_id?: 2;
  updated_at?: string;
}

const PersonalView: React.FC = () => {
  const { user } = useAuth();
  const { navigate, addListener } = useNavigation();
  const { params } = useRoute();
  const { personalData } = params as RouteParam;
  const [personal, setPersonal] = useState<Personal>();
  const [gyms, setGyms] = useState<Gym[]>([]);
  const [existingPlan, setExistingPlan] = useState<ExistingPlanProps>();

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      async function loadGyms() {
        const response = await api.get(`/professionals/${personalData.id}`);

        setPersonal(response.data);
        setGyms(response.data.gyms);
      }

      async function getExistingPlan() {
        try {
          const response = await api.get(
            `/contracts/${user.student.id}/isvalid/${personalData.id}`,
          );

          console.log(response.data);

          setExistingPlan(response.data);
        } catch (err) {
          console.log(err);
        }
      }

      try {
        loadGyms();
        getExistingPlan();
      } catch (err) {
        console.log(err);
      }
    });

    return unsubscribe;
  }, [addListener, personalData, user]);

  const handleNavigate = useCallback(() => {
    if (existingPlan) {
      navigate('Schedule', {
        personalData: personal,
        planId: existingPlan.professionals_plan_id,
        plan: existingPlan,
        planAmout: existingPlan.amount,
      });
    } else {
      navigate('Plans', { personalData: personal });
    }
  }, [existingPlan, navigate, personal]);

  return (
    <Container>
      <HeaderPurple>Personal</HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{ uri: personalData.user.img_profile ?? PlaceholderImage }}
          />

          <Name>{personalData.user.name}</Name>

          {personalData.doc_professional && (
            <ConfefContainer>
              <ConfefLabel>CREF/CONFEF: </ConfefLabel>
              <ConfefValue>{personalData.doc_professional}</ConfefValue>
            </ConfefContainer>
          )}

          <AvaliationHolder>
            <AvaliationIcon source={IconAvaliation} />
            <Avaliation>
              {personalData?.assessments[0]?.assessments
                ? Number(personalData?.assessments[0]?.assessments).toFixed(1)
                : 0}
            </Avaliation>
          </AvaliationHolder>

          <Button onPress={() => (personal ? handleNavigate() : null)}>
            Agendar
          </Button>

          <Divider />

          <Wrapper>
            <Title>
              {personalData.specialties.length > 1
                ? 'Especialidades'
                : 'Especialidade'}
              :
            </Title>
            <Text>
              {personalData.specialties
                .map((item) => item.description)
                .join(', ')}
              .
            </Text>

            {gyms && (
              <>
                <Title>
                  {gyms.length > 1
                    ? 'Locais de atendimento'
                    : 'Local de atendimento'}
                  :
                </Title>
                <Text>{gyms.map((gym) => gym.name).join(', ')}.</Text>
              </>
            )}

            <Title>Biografia:</Title>
            <Text>{personalData.bibliography ?? 'Não informado'}</Text>

            <Title>Instagram:</Title>
            <Text>{personalData.instagram ?? 'Não informado'}</Text>
          </Wrapper>
        </FormContainer>
      </ContainerScroll>
    </Container>
  );
};

export default PersonalView;
