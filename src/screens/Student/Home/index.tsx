/* eslint-disable no-nested-ternary */
import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import { IconGps } from '~/assets/icons';
import { BarLogo } from '~/assets/images';
import { Button, PersonalCard } from '~/components';
import { useAuth } from '~/hooks/auth';
import { Personal } from '~/models';
import { getLocation } from '~/services/location';
import { getAllPersonals } from '~/services/personal';

import {
  Container,
  LogoContainer,
  Logo,
  LocationContainer,
  LocationWrapper,
  LocationAddressWrapper,
  LocationIcon,
  LocationAddress,
  LocationAddressLabel,
  FindProfessionalTitle,
  HeaderSearch,
  InputContainer,
  Input,
  Feed,
  TouchableOpacity,
  SearchFilterWrapper,
  SearchFilterTitle,
  ActivateLocationWrapper,
  LocationOff,
  LocationOn,
} from './styles';

import ModalPassword from '../Profile/EditProfile/ModalPassword';

const Home: React.FC = () => {
  const { user } = useAuth();
  const { navigate } = useNavigation();
  const [loading, setLoading] = useState(true);
  const [refreshing] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [address, setAddress] = useState(null);
  const [query, setQuery] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [page, setPage] = useState(1);
  const [personals, setPersonals] = useState<Personal[]>([]);
  const [personalsFiltered, setPersonalsFiltered] = useState<Personal[]>([]);
  const [buttonText, setButtonText] = useState(
    'Desativar filtro de localidade',
  );
  const [modalPassword, setModalPassword] = useState(false);

  useEffect(() => {
    if (user.is_redefinition) {
      setModalPassword(!modalPassword);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const loadLocation = useCallback(async () => {
    const response = await getLocation();

    if (!response) {
      await setErrorMsg(
        'Permissão para acessar localização negada. Favor verificar.',
      );
    } else {
      await setAddress(response.address);
      await setQuery(response.query);
      const personalsQuery = await getAllPersonals(response.query, 1);
      await setPersonals(personalsQuery);
      await setPersonalsFiltered(personalsQuery);
      setLoading(false);
      return;
    }

    const personalsQuery = await getAllPersonals(query, 1);
    await setPersonals(personalsQuery);
    await setPersonalsFiltered(personalsQuery);
    setLoading(false);
  }, [query]);

  const loadOnScroll = useCallback(async () => {
    setPage(page + 1);
    const personalsQuery = await getAllPersonals(query, page + 1, personals);
    await setPersonals(personalsQuery);
    await setPersonalsFiltered(personalsQuery);
  }, [page, query, personals]);

  const handleSearch = useCallback(
    (text) => {
      if (text) {
        const personalFiltered = personals.filter((personal) => {
          const itemData = personal.user.name
            ? personal.user.name.toUpperCase()
            : ''.toUpperCase();

          const textData = text.toUpperCase();

          return itemData.indexOf(textData) > -1;
        });

        setPersonalsFiltered(personalFiltered);
        setSearchText(text);
      } else {
        setPersonalsFiltered(personals);
        setSearchText(text);
      }
    },
    [personals],
  );

  const refreshList = useCallback(
    async (newQuery?: string) => {
      await setLoading(true);
      await setPage(1);
      const personalsQuery = await getAllPersonals(newQuery ?? query, 1);
      await setPersonals(personalsQuery);
      await setPersonalsFiltered(personalsQuery);
      setLoading(false);
    },
    [query],
  );

  const turnOffLocalizationFilter = useCallback(async () => {
    await setQuery(null);
    await refreshList('');
  }, [refreshList]);

  const personalsListMemo = useMemo(
    () => (
      <Feed
        keyboardShouldPersistTaps="handled"
        onEndReached={searchText.length > 0 ? null : loadOnScroll}
        refreshing={refreshing}
        onRefresh={refreshList}
        data={personalsFiltered}
        keyExtractor={(item: Personal) => String(item.id)}
        renderItem={({ item: personal }) => {
          return <PersonalCard key={personal.id} data={personal} />;
        }}
      />
    ),
    [personalsFiltered, loadOnScroll, searchText, refreshing, refreshList],
  );

  const handleNavigateFilter = async () => {
    navigate('Filter', {
      onReturn: async (newQuery) => {
        if (newQuery) {
          setLoading(true);
          await setPage(1);
          const personalsQuery = await getAllPersonals(newQuery, 1);
          await setPersonals(personalsQuery);
          await setPersonalsFiltered(personalsQuery);
          setLoading(false);
          setQuery(newQuery);
          setButtonText('Limpar pesquisa');
        }
      },
    });
  };

  const checkNull = (text: string) => {
    if (text) {
      return text;
    }
    return null;
  };

  useEffect(() => {
    (async () => {
      await loadLocation();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Container>
        <LogoContainer>
          <Logo source={BarLogo} />
        </LogoContainer>

        <LocationContainer>
          <LocationWrapper>
            <LocationIcon source={IconGps} />
            <LocationAddressWrapper>
              <LocationAddressLabel>Você está em:</LocationAddressLabel>
              {address !== null ? (
                <LocationAddress>
                  {checkNull(address.name)} -{' '}
                  {checkNull(address.neighborhood) ?? address.city}
                </LocationAddress>
              ) : (
                <LocationAddress>
                  {errorMsg !== null ? errorMsg : 'Carregando...'}
                </LocationAddress>
              )}
            </LocationAddressWrapper>
          </LocationWrapper>
        </LocationContainer>

        <FindProfessionalTitle>
          Encontre profissionais perto de você...
        </FindProfessionalTitle>

        <HeaderSearch>
          <InputContainer>
            <Ionicons
              name="ios-search"
              size={25}
              color="black"
              style={{ paddingHorizontal: 16 }}
            />

            <Input
              placeholder="Buscar por nome"
              value={searchText}
              onChangeText={(text) => {
                handleSearch(text);
              }}
              returnKeyType="search"
            />

            <TouchableOpacity onPress={() => handleNavigateFilter()}>
              <Ionicons
                name="md-funnel"
                size={25}
                color="black"
                style={{ paddingHorizontal: 16 }}
              />
            </TouchableOpacity>
          </InputContainer>
        </HeaderSearch>

        {!query && loading === false ? (
          <ActivateLocationWrapper>
            <LocationOff>Filtro de localização desativado.</LocationOff>

            <TouchableOpacity
              onPress={async () => {
                await setLoading(true);
                await loadLocation();
                setButtonText('Desativar filtro de localização');
              }}
            >
              <LocationOn>Ativar.</LocationOn>
            </TouchableOpacity>
          </ActivateLocationWrapper>
        ) : null}

        {loading === true ? (
          <View
            style={{
              flex: 1,
              height: '100%',
              justifyContent: 'center',
            }}
          >
            <ActivityIndicator size="large" color="#37153D" />
          </View>
        ) : personalsFiltered.length !== 0 ? (
          personalsListMemo
        ) : null}

        {personalsFiltered.length === 0 && loading === false ? (
          <SearchFilterWrapper>
            <SearchFilterTitle>Nenhum personal encontrado.</SearchFilterTitle>

            {searchText === '' ? (
              <Button
                width={buttonText === 'Limpar pesquisa' ? 150 : 240}
                height={35}
                onPress={turnOffLocalizationFilter}
              >
                {buttonText}
              </Button>
            ) : null}
          </SearchFilterWrapper>
        ) : null}
      </Container>

      <ModalPassword
        isRedefinition
        isVisible={modalPassword}
        closeModal={() => setModalPassword(!modalPassword)}
      />
    </>
  );
};

export default Home;
