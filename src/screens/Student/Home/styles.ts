import { FlatList } from 'react-native';

import Constants from 'expo-constants';
import { LinearGradient } from 'expo-linear-gradient';
import styled from 'styled-components/native';

import { Personal } from '~/models';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
  padding-top: ${Constants.statusBarHeight - 20}px;
`;

export const LogoContainer = styled.View`
  width: 100%;
  height: 100px;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.background};
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
`;

export const Logo = styled.Image.attrs({
  resizeMode: 'contain',
})`
  height: 25px;
`;

export const LocationContainer = styled(LinearGradient).attrs({
  start: { x: 0, y: 0 },
  end: { x: 1, y: 0 },
  colors: ['#512758', '#37153D'],
  elevation: 3,
  borderRadius: 5,
  shadowRadius: 5,
  shadowOffset: { width: 0, height: 0 },
  shadowOpacity: 0.6,
  shadowColor: 'black',
  overflow: 'hidden',
})`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 20px;
  height: 75px;
  border-radius: 35px;
  padding: 0 20px;
`;

export const LocationWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  width: 90%;
  margin-right: 15px;
`;

export const LocationIcon = styled.Image`
  margin-right: 10px;
`;

export const LocationAddressWrapper = styled.View`
  width: 92%;
`;

export const LocationAddressLabel = styled.Text`
  color: ${({ theme }) => theme.colors.white};
  font-size: ${({ theme }) => theme.fontSizes.size11};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
`;

export const LocationAddress = styled.Text`
  color: ${({ theme }) => theme.colors.white};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
`;

export const FindProfessionalTitle = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  text-align: center;
`;

export const ActivateLocationWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 100%;
  margin: 5px 0;
`;

export const LocationOn = styled.Text`
  color: ${({ theme }) => theme.colors.purple};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  text-align: center;
  padding: 5px;
`;

export const LocationOff = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  text-align: center;
`;

export const HeaderSearch = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 0 20px;
  margin: 20px 0 0;
`;

export const InputContainer = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: #fff;
  height: 44px;
  border-radius: 30px;
  border-color: ${({ theme }) => theme.colors.shadow};
  border-width: 1px;
  width: 100%;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: '#636363',
})`
  flex: 1;
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  font-size: 13px;
  color: #000000;
  width: 100%;
  text-align: center;
`;

export const Feed = styled(FlatList as new () => FlatList<Personal>).attrs({
  showsVerticalScrollIndicator: false,
})`
  flex: 1;
  width: 100%;
  padding: 0 20px;
`;

export const TouchableOpacity = styled.TouchableOpacity``;

export const SearchFilterWrapper = styled.View`
  margin: 30px 0;
  justify-content: center;
  align-items: center;
`;

export const SearchFilterTitle = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  text-align: center;
`;
