/* eslint-disable react/jsx-wrap-multilines */
import React, { useEffect, useState, useCallback } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { ClassCard } from '~/components';
import { useAuth } from '~/hooks/auth';
import { StudentClass } from '~/models';
import { getStudentClasses } from '~/services/student';

import {
  Container,
  HeaderContainer,
  Title,
  Divider,
  Feed,
  Card,
  CardTitle,
} from './styles';

const StudentClasses: React.FC = () => {
  const { user } = useAuth();
  const { addListener } = useNavigation();
  const [classes, setClasses] = useState([]);
  const [page, setPage] = useState(1);
  const [refreshing] = useState(false);
  const [loading, setLoading] = useState(true);

  const loadStudentClasses = useCallback(async () => {
    await setPage(1);
    const classesQuery = await getStudentClasses(user.student.id, 1);
    console.log(classesQuery);
    await setClasses(classesQuery);
    setLoading(false);
  }, [user]);

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      try {
        loadStudentClasses();
      } catch (err) {
        console.log(err);
      }
    });

    return unsubscribe;
  }, [addListener, loadStudentClasses]);

  const refreshList = useCallback(async () => {
    setLoading(true);
    await setPage(1);
    await setClasses([]);
    const classesQuery = await getStudentClasses(user.student.id, 1);
    await setClasses(classesQuery);
    setLoading(false);
  }, [user]);

  const loadOnScroll = useCallback(async () => {
    await setPage(page + 1);
    const classesQuery = await getStudentClasses(
      user.student.id,
      page + 1,
      classes,
    );
    await setClasses(classesQuery);
  }, [page, user, classes]);

  return (
    <>
      <Container>
        <HeaderContainer>
          <Title>Minhas aulas</Title>
        </HeaderContainer>

        <Divider />

        {classes.length !== 0 ? (
          <Feed
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}
            onEndReachedThreshold={0.5}
            onEndReached={loadOnScroll}
            refreshing={refreshing}
            onRefresh={refreshList}
            data={classes.map((e) => e)}
            keyExtractor={(item: StudentClass) => String(item.id)}
            renderItem={({ item }) => {
              const myClass: StudentClass = item;
              return <ClassCard key={myClass.id} data={myClass} />;
            }}
          />
        ) : null}

        {classes.length === 0 && !loading ? (
          <Card>
            <CardTitle>Nenhuma aula encontrada.</CardTitle>
          </Card>
        ) : null}
      </Container>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </>
  );
};

export default StudentClasses;
