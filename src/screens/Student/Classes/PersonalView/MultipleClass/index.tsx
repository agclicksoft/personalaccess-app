/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect, useState } from 'react';
import { Linking } from 'react-native';
import { FlatList, LogBox } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import { parseISO, format, addHours, differenceInDays } from 'date-fns';
import { pt } from 'date-fns/locale';

import {
  IconWhatsApp,
  IconEmail,
  IconPhone,
  IconPackage,
} from '~/assets/icons';
import { ContainerScroll, HeaderPurple } from '~/components';
import { StudentClass } from '~/models';
import ScheduledService from '~/models/ScheduledServices';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';
import {
  getPaymentStatus,
  getStudentStatus,
  getStudentStatusBG,
  getStudentStatusColor,
} from '~/utils/geStatus';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Card,
  CardContent,
  Image,
  ClassWrapper,
  ClassHolder,
  ClassQuestion,
  ClassAnswer,
  ClassDivider,
  StatusButton,
  StatusButtonText,
  ButtonWrapper,
  TouchableOpacity,
  ButtonImage,
  SchedulesTitle,
  SchedulesSubTitle,
  StatusPayment,
  StatusPaymentLabel,
  PaymentStatusButton,
  PaymentStatusButtonText,
} from './styles';

import ConfirmationModal from '../Confirmation';

interface Params {
  myClass: StudentClass;
}

LogBox.ignoreAllLogs();

const MultipleClass: React.FC = () => {
  const { navigate } = useNavigation();
  const { params } = useRoute();
  const { myClass } = params as Params;
  const [personal, setPersonal] = useState(null);
  const [modal, setModal] = useState(false);
  const [classes, setClasses] = useState<StudentClass>(myClass);
  const [selectedClass, setSelectedClass] = useState<ScheduledService>(null);
  const [canceled, setCanceled] = useState(false);

  // const daysTillAppointment = differenceInDays(
  //   parseISO(myClass.scheduledServices[0].date),
  //   new Date(),
  // );

  useEffect(() => {
    async function getPersonal() {
      const response = await api.get(
        `/professionals/${myClass.professional.id}`,
      );

      setPersonal(response.data);
    }

    async function getClasses() {
      const response = await api.get(`/contracts/${myClass.id}`);

      setClasses(response.data);
      setCanceled(false);
    }

    getPersonal();

    if (canceled) {
      getClasses();
    }
  }, [myClass, canceled]);

  const navigateWithStatus = useCallback(
    async (status: string) => {
      if (
        status === 'PENDENTE' ||
        status === 'REMARCADO' ||
        status === 'CANCELADO' ||
        status === 'RECUSADO'
      ) {
        setModal(!modal);
      } else {
        null;
      }
    },
    [modal],
  );

  return (
    <Container>
      <HeaderPurple>Aula</HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{
              uri: myClass.professional.user.img_profile ?? PlaceholderImage,
            }}
          />

          <Name>{myClass.professional.user.name}</Name>

          <Age>{myClass.professional.doc_professional.toLocaleUpperCase()}</Age>

          <ButtonWrapper>
            <TouchableOpacity
              onPress={() => {
                Linking.canOpenURL('whatsapp://send?text=Oi').then(
                  (supported) => {
                    if (supported) {
                      return Linking.openURL(
                        `whatsapp://send?phone=55${personal?.user.contact}&text=Olá, lhe contratei no Personal Access, tudo bem?`,
                      );
                    }
                    return Linking.openURL(
                      `https://api.whatsapp.com/send?phone=55${personal?.user.contact}&text=Olá, lhe contratei no Personal Access, tudo bem?`,
                    );
                  },
                );
              }}
            >
              <ButtonImage source={IconWhatsApp} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `mailto:${personal.user?.email}?subject=Olá, lhe contratei pelo Personal Access!`,
                );
              }}
            >
              <ButtonImage source={IconEmail} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${personal.user?.contact}`);
              }}
            >
              <ButtonImage source={IconPhone} />
            </TouchableOpacity>
          </ButtonWrapper>

          <Divider />

          <SchedulesTitle>Agendamentos</SchedulesTitle>

          <SchedulesSubTitle>
            {myClass?.amount_used} de {myClass?.amount} aulas contratadas
          </SchedulesSubTitle>

          <StatusPayment
            style={{
              marginBottom: myClass?.method_payment !== 'PENDENTE' ? 20 : 0,
            }}
          >
            Pagamento:{' '}
            <StatusPaymentLabel>
              {getPaymentStatus(myClass?.method_payment)}
            </StatusPaymentLabel>
          </StatusPayment>

          {/* {myClass?.method_payment === 'BOLETO' && daysTillAppointment >= 5 && (
            <TouchableOpacity
              onPress={() =>
                navigate('SelectPayment', {
                  myClass,
                  regenerate: true,
                })
              }
            >
              <PaymentStatusButton>
                <PaymentStatusButtonText>
                  Gerar um novo boleto
                </PaymentStatusButtonText>
              </PaymentStatusButton>
            </TouchableOpacity>
          )} */}

          {myClass?.method_payment === 'PENDENTE' &&
          classes.scheduledServices.every((e) => e.status === 'CONFIRMADO') &&
          myClass?.amount_used === myClass?.amount ? (
            <TouchableOpacity
              onPress={() =>
                navigate('SelectPayment', {
                  myClass,
                })
              }
            >
              <PaymentStatusButton>
                <PaymentStatusButtonText>
                  Realizar pagamento
                </PaymentStatusButtonText>
              </PaymentStatusButton>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={() => null}>
              <PaymentStatusButton>
                <PaymentStatusButtonText>Não liberado</PaymentStatusButtonText>
              </PaymentStatusButton>
            </TouchableOpacity>
          )}

          {classes?.scheduledServices?.length !== 0 ? (
            <FlatList
              showsVerticalScrollIndicator={false}
              data={classes?.scheduledServices}
              keyExtractor={(option) => String(option.id)}
              renderItem={({ item }) => (
                <Card
                  key={item.id}
                  onPress={async () => {
                    if (item.status === 'CONCLUIDO') {
                      navigate('ClassDescriptionView', {
                        myClass,
                      });
                    } else {
                      await setSelectedClass(item);
                      navigateWithStatus(item.status);
                    }

                    // if (
                    //   item.status === 'PENDENTE' ||
                    //   item.status === 'RECUSADO' ||
                    //   item.status === 'REMARCADO'
                    // ) {
                    //   await setSelectedClass(item);

                    //   console.log(selectedClass);

                    //   setModal(!modal);
                    // } else if (item.status === 'CONCLUIDO') {
                    //   navigate('ClassDescriptionView', {
                    //     myClass,
                    //   });
                    // } else {
                    //   null;
                    // }
                  }}
                >
                  <CardContent>
                    <Image source={IconPackage} />

                    <ClassWrapper>
                      <ClassHolder>
                        <ClassQuestion>Data/hora: </ClassQuestion>
                        <ClassAnswer>
                          {format(
                            addHours(parseISO(item.date), 3),
                            "dd/MM/yy '-' HH:mm ",
                            {
                              locale: pt,
                            },
                          )}
                        </ClassAnswer>
                      </ClassHolder>

                      <ClassHolder>
                        <ClassQuestion>Local: </ClassQuestion>
                        <ClassAnswer>{item.establishment.name}</ClassAnswer>
                      </ClassHolder>
                    </ClassWrapper>
                  </CardContent>

                  <ClassDivider />

                  <StatusButton
                    style={{
                      backgroundColor: getStudentStatusBG(item.status),
                    }}
                  >
                    <StatusButtonText
                      style={{
                        color: getStudentStatusColor(item.status),
                      }}
                    >
                      {getStudentStatus(item.status)}
                    </StatusButtonText>
                  </StatusButton>
                </Card>
              )}
            />
          ) : null}
        </FormContainer>
      </ContainerScroll>

      <ConfirmationModal
        isVisible={modal}
        schedule={selectedClass}
        contract={myClass}
        closeModal={() => setModal(!modal)}
        personalData={myClass.professional}
      />
    </Container>
  );
};

export default MultipleClass;
