/* eslint-disable no-nested-ternary */
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { ActivityIndicator, Linking, Switch, View } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import { parseISO, format, addHours } from 'date-fns';
import { pt } from 'date-fns/locale';
import { showMessage } from 'react-native-flash-message';
import { AirbnbRating } from 'react-native-ratings';

import { IconWhatsApp, IconEmail, IconPhone } from '~/assets/icons';
import { Button, ContainerScroll, Input } from '~/components';
import { useAuth } from '~/hooks/auth';
import { Rating, StudentClass } from '~/models';
import TrainingDescription from '~/models/TrainingDescription';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';

import {
  Container,
  HeaderPurple,
  HeaderWrapper,
  HeaderTitle,
  GenericButton,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Card,
  CardContent,
  ClassWrapper,
  ClassHolder,
  ClassQuestion,
  ClassAnswer,
  ButtonWrapper,
  TouchableOpacity,
  ButtonImage,
  SchedulesTitle,
  RatingTitle,
  TrainingTitle,
  TrainingCardContent,
  TrainingDescriptionText,
  SwitchTitle,
  SwitchContainer,
} from './styles';

interface Params {
  myClass: StudentClass;
}

const ClassDescriptionView: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { user } = useAuth();
  const { navigate } = useNavigation();
  const { params } = useRoute();
  const { myClass } = params as Params;
  const [loading, setLoading] = useState(false);
  const [personal, setPersonal] = useState(null);
  const [selectedClass] = useState<StudentClass>(myClass);
  const [ratingData, setRatingData] = useState<Rating>(null);
  const [isEnabled, setIsEnabled] = useState(false);
  const [trainingDetails, setTrainingDetails] = useState<TrainingDescription>(
    null,
  );
  const [rating, setRating] = useState(3);

  const getTrainingDetails = useCallback(async () => {
    const response = await api.get(
      `/scheduled-services/${myClass.scheduledServices[0].id}`,
    );

    setTrainingDetails(response.data.trainingDescription);
    setRatingData(response.data.assessments);

    console.log(response.data);
  }, [myClass.scheduledServices]);

  const handleSubmit = useCallback(
    async (data) => {
      setLoading(true);

      try {
        const response = await api.post(
          `/scheduled-services/${myClass.scheduledServices[0].id}/assessments`,
          {
            comments: data.comment,
            anonimo: !isEnabled,
            assessments_value: rating,
            student_id: user.student.id,
          },
        );

        console.log(response.data);

        getTrainingDetails();

        setLoading(false);

        showMessage({
          type: 'success',
          message: 'Avaliação enviada com sucesso!',
          titleStyle: {
            textAlign: 'center',
          },
        });
      } catch (err) {
        setLoading(false);
        console.log(err);
        showMessage({
          type: 'danger',
          message: 'Algo de errado ocorreu, tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
        });
      }
    },
    [
      myClass.scheduledServices,
      isEnabled,
      rating,
      user.student.id,
      getTrainingDetails,
    ],
  );

  const getPersonal = useCallback(async () => {
    const response = await api.get(`/professionals/${myClass.professional.id}`);

    setPersonal(response.data);
  }, [myClass.professional.id]);

  useEffect(() => {
    getPersonal();
    getTrainingDetails();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const formattedDate = format(
    addHours(parseISO(selectedClass.scheduledServices[0].date), 3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Data/hora: ', answer: formattedDate },
    {
      id: 2,
      question: 'Local: ',
      answer: selectedClass.scheduledServices[0].establishment.name,
    },
    {
      id: 3,
      question: 'Pacote: ',
      answer: selectedClass.professionalPlan.plan.description,
    },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <ClassHolder key={item.id}>
          <ClassQuestion>{item.question}</ClassQuestion>
          <ClassAnswer>{item.answer}</ClassAnswer>
        </ClassHolder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  return (
    <Container>
      <HeaderPurple>
        <GenericButton onPress={() => navigate('Class')}>
          <HeaderWrapper>
            <Ionicons name="ios-arrow-round-back" size={24} color="#fff" />
            <HeaderTitle>Aula concluída</HeaderTitle>
          </HeaderWrapper>
        </GenericButton>
      </HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{
              uri:
                selectedClass.professional.user.img_profile ?? PlaceholderImage,
            }}
          />

          <Name>{selectedClass.professional.user.name}</Name>

          <Age>
            {selectedClass.professional.doc_professional.toLocaleUpperCase()}
          </Age>

          <Divider />

          <Card onPress={() => null}>
            <CardContent>
              <ClassWrapper>{optionsMemo}</ClassWrapper>
            </CardContent>
          </Card>

          <ButtonWrapper>
            <TouchableOpacity
              onPress={() => {
                Linking.canOpenURL('whatsapp://send?text=Oi').then(
                  (supported) => {
                    if (supported) {
                      return Linking.openURL(
                        `whatsapp://send?phone=55${personal?.user.contact}&text=Olá, lhe contratei no Personal Access, tudo bem?`,
                      );
                    }
                    return Linking.openURL(
                      `https://api.whatsapp.com/send?phone=55${personal?.user.contact}&text=Olá, lhe contratei no Personal Access, tudo bem?`,
                    );
                  },
                );
              }}
            >
              <ButtonImage source={IconWhatsApp} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `mailto:${personal.user?.email}?subject=Olá, lhe contratei pelo Personal Access!`,
                );
              }}
            >
              <ButtonImage source={IconEmail} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${personal.user?.contact}`);
              }}
            >
              <ButtonImage source={IconPhone} />
            </TouchableOpacity>
          </ButtonWrapper>

          <Divider />

          <SchedulesTitle>Descrição de treino</SchedulesTitle>

          {trainingDetails && (
            <Card onPress={() => null}>
              <TrainingCardContent>
                <TrainingTitle>Grupamentos:</TrainingTitle>
                <TrainingDescriptionText>
                  {trainingDetails.groupings.split(',').join(', ') ??
                    'Não informado'}
                  .
                </TrainingDescriptionText>

                <TrainingTitle>Modalidade:</TrainingTitle>
                <TrainingDescriptionText>
                  {trainingDetails.modality.split(',').join(', ') ??
                    'Não informado'}
                  .
                </TrainingDescriptionText>

                <TrainingTitle>Observações:</TrainingTitle>
                <TrainingDescriptionText>
                  {trainingDetails.note ?? 'Não informado'}
                </TrainingDescriptionText>
              </TrainingCardContent>
            </Card>
          )}

          {!trainingDetails && (
            <Card onPress={() => null}>
              <TrainingCardContent>
                <TrainingTitle>Treinamento não localizado.</TrainingTitle>
              </TrainingCardContent>
            </Card>
          )}

          {!ratingData ? (
            <>
              <RatingTitle>Como foi sua aula?</RatingTitle>

              <AirbnbRating
                count={5}
                reviews={['Horrível', 'Ruim', 'Razoável', 'Boa', 'Excelente']}
                defaultRating={rating}
                size={20}
                reviewSize={16}
                onFinishRating={(number) => setRating(number)}
              />

              <Form ref={formRef} onSubmit={handleSubmit}>
                <Input
                  name="comment"
                  placeholder="Comentário"
                  style={{
                    textAlign: 'left',
                    textAlignVertical: 'top',
                    alignSelf: 'flex-start',
                    marginTop: 10,
                    borderRadius: 30,
                    width: '100%',
                    height: '100%',
                  }}
                  height={150}
                  multiline
                />
              </Form>

              <SwitchContainer>
                <SwitchTitle>Mostrar meu nome</SwitchTitle>

                <Switch
                  trackColor={{ false: '#767577', true: '#541077' }}
                  thumbColor="#ffffff"
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={toggleSwitch}
                  value={isEnabled}
                />
              </SwitchContainer>

              <Button onPress={() => formRef.current?.submitForm()}>
                Enviar avaliação
              </Button>
            </>
          ) : (
            <>
              <RatingTitle>Sua avaliação desta aula:</RatingTitle>

              <AirbnbRating
                count={5}
                reviews={['Horrível', 'Ruim', 'Razoável', 'Boa', 'Excelente']}
                defaultRating={ratingData?.assessments_value}
                size={20}
                reviewSize={16}
                onFinishRating={(number) => setRating(number)}
                isDisabled
              />

              <Form ref={formRef} onSubmit={handleSubmit}>
                <Input
                  name="comment"
                  placeholder="Comentário"
                  style={{
                    textAlign: 'left',
                    textAlignVertical: 'top',
                    alignSelf: 'flex-start',
                    marginTop: 10,
                    borderRadius: 30,
                    width: '100%',
                    height: '100%',
                  }}
                  height={150}
                  multiline
                  value={ratingData?.comments}
                  editable={false}
                />
              </Form>
            </>
          )}
        </FormContainer>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default ClassDescriptionView;
