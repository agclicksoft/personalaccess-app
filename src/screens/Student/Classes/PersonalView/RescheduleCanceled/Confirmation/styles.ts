import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: ${({ theme }) => theme.colors.purple};
  width: 100%;
  height: 100%;
  padding: 17px 20px;
  justify-content: center;
  align-items: center;
`;

export const Description = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaBold};
  font-size: 18px;
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
  margin: 20px 0;
`;

export const ImageModal = styled.Image.attrs({
  resizeMode: 'contain',
})`
  height: 40px;
  width: 40px;
  margin-left: 15px;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 100%;
`;
