/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { useEffect, useState } from 'react';

import { useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';

import { Button } from '~/components';
import { Personal, ScheduledServices, StudentClass } from '~/models';
import api from '~/services/api';

import { Container, Title, Subtitle, TouchableOpacity } from './styles';

interface Props {
  isVisible: boolean;
  contract: StudentClass;
  schedule: ScheduledServices;
  closeModal: any;
  personalData: Personal;
}

const ModalReschedule: React.FC<Props> = ({
  isVisible,
  closeModal,
  contract,
  personalData,
  schedule,
}) => {
  const { navigate, addListener } = useNavigation();
  const [personal, setPersonal] = useState<Personal>();

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      async function loadGyms() {
        const response = await api.get(`/professionals/${personalData?.id}`);

        setPersonal(response.data);
      }

      try {
        loadGyms();
      } catch (err) {
        console.log(err);
      }
    });

    return unsubscribe;
  }, [addListener, personalData]);

  return (
    <Modal
      backdropOpacity={0.5}
      isVisible={isVisible}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      useNativeDriver
      hideModalContentWhileAnimating
      propagateSwipe
      onBackdropPress={closeModal}
      onBackButtonPress={closeModal}
      style={{
        width: '100%',
        height: '90%',
        margin: 0,
        padding: 0,
      }}
    >
      <Container>
        <Title>{`Aula agendada.\n Deseja remarcar?`}</Title>

        <Button
          onPress={() => {
            closeModal();

            navigate('Reschedule', {
              personalData: personal,
              planId: contract.professionals_plan_id,
              plan: contract,
              schedule,
            });
          }}
          style={{ backgroundColor: '#F76E1E' }}
        >
          Remarcar aula
        </Button>

        <TouchableOpacity onPress={closeModal}>
          <Subtitle>Voltar</Subtitle>
        </TouchableOpacity>
      </Container>
    </Modal>
  );
};

export default ModalReschedule;
