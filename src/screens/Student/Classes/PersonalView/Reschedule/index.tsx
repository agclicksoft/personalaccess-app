import React, { useCallback, useState, useMemo } from 'react';
import { Alert } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import {
  format,
  addMonths,
  addDays,
  startOfToday,
  addHours,
  parseISO,
} from 'date-fns';
import { pt } from 'date-fns/locale';
import _ from 'lodash';
import moment from 'moment';
import { Calendar } from 'react-native-calendars';

import {
  Button,
  ButtonOption,
  ContainerScroll,
  HeaderBack,
  Picker,
} from '~/components';
import { useAuth } from '~/hooks/auth';
import { useNotification } from '~/hooks/notifications';
import { Personal, ScheduledServices } from '~/models';
import api from '~/services/api';

import {
  Container,
  PageTitle,
  PageSubtitle,
  Wrapper,
  ButtonWrapper,
  ButtonOptionsContainer,
} from './styles';

import LocaleConfig from './calendarLocale';
import ConfirmationModal from './Confirmation';

LocaleConfig;

interface ExistingPlanProps {
  amount?: number;
  amount_used?: number;
  id?: number;
  ipte: null;
  method_payment?: string;
  payment_due_date?: string;
  price?: number;
  professional_id?: number;
  professionals_plan_id?: number;
  status?: string;
  status_payment?: string;
  student_id?: 2;
  updated_at?: string;
}

interface RouteParam {
  personalData: Personal;
  planId?: number;
  plan?: ExistingPlanProps;
  schedule?: ScheduledServices;
}

const Reschedule: React.FC = () => {
  const { user } = useAuth();
  const { params } = useRoute();
  const { navigate } = useNavigation();
  const { sendNewNotification } = useNotification();
  const { personalData, planId, plan, schedule } = params as RouteParam;
  const [planData, setPlanData] = useState(plan ?? null);
  const [planIdData, setPlanIdData] = useState<number>(planId ?? null);
  const [selected, setSelected] = useState('');
  const [hours, setHours] = useState<string[]>([]);
  const [hourSelected, setHourSelected] = useState('');
  const [gym, setGym] = useState(null);
  const [scheduled, setScheduled] = useState(false);

  const getDaysAvailable = (weekday: string) => {
    switch (weekday) {
      case 'DOMINGO':
        return 0;
      case 'SEGUNDA':
        return 1;
      case 'TERCA':
        return 2;
      case 'QUARTA':
        return 3;
      case 'QUINTA':
        return 4;
      case 'SEXTA':
        return 5;
      case 'SABADO':
        return 6;
      default:
        return 'Dia inválido';
    }
  };

  const getAvailableDates = useCallback(() => {
    const enabledDates = {};
    const daysToEnable = personalData.schedules.map((item) =>
      getDaysAvailable(item.week_day),
    );

    const start = format(addHours(new Date(), 1), "yyyy'-'MM'-'dd");
    const end = format(addMonths(startOfToday(), 6), "yyyy'-'MM'-'dd");
    for (let m = moment(start); m.diff(end, 'days') <= 0; m.add(1, 'days')) {
      if (_.includes(daysToEnable, m.weekday())) {
        enabledDates[m.format('YYYY-MM-DD')] = { disabled: false };
      }
    }

    return enabledDates;
  }, [personalData]);

  const getScheduleHours = useCallback(
    async (date) => {
      const response = await api.get(
        `/professionals/${personalData.id}/schedules/${date}`,
      );

      console.log(response.data);
      setHours(response.data);
    },
    [personalData],
  );

  const hoursOptions = useMemo(
    () =>
      hours.map((item, index) => (
        <ButtonOption
          key={String(index)}
          onPress={() => setHourSelected(item)}
          selected={item === hourSelected}
        >
          {item}
        </ButtonOption>
      )),
    [hours, hourSelected],
  );

  const rescheduleClass = useCallback(async () => {
    const body = {
      professional_id: personalData.id,
      establishment_id: gym,
      date: `${selected} ${hourSelected}`,
      student_id: user.student.id,
      plan_id: planIdData,
      status: 'REMARCADO',
    };

    try {
      const response = await api.put(
        `/scheduled-services/${schedule.id}/status`,
        body,
      );
      console.log(response.data);

      if (response.data) {
        setScheduled(true);

        await sendNewNotification({
          token: personalData.user.expo_data,
          title: 'Aula reagendada!',
          body: `Sua aula com ${user.name}, do dia ${format(
            addHours(parseISO(schedule.date), 3),
            "dd/MM/yy 'às' HH:mm",
            {
              locale: pt,
            },
          )} foi reagendada.`,
        });
      }

      setTimeout(() => {
        setScheduled(false);
        navigate('Class');
      }, 3000);
    } catch (err) {
      console.log(err);
    }
  }, [
    personalData.id,
    personalData.user.expo_data,
    gym,
    selected,
    hourSelected,
    user.student.id,
    user.name,
    planIdData,
    schedule.id,
    schedule.date,
    sendNewNotification,
    navigate,
  ]);

  return (
    <>
      <Container>
        <HeaderBack>Reagendar</HeaderBack>

        <ContainerScroll>
          <Wrapper>
            {!planData && <PageTitle>Agendar</PageTitle>}

            {/* {planData && (
              <>
                <ExistingPlanContainer>
                  <PackageTitle>Pacote contratado:</PackageTitle>

                  <ExistingPlanTitle>
                    {' '}
                    {planData.amount} aulas.
                  </ExistingPlanTitle>
                </ExistingPlanContainer>

                <ExistingPlanAmount>Você ainda possui:</ExistingPlanAmount>

                <ExistingPlanTitle>
                  {planData.amount - planData.amount_used}
                  {planData.amount - planData.amount_used > 1
                    ? ' aulas'
                    : ' aula'}{' '}
                  disponíveis.
                </ExistingPlanTitle>
              </>
            )} */}

            <PageSubtitle>Selecione a data:</PageSubtitle>

            <Calendar
              theme={{
                arrowColor: '#37153D',
                textDayFontFamily: 'SofiaPro-Medium',
                selectedDayTextColor: '#fff',
                selectedDayBackgroundColor: '#37153D',
                'stylesheet.day.period': {
                  base: {
                    overflow: 'hidden',
                    height: 34,
                    alignItems: 'center',
                    width: 38,
                  },
                },
                'stylesheet.day.basic': {
                  base: {
                    width: 25,
                    height: 25,
                    alignItems: 'center',
                  },
                  text: {
                    marginTop: 0,
                    fontSize: 16,
                    fontFamily: 'SofiaPro-Medium',
                    fontWeight: '300',
                    color: '#43515c',
                    backgroundColor: 'rgba(255, 255, 255, 0)',
                  },
                },
                'stylesheet.calendar.header': {
                  monthText: {
                    margin: 0,
                    padding: 10,
                  },
                  arrow: {
                    padding: 2,
                  },
                },
              }}
              disabledByDefault
              disableAllTouchEventsForDisabledDays
              hideExtraDays
              markedDates={{
                ...getAvailableDates(),
                [selected]: {
                  selected: true,
                },
              }}
              onDayPress={async (date) => {
                await setHourSelected('');
                await setGym(null);
                setSelected(date.dateString);
                console.log(date.dateString);
                await getScheduleHours(date.dateString);
              }}
            />

            {hours.length > 0 && selected !== '' && (
              <>
                <PageSubtitle>Selecione o horário:</PageSubtitle>

                <ButtonOptionsContainer>{hoursOptions}</ButtonOptionsContainer>
              </>
            )}

            {hours.length === 0 && selected !== '' && (
              <PageSubtitle style={{ textAlign: 'center' }}>
                Não existem horários disponíveis para este dia.
              </PageSubtitle>
            )}

            {hourSelected !== '' && (
              <>
                <PageSubtitle>Selecione a academia:</PageSubtitle>

                <Picker
                  items={personalData.gyms.map((item) => {
                    return {
                      label: item.name,
                      value: item.id,
                      key: item.id,
                    };
                  })}
                  value={gym}
                  selected={gym}
                  onValueChange={(value) => setGym(value)}
                  label="Selecionar academia"
                />
              </>
            )}
          </Wrapper>

          <ButtonWrapper>
            <Button
              onPress={() => {
                gym
                  ? rescheduleClass()
                  : Alert.alert('Selecione data, horário e academia.');
              }}
              style={{ backgroundColor: gym ? '#F76E1E' : '#999' }}
            >
              Confirmar
            </Button>
          </ButtonWrapper>
        </ContainerScroll>
      </Container>

      <ConfirmationModal isVisible={scheduled} />
    </>
  );
};

export default Reschedule;
