import React from 'react';

import LottieView from 'lottie-react-native';
import Modal from 'react-native-modal';

import { SuccessAnimation } from '~/assets/animations';

import { Container, Description } from './styles';

interface Props {
  isVisible: boolean;
  nameUserLiked?: string;
}

const ConfirmationModal: React.FC<Props> = ({ isVisible }) => {
  return (
    <Modal
      backdropOpacity={0}
      isVisible={isVisible}
      animationIn="slideInUp"
      animationOut="slideOutUp"
      useNativeDriver
      hideModalContentWhileAnimating
      propagateSwipe
      style={{
        width: '100%',
        height: '100%',
        margin: 0,
        padding: 0,
      }}
    >
      <>
        <Container>
          <LottieView
            style={{
              height: 120,
            }}
            source={SuccessAnimation}
            autoPlay
            loop={false}
          />

          <Description>Aula reagendada com sucesso!</Description>
        </Container>
      </>
    </Modal>
  );
};

export default ConfirmationModal;
