/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { useEffect, useState } from 'react';

import { useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';

import { Button } from '~/components';
import { ScheduledServices, StudentClass, Personal } from '~/models';
import api from '~/services/api';

import {
  Container,
  ContainerCanceled,
  TitlePurple,
  Title,
  TitleOrange,
} from './styles';

interface NewLikeModalProps {
  isVisible: boolean;
  contract: StudentClass;
  schedule: ScheduledServices;
  closeModal: any;
  personalData: Personal;
}

const ConfirmationModal: React.FC<NewLikeModalProps> = ({
  isVisible,
  closeModal,
  schedule,
  contract,
  personalData,
}) => {
  const { navigate, addListener } = useNavigation();
  const [personal, setPersonal] = useState<Personal>();

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      async function loadPersonal() {
        const response = await api.get(`/professionals/${personalData?.id}`);

        setPersonal(response.data);
      }

      try {
        loadPersonal();
      } catch (err) {
        console.log(err);
      }
    });

    return unsubscribe;
  }, [addListener, personalData]);

  return (
    <Modal
      backdropOpacity={0.5}
      isVisible={isVisible}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      useNativeDriver
      hideModalContentWhileAnimating
      propagateSwipe
      onBackdropPress={closeModal}
      onBackButtonPress={closeModal}
      style={{
        width: '100%',
        height: '70%',
        margin: 0,
        padding: 0,
      }}
    >
      <>
        {schedule?.status === 'PENDENTE' && (
          <Container>
            <TitlePurple>Sua aula ainda não foi confirmada.</TitlePurple>

            <Button
              width={220}
              onPress={closeModal}
              style={{ backgroundColor: '#F76E1E' }}
            >
              Aguardar confirmação
            </Button>

            <Button
              width={220}
              onPress={() => {
                if (personal) {
                  closeModal();
                  navigate('Reschedule', {
                    personalData: personal,
                    planId: contract.professionals_plan_id,
                    plan: contract,
                    schedule,
                  });
                } else {
                  null;
                }
              }}
              style={{ backgroundColor: '#707070' }}
            >
              Remarcar agendamento
            </Button>
          </Container>
        )}

        {schedule?.status === 'REMARCADO' && (
          <Container>
            <TitlePurple>Sua aula ainda não foi confirmada.</TitlePurple>

            <Button
              width={220}
              onPress={closeModal}
              style={{ backgroundColor: '#F76E1E' }}
            >
              Aguardar confirmação
            </Button>

            <Button
              width={220}
              onPress={() => {
                if (personal) {
                  navigate('Reschedule', {
                    personalData: personal,
                    planId: contract.professionals_plan_id,
                    plan: contract,
                    schedule,
                  });
                } else {
                  null;
                }
                closeModal();
              }}
              style={{ backgroundColor: '#707070' }}
            >
              Remarcar agendamento
            </Button>
          </Container>
        )}

        {schedule?.status === 'CANCELADO' && (
          <ContainerCanceled>
            <TitlePurple>
              Sua aula foi cancelada pelo personal e você recebeu crédito para
              marcar uma nova.
            </TitlePurple>

            {schedule.comments ? (
              <Title>
                <TitleOrange>Justificativa:</TitleOrange>
                {`\n ${schedule.comments ?? 'Não localizada'}`}
              </Title>
            ) : (
              <Title>Nenhuma justificativa encontrada.</Title>
            )}

            {/* <Button
              width={220}
              onPress={() => {
                closeModal();

                navigate('Reschedule', {
                  personalData: personal,
                  planId: contract.professionals_plan_id,
                  plan: contract,
                  schedule,
                });
              }}
              style={{ backgroundColor: '#707070' }}
            >
              Remarcar agendamento
            </Button> */}

            <Button
              width={220}
              onPress={() => {
                if (personal) {
                  closeModal();

                  navigate('RescheduleCanceled', {
                    personalData: personal,
                    planId: contract.professionals_plan_id,
                    plan: contract,
                    schedule,
                    individual: !(contract.amount > 1),
                  });
                } else {
                  null;
                }
              }}
              style={{ backgroundColor: '#F76E1E' }}
            >
              Agendar nova aula
            </Button>

            <Button
              width={220}
              onPress={closeModal}
              style={{ backgroundColor: '#707070' }}
            >
              Voltar
            </Button>
          </ContainerCanceled>
        )}

        {schedule?.status === 'RECUSADO' && (
          <Container>
            <TitlePurple>
              Sua aula foi recusada pelo personal e você pode reagendá-la para
              uma outra data.
            </TitlePurple>

            {/* <Title>
              {`Justificativa:\n ${schedule.comments ?? 'Não localizada'}`}
            </Title> */}

            <Button
              width={220}
              onPress={() => {
                if (personal) {
                  closeModal();

                  navigate('Reschedule', {
                    personalData: personal,
                    planId: contract.professionals_plan_id,
                    plan: contract,
                    schedule,
                  });
                } else {
                  null;
                }
              }}
              style={{ backgroundColor: '#707070' }}
            >
              Remarcar agendamento
            </Button>

            <Button
              width={220}
              onPress={closeModal}
              style={{ backgroundColor: '#F76E1E' }}
            >
              Voltar
            </Button>
          </Container>
        )}
      </>
    </Modal>
  );
};

export default ConfirmationModal;
