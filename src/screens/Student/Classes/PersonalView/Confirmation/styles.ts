import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: ${({ theme }) => theme.colors.background};
  height: 300px;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  margin: 0 30px;
`;

export const ContainerCanceled = styled.View`
  background-color: ${({ theme }) => theme.colors.background};
  height: 450px;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  margin: 0 30px;
`;

export const TitleOrange = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.orange};
  margin: 15px 10px 5px;
  text-align: center;
`;

export const TitlePurple = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size22};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.purple};
  margin: 15px 15px 5px;
  text-align: center;
`;

export const Title = styled.Text`
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  color: ${({ theme }) => theme.colors.grayDark};
  margin: 15px 10px;
  text-align: center;
`;

export const TouchableOpacity = styled.TouchableOpacity`
  margin-top: 20px;
`;

export const Subtitle = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  font-size: 18px;
  color: ${({ theme }) => theme.colors.gray};
  text-align: center;
`;
