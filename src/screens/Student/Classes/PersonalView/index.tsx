/* eslint-disable no-nested-ternary */
import React from 'react';

import { useRoute } from '@react-navigation/native';

import { StudentClass } from '~/models';

import MultipleClass from './MultipleClass';
import OnlyClass from './OnlyClass';

interface Params {
  myClass: StudentClass;
}

const PersonalView: React.FC = () => {
  const { params } = useRoute();
  const { myClass } = params as Params;

  return myClass.amount === 1 ? <OnlyClass /> : <MultipleClass />;
};

export default PersonalView;
