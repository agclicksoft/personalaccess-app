/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Linking } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import { parseISO, format, addHours, differenceInDays } from 'date-fns';
import { pt } from 'date-fns/locale';

import { IconWhatsApp, IconEmail, IconPhone } from '~/assets/icons';
import { ContainerScroll, HeaderPurple } from '~/components';
import { StudentClass } from '~/models';
import api from '~/services/api';
import { PlaceholderImage } from '~/utils/consts';
import {
  getPaymentStatus,
  getStudentStatus,
  getStudentStatusBG,
  getStudentStatusColor,
} from '~/utils/geStatus';

import {
  Container,
  FormContainer,
  ProfileImage,
  Name,
  Age,
  Divider,
  Card,
  CardContent,
  ClassWrapper,
  ClassHolder,
  ClassQuestion,
  ClassAnswer,
  ClassDivider,
  StatusButton,
  StatusButtonText,
  ButtonWrapper,
  TouchableOpacity,
  ButtonImage,
  StatusPayment,
  StatusPaymentLabel,
  PaymentStatusButton,
  PaymentStatusButtonText,
} from './styles';

import ConfirmationModal from '../Confirmation';

interface Params {
  myClass: StudentClass;
}

const OnlyClass: React.FC = () => {
  const { navigate } = useNavigation();
  const { params } = useRoute();
  const { myClass } = params as Params;
  const [personal, setPersonal] = useState(null);
  const [modal, setModal] = useState(false);
  const [selectedClass, setSelectedClass] = useState<StudentClass>(myClass);
  const [canceled, setCanceled] = useState(false);

  // const daysTillAppointment = differenceInDays(
  //   parseISO(myClass.scheduledServices[0].date),
  //   new Date(),
  // );

  useEffect(() => {
    async function getPersonal() {
      const response = await api.get(
        `/professionals/${myClass.professional.id}`,
      );

      setPersonal(response.data);
    }

    async function getMyClass() {
      const response = await api.get(`/contracts/${myClass.id}`);

      setSelectedClass(response.data);
    }

    getPersonal();

    if (canceled) {
      getMyClass();
    }
  }, [myClass, canceled]);

  const formattedDate = format(
    addHours(parseISO(selectedClass.scheduledServices[0].date), 3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Data/hora: ', answer: formattedDate },
    {
      id: 2,
      question: 'Local: ',
      answer: selectedClass.scheduledServices[0].establishment.name,
    },
    {
      id: 3,
      question: 'Pacote: ',
      answer: selectedClass.professionalPlan.plan.description,
    },
    // { id: 4, question: 'Pagamento: ', answer: selectedClass.method_payment },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <ClassHolder key={item.id}>
          <ClassQuestion>{item.question}</ClassQuestion>
          <ClassAnswer>{item.answer}</ClassAnswer>
        </ClassHolder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const navigateWithStatus = useCallback(
    async (status: string) => {
      if (status === 'CONCLUIDO') {
        navigate('ClassDescriptionView', {
          myClass,
        });
      } else if (
        status === 'PENDENTE' ||
        status === 'REMARCADO' ||
        status === 'CANCELADO' ||
        status === 'RECUSADO'
      ) {
        setModal(!modal);
        console.log(status);
      } else {
        null;
      }
    },
    [modal, myClass, navigate],
  );

  return (
    <Container>
      <HeaderPurple>Aula</HeaderPurple>

      <ContainerScroll>
        <FormContainer>
          <ProfileImage
            source={{
              uri:
                selectedClass.professional.user.img_profile ?? PlaceholderImage,
            }}
          />

          <Name>{selectedClass.professional.user.name}</Name>

          <Age>
            {selectedClass.professional.doc_professional.toLocaleUpperCase()}
          </Age>

          <Divider />

          <ButtonWrapper>
            <TouchableOpacity
              onPress={() => {
                Linking.canOpenURL('whatsapp://send?text=Oi').then(
                  (supported) => {
                    if (supported) {
                      return Linking.openURL(
                        `whatsapp://send?phone=55${personal?.user.contact}&text=Olá, lhe contratei no Personal Access, tudo bem?`,
                      );
                    }
                    return Linking.openURL(
                      `https://api.whatsapp.com/send?phone=55${personal?.user.contact}&text=Olá, lhe contratei no Personal Access, tudo bem?`,
                    );
                  },
                );
              }}
            >
              <ButtonImage source={IconWhatsApp} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  `mailto:${personal.user?.email}?subject=Olá, lhe contratei pelo Personal Access!`,
                );
              }}
            >
              <ButtonImage source={IconEmail} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${personal.user?.contact}`);
              }}
            >
              <ButtonImage source={IconPhone} />
            </TouchableOpacity>
          </ButtonWrapper>

          {selectedClass?.scheduledServices[0].status !== 'CANCELADO' &&
            selectedClass?.scheduledServices[0].status !== 'RECUSADO' && (
              <>
                <StatusPayment
                  style={{
                    marginBottom:
                      selectedClass?.method_payment !== 'PENDENTE' ? 20 : 0,
                  }}
                >
                  Pagamento:{' '}
                  <StatusPaymentLabel>
                    {getPaymentStatus(selectedClass?.method_payment)}
                  </StatusPaymentLabel>
                </StatusPayment>

                {/* {selectedClass?.method_payment === 'BOLETO' &&
                  daysTillAppointment >= 5 && (
                    <TouchableOpacity
                      onPress={() =>
                        navigate('SelectPayment', {
                          myClass,
                          regenerate: true,
                        })
                      }
                    >
                      <PaymentStatusButton>
                        <PaymentStatusButtonText>
                          Gerar um novo boleto
                        </PaymentStatusButtonText>
                      </PaymentStatusButton>
                    </TouchableOpacity>
                  )} */}

                {selectedClass?.method_payment === 'PENDENTE' &&
                selectedClass?.scheduledServices[0].status === 'CONFIRMADO' ? (
                  <TouchableOpacity
                    onPress={() =>
                      navigate('SelectPayment', {
                        myClass,
                      })
                    }
                  >
                    <PaymentStatusButton>
                      <PaymentStatusButtonText>
                        Realizar pagamento
                      </PaymentStatusButtonText>
                    </PaymentStatusButton>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity onPress={() => null}>
                    <PaymentStatusButton>
                      <PaymentStatusButtonText>
                        Não liberado
                      </PaymentStatusButtonText>
                    </PaymentStatusButton>
                  </TouchableOpacity>
                )}
              </>
            )}

          <Card
            onPress={() =>
              navigateWithStatus(selectedClass.scheduledServices[0].status)
            }
          >
            <CardContent>
              <ClassWrapper>{optionsMemo}</ClassWrapper>
            </CardContent>

            <ClassDivider />

            <StatusButton
              style={{
                backgroundColor: getStudentStatusBG(
                  selectedClass.scheduledServices[0].status,
                ),
              }}
            >
              <StatusButtonText
                style={{
                  color: getStudentStatusColor(
                    selectedClass.scheduledServices[0].status,
                  ),
                }}
              >
                {getStudentStatus(selectedClass.scheduledServices[0].status)}
              </StatusButtonText>
            </StatusButton>
          </Card>
        </FormContainer>
      </ContainerScroll>

      <ConfirmationModal
        isVisible={modal}
        schedule={myClass.scheduledServices[0]}
        contract={myClass}
        closeModal={() => setModal(!modal)}
        personalData={myClass.professional}
      />
    </Container>
  );
};

export default OnlyClass;
