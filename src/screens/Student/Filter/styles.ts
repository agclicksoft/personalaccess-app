import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const SearchContainer = styled.View`
  width: 100%;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  font-size: ${({ theme }) => theme.fontSizes.size14};
  color: ${({ theme }) => theme.colors.orange};
  padding: 20px;
`;

export const SpecialtiesContainer = styled.View``;
