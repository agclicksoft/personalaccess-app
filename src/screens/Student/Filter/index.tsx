/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useMemo, useCallback } from 'react';

import {
  HeaderBack,
  ContainerScroll,
  GoogleSearch,
  Checkbox,
} from '~/components';
import specialtiesList from '~/utils/specialtiesList';

import {
  Container,
  SearchContainer,
  Title,
  SpecialtiesContainer,
} from './styles';

interface Props {
  state?: string;
  city?: string;
  neighborhood?: string;
  street?: string;
  country?: string;
  name?: string;
}

const Filter: any = ({ navigation, route }) => {
  const [address, setAddress] = useState<Props>(null);
  const [specialties, setSpecialties] = useState([]);

  const toggleSpecialty = useCallback(
    async (specialty: string) => {
      let newArr = [];

      if (!specialties.includes(specialty)) {
        newArr = [...specialties, specialty];
      } else {
        newArr = specialties.filter((a) => a !== specialty);
      }

      await setSpecialties(newArr);
    },
    [specialties],
  );

  const optionsMemo = useMemo(
    () =>
      specialtiesList.map((item) => (
        <Checkbox
          key={item.id}
          title={item.description}
          onPress={() => {
            toggleSpecialty(item.description);
          }}
          value={specialties.includes(item.description)}
        />
      )),
    [specialties, toggleSpecialty],
  );

  const getSpecialtyQuery = useCallback(() => {
    let query = 'specialty=';
    let empty = true;

    specialties.map((item) => {
      if (item) {
        query += `${item}, `;
        empty = false;
      }
    });

    !empty ? (query = query.slice(0, query.length - 2)) : null;

    return !empty ? query : '';
  }, [specialties]);

  const getLocationQuery = useCallback(
    async (type) => {
      switch (type) {
        case 'state':
          return address?.state ? `${type}=${address.state}` : '';
        case 'district':
          return address?.city ? `${type}=${address.city}` : '';
        case 'neighbourhood':
          return address?.neighborhood ? `${type}=${address.neighborhood}` : '';

        default:
          break;
      }
    },
    [address],
  );

  const getAllQueries = async () => {
    let aux = '';

    aux += await getSpecialtyQuery();

    aux += (await getLocationQuery('state'))
      ? `&${await getLocationQuery('state')}`
      : '';
    aux += (await getLocationQuery('district'))
      ? `&${await getLocationQuery('district')}`
      : '';
    aux += (await getLocationQuery('neighbourhood'))
      ? `&${await getLocationQuery('neighbourhood')}`
      : '';

    console.log(aux);

    return aux;
  };

  return (
    <Container>
      <HeaderBack
        rightButton
        rightButtonTitle="Aplicar"
        onPress={async () => {
          const query = await getAllQueries();
          route.params.onReturn(query);
          navigation.goBack();
        }}
      >
        Filtros de busca
      </HeaderBack>

      <ContainerScroll>
        <SearchContainer>
          <Title>Locais de atendimento:</Title>

          <GoogleSearch setField={setAddress} />
        </SearchContainer>

        <SpecialtiesContainer>
          <Title>Especialidades:</Title>
          {optionsMemo}
        </SpecialtiesContainer>
      </ContainerScroll>
    </Container>
  );
};

export default Filter;
