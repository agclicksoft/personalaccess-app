/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import CardFlip from 'react-native-card-flip';
import { showMessage } from 'react-native-flash-message';

import { IconFileWhite } from '~/assets/icons';
import { Button, ContainerScroll, HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import { StudentClass } from '~/models';
import api from '~/services/api';
import formatValue from '~/utils/formatValue';

import { styles } from './CardFlip/styles';
import {
  Container,
  Content,
  Card,
  BrandImage,
  CardInfo,
  CardNameHolder,
  ButtonWrapper,
  Bold,
} from './styles';

interface Params {
  myClass?: StudentClass;
}

const PayInHands: React.FC = () => {
  const cardRef = useRef<any>();
  const { navigate } = useNavigation();
  const { user } = useAuth();
  const { params } = useRoute();
  const { myClass } = params as Params;
  const [loading, setLoading] = useState(false);

  const chargePayment = useCallback(async () => {
    setLoading(true);

    if (
      !user.name ||
      !user.email ||
      !user.cpf ||
      !user.contact ||
      !user.birthday
    ) {
      setLoading(false);

      showMessage({
        type: 'danger',
        message: 'Perfil incompleto',
        description: 'O atualize e tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });

      setTimeout(() => {
        navigate('EditProfile');
      }, 2000);

      return;
    }

    try {
      const response = await api.put(`/contracts/${myClass.id}`, {
        method_payment: 'MAOS',
        payment_date: myClass.scheduledServices[0].date,
      });

      console.log(response.data);

      showMessage({
        type: 'success',
        message: 'Sua escolha foi confirmada!',
        description:
          'Entre em contato com seu personal para definir os detalhes',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });

      navigate('Class');

      setLoading(false);
    } catch (err) {
      console.log(err);
      console.log(err?.response);
      setLoading(false);

      showMessage({
        type: 'danger',
        message: 'Erro ao processar sua escolja',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [myClass, navigate, user]);

  return (
    <>
      <Container>
        <HeaderBack>Pagamento em mãos</HeaderBack>

        <ContainerScroll>
          <Content>
            <CardFlip ref={cardRef} style={styles.cardContainer}>
              <Card style={[styles.card, styles.card1]}>
                <BrandImage source={IconFileWhite} />

                <CardInfo style={{ color: '#fff' }}>
                  {myClass.professionalPlan.plan.description} -{' '}
                  {myClass.professional.user.name}
                </CardInfo>

                <CardInfo style={{ color: '#fff' }}>
                  Confirme os dados abaixo:
                </CardInfo>

                <CardNameHolder>
                  <CardInfo style={{ color: '#fff' }}>Valor: </CardInfo>

                  <Bold>{formatValue(myClass.professionalPlan.price)}</Bold>
                </CardNameHolder>

                <CardNameHolder>
                  <Bold>Pagamento em mãos no dia da aula.</Bold>
                </CardNameHolder>
              </Card>

              <Card style={[styles.card, styles.card2]} />
            </CardFlip>

            <ButtonWrapper>
              <Button onPress={() => chargePayment()}>Confirmar</Button>
            </ButtonWrapper>
          </Content>
        </ContainerScroll>
      </Container>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000080',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )}
    </>
  );
};

export default PayInHands;
