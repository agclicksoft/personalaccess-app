/* eslint-disable consistent-return */
import React, { useMemo, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation, useRoute } from '@react-navigation/native';
import axios from 'axios';
import { addHours, format, parseISO, differenceInDays } from 'date-fns';
import { pt } from 'date-fns/locale';
import { showMessage } from 'react-native-flash-message';

import { IconCreditCard, IconVerified, IconFile } from '~/assets/icons';
import { ContainerScroll, HeaderBack } from '~/components';
import { StudentClass } from '~/models';
import { PlaceholderImage } from '~/utils/consts';
import formatValue from '~/utils/formatValue';
import { PAGSEGURO_EMAIL, PAGSEGURO_TOKEN } from '~/utils/pagSeguroToken';

import {
  Container,
  ContentWrapper,
  PageTitle,
  Card,
  CardContent,
  Image,
  Wrapper,
  Holder,
  Question,
  Answer,
  Divider,
  StatusButton,
  StatusButtonText,
  MenuContainer,
  MenuButton,
  MenuIcon,
  TextWrapper,
  MenuTitle,
  SlipInfo,
} from './styles';

interface RouteParam {
  myClass?: StudentClass;
  regenerate?: boolean;
}

const SelectPayment: React.FC = () => {
  const { navigate } = useNavigation();
  const { params } = useRoute();
  const { myClass, regenerate } = params as RouteParam;
  const [loading, setLoading] = useState(false);

  const createSession = async () => {
    try {
      let sessionCode = null;
      setLoading(true);

      const response = await axios.post(
        `https://ws.pagseguro.uol.com.br/v2/sessions?email=${PAGSEGURO_EMAIL}&token=${PAGSEGURO_TOKEN}`,
      );

      if (response.data) {
        sessionCode = response.data
          .toString()
          .replace(
            '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?><session><id>',
            '',
          )
          .replace('</id></session>', '');

        console.log(`Session ID: ${sessionCode}`);
      }

      setLoading(false);
      return sessionCode;
    } catch (err) {
      console.log(err);
      console.log(err?.response);
      setLoading(false);

      showMessage({
        type: 'danger',
        message: 'Serviço de pagamento indisponível',
        description: 'Tente novamente mais tarde',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });

      return null;
    }
  };

  // const daysTillAppointment = differenceInDays(
  //   parseISO(myClass.scheduledServices[0].date),
  //   new Date(),
  // );

  const formattedDate = format(
    addHours(parseISO(myClass.scheduledServices[0].date), 3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Data/hora: ', answer: formattedDate },
    {
      id: 2,
      question: 'Local: ',
      answer: myClass.scheduledServices[0].establishment.name,
    },
    { id: 3, question: 'Personal: ', answer: myClass.professional.user.name },
    {
      id: 4,
      question: 'Pacote: ',
      answer: myClass.professionalPlan.plan.description,
    },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <Holder key={item.id}>
          <Question>{item.question}</Question>
          <Answer>{item.answer}</Answer>
        </Holder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const paymentOptions = [
    {
      title: 'Cartão de crédito',
      icon: IconCreditCard,
      screen: 'CreditCard',
    },
    {
      title: 'Pagar no dia',
      icon: IconVerified,
      screen: 'PayInHands',
    },
  ];

  const paymentMemo = useMemo(
    () =>
      paymentOptions.map((item) => (
        <MenuButton
          key={item.title}
          onPress={async () => {
            if (item.title !== 'Pagar no dia') {
              const sessionId = await createSession();
              sessionId !== null
                ? navigate(item.screen, {
                    myClass,
                    sessionId,
                  })
                : null;
            } else {
              navigate(item.screen, {
                myClass,
              });
            }
          }}
        >
          <MenuContainer>
            <Holder>
              <MenuIcon source={item.icon} />

              <TextWrapper>
                <MenuTitle>{item.title}</MenuTitle>
              </TextWrapper>
            </Holder>

            <Ionicons
              name="ios-arrow-forward"
              size={21}
              color="rgba(112,112,112,0.5)"
            />
          </MenuContainer>
        </MenuButton>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <Container>
      <HeaderBack>Selecionar pagamento</HeaderBack>

      <ContainerScroll>
        <ContentWrapper>
          <PageTitle>Detalhes do agendamento</PageTitle>

          <Card onPress={() => null}>
            <CardContent>
              <Image
                source={{
                  uri:
                    myClass.professional.user.img_profile ?? PlaceholderImage,
                }}
              />

              <Wrapper>{optionsMemo}</Wrapper>
            </CardContent>

            <Divider />

            <StatusButton>
              <StatusButtonText>
                {formatValue(myClass.professionalPlan.price)}
              </StatusButtonText>
            </StatusButton>
          </Card>

          {/* {!regenerate && daysTillAppointment >= 5 && paymentMemo} */}

          {/* {!regenerate && daysTillAppointment <= 4 && ( */}
          {/* <> */}
          {/* <SlipInfo>
                Pagamento por boleto indisponível devido a proximidade de seu
                agendamento, selecione uma das opções abaixo:
              </SlipInfo> */}

          {paymentMemo}
          {/* </> */}
          {/* )} */}

          {/* {daysTillAppointment >= 5 && (
            <MenuButton
              onPress={async () => {
                const sessionId = await createSession();
                sessionId !== null
                  ? navigate('BankSlip', {
                      myClass,
                      sessionId,
                    })
                  : null;
              }}
            >
              <MenuContainer>
                <Holder>
                  <MenuIcon source={IconFile} />

                  <TextWrapper>
                    <MenuTitle>Boleto bancário</MenuTitle>
                  </TextWrapper>
                </Holder>

                <Ionicons
                  name="ios-arrow-forward"
                  size={21}
                  color="rgba(112,112,112,0.5)"
                />
              </MenuContainer>
            </MenuButton>
          )} */}
        </ContentWrapper>
      </ContainerScroll>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000029',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#37153D" />
        </View>
      )}
    </Container>
  );
};

export default SelectPayment;
