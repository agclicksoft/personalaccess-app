import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F9F9F9',
    marginVertical: 30,
  },
  cardContainer: {
    width: '100%',
    height: 186,
    backgroundColor: '#F9F9F9',
  },
  card: {
    width: '100%',
    height: 186,
    borderRadius: 5,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
  },
  card1: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 30,
  },
  card2: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  label: {
    textAlign: 'center',
    fontSize: 55,
    fontFamily: 'System',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});
