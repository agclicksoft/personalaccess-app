/* eslint-disable no-nested-ternary */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';

import { useNavigation, useRoute } from '@react-navigation/native';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/mobile';
import CardFlip from 'react-native-card-flip';
import { showMessage } from 'react-native-flash-message';
import WebView from 'react-native-webview';

import { IconVisa, IconMastercard } from '~/assets/icons';
import {
  Button,
  ContainerScroll,
  HeaderBack,
  Input,
  MaskedInput,
} from '~/components';
import { useAuth } from '~/hooks/auth';
import { StudentClass } from '~/models';
import api from '~/services/api';

import { styles } from './CardFlip/styles';
import {
  Container,
  Content,
  Card,
  Label,
  WeightHeightWrapper,
  WeightHolder,
  HeightHolder,
  BrandImage,
  CardInfo,
  CardNameHolder,
  CVVHolder,
  CVVWrapper,
  ButtonWrapper,
} from './styles';

interface Params {
  myClass?: StudentClass;
  sessionId?: string;
}

const CreditCard: React.FC = () => {
  const webViewRef = useRef(null);
  const cardRef = useRef<any>();
  const formRef = useRef<FormHandles>(null);
  const { user } = useAuth();
  const { navigate } = useNavigation();
  const { params } = useRoute();
  const { myClass, sessionId } = params as Params;
  const [hash, setHash] = useState(null);
  const [cardToken, setCardToken] = useState(null);
  const [cardNumber, setCardNumber] = useState(null);
  const [cardDate, setCardDate] = useState(null);
  const [cvv, setCVV] = useState(null);
  const [cardName, setCardName] = useState(null);
  const [showBackCard, setShowBackCard] = useState(false);
  const [loading, setLoading] = useState(false);

  const createCardHash = useCallback(async () => {
    try {
      setLoading(true);

      if (!cardNumber || !cvv || !cardDate || !cardName) {
        setLoading(false);

        showMessage({
          type: 'danger',
          message: 'Preencha todas as informações',
          titleStyle: {
            textAlign: 'center',
          },
        });

        return;
      }

      const number = cardNumber.replace(/[^0-9]/g, '');
      const expirationMonth = cardDate.replace(/[^0-9]/g, '').substring(0, 2);
      const expirationYear = cardDate.replace(/[^0-9]/g, '').substring(2, 6);
      const brand = number.startsWith('4') ? 'visa' : 'mastercard';

      if (
        !user.name ||
        !user.email ||
        !user.cpf ||
        !user.contact ||
        !user.birthday
      ) {
        setLoading(false);

        showMessage({
          type: 'danger',
          message: 'Perfil incompleto',
          description: 'O atualize e tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });

        setTimeout(() => {
          navigate('EditProfile');
        }, 2000);

        return;
      }

      const getCardToken = `
      PagSeguroDirectPayment.setSessionId('${sessionId}');

      PagSeguroDirectPayment.createCardToken({
          cardNumber: '${cardNumber}',
          brand: '${brand}',
          cvv: '${cvv}',
          expirationMonth: '${expirationMonth}',
          expirationYear: '${expirationYear}',
          success: function (response) {
             window.ReactNativeWebView.postMessage(
              JSON.stringify({
                data: response.card.token,
                message: 'cardToken',
              })
            );
          },
          error: function(response) {
            window.ReactNativeWebView.postMessage(
              JSON.stringify({
                message: response,
              })
            );
          },
        });
      `;

      await webViewRef.current.injectJavaScript(getCardToken);
    } catch (err) {
      setLoading(false);
      console.log(err);

      showMessage({
        type: 'danger',
        message: 'Erro ao processar seu pagamento',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [
    cardNumber,
    cvv,
    cardDate,
    cardName,
    user,
    sessionId,
    navigate,
    webViewRef,
  ]);

  const chargePayment = useCallback(async () => {
    setLoading(true);

    console.log(`CardToken: ${cardToken}`);
    console.log(`Hash: ${hash}`);

    const phone = user.contact.replace(/[^0-9]/g, '');

    try {
      const response = await api.post(`/pagseguro/charge`, {
        sender: {
          name: cardName,
          email: user.email,
          cpf_cnpj: user.cpf.replace(/[^0-9]/g, ''),
          area_code: phone.length > 9 ? phone.substring(0, 2) : phone,
          phone: phone.length > 9 ? phone.slice(2) : phone,
          birth_date: user.birthday,
        },
        shipping: {
          street: user.address_street
            ? `${user.address_street}`
            : 'Rua não informada',
          number: user.address_number ? `${user.address_number}` : '1',
          district: user.address_neighborhood
            ? `${user.address_neighborhood}`
            : 'Bairro não informado',
          city: user.address_city
            ? `${user.address_city}`
            : 'Cidade Não informada',
          state: user.address_uf ? `${user.address_uf}` : 'RJ',
          postal_code: user.address_zipcode
            ? user.address_zipcode.replace(/[^0-9]/g, '')
            : '23040300',
          same_for_billing: true,
        },
        item: {
          qtde: '1',
          value: myClass?.professionalPlan?.price,
          description: myClass?.professionalPlan?.plan?.description,
        },
        method: 'creditCard',
        credit_card_token: cardToken,
        value: myClass?.professionalPlan?.price,
        hash,
        reference: `${myClass.id}`,
      });

      if (response.status === 200) {
        console.log(response.data);

        await api.put(`/contracts/${myClass.id}`, {
          method_payment: 'CARTAO',
        });
      }

      setLoading(false);

      showMessage({
        type: 'success',
        message: 'Pagamento processado com sucesso!',
        titleStyle: {
          textAlign: 'center',
        },
      });

      navigate('Class');
    } catch (err) {
      console.log(err);
      console.log(err?.response);

      showMessage({
        type: 'danger',
        message: 'Erro ao processar seu pagamento',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });

      setLoading(false);
    }
  }, [cardName, cardToken, hash, myClass, user, navigate]);

  const getSenderHash = `
    PagSeguroDirectPayment.setSessionId('${sessionId}');
    PagSeguroDirectPayment.onSenderHashReady(function(response){
        if(response.status == 'error') {
          window.ReactNativeWebView.postMessage(JSON.stringify({
            message: 'response.message'
          }));
          return false;
        }
        var hash = response.senderHash;
        window.ReactNativeWebView.postMessage(JSON.stringify({
          data: hash,
          message: 'senderHash'
        }));
      });
      `;

  return (
    <>
      <Container>
        <HeaderBack>Cartão de crédito</HeaderBack>

        <ContainerScroll>
          <Content>
            <CardFlip ref={cardRef} style={styles.cardContainer}>
              <Card style={[styles.card, styles.card1]}>
                <BrandImage
                  source={
                    cardNumber?.startsWith('4')
                      ? IconVisa
                      : cardNumber?.startsWith('5')
                      ? IconMastercard
                      : null
                  }
                />

                <CardInfo style={{ color: '#fff' }}>{cardNumber}</CardInfo>

                <CardNameHolder>
                  <CardInfo style={{ color: '#fff' }}>{cardName}</CardInfo>

                  <CardInfo style={{ color: '#fff' }}>{cardDate}</CardInfo>
                </CardNameHolder>
              </Card>

              <Card style={[styles.card, styles.card2]}>
                <CVVHolder>
                  <CVVWrapper>
                    <CardInfo style={{ color: '#000', textAlign: 'center' }}>
                      {cvv}
                    </CardInfo>
                  </CVVWrapper>
                </CVVHolder>
              </Card>
            </CardFlip>

            {hash ? (
              <Form ref={formRef} onSubmit={null}>
                <Label>Número do cartão:</Label>

                <MaskedInput
                  name="cardNumber"
                  type="credit-card"
                  options={{
                    obfuscated: false,
                  }}
                  placeholder="4546 5456 4564 6546"
                  keyboardType="decimal-pad"
                  width="100%"
                  onFocus={() => {
                    if (showBackCard) {
                      setShowBackCard(false);
                      cardRef.current?.flip();
                    }
                  }}
                  onChangeText={(text) => setCardNumber(text)}
                  value={cardNumber ?? null}
                />

                <WeightHeightWrapper>
                  <WeightHolder>
                    <Label>Validade:</Label>
                    <MaskedInput
                      name="valid"
                      type="custom"
                      options={{
                        mask: '99/9999',
                      }}
                      placeholder="12/2026"
                      keyboardType="decimal-pad"
                      width="100%"
                      maxLength={7}
                      initialValue=""
                      onFocus={() => {
                        if (showBackCard) {
                          setShowBackCard(false);
                          cardRef.current?.flip();
                        }
                      }}
                      onChangeText={(text) => setCardDate(text)}
                      value={cardDate ?? null}
                    />
                  </WeightHolder>

                  <HeightHolder>
                    <Label>CVV:</Label>

                    <MaskedInput
                      name="cvv"
                      type="custom"
                      options={{
                        mask: '999',
                      }}
                      placeholder="123"
                      keyboardType="decimal-pad"
                      width="100%"
                      maxLength={3}
                      initialValue=""
                      onFocus={() => {
                        if (!showBackCard) {
                          setShowBackCard(true);
                          cardRef.current?.flip();
                        }
                      }}
                      onChangeText={(text) => setCVV(text)}
                      value={cvv ?? null}
                    />
                  </HeightHolder>
                </WeightHeightWrapper>

                <Label>Nome do titular:</Label>
                <Input
                  name="name"
                  width="100%"
                  placeholder="Nome como aparece em seu cartão"
                  onFocus={() => {
                    if (showBackCard) {
                      setShowBackCard(false);
                      cardRef.current?.flip();
                    }
                  }}
                  onChangeText={(text) => setCardName(text.toUpperCase())}
                  value={cardName ?? null}
                />

                <ButtonWrapper>
                  <Button onPress={() => createCardHash()}>
                    Confirmar pagamento
                  </Button>
                </ButtonWrapper>
              </Form>
            ) : (
              <View
                style={{
                  marginTop: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <CardInfo style={{ color: '#000' }}>
                  Conectando ao servidor de pagamento
                </CardInfo>
                <ActivityIndicator size="large" color="#37153D" />
              </View>
            )}
          </Content>
        </ContainerScroll>
      </Container>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000080',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )}

      <View>
        <WebView
          source={{
            uri: `https://personalaccess-api-test.herokuapp.com/api/pagseguro`,
          }}
          ref={webViewRef}
          injectedJavaScript={getSenderHash}
          javaScriptEnabled
          onMessage={async (event) => {
            const response = JSON.parse(event.nativeEvent.data);

            if (response.message === 'senderHash') {
              await setHash(response.data);
              console.log(`SenderHash: ${response.data}`);
            } else if (response.message === 'cardToken') {
              await setCardToken(response.data);
              console.log(`CardToken: ${response.data}`);

              chargePayment();
            }
          }}
        />
      </View>
    </>
  );
};

export default CreditCard;
