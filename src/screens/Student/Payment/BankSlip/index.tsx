/* eslint-disable no-nested-ternary */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-return-assign */
import React, { useCallback, useRef, useState } from 'react';
import { ActivityIndicator, View, Clipboard } from 'react-native';

// import Clipboard from '@react-native-community/clipboard';
import { useNavigation, useRoute } from '@react-navigation/native';
import Axios from 'axios';
import { addDays, format, startOfToday, parseISO } from 'date-fns';
import CardFlip from 'react-native-card-flip';
import { showMessage } from 'react-native-flash-message';
import WebView from 'react-native-webview';

import { IconFileWhite } from '~/assets/icons';
import { Button, ContainerScroll, HeaderBack } from '~/components';
import { useAuth } from '~/hooks/auth';
import { StudentClass } from '~/models';
import api from '~/services/api';
import formatValue from '~/utils/formatValue';
import { PAGSEGURO_EMAIL, PAGSEGURO_TOKEN } from '~/utils/pagSeguroToken';

import { styles } from './CardFlip/styles';
import {
  Container,
  Content,
  Card,
  BrandImage,
  CardInfo,
  CardNameHolder,
  ButtonWrapper,
  Bold,
  BankSlipContainer,
  BankSlipDueDate,
  BankSlipValue,
} from './styles';

interface Params {
  myClass?: StudentClass;
  sessionId?: string;
}

const BankSlip: React.FC = () => {
  const cardRef = useRef<any>();
  const { navigate } = useNavigation();
  const { user } = useAuth();
  const { params } = useRoute();
  const { myClass, sessionId } = params as Params;
  const [loading, setLoading] = useState(false);
  const [hash, setHash] = useState(null);
  const [barcode, setBarCode] = useState(null);
  const [dueDate, setDueDate] = useState<string>(null);
  const [clip, setClip] = useState(false);

  const chargePayment = useCallback(async () => {
    setLoading(true);

    if (
      !user.name ||
      !user.email ||
      !user.cpf ||
      !user.contact ||
      !user.birthday
    ) {
      setLoading(false);

      showMessage({
        type: 'danger',
        message: 'Perfil incompleto',
        description: 'O atualize e tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });

      setTimeout(() => {
        navigate('EditProfile');
      }, 2000);

      return;
    }

    const phone = user.contact.replace(/[^0-9]/g, '');

    const data = {
      reference: `${myClass.id}`,
      firstDueDate: `${format(addDays(startOfToday(), 3), "yyyy'-'MM'-'dd")}`,
      numberOfPayments: '1',
      periodicity: 'monthly',
      amount: `${myClass?.professionalPlan?.price}`,
      instructions:
        'Boleto referente a contratação de seu pacote de aulas junto ao Personal Access.',
      description: `${myClass?.professionalPlan?.plan?.description}`,
      customer: {
        document: {
          type: 'CPF',
          value: `${user.cpf.replace(/[^0-9]/g, '')}`,
        },
        name: `${user.name}`,
        email: `${user.email}`,
        phone: {
          areaCode: `${phone.length > 9 ? phone.substring(0, 2) : phone}`,
          number: `${phone.length > 9 ? phone.slice(2) : phone}`,
        },
        address: {
          postalCode: user.address_zipcode
            ? user.address_zipcode.replace(/[^0-9]/g, '')
            : '23040300',
          street: user.address_street
            ? `${user.address_street}`
            : 'Rua não informada',
          number: user.address_number ? `${user.address_number}` : '1',
          district: user.address_neighborhood
            ? `${user.address_neighborhood}`
            : 'Bairro não informado',
          city: user.address_city
            ? `${user.address_city}`
            : 'Cidade Não informada',
          state: user.address_uf ? `${user.address_uf}` : 'RJ',
        },
      },
    };

    try {
      const response = await Axios.post(
        `https://ws.pagseguro.uol.com.br/recurring-payment/boletos?email=${PAGSEGURO_EMAIL}&token=${PAGSEGURO_TOKEN}`,
        data,
      );

      if (response.status === 201) {
        console.log(response.data);

        await api.put(`/contracts/${myClass.id}`, {
          method_payment: 'BOLETO',
          ipte: response.data.boletos[0].barcode,
          payment_due_date: response.data.boletos[0].dueDate,
        });

        setLoading(false);
        setBarCode(response.data.boletos[0].barcode);
        setDueDate(response.data.boletos[0].dueDate);
      }

      setLoading(false);
    } catch (err) {
      console.log(err);
      console.log(err?.response);
      setLoading(false);

      showMessage({
        type: 'danger',
        message: 'Erro ao gerar seu boleto',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  }, [myClass, navigate, user]);

  const getSenderHash = `
    PagSeguroDirectPayment.setSessionId('${sessionId}');
    PagSeguroDirectPayment.onSenderHashReady(function(response){
        if(response.status == 'error') {
          window.ReactNativeWebView.postMessage(JSON.stringify({
            message: 'response.message'
          }));
          return false;
        }
        var hash = response.senderHash;
        window.ReactNativeWebView.postMessage(JSON.stringify({
          data: hash,
          message: 'senderHash'
        }));
      });
      `;

  return (
    <>
      <Container>
        <HeaderBack>Boleto bancário</HeaderBack>

        <ContainerScroll>
          <Content>
            <CardFlip ref={cardRef} style={styles.cardContainer}>
              <Card style={[styles.card, styles.card1]}>
                <BrandImage source={IconFileWhite} />

                <CardInfo style={{ color: '#fff' }}>
                  {myClass.professionalPlan.plan.description} -{' '}
                  {myClass.professional.user.name}
                </CardInfo>

                <CardInfo style={{ color: '#fff' }}>
                  Confirme os dados abaixo:
                </CardInfo>

                <CardNameHolder>
                  <CardInfo style={{ color: '#fff' }}>Valor: </CardInfo>

                  <Bold>{formatValue(myClass.professionalPlan.price)}</Bold>
                </CardNameHolder>

                <CardNameHolder>
                  <CardInfo style={{ color: '#fff' }}>Vencimento: </CardInfo>

                  <Bold>{format(addDays(startOfToday(), 3), "dd'/'MM")}</Bold>
                </CardNameHolder>
              </Card>

              <Card style={[styles.card, styles.card2]} />
            </CardFlip>

            {hash && !barcode && (
              <ButtonWrapper>
                <Button onPress={() => chargePayment()}>Gerar boleto</Button>
              </ButtonWrapper>
            )}

            {!hash && !barcode && (
              <View
                style={{
                  marginTop: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <CardInfo style={{ color: '#000' }}>
                  Conectando ao servidor de pagamento
                </CardInfo>
                <ActivityIndicator size="large" color="#37153D" />
              </View>
            )}

            {barcode && dueDate && (
              <BankSlipContainer>
                <BankSlipValue>
                  {formatValue(myClass?.professionalPlan?.price)}
                </BankSlipValue>

                <BankSlipDueDate>
                  Vencimento: {format(parseISO(dueDate), 'dd/MM')}
                </BankSlipDueDate>

                <CardInfo
                  style={{
                    color: '#000',
                    textAlign: 'center',
                  }}
                >
                  Utilize o número do código de barras abaixo para realizar o
                  pagamento do boleto referente ao seu pacote de aulas:
                </CardInfo>

                <CardInfo
                  style={{
                    color: '#000',
                    textAlign: 'center',
                  }}
                >
                  {barcode}
                </CardInfo>

                <Button
                  onPress={() => {
                    Clipboard.setString(barcode);
                    setClip(true);

                    setTimeout(() => {
                      setClip(false);
                    }, 3000);
                  }}
                >
                  Copiar código
                </Button>

                {clip && (
                  <CardInfo
                    style={{
                      color: '#000',
                      textAlign: 'center',
                    }}
                  >
                    Código copiado.
                  </CardInfo>
                )}
              </BankSlipContainer>
            )}
          </Content>
        </ContainerScroll>
      </Container>

      {loading && (
        <View
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            backgroundColor: '#00000080',
            position: 'absolute',
          }}
        >
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )}

      <View>
        <WebView
          source={{
            uri: `https://personalaccess-api-test.herokuapp.com/api/pagseguro`,
          }}
          injectedJavaScript={getSenderHash}
          javaScriptEnabled
          onMessage={async (event) => {
            const response = JSON.parse(event.nativeEvent.data);

            console.log(response);

            if (response.message === 'senderHash') {
              await setHash(response.data);
              console.log(`SenderHash: ${response.data}`);
            }
          }}
        />
      </View>
    </>
  );
};

export default BankSlip;
