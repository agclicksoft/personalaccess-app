import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Content = styled.View`
  flex: 1;
  align-items: center;
  padding: 20px 20px 60px;
`;

export const Card = styled.View``;

export const Touchable = styled.TouchableOpacity``;

export const Label = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  margin: 12px 0 0 16px;
`;

export const WeightHeightWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

export const WeightHolder = styled.View`
  flex: 1;
  margin-right: 20px;
`;

export const HeightHolder = styled.View`
  flex: 1;
`;

export const BrandImage = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 25px;
`;

export const CardInfo = styled.Text`
  color: #fff;
  margin: 5px 0;
`;

export const Bold = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaBold};
  font-size: 16px;
  color: #fff;
`;

export const CardNameHolder = styled.View`
  color: #fff;
  width: 100%;
  flex-direction: row;
  align-items: center;
`;

export const CVVHolder = styled.View`
  background-color: #000;
  height: 30px;
  width: 100%;
`;

export const CVVWrapper = styled.View`
  background-color: #fff;
  padding: 5px;
  margin-right: 30px;
  height: 30px;
  width: 80px;
  align-self: flex-end;
`;

export const ButtonWrapper = styled.View`
  flex-direction: row;
  margin-top: 30px;
`;

export const BankSlipContainer = styled.View`
  margin-top: 30px;
  align-items: center;
  padding: 0 20px;
`;

export const BankSlipValue = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSizes.size22};
  font-family: ${({ theme }) => theme.fontFamily.sofiaBold};
`;

export const BankSlipDueDate = styled.Text`
  color: ${({ theme }) => theme.colors.gray};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaSemiBold};
  text-transform: uppercase;
`;
