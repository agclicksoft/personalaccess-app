import React from 'react';
import { Image } from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';

import * as PersonalViews from '~/screens/Personal';
import personalIconsTabBar from '~/utils/personalIconsTabBar';

const Tab = createBottomTabNavigator();
const App = createStackNavigator();

const TabBar: React.FC = () => {
  return (
    <Tab.Navigator
      initialRouteName="Profile"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          const { active, inactive } = personalIconsTabBar[route.name];
          return (
            <Image
              source={focused ? active : inactive}
              style={{ resizeMode: 'contain' }}
            />
          );
        },
      })}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        labelPosition: 'below-icon',
        style: {
          elevation: 5,
          shadowRadius: 5,
          shadowOffset: { width: 0, height: 0 },
          shadowOpacity: 0.1,
          shadowColor: `${({ theme }) => theme.colors.shadow}`,
          paddingHorizontal: 30,
        },
        activeTintColor: '#37153D',
        inactiveTintColor: '#37153D',
        labelStyle: {
          fontFamily: 'SofiaPro-Regular',
          fontSize: 11,
        },
      }}
    >
      <Tab.Screen
        name="Statistics"
        component={PersonalViews.Statistics}
        options={{ tabBarLabel: 'Estatísticas' }}
      />
      <Tab.Screen
        name="Class"
        component={PersonalViews.Class}
        options={{ tabBarLabel: 'Solicitações' }}
      />
      <Tab.Screen
        name="Profile"
        component={PersonalViews.Profile}
        options={{ tabBarLabel: 'Meu perfil' }}
      />
    </Tab.Navigator>
  );
};

const PersonalRoutes: React.FC = () => {
  return (
    <App.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <App.Screen name="Home" component={TabBar} />
      <App.Screen name="EditProfile" component={PersonalViews.EditProfile} />
      <App.Screen name="PublicView" component={PersonalViews.PublicView} />
      <App.Screen name="Schedule" component={PersonalViews.Schedule} />
      <App.Screen
        name="StandartSchedule"
        component={PersonalViews.StandartSchedule}
      />
      <App.Screen name="MyAgenda" component={PersonalViews.MyAgenda} />
      <App.Screen name="BlockAgenda" component={PersonalViews.BlockAgenda} />
      <App.Screen
        name="ServiceLocations"
        component={PersonalViews.ServiceLocations}
      />
      <App.Screen name="NewGym" component={PersonalViews.NewGym} />
      <App.Screen name="EditGym" component={PersonalViews.EditGym} />
      <App.Screen name="Specialties" component={PersonalViews.Specialties} />
      <App.Screen name="Payments" component={PersonalViews.Payments} />
      <App.Screen name="Avaliations" component={PersonalViews.Avaliations} />
      <App.Screen
        name="ServicePackages"
        component={PersonalViews.ServicePackages}
      />
      <App.Screen
        name="ServicePackagesEditPrice"
        component={PersonalViews.ServicePackagesEditPrice}
      />
      <App.Screen
        name="ServicePackagesNew"
        component={PersonalViews.ServicePackagesNew}
      />
      <App.Screen name="BankAccount" component={PersonalViews.BankAccount} />
      <App.Screen name="TermsOfUse" component={PersonalViews.TermsOfUse} />
      <App.Screen name="StudentView" component={PersonalViews.StudentView} />
      <App.Screen name="FinishClass" component={PersonalViews.FinishClass} />
      <App.Screen
        name="ClassDescription"
        component={PersonalViews.ClassDescription}
      />
      <App.Screen
        name="ClassDescriptionView"
        component={PersonalViews.ClassDescriptionView}
      />
      <App.Screen name="AveragePrice" component={PersonalViews.AveragePrice} />
      <App.Screen
        name="AveragePriceView"
        component={PersonalViews.AveragePriceView}
      />
      <App.Screen name="MostCharged" component={PersonalViews.MostCharged} />
      <App.Screen
        name="MostChargedView"
        component={PersonalViews.MostChargedView}
      />
      <App.Screen
        name="AverageSchedules"
        component={PersonalViews.AverageSchedules}
      />
      <App.Screen
        name="AverageSchedulesView"
        component={PersonalViews.AverageSchedulesView}
      />
      <App.Screen
        name="NumberRequests"
        component={PersonalViews.NumberRequests}
      />
      <App.Screen
        name="NumberRequestsAndRejectsView"
        component={PersonalViews.NumberRequestsAndRejectsView}
      />
      <App.Screen
        name="NumberRejects"
        component={PersonalViews.NumberRejects}
      />
    </App.Navigator>
  );
};

export default PersonalRoutes;
