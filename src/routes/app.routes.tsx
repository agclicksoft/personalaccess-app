import React from 'react';

import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';

import { useAuth } from '~/hooks/auth';

import PersonalBottomBars from './personal.routes';
import StudentRoutes from './student.routes';

const App = createStackNavigator();

const AppRoutes: React.FC = () => {
  const { user } = useAuth();

  return user.role === 'student' ? (
    <App.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.ModalSlideFromBottomIOS,
      }}
    >
      <App.Screen name="Home" component={StudentRoutes} />
    </App.Navigator>
  ) : (
    <App.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.ModalSlideFromBottomIOS,
      }}
    >
      <App.Screen name="Home" component={PersonalBottomBars} />
    </App.Navigator>
  );
};

export default AppRoutes;
