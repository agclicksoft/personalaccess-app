import React from 'react';

import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';

import * as Views from '~/screens';

const AuthStack = createStackNavigator();

const AuthRoutes: React.FC = () => (
  <AuthStack.Navigator
    screenOptions={{
      headerShown: false,
      ...TransitionPresets.ModalSlideFromBottomIOS,
    }}
  >
    <AuthStack.Screen name="SignIn" component={Views.SignIn} />
    <AuthStack.Screen name="SignUp" component={Views.SignUp} />
    <AuthStack.Screen name="ResetPassword" component={Views.ResetPassword} />
  </AuthStack.Navigator>
);

export default AuthRoutes;
