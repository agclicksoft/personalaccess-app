/* eslint-disable global-require */
import React from 'react';

import { OpenSans_400Regular } from '@expo-google-fonts/open-sans';
import { AppLoading } from 'expo';
import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';

import { useAuth } from '~/hooks/auth';

import AppRoutes from './app.routes';
import AuthRoutes from './auth.routes';

const Routes: React.FC = () => {
  const { signed, loading } = useAuth();

  const [fontsLoaded] = useFonts({
    SofiaProLight: require('~/assets/fonts/SofiaPro-Regular.ttf'),
    'SofiaPro-Regular': require('~/assets/fonts/SofiaPro-Regular.ttf'),
    'SofiaPro-Medium': require('~/assets/fonts/SofiaPro-Medium.ttf'),
    'SofiaPro-Bold': require('~/assets/fonts/SofiaPro-Bold.ttf'),
    'SofiaPro-SemiBold': require('~/assets/fonts/sofia_pro_semi_bold.ttf'),
    OpenSans_400Regular,
  });

  if (loading || !fontsLoaded) {
    return <AppLoading />;
  }

  SplashScreen.hideAsync();
  return signed ? <AppRoutes /> : <AuthRoutes />;
};

export default Routes;
