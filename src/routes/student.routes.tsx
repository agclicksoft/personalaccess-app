import React from 'react';
import { Image } from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';

import * as StudentViews from '~/screens/Student';
import listIconsTabBar from '~/utils/listIconsTabBar';

const Tab = createBottomTabNavigator();
const App = createStackNavigator();

const HomeStack: React.FC = () => {
  return (
    <App.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <App.Screen name="Home" component={StudentViews.Home} />
    </App.Navigator>
  );
};

const ProfileStack: React.FC = () => {
  return (
    <App.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <App.Screen name="Profile" component={StudentViews.Profile} />
    </App.Navigator>
  );
};

const StudentBottomBars: React.FC = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          const { active, inactive } = listIconsTabBar[route.name];
          return (
            <Image
              source={focused ? active : inactive}
              style={{ resizeMode: 'contain' }}
            />
          );
        },
      })}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        labelPosition: 'below-icon',
        tabStyle: {
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
        },
        style: {
          elevation: 5,
          shadowRadius: 5,
          shadowOffset: { width: 0, height: 0 },
          shadowOpacity: 0.1,
          shadowColor: `${({ theme }) => theme.colors.shadow}`,
          paddingHorizontal: 30,
        },
        activeTintColor: '#37153D',
        inactiveTintColor: '#37153D',
        labelStyle: {
          fontFamily: 'SofiaPro-Regular',
          fontSize: 11,
        },
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStack}
        options={{ tabBarLabel: 'Início' }}
      />
      <Tab.Screen
        name="Class"
        component={StudentViews.StudentClasses}
        options={{ tabBarLabel: 'Minhas aulas' }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStack}
        options={{ tabBarLabel: 'Meu perfil' }}
      />
    </Tab.Navigator>
  );
};

const StudentRoutes: React.FC = () => {
  return (
    <App.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      <App.Screen name="Home" component={StudentBottomBars} />
      <App.Screen name="Filter" component={StudentViews.Filter} />
      <App.Screen name="Personal" component={StudentViews.Personal} />
      <App.Screen name="Plans" component={StudentViews.Plans} />
      <App.Screen name="Schedule" component={StudentViews.Schedule} />
      <App.Screen name="Reschedule" component={StudentViews.Reschedule} />
      <App.Screen
        name="RescheduleCanceled"
        component={StudentViews.RescheduleCanceled}
      />
      <App.Screen name="Gyms" component={StudentViews.Gyms} />
      <App.Screen name="EditProfile" component={StudentViews.EditProfile} />
      <App.Screen name="PublicView" component={StudentViews.PublicView} />
      <App.Screen name="ParQ" component={StudentViews.ParQ} />
      <App.Screen name="TermsOfUse" component={StudentViews.TermsOfUse} />
      <App.Screen name="Payments" component={StudentViews.Payments} />
      <App.Screen name="SelectPayment" component={StudentViews.SelectPayment} />
      <App.Screen name="CreditCard" component={StudentViews.CreditCard} />
      <App.Screen name="PayInHands" component={StudentViews.PayInHands} />
      <App.Screen name="BankSlip" component={StudentViews.BankSlip} />
      <App.Screen name="PersonalView" component={StudentViews.PersonalView} />
      <App.Screen
        name="ClassDescriptionView"
        component={StudentViews.ClassDescriptionView}
      />
    </App.Navigator>
  );
};

export default StudentRoutes;
