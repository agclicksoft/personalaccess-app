export default {
  colors: {
    background: '#ffffff',
    gray: '#D8D8D8',
    blackTransparent: '#00000029',
    pink: '#E4007D',
    red: '#E20613',
    red2: '#E2051D',
    black: '#000000',
    yellow: '#FFEC00',
    online: '#24CE57',
  },
  fontFamily: {
    sofiaRegular: 'Poppins_400Regular',
    sofiaMedium: 'Poppins_500Medium',
    sofiaSemiBold: 'Poppins_600SemiBold',
    sofiaBold: 'Poppins_700Bold',
    poppinsExtraBold: 'Poppins_800ExtraBold',
    openSans: 'OpenSans_400Regular',
  },
  fontSizes: {
    _13: '13px',
    _16: '16px',
    _18: '18px',
    _20: '20px',
    _21: '21px',
    _23: '23px',
    _28: '28px',
  },
  spacing: {
    default: '16px',
  },
  transition: {
    default: '180ms easeInOut',
  },
} as const;
