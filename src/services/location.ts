import { Platform } from 'react-native';

import * as Location from 'expo-location';

import { convertStates } from '~/utils/convertStates';

interface Response {
  query?: string;
  address?: {
    state?: string;
    city?: string;
    neighborhood?: string;
    street?: string;
    country?: string;
    name?: string;
  };
}

export async function getLocation(): Promise<Response> {
  const { status } = await Location.requestPermissionsAsync();
  let query = null;
  let fullAdress = null;

  if (status !== 'granted') {
    return null;
  }

  const locationQuery = await Location.getCurrentPositionAsync({
    accuracy: Location.Accuracy.High,
  });

  if (locationQuery !== null) {
    const pos = {
      lat: locationQuery.coords.latitude,
      lng: locationQuery.coords.longitude,
    };

    const address = await Location.reverseGeocodeAsync({
      latitude: pos.lat,
      longitude: pos.lng,
    });

    console.log(address[0]);

    fullAdress = {
      state:
        Platform.OS === 'ios'
          ? convertStates(address[0].region)
          : address[0].region,
      city: Platform.OS === 'android' ? address[0].subregion : address[0].city,
      neighborhood: address[0].district,
      street: address[0].street,
      country: address[0].country,
      name:
        Platform.OS === 'android'
          ? `${address[0].street} - ${address[0].name}`
          : address[0].name,
    };

    query = `state=${fullAdress.state}&district=${fullAdress.city}&neighborhood=${fullAdress.neighborhood}`;
  }

  return { query, address: fullAdress };
}
