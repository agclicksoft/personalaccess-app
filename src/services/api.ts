import axios from 'axios';

const api = axios.create({
  baseURL: 'https://api.personalaccess.app/api',
});

export default api;
