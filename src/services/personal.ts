import { PersonalClass, Personal, ProfessionalPlan } from '~/models';

import api from './api';

export async function getAllPersonals(
  query: string,
  pageNumber: number,
  personals?: Personal[],
): Promise<Personal[]> {
  console.log(query);

  let personalQuery = [];

  const response = await api.get(`/professionals?page=${pageNumber}&${query}`);

  if (response) {
    personalQuery =
      pageNumber === 1
        ? response.data.data
        : [...personals, ...response.data.data];
  }

  return personalQuery;
}

export async function getShowPersonalPlans(
  personalId: number,
): Promise<ProfessionalPlan[]> {
  const response = await api.get(`/professionals/${personalId}`);

  return response.data.professionalPlans;
}

export async function deletePersonalPlan(planId: number): Promise<void> {
  await api.delete(`/professionals/plans/${planId}`);
}

export async function deletePersonalGym(
  personalId: number,
  gymId: number,
): Promise<void> {
  await api.delete(`/professionals/${personalId}/gyms/${gymId}`);
}

export async function getPersonalClasses(
  id: number,
  pageNumber: number,
  classes?: PersonalClass[],
): Promise<Personal[]> {
  let classesQuery: PersonalClass[] = [];

  const response = await api.get(
    `/professionals/${id}/contracts?page=${pageNumber}`,
  );

  if (response) {
    classesQuery =
      pageNumber === 1
        ? response.data.data
        : [...classes, ...response.data.data];
  }

  return classesQuery;
}
