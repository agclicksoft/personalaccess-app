import { StudentClass } from '~/models';

import api from './api';

export async function getStudentClasses(
  id: number,
  pageNumber: number,
  classes?: StudentClass[],
): Promise<StudentClass[]> {
  let classesQuery: StudentClass[] = [];

  const response = await api.get(
    `/students/${id}/contracts?page=${pageNumber}`,
  );

  if (response) {
    classesQuery =
      pageNumber === 1
        ? response.data.data
        : [...classes, ...response.data.data];
  }

  console.log(response.data.data);

  return classesQuery;
}
