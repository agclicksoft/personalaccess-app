import { AxiosResponse } from 'axios';

import { Personal, Student, User } from '~/models';

import api from './api';

export async function signIn(email: string, password: string): Promise<User> {
  const response = await api.post('/users/authenticate', {
    email,
    password,
  });
  let activeUser: User = null;
  let student: Student = null;
  let personal: Personal = null;

  api.defaults.headers.Authorization = `Bearer ${response.data.token}`;

  api.defaults.headers.Authorization = `Bearer ${response.data.token}`;

  try {
    if (response.data.student?.id) {
      const responseStudent = await api.get(
        `/students/${response.data.student.id}`,
      );

      student = responseStudent.data;
    }

    if (response.data.professional?.id) {
      const responsePersonal = await api.get(
        `/professionals/${response.data.professional.id}`,
      );

      personal = responsePersonal.data;
    }

    if (personal !== null) {
      activeUser = {
        ...response.data,
        professional: personal,
      };
    } else {
      activeUser = {
        ...response.data,
        student,
      };
    }
  } catch (err) {
    console.log(err);
  }

  return activeUser;
}

export async function signUp(
  name: string,
  email: string,
  contact: string,
  password: string,
  role: string,
  cref?: string,
  cpf?: string,
  isPersonal?: boolean,
): Promise<User> {
  let response: AxiosResponse = null;

  if (isPersonal) {
    response = await api.post('/users/sign-up', {
      user: {
        name,
        username: email,
        email,
        password,
        contact,
        role: 'professional',
        cpf,
      },
      professional: {
        doc_professional: cref,
        doc_identity: 'document',
      },
    });
  } else {
    response = await api.post('/users/sign-up', {
      user: {
        name,
        username: email,
        email,
        password,
        contact,
        role: 'student',
      },
    });
  }

  api.defaults.headers.Authorization = `Bearer ${response.data.token}`;

  let activeUser: User = null;
  let student: Student = null;
  let personal: Personal = null;

  try {
    if (response.data?.student?.id) {
      const responseStudent = await api.get(
        `/students/${response.data.student?.id}`,
      );

      student = responseStudent.data;
      console.log(student);
    }

    if (response.data?.professional?.id) {
      const responsePersonal = await api.get(
        `/professionals/${response.data.professional?.id}`,
      );

      personal = responsePersonal.data;
      console.log(personal);
    }

    activeUser = {
      ...response.data,
      student: student ?? null,
      professional: personal ?? null,
    };
  } catch (err) {
    console.log(err);
  }

  return activeUser;
}

export async function updateStudentProfile(user: User): Promise<Student> {
  const response = await api.put(`/students/${user.student.id}`, {
    id: user.student.id,
    user: {
      id: user.id,
      name: user.name,
      cpf: user.cpf,
      birthday: user.birthday,
      email: user.email,
      genre: user.genre,
      contact: user.contact,
      emergency_phone: user.emergency_phone,
      address_zipcode: user.address_zipcode,
      address_uf: user.address_uf,
      address_city: user.address_city,
      address_neighborhood: user.address_neighborhood,
      address_street: user.address_street,
      address_number: user.address_number,
      address_complement: user.address_complement,
      is_redefinition: user.is_redefinition,
      expo_data: user.expo_data,
    },
  });

  const responseStudent = await api.get(`/students/${response.data.id}`);

  const activeUser: User = {
    ...response.data.user,
    student: responseStudent.data ?? null,
  };

  return activeUser;
}

export async function updatePersonalProfile(user: User): Promise<Personal> {
  const response = await api.put(`/professionals/${user.professional.id}`, {
    id: user.professional.id,
    doc_professional: user.professional.doc_professional,
    bibliography: user.professional.bibliography,
    instagram: user.professional.instagram,
    user: {
      id: user.id,
      name: user.name,
      cpf: user.cpf,
      birthday: user.birthday,
      email: user.email,
      contact: user.contact,
      emergency_phone: user.emergency_phone,
      is_redefinition: user.is_redefinition,
      genre: user.genre,
      expo_data: user.expo_data,
    },
  });

  const responsePersonal = await api.get(`/professionals/${response.data.id}`);

  const activeUser: User = {
    ...response.data.user,
    professional: responsePersonal.data ?? null,
  };

  return activeUser;
}
