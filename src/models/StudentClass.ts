import { ProfessionalPlan } from '.';
import Personal from './Personal';
import ScheduledService from './ScheduledServices';
import Student from './Student';

interface StudentClass {
  id?: number;
  professional_id?: number;
  professionals_plan_id?: number;
  student_id?: number;
  amount?: number;
  amount_used?: number;
  price?: number;
  status?: string;
  created_at?: string;
  updated_at?: string;
  status_payment?: string;
  method_payment?: string;
  ipte?: string;
  payment_due_date?: string;
  student?: Student;
  professional?: Personal;
  professionalPlan?: ProfessionalPlan;
  scheduledServices?: ScheduledService[];
}
export default StudentClass;
