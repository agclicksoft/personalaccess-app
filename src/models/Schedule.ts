interface Schedule {
  id?: number;
  professional_id?: number;
  week_day?: string;
  period_of?: string;
  period_until?: string;
  interval?: string;
  created_at?: string;
  updated_at?: string;
}

export default Schedule;
