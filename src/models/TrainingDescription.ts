interface TrainingDescription {
  id?: number;
  groupings?: string;
  modality?: string;
  note?: string;
  created_at?: string;
  updated_at?: string;
}

export default TrainingDescription;
