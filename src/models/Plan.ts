interface Plan {
  id?: number;
  description?: string;
  amount?: number;
  price?: number;
  duo?: boolean;
  order?: string;
}
export default Plan;
