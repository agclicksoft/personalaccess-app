import User from './User';

interface Student {
  id?: number;
  user_id?: number;
  created_at?: string;
  updated_at?: string;
  user?: User;
}
export default Student;
