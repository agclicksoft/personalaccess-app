interface Specialty {
  id?: number;
  professional_id?: number;
  description?: string;
  created_at?: string;
  updated_at?: string;
  checked?: boolean;
}
export default Specialty;
