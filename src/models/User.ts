import ParQQuestion from './ParQQuestion';
import Personal from './Personal';
import Student from './Student';

interface User {
  id?: number;
  user_id?: number;
  expo_data?: string;
  token?: string;
  name?: string;
  username?: string;
  birthday?: string;
  email?: string;
  genre?: string;
  role?: string;
  cpf?: string;
  password?: string;
  img_profile?: string;
  contact?: string;
  lat?: string;
  lng?: string;
  emergency_phone?: string;
  address_country?: string;
  address_uf?: string;
  address_city?: string;
  address_neighborhood?: string;
  address_street?: string;
  address_zipcode?: string;
  address_complement?: string;
  address_number?: string;
  is_redefinition?: boolean;
  created_at?: string;
  updated_at?: string;
  professional?: Personal;
  student?: Student;
  faqAnswered?: ParQQuestion[];
}

export default User;
