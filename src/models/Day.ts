import Hour from './Hour';

interface Day {
  key: string;
  name: string;
  isSelected: boolean;
  hours: Hour[];
}
export default Day;
