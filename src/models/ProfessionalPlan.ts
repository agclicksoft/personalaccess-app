import Plan from './Plan';

interface ProfessionalPlan {
  id?: number;
  professional_id?: number;
  plan_id?: number;
  created_at?: string;
  updated_at?: string;
  price?: any;
  is_deleted?: boolean;
  plan: Plan;
}
export default ProfessionalPlan;
