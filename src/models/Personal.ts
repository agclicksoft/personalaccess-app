import Gym from './Gym';
import ProfessionalPlan from './ProfessionalPlan';
import Schedule from './Schedule';
import Specialty from './Specialty';
import User from './User';

interface Personal {
  id?: number;
  user_id?: number;
  doc_identity?: string;
  contact?: string;
  bank_number?: string;
  bank_agency?: string;
  bibliography?: string;
  doc_professional?: string;
  created_at?: string;
  updated_at?: string;
  bank_type?: string;
  bank_name?: string;
  instagram?: string;
  specialties?: Specialty[];
  schedules?: Schedule[];
  gyms?: Gym[];
  user?: User;
  professionalPlans?: ProfessionalPlan[];
  assessments?: [
    {
      assessments?: number;
      professional_id?: number;
      count?: number;
    },
  ];
  rating: [
    {
      assessments?: number;
      professional_id?: number;
      count?: number;
    },
  ];
}
export default Personal;
