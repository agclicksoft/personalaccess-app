import ProfessionalPlan from './ProfessionalPlan';

interface Contract {
  id?: number;
  professional_id?: number;
  professionals_plan_id?: number;
  student_id?: number;
  amount?: number;
  amount_used?: number;
  price?: number;
  status?: string;
  created_at?: string;
  updated_at?: string;
  status_payment?: string;
  method_payment?: string;
  ipte?: string;
  payment_due_date?: string;
  professionalPlan?: ProfessionalPlan;
}
export default Contract;
