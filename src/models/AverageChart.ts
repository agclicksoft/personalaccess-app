interface AverageChart {
  id?: number;
  avg?: number;
  min?: number;
  max?: number;
  avg_professional?: number;
  professional_id?: number;
  plan_id?: number;
  created_at?: string;
  updated_at?: string;
  uf: string;
  city: string;
  price?: number;
  is_deleted?: boolean;
  state?: string;
  district?: string;
  neighborhood?: string;
  state_id?: number;
  district_id?: number;
  establishment_id?: number;
}
export default AverageChart;
