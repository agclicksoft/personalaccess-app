import ProfessionalPlan from './ProfessionalPlan';
import ScheduledService from './ScheduledServices';
import Student from './Student';

interface PersonalClass {
  id?: number;
  professional_id?: number;
  professionals_plan_id?: number;
  student_id?: number;
  amount?: number;
  amount_used?: number;
  price?: number;
  status?: string;
  created_at?: string;
  updated_at?: string;
  status_payment?: string;
  method_payment?: string;
  ipte?: string;
  payment_due_date?: string;
  student?: Student;
  professionalPlan?: ProfessionalPlan;
  scheduledServices?: ScheduledService[];
}
export default PersonalClass;
