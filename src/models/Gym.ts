interface Gym {
  id?: number;
  name?: string;
  cnpj?: string;
  address_city?: string;
  address_complement?: string;
  address_neighborhood?: string;
  address_number?: string;
  address_state?: string;
  address_street?: string;
  address_uf?: string;
  address_zipcode?: string;
  created_at?: string;
  updated_at?: string;
  pivot: {
    id?: number;
    establishment_id?: number;
    professional_id?: number;
  };
}

export default Gym;
