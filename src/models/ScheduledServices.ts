import Establishment from './Establishment';
import Rating from './Rating';
import TrainingDescription from './TrainingDescription';

interface ScheduledService {
  id?: number;
  professional_id?: number;
  establishment_id?: number;
  status?: string;
  date?: string;
  total?: string;
  comments?: string;
  elapsed_time?: string;
  created_at?: string;
  updated_at?: string;
  description?: string;
  contract_id?: number;
  training_description_id?: string;
  tk_auth_service?: number;
  student_is_present?: boolean;
  status_payment?: string;
  establishment?: Establishment;
  trainingDescription?: TrainingDescription;
  assessments?: Rating;
}

export default ScheduledService;
