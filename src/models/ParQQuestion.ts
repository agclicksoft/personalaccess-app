interface ParQQuestion {
  id?: number;
  faq_answer_id?: number;
  faq_question_id?: number;
  created_at?: string;
  updated_at?: string;
  user_id?: number;
  answered?: string;
  attachments?: [
    {
      id?: number;
      user_id?: number;
      name?: string;
      path?: string;
      created_at?: string;
      updated_at?: string;
      key?: string;
      faq_answered_id?: number;
    },
  ];
  faqQuestion?: {
    id?: number;
    question?: string;
    created_at?: string;
    updated_at?: string;
    faqAnswers?: [];
  };
}

export default ParQQuestion;
