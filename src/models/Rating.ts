import Student from './Student';

interface Rating {
  id?: number;
  student_id?: number;
  professional_id?: number;
  assessments_value?: number;
  created_at?: string;
  updated_at?: string;
  comments?: string;
  anonimo?: boolean;
  scheduled_service_id?: number;
  student?: Student;
}

export default Rating;
