import Day from './Day';

interface Week {
  week?: Day[];
}
export default Week;
