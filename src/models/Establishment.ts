interface Establishment {
  id?: number;
  name?: string;
  cnpj?: string;
  address_zipcode?: string;
  address_street?: string;
  address_number?: string;
  address_complement?: string;
  address_state?: string;
  address_city?: string;
  address_uf?: string;
  address_neighborhood?: string;
  created_at?: string;
  updated_at?: string;
}
export default Establishment;
