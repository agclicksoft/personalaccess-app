import React from 'react';

import RNPickerSelect, { PickerSelectProps } from 'react-native-picker-select';

import { style, customStyle } from './styles';

interface Values extends PickerSelectProps {
  label?: string;
  selected?: boolean;
}

const Picker: React.FC<Values> = ({ label, selected, ...rest }) => {
  return (
    <RNPickerSelect
      style={selected ? customStyle : style}
      placeholder={{
        label: label ?? 'Selecionar',
      }}
      doneText="Selecionar"
      useNativeAndroidPickerStyle={false}
      pickerProps={{ mode: 'dropdown' }}
      {...rest}
    />
  );
};

export default Picker;
