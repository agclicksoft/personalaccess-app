import styled, { css } from 'styled-components/native';

interface IContainerProps {
  isFocused: boolean;
  isErrored: boolean;
}

export const Container = styled.View<IContainerProps>`
  width: 330px;
  height: 40px;
  margin: 15px 0 0;
  flex-direction: row;
  align-items: center;
  border-width: 0.3px;
  border-color: #fff;
  border-radius: 20px;
  background-color: #fff;
  ${(props) =>
    props.isFocused &&
    css`
      border-color: #29c872;
    `}
  ${(props) =>
    props.isErrored &&
    css`
      border-color: #f55252;
    `}
`;

export const TextInput = styled.TextInput`
  flex: 1;
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSizes.size14};
  padding: 0 16px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
  text-align: left;
`;

export const ChangePasswordText = styled.Text`
  color: ${({ theme }) => theme.colors.purple};
  font-size: ${({ theme }) => theme.fontSizes.size12};
  margin-right: 12px;
`;
