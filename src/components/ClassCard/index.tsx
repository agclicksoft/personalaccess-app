import React, { useMemo } from 'react';

import { useNavigation } from '@react-navigation/native';
import { format, addHours, parseISO } from 'date-fns';
import { pt } from 'date-fns/locale';

import { StudentClass } from '~/models';
import { PlaceholderImage } from '~/utils/consts';
import { getPaymentStatus, getStudentStatusBG } from '~/utils/geStatus';

import {
  Card,
  CardContent,
  Image,
  Wrapper,
  Holder,
  Question,
  Answer,
  Divider,
  StatusButton,
  StatusButtonText,
} from './styles';

interface Props {
  data?: StudentClass;
}

const ClassCard: React.FC<Props> = ({ data }) => {
  const myClass: StudentClass = data;
  const { navigate } = useNavigation();

  const formattedDate = format(
    addHours(parseISO(myClass.created_at), -3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Personal: ', answer: myClass.professional.user.name },
    {
      id: 2,
      question: 'Pacote: ',
      answer: myClass.professionalPlan.plan.description,
    },
    {
      id: 3,
      question: 'Pagamento: ',
      answer: getPaymentStatus(myClass.method_payment),
    },
    {
      id: 4,
      question: 'Adesão em: ',
      answer: formattedDate,
    },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <Holder key={item.id}>
          <Question>{item.question}</Question>
          <Answer>{item.answer}</Answer>
        </Holder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return myClass.amount === 1 ? (
    <Card
      onPress={() =>
        navigate('PersonalView', {
          myClass,
        })
      }
    >
      <CardContent>
        <Image
          source={{
            uri: myClass.professional.user.img_profile ?? PlaceholderImage,
          }}
        />
        <Wrapper>{optionsMemo}</Wrapper>
      </CardContent>

      <Divider />

      <StatusButton
        style={{
          backgroundColor: getStudentStatusBG(myClass.status),
        }}
      >
        <StatusButtonText
          style={{
            color: '#37153D',
          }}
        >
          Visualizar aula
        </StatusButtonText>
      </StatusButton>
    </Card>
  ) : (
    <Card
      onPress={() =>
        navigate('PersonalView', {
          myClass,
        })
      }
    >
      <CardContent>
        <Image
          source={{
            uri: myClass.professional.user.img_profile ?? PlaceholderImage,
          }}
        />
        <Wrapper>{optionsMemo}</Wrapper>
      </CardContent>

      <Divider />

      <StatusButton
        style={{
          backgroundColor: getStudentStatusBG(myClass.status),
        }}
      >
        <StatusButtonText
          style={{
            color: '#37153D',
          }}
        >
          Visualizar aulas
        </StatusButtonText>
      </StatusButton>
    </Card>
  );
};

export default ClassCard;
