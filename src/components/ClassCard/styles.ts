import { Dimensions } from 'react-native';

import styled from 'styled-components/native';

const { width } = Dimensions.get('window');

export const Card = styled.TouchableOpacity.attrs({
  activeOpacity: 0.5,
})`
  background-color: ${({ theme }) => theme.colors.white};
  border-radius: 8px;
  width: 100%;
  padding: 25px 10px;
  margin: 10px 0;
  border-width: 0.8px;
  border-color: ${({ theme }) => theme.colors.shadow};
`;

export const CardContent = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  width: 100%;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'cover',
})`
  height: ${width * 0.3}px;
  width: ${width * 0.3}px;
  border-radius: 60px;
  margin: 0 10px;
`;

export const Wrapper = styled.View`
  flex: 1;
  flex-shrink: 1;
  padding: 15px 0;
`;

export const Holder = styled.View`
  flex: 1;
  width: 100%;
  flex-direction: row;
`;

export const Question = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaRegular};
`;

export const Answer = styled.Text`
  color: ${({ theme }) => theme.colors.blackLight};
  font-size: ${({ theme }) => theme.fontSizes.size13};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  text-transform: capitalize;
`;

export const Divider = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.shadow};
  width: 100%;
  margin: 15px 0 0;
`;

export const StatusButton = styled.View`
  width: 70%;
  height: 35px;
  border-radius: 30px;
  margin: 25px 0 0;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

export const StatusButtonText = styled.Text`
  color: ${({ theme }) => theme.colors.blackLight};
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
`;
