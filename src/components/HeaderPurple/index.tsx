/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import { Container, GenericButton, Wrapper, Title } from './styles';

import Button from '../Button';

interface Props {
  children: React.ReactNode;
  onPress?: any;
  rightButton?: boolean;
  rightButtonTitle?: string;
}

const HeaderPurple: React.FC<Props> = ({
  children,
  onPress,
  rightButton,
  rightButtonTitle,
}) => {
  const { goBack } = useNavigation();

  return (
    <Container>
      <GenericButton onPress={() => goBack()}>
        <Wrapper>
          <Ionicons name="ios-arrow-round-back" size={24} color="#fff" />
          <Title>{children}</Title>
        </Wrapper>
      </GenericButton>

      {rightButton ? (
        <Button height={30} width={100} onPress={onPress}>
          {rightButtonTitle}
        </Button>
      ) : null}
    </Container>
  );
};

export default HeaderPurple;
