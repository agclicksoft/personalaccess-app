import styled from 'styled-components/native';

export const Container = styled.View`
  background: #ffffff;
  border-radius: 15px;
  overflow: hidden;
`;
