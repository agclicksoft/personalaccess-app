import React from 'react';

import Modal from 'react-native-modal';

import { Container } from './styles';

import ContainerScrollModal from '../ContainerScrollModal';

interface ModalComponentProps {
  isVisible: boolean;
  closeModal?: () => void;
}

const ModalComponent: React.FC<ModalComponentProps> = ({
  isVisible,
  closeModal,
  children,
}) => {
  return (
    <Modal
      backdropOpacity={0.6}
      isVisible={isVisible}
      animationIn="fadeIn"
      animationOut="fadeOut"
      onBackdropPress={closeModal}
      onBackButtonPress={closeModal}
      useNativeDriver
      hideModalContentWhileAnimating
      propagateSwipe
      style={{ flex: 1 }}
      coverScreen={false}
    >
      <Container>
        <ContainerScrollModal>{children}</ContainerScrollModal>
      </Container>
    </Modal>
  );
};

export default ModalComponent;
