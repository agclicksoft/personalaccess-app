/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { useNavigation } from '@react-navigation/native';

import { IconAvaliation } from '~/assets/icons';
import { Personal } from '~/models';
import { PlaceholderImage } from '~/utils/consts';

import {
  Card,
  Image,
  Wrapper,
  Name,
  Specialty,
  Biography,
  AvaliationHolder,
  AvaliationIcon,
  Avaliation,
} from './styles';

const PersonalCard: any = ({ data }) => {
  const personal: Personal = data;
  const { navigate } = useNavigation();

  return (
    <Card onPress={() => navigate('Personal', { personalData: personal })}>
      <Image
        source={{
          uri: personal.user.img_profile ?? PlaceholderImage,
        }}
      />
      <Wrapper>
        <Name>{personal.user.name}</Name>
        <Specialty>
          {personal.specialties.map((e) => e.description).join(', ')}
        </Specialty>
        <Biography>{personal.bibliography}</Biography>

        <AvaliationHolder>
          <AvaliationIcon source={IconAvaliation} />
          <Avaliation>
            {personal.assessments[0]?.assessments
              ? Number(personal?.assessments[0]?.assessments).toFixed(1)
              : 0}
          </Avaliation>
        </AvaliationHolder>
      </Wrapper>
    </Card>
  );
};

export default PersonalCard;
