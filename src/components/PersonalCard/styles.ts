import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

export const Card = styled.TouchableOpacity.attrs({
  activeOpacity: 1,
})`
  flex-direction: row;
  background-color: ${({ theme }) => theme.colors.white};
  border-radius: 8px;
  width: 100%;
  padding: 25px 10px;
  margin: 10px 0;
  border-width: 0.8px;
  border-color: ${({ theme }) => theme.colors.shadow};
`;

export const Image = styled.Image.attrs({
  resizeMode: 'cover',
})`
  height: 120px;
  width: 120px;
  border-radius: 60px;
`;

export const Wrapper = styled.View`
  flex: 1;
  width: 100%;
  margin: 0 15px;
  justify-content: space-between;
`;

export const Name = styled.Text`
  color: #37153d;
  font-size: ${({ theme }) => theme.fontSizes.size18};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  text-transform: capitalize;
`;

export const Specialty = styled.Text.attrs({
  numberOfLines: 2,
})`
  color: #2e2e2e;
  font-size: ${({ theme }) => theme.fontSizes.size12};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  margin: 5px 0;
`;

export const Biography = styled.Text`
  color: #707070;
  font-size: ${({ theme }) => theme.fontSizes.size11};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
`;

export const AvaliationHolder = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 5px;
  background-color: ${({ theme }) => theme.colors.background};
  align-self: flex-start;
  padding: 5px;
  border-radius: 6px;
`;

export const Avaliation = styled.Text`
  color: ${({ theme }) => theme.colors.orange};
  font-size: ${({ theme }) => theme.fontSizes.size11};
  font-family: ${({ theme }) => theme.fontFamily.sofiaLight};
  margin-left: 6px;
`;

export const AvaliationIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  height: 20px;
  width: 20px;
`;
