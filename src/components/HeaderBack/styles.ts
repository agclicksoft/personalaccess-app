import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 15px 0;
  padding-right: 15px;
  padding-top: ${getStatusBarHeight()}px;
  margin-bottom: 2px;
  border-bottom-width: 1px;
  border-bottom-color: ${({ theme }) => theme.colors.border};
  width: 100%;
`;

export const GenericButton = styled.TouchableWithoutFeedback``;

export const Wrapper = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 0 25px;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  font-size: ${({ theme }) => theme.fontSizes.size14};
  color: ${({ theme }) => theme.colors.purple};
  margin-left: 16px;
`;
