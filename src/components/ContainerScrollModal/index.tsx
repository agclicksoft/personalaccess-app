import React from 'react';
import { KeyboardAvoidingView, ScrollView, Platform } from 'react-native';

const ContainerScrollModal: React.FC = ({ children }) => {
  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator
        >
          {children}
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

export default ContainerScrollModal;
