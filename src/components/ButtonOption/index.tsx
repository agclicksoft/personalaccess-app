import React from 'react';

import { RectButtonProperties } from 'react-native-gesture-handler';

import { Container, RectButtonGradient, ButtonText } from './styles';

interface ButtonProps extends RectButtonProperties {
  children: string;
  selected?: boolean;
}

const ButtonOption: React.FC<ButtonProps> = ({
  children,
  selected,
  ...rest
}) => (
  <RectButtonGradient
    {...rest}
    selected={selected}
    style={{
      borderRadius: 20,
      shadowRadius: 5,
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.6,
      shadowColor: 'black',
      overflow: 'hidden',
    }}
    rippleColor="rgba(0,0,0,0.2)"
  >
    <Container>
      <ButtonText selected={selected}>{children}</ButtonText>
    </Container>
  </RectButtonGradient>
);

export default ButtonOption;
