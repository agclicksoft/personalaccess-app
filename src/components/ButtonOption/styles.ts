import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

interface GenderContainerProps {
  selected?: boolean;
}

export const RectButtonGradient = styled(RectButton)<GenderContainerProps>`
  justify-content: center;
  align-items: center;
  width: 28%;
  height: 46px;
  margin: 10px 7.2px;
  background-color: ${({ selected }) => (selected ? '#37153D' : '#999')};
`;

export const Container = styled.View`
  width: 100%;
  height: 100%;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text<GenderContainerProps>`
  color: #ffffff;
  font-size: 16px;
  font-family: ${({ theme }) => theme.fontFamily.sofiaBold};
  line-height: 25px;
`;
