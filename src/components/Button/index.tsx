import React from 'react';

import { RectButtonProperties } from 'react-native-gesture-handler';

import { Container, ButtonText, LinearContainer } from './styles';

interface ButtonProps extends RectButtonProperties {
  children: string;
  height?: number;
  width?: any;
  marginTop?: number;
}

const Button: React.FC<ButtonProps> = ({
  children,
  height,
  width,
  marginTop,
  ...rest
}) => {
  return (
    <LinearContainer
      style={{
        height: height ?? 40,
        width: width ?? 190,
        marginTop: marginTop ?? 16,
      }}
    >
      <Container {...rest}>
        <ButtonText>{children}</ButtonText>
      </Container>
    </LinearContainer>
  );
};

export default Button;
