import styled from 'styled-components/native';

export const LinearContainer = styled.View`
  background-color: #37153d;
  border-radius: 20px;
  margin-top: 16px;
  justify-content: center;
  align-items: center;
`;

export const Container = styled.TouchableOpacity`
  width: 100%;
  height: 100%;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: ${({ theme }) => theme.fontSizes.size15};
  font-family: ${({ theme }) => theme.fontFamily.sofiaMedium};
  text-align: center;
`;
