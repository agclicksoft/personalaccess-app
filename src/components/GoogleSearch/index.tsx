/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useEffect, useRef } from 'react';
import { Alert, Platform, View, Linking } from 'react-native';

import * as Location from 'expo-location';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import { PLACES_API } from '~/utils/consts';
import { convertStates } from '~/utils/convertStates';

import { style } from './styles';

interface Props {
  setField: any;
  searchGyms?: boolean;
  initialValue?: string;
}

const GoogleSearch: React.FC<Props> = ({
  setField,
  searchGyms,
  initialValue,
}) => {
  const ref = useRef(null);

  const requestAuthorization = useCallback(async () => {
    const { status } = await Location.requestPermissionsAsync();

    if (status !== 'granted') {
      Alert.alert(
        'Precisamos de autorização para buscar academias próximas a você.',
        '',
        [
          {
            text: 'Ok',
            onPress: () => {
              if (Platform.OS === 'ios') {
                Linking.openURL('app-settings:');
              } else {
                Linking.openSettings();
              }
            },
          },
        ],

        { cancelable: false },
      );

      return null;
    }

    console.log('granted');
    return 'granted';
  }, []);

  useEffect(() => {
    requestAuthorization();
    ref.current.setAddressText(initialValue ?? '');
  }, [initialValue, requestAuthorization]);

  return (
    <View>
      <GooglePlacesAutocomplete
        ref={ref}
        placeholder="Pesquisar endereço"
        fetchDetails
        enablePoweredByContainer={false}
        minLength={6}
        GooglePlacesSearchQuery={{
          rankby: 'distance',
          type: 'gym',
          key: PLACES_API,
        }}
        currentLocation={searchGyms}
        currentLocationLabel="Buscar academias próximas a você"
        onPress={async (data, details = null) => {
          let fullAdress = null;
          const { lat } = details.geometry.location;
          const { lng } = details.geometry.location;

          const address = await Location.reverseGeocodeAsync({
            latitude: lat,
            longitude: lng,
          });

          fullAdress = {
            state:
              Platform.OS === 'ios'
                ? convertStates(address[0].region)
                : address[0].region,
            city:
              Platform.OS === 'android'
                ? address[0].subregion
                : address[0].city,
            neighborhood: address[0].district,
            street: address[0].street,
            country: address[0].country,
            name: address[0].name,
            gym: data.name ?? data.structured_formatting.main_text,
            gymAddress:
              data.vicinity ?? data.structured_formatting.secondary_text,
            uf:
              Platform.OS === 'android'
                ? convertStates(address[0].region)
                : address[0].region,
          };

          console.log(fullAdress);
          await setField(fullAdress);
        }}
        query={{
          key: PLACES_API,
          language: 'pt-BR',
          components: 'country:br',
        }}
        styles={style}
      />
    </View>
  );
};

export default GoogleSearch;
