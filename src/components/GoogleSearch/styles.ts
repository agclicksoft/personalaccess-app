import { StyleSheet } from 'react-native';

export const style = StyleSheet.create({
  textInputContainer: {
    backgroundColor: 'rgba(0,0,0,0)',
    borderTopWidth: 0,
    borderBottomWidth: 0,
    maxWidth: '90%',
    minWidth: '90%',
  },
  textInput: {
    marginLeft: 0,
    marginRight: 0,
    color: '#000000',
    fontFamily: 'SofiaPro-Regular',
    fontSize: 14,
    backgroundColor: '#fff',
    height: 40,
    borderRadius: 50,
    borderWidth: 0.1,
    paddingLeft: 20,
    paddingRight: 20,
    borderColor: '#fff',
  },
  listView: {
    maxWidth: '90%',
    marginTop: 10,
    backgroundColor: '#fff',
  },
  container: {
    alignSelf: 'center',
  },
});
