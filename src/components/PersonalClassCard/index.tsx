import React, { useMemo } from 'react';

import { useNavigation } from '@react-navigation/native';
import { format, addHours, parseISO } from 'date-fns';
import { pt } from 'date-fns/locale';

import { PersonalClass } from '~/models';
import { PlaceholderImage } from '~/utils/consts';
import { getPaymentStatus, getStatusBG } from '~/utils/geStatus';

import {
  Card,
  CardContent,
  Image,
  Wrapper,
  Holder,
  Question,
  Answer,
  Divider,
  StatusButton,
  StatusButtonText,
} from './styles';

interface Props {
  data?: PersonalClass;
}

const PersonalClassCard: React.FC<Props> = ({ data }) => {
  const myClass: PersonalClass = data;
  const { navigate } = useNavigation();

  const formattedDate = format(
    addHours(parseISO(myClass.created_at), -3),
    "dd/MM/yy '-' HH:mm ",
    {
      locale: pt,
    },
  );

  const options = [
    { id: 1, question: 'Aluno: ', answer: myClass.student.user.name },
    {
      id: 2,
      question: 'Pacote: ',
      answer: myClass.professionalPlan.plan.description,
    },
    {
      id: 3,
      question: 'Pagamento: ',
      answer: getPaymentStatus(myClass.method_payment),
    },
    {
      id: 4,
      question: 'Adesão em: ',
      answer: formattedDate,
    },
  ];

  const optionsMemo = useMemo(
    () =>
      options.map((item) => (
        <Holder key={item.id}>
          <Question>{item.question}</Question>
          <Answer>{item.answer}</Answer>
        </Holder>
      )),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return myClass.amount === 1 ? (
    <Card
      onPress={() =>
        navigate('StudentView', {
          myClass,
        })
      }
    >
      <CardContent>
        <Image
          source={{
            uri: myClass.student.user.img_profile ?? PlaceholderImage,
          }}
        />
        <Wrapper>{optionsMemo}</Wrapper>
      </CardContent>

      <Divider />

      <StatusButton
        style={{
          backgroundColor: '#37153D',
        }}
      >
        <StatusButtonText
          style={{
            color: '#fff',
          }}
        >
          Visualizar aula
        </StatusButtonText>
      </StatusButton>
    </Card>
  ) : (
    <Card
      onPress={() =>
        navigate('StudentView', {
          myClass,
        })
      }
    >
      <CardContent>
        <Image
          source={{
            uri: myClass.student.user.img_profile ?? PlaceholderImage,
          }}
        />
        <Wrapper>{optionsMemo}</Wrapper>
      </CardContent>

      <Divider />

      <StatusButton
        style={{
          backgroundColor: getStatusBG(myClass.status),
        }}
      >
        <StatusButtonText
          style={{
            color: '#fff',
          }}
        >
          Visualizar aulas
        </StatusButtonText>
      </StatusButton>
    </Card>
  );
};

export default PersonalClassCard;
