import React, {
  createContext,
  useState,
  useEffect,
  useContext,
  useCallback,
} from 'react';
import { Platform } from 'react-native';

import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';

import { User } from '~/models';

import { useAuth } from './auth';

interface NotificationProps {
  token: string;
  title: string;
  body: string;
}

interface NotificationContextData {
  expoPushToken: string;
  sendNewNotification(credentials: NotificationProps): Promise<void>;
}

const NotificationContext = createContext<NotificationContextData>(
  {} as NotificationContextData,
);

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

const NotificationProvider: React.FC = ({ children }) => {
  const { user, signed, updatePersonal, updateStudent } = useAuth();
  const [expoPushToken, setExpoPushToken] = useState('');

  const setUserExpoPushToken = useCallback(
    async (token: string) => {
      setExpoPushToken(token);
      console.log(`Token: ${token}`);

      try {
        if (signed && user.role === 'professional') {
          const formData: User = {
            id: user.id,
            expo_data: token,
            professional: {
              ...user.professional,
            },
          };
          console.log(formData);
          await updatePersonal(formData);
          console.log(user.role);
        } else if (signed && user.role === 'student') {
          const formData: User = {
            id: user.id,
            expo_data: token,
            student: {
              ...user.student,
            },
          };
          console.log(formData);
          await updateStudent(formData);
          console.log(user.role);
        }
      } catch (err) {
        console.log(`Notification error:${err}`);
      }
    },
    [signed, updatePersonal, updateStudent, user],
  );

  async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS,
      );

      let finalStatus = existingStatus;

      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS,
        );
        finalStatus = status;
      }

      if (finalStatus !== 'granted') {
        console.log('Failed to get push token for push notification!');
        return;
      }

      token = (await Notifications.getExpoPushTokenAsync()).data;
      console.log(token);
    } else {
      console.log('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
      });
    }

    // eslint-disable-next-line consistent-return
    return token;
  }

  useEffect(() => {
    if (signed) {
      registerForPushNotificationsAsync().then((token) => {
        setUserExpoPushToken(token);
      });

      // This listener is fired whenever a notification is received while the app is foregrounded
      Notifications.addNotificationReceivedListener((data) => {
        console.log(data);
      });

      // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
      Notifications.addNotificationResponseReceivedListener((response) => {
        console.log(response);
      });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [signed]);

  const sendNewNotification = useCallback(async ({ token, title, body }) => {
    const message = {
      to: token,
      sound: 'default',
      title,
      body,
      data: { data: 'goes here' },
    };

    await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
  }, []);

  return (
    <NotificationContext.Provider
      value={{
        expoPushToken,
        sendNewNotification,
      }}
    >
      {children}
    </NotificationContext.Provider>
  );
};

function useNotification(): NotificationContextData {
  const context = useContext(NotificationContext);

  if (!context) {
    throw new Error('useAuth must be used within an NotificationProvider');
  }

  return context;
}

export { NotificationProvider, useNotification };
