import React, {
  createContext,
  useState,
  useEffect,
  useContext,
  useCallback,
} from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

import User from '~/models/User';
import api from '~/services/api';
import * as auth from '~/services/auth';

interface SignInCredentials {
  email: string;
  password: string;
}

interface SignUpCredentials {
  name: string;
  email: string;
  contact: string;
  password: string;
  role: string;
  cref?: string;
  cpf?: string;
  isPersonal?: boolean;
}

interface AuthContextData {
  signed: boolean;
  user: User | null;
  loading: boolean;
  signIn(credentials: SignInCredentials): Promise<void>;
  signUp(credentials: SignUpCredentials): Promise<void>;
  updateStudent(credentials: User): Promise<void>;
  updatePersonal(credentials: User): Promise<void>;
  signOut(): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadStoragedData(): Promise<void> {
      const [storagedUser, storagedToken] = await AsyncStorage.multiGet([
        '@Personal: user',
        '@Personal: token',
      ]);

      if (storagedUser[1] && storagedToken[1]) {
        api.defaults.headers.Authorization = `Bearer ${storagedToken[1]}`;
        setUser(JSON.parse(storagedUser[1]));
      }

      setLoading(false);
    }

    loadStoragedData();
  }, []);

  const signUp = useCallback(
    async ({ name, email, contact, password, role, cref, cpf, isPersonal }) => {
      const response = await auth.signUp(
        name,
        email,
        contact,
        password,
        role,
        cref,
        cpf,
        isPersonal,
      );

      setUser(response);

      api.defaults.headers.Authorization = `Bearer ${response.token}`;

      await AsyncStorage.multiSet([
        ['@Personal: token', response.token],
        ['@Personal: user', JSON.stringify(response)],
      ]);
    },
    [],
  );

  const signIn = useCallback(async ({ email, password }) => {
    const response = await auth.signIn(email, password);

    setUser(response);

    console.log(response.token);

    api.defaults.headers.Authorization = `Bearer ${response.token}`;

    await AsyncStorage.multiSet([
      ['@Personal: token', response.token],
      ['@Personal: user', JSON.stringify(response)],
    ]);
  }, []);

  const updateStudent = useCallback(async (updatedStudent) => {
    const response = await auth.updateStudentProfile(updatedStudent);

    setUser(response);

    await AsyncStorage.multiSet([
      ['@Personal: user', JSON.stringify(response)],
    ]);
  }, []);

  const updatePersonal = useCallback(async (updatedPersonal) => {
    const response = await auth.updatePersonalProfile(updatedPersonal);

    setUser(response);

    await AsyncStorage.multiSet([
      ['@Personal: user', JSON.stringify(response)],
    ]);
  }, []);

  function signOut(): void {
    AsyncStorage.clear().then(() => {
      setUser(null);
    });
  }

  return (
    <AuthContext.Provider
      value={{
        signed: !!user,
        user,
        loading,
        signIn,
        signUp,
        updateStudent,
        updatePersonal,
        signOut,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  return context;
}
