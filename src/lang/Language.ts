export default class Language {
  language: string;

  pages: {
    resetPassword: {
      title: string;
    };
  };
}
